package com.lp.test.excel;

import com.lp.excel.ExcelUtil;
import com.lp.excel.pojo.Student1;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.lp.test.BaseTest.ROOT_PATH;

/**
 * @author peng.liu
 * @date 2017/9/26 17:56
 */

public class ExcelStringTemplate {

    @Test
    public void testObject2Excel() throws Exception {

        String tempPath = ROOT_PATH+"string_template.xls";
        List<Student1> list = new ArrayList<>();
        list.add(new Student1("1010001", "盖伦", "六年级三班"));
        list.add(new Student1("1010002", "古尔丹", "一年级三班"));
        list.add(new Student1("1010003", "蒙多(被开除了)", "六年级一班"));
        list.add(new Student1("1010004", "萝卜特", "三年级二班"));
        list.add(new Student1("1010005", "奥拉基", "三年级二班"));
        list.add(new Student1("1010006", "得嘞", "四年级二班"));
        list.add(new Student1("1010007", "瓜娃子", "五年级一班"));
        list.add(new Student1("1010008", "战三", "二年级一班"));
        list.add(new Student1("1010009", "李四", "一年级一班"));
        // 基于StringTemplate导出Excel
        ExcelUtil.exportExcelFile(new File(tempPath), list);
    }
}
