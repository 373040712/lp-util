package com.lp.test.excel;

import com.lp.ztest.AbcLp;
import com.lp.test.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author peng.liu
 * @date 2019/4/23 11:29
 */

public class LpTest extends BaseTest {

    @Autowired
    private AbcLp abcLp;

    @Test
    public void test(){
        abcLp.test();
    }
}
