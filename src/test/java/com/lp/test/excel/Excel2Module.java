package com.lp.test.excel;

import com.google.common.collect.ImmutableList;
import com.lp.ListUtils;
import com.lp.excel.TExcelUtils;
import com.lp.excel.pojo.Student1;
import com.lp.excel.pojo.Student2;
import org.junit.Test;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.lp.test.BaseTest.ROOT_PATH;

public class Excel2Module {

    private static final String SQL = "UPDATE tms_waybill_goods set load_volums={0} where waybill_id=''{1}'' and good_code=''{2}'';";
    private static final String W_SQL = "UPDATE tms_waybill set load_volums={0} where id=''{1}'';";
    private static final String X_SQL = "UPDATE tms_waybill SET line_remark=''{1}'' WHERE oms_order_id=''{0}'';";

    @Test
    public void testExcel() throws Exception {
        String idPath = ROOT_PATH + "123.xlsx";
        String goodPath = ROOT_PATH + "good_1.xlsx";
        List<List<String>> idList = TExcelUtils.getInstance().readExcel2List(idPath);
        List<List<String>> goodList = TExcelUtils.getInstance().readExcel2List(goodPath);
        Map<String, String> idMap = ListUtils.toMap(idList, it -> it.get(0), it -> it.get(1));
        Map<String, List<List<String>>> goodListMap = ListUtils.groupingBy(goodList, it -> it.get(0));
        List<List<String>> result = new ArrayList<>();
        idMap.entrySet().forEach(it -> {
            result.add(ImmutableList.of(MessageFormat.format(X_SQL, it.getKey(), it.getValue())));
        });
//        goodListMap.entrySet().forEach(it -> {
//            it.getValue().forEach(i -> result.add(ImmutableList.of(MessageFormat.format(SQL, i.get(2), idMap.get(i.get(0)), i.get(1)))));
//            Double volums = it.getValue().stream().mapToDouble(e -> Double.valueOf(e.get(2))).sum();
//            result.add(ImmutableList.of(MessageFormat.format(W_SQL, round(volums, 4).toString(), idMap.get(it.getKey()))));
//        });
        TExcelUtils.getInstance().exportObjects2Excel(result, ROOT_PATH + "sql.xlsx");
    }

    @Test
    public void excel2Object() throws Exception {

        String path = ROOT_PATH + "students_01.xlsx";

        System.out.println("读取全部：");
        List<Student1> students = TExcelUtils.getInstance().readExcel2Objects(path, Student1.class);
        for (Student1 stu : students) {
            System.out.println(stu);
        }

        System.out.println("读取指定行数：");
        students = TExcelUtils.getInstance().readExcel2Objects(path, Student1.class, 0, 3, 0);
        for (Student1 stu : students) {
            System.out.println(stu);
        }
    }

    @Test
    public void excel2Object2() throws Exception {

        String path = ROOT_PATH + "students_02.xlsx";

        // 不基于注解,将Excel内容读至List<List<String>>对象内
        List<List<String>> lists = TExcelUtils.getInstance().readExcel2List(path, 1, 3, 0);
        System.out.println("读取Excel至String数组：");
        for (List<String> list : lists) {
            System.out.println(list);
        }
        // 基于注解,将Excel内容读至List<Student2>对象内
        List<Student2> students = TExcelUtils.getInstance().readExcel2Objects(path, Student2.class, 0);
        System.out.println("读取Excel至对象数组(支持类型转换)：");
        for (Student2 st : students) {
            System.out.println(st);
        }
    }
}
