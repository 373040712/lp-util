package com.lp.test.mock;

import com.lp.mock.*;
import com.lp.test.BaseTest;
import org.junit.Test;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

/**
 * @author peng.liu
 * @date 2017/9/26 16:21
 */

public class LoginPresenterTest extends BaseTest {

    @Autowired
    @InjectMocks // 注入mock对象
    private LoginPresenter loginPresenter;

    @Autowired
    @Mock // 创建一个mock对象
    private UserManager userManager;

    @Autowired
    @Spy // 创建一个mock对象
    private PasswordValidator passwordValidator;

    @Autowired
    @Spy // 除非指定，否者调用这个对象的默认实现，同时又能拥有验证方法调用的功能
    // In my honest opinion, this is neater than using a spy, since a spy requires an instance,
    // which means you have to create an instantiatable subclass of your abstract class.
    // 如果是个接口需要这个注解才能执行doCallRealMethod
    private UsernameValidator usernameValidator;

    @Override
    public void setup() throws Exception {
        super.setup();
        // 需要去代理，不然无法mock
        userManager = (UserManager) unwrapProxy(userManager);
        passwordValidator = (PasswordValidator) unwrapProxy(passwordValidator);
        MockitoAnnotations.initMocks(this);
    }

    /**
     * 验证方法调用
     *
     * @throws Exception
     */
    @Test
    public void verifyLogin() throws Exception {

        // spy对象的方法也可以指定特定的行为
        // 用thenReturn 会走go()方法体，然后将返回值Mock掉
        Mockito.when(usernameValidator.verifyUsername("lp")).thenReturn(true);
        // 用doReturn 不走go()方法体
        // Mockito.doReturn(true).when(usernameValidator).verifyUsername("lp");

        Mockito.doCallRealMethod().when(passwordValidator).verifyPassword("123456");

        loginPresenter.login("lp", "123456");

        // 验证方法是否执行
        Mockito.verify(userManager).performLogin("lp", "123456");

        // 验证方法执行次数（atMost(count), atLeast(count), never()等等）
        Mockito.verify(userManager, Mockito.times(1)).performLogin("lp", "123456");

        // 验证方法，不关心参数（anyInt, anyLong, anyDouble等等。anyObject表示任何对象，any(clazz)表示任何属于clazz的对象）
        // （anyCollection，anyCollectionOf(clazz), anyList(Map, set), anyListOf(clazz)等等）
        Mockito.verify(userManager).performLogin(anyString(), anyString());
    }

    /**
     * 指定mock对象的某个方法返回特定的值
     *
     * @throws Exception
     */
    @Test
    public void whenLogin() throws Exception {

        // 用thenReturn 会走go()方法体，然后将返回值Mock掉
        Mockito.when(usernameValidator.verifyUsername("lp")).thenReturn(true);
        // 用doReturn 不走go()方法体
        // Mockito.doReturn(true).when(usernameValidator).verifyUsername("lp");

        // 当调用mockValidator的verifyPassword方法，同时传入"123456"时，返回true
        Mockito.when(passwordValidator.verifyPassword("654321")).thenReturn(true);

        loginPresenter.login("lp", "654321");

        Mockito.verify(userManager).performLogin(anyString(), anyString());
    }

    /**
     * 指定mock对象执行特定的动作
     * <p>
     * Mockito.doAnswer(desiredAnswer).when(mockObject).targetMethod(args);
     *
     * 指定一个方法执行特定的动作，这个功能一般是用在目标的方法是void类型的时候
     * 
     * @throws Exception
     */
    @Test
    public void doAnswerLogin() throws Exception {

        // 用thenReturn 会走go()方法体，然后将返回值Mock掉
        Mockito.when(usernameValidator.verifyUsername("lp")).thenReturn(true);
        // 用doReturn 不走go()方法体
        // Mockito.doReturn(true).when(usernameValidator).verifyUsername("lp");

        // Mockito.doCallRealMethod().when(passwordValidator).verifyPassword("123456");
        Mockito.when(passwordValidator.verifyPassword("123456")).thenCallRealMethod();
        // 当调用userManager的performLogin方法时，会执行answer里面的代码，
        // 我们上面的例子是直接调用传入的callback的onFailure方法，同时传给onFailure方法500和"Server error"。
        Mockito.doAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) throws Throwable {
                // 这里可以获得传给performLogin的参数
                Object[] arguments = invocation.getArguments();

                // callback是第三个参数
                Callback callback = (Callback) arguments[2];

                callback.onFailure(500, "Server error");

                return 500;
            }
        }).when(userManager).performLogin(anyString(), anyString(), any(Callback.class));

        loginPresenter.loginCallback("lp", "123456");

        Mockito.verify(userManager).performLogin(anyString(), anyString(), any(Callback.class));

        // “什么都不做”，那么可以使用Mockito.doNothing()。
        // 如果想指定目标方法“抛出一个异常”，那么可以使用Mockito.doThrow(desiredException)。
        // 如果你想让目标方法调用真实的逻辑，可以使用Mockito.doCallRealMethod()
    }

}
