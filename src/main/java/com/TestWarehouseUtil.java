package com;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.SSLs;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.lp.excel.TExcelUtils;
import com.lp.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.client.HttpClient;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author liupeng@newhope.cn
 * @date 2024/03/26
 **/
@Slf4j
public class TestWarehouseUtil {

    private final static String ROOT_PATH = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test"
            + File.separator + "resources" + File.separator;

    public static void main(String[] args) throws Exception {
        HCB hcb = HCB.custom().ssl()
                .pool(100, 10)
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2)
                .retry(0);
        Header[] headers = HttpHeader.custom().contentType("application/json")
                .other("Authorization", "Bearer 06A72EE4E2CC46D2B2316ED48F3DA5EC")
                .build();
        HttpClient client = hcb.build();

        HttpConfig config = HttpConfig.custom().headers(headers)
                .url("https://wms-web-plus.yunlizhi.cn/warehouse_foundation/warehouse/page_v2?page=1&size=1000")
                .encoding("utf-8")
                .json(JSON.toJSONString(new JSONObject()
//                        .fluentPut("code","T10000185004")
                )).client(client);
        String result = HttpClientUtil.post(config);
        JSONObject data = JSON.parseObject(result).getJSONObject("data");
        JSONArray records = data.getJSONArray("records");

        String dateStart = "2024-03-01";
        String dateEnd = "2024-03-27";
        List<WarehouseInfo> list = new ArrayList<>();
        for (int i = 0; i < records.size(); i++) {
            JSONObject jsonObject = records.getJSONObject(i);
            String warehouseStatus = jsonObject.getString("warehouseStatus");
            if (Objects.equals(warehouseStatus, "REVOKED")) {
                continue;
            }
            String warehouseCode = jsonObject.getString("warehouseCode");
            String warehouseName = jsonObject.getString("warehouseName");
            JSONObject queryData = queryData(warehouseCode, dateStart, dateEnd);
            WarehouseInfo info = queryData.toJavaObject(WarehouseInfo.class);
            info.setDateStart(dateStart);
            info.setDateEnd(dateEnd);
            info.setWarehouseCode(warehouseCode);
            info.setWarehouseName(warehouseName);
            list.add(info);
            log.info("------已完成仓库：{}-{}", warehouseCode, warehouseName);
            Thread.sleep(500);
        }

        TExcelUtils.getInstance().exportObjects2Excel(list, WarehouseInfo.class, true, null, true, ROOT_PATH + "仓库三效三准.xlsx");
    }

    private static JSONObject queryData(String warehouseCode, String dateStart, String dateEnd) throws HttpProcessException {
        HCB hcb = HCB.custom().ssl()
                .pool(100, 10)
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2)
                .retry(0);
        Header[] headers = HttpHeader.custom().contentType("application/json")
                .other("Authorization", "Bearer 3851C426132046D8835DF756D53D0951")
                .build();
        HttpClient client = hcb.build();

        HttpConfig config = HttpConfig.custom().headers(headers)
                .url("https://wms-web-plus.yunlizhi.cn/warehouse_data/new_accurate_efficiency/head_accurate")
                .encoding("utf-8")
                .json(JSON.toJSONString(new JSONObject()
                        .fluentPut("warehouseCode", warehouseCode)
                        .fluentPut("dateStart", dateStart)
                        .fluentPut("dateEnd", dateEnd)
                )).client(client);
        String result = HttpClientUtil.post(config);
        return JSON.parseObject(result).getJSONObject("data");
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class WarehouseInfo {
        @Excel(title = "开始日期", order = 1)
        private String dateStart;
        @Excel(title = "结束日期", order = 2)
        private String dateEnd;
        @Excel(title = "仓库编号", order = 3)
        private String warehouseCode;
        @Excel(title = "仓库名称", order = 4)
        private String warehouseName;
        @Excel(title = "人效", order = 5)
        private BigDecimal peopleEfficiency;
        @Excel(title = "坪效", order = 6)
        private BigDecimal pingEfficiency;
        @Excel(title = "时效", order = 7)
        private BigDecimal timeEfficiency;
        @Excel(title = "库存准确率", order = 8)
        private BigDecimal stockAccurate;
        @Excel(title = "库位准确率", order = 9)
        private BigDecimal locationAccurate;
        @Excel(title = "分拣准确率", order = 10)
        private BigDecimal partPickAccurate;
    }
}
