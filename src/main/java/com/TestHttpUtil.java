package com;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.SSLs;
import com.google.common.collect.Lists;
import com.lp.excel.TExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.client.HttpClient;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2021/03/31
 **/
@Slf4j
public class TestHttpUtil {

    public static void main(String[] args) throws Exception {
        HCB hcb = HCB.custom().ssl()
                .pool(100, 10)
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2)
                .retry(0);
        Header[] headers = HttpHeader.custom().contentType("application/json")
                .other("Token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiJjZTMyMzhkZDIzZDY2Njg5ODJhNzEzNTI3YmMyMzZlNSIsIkxPR0lOX1RJTUUiOiIxNjgxOTk3MTcyMTAyIiwiVE9LRU5fVFlQRSI6InBjIn0.fNjbnuubqjRKN5X-4xUMQ0z9KUYDvtoPgWq6shpLLh0")
                .build();
        HttpClient client = hcb.build();

        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "audit.xlsx");
        AtomicInteger index = new AtomicInteger(0);
        Lists.partition(aList, 1000).forEach(it -> {
            List<String> refCodes = it.stream().map(item -> item.get(0)).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(refCodes)) {
                HttpConfig config = HttpConfig.custom().headers(headers)
                        .url("https://mp.yunlizhi.cn/bms_web_plus/bms_middle_platform/manual/refresh_clarify/v2")
                        .encoding("utf-8")
                        .json(JSON.toJSONString(new JSONObject()
                                .fluentPut("codes", refCodes)
                                .fluentPut("refreshEntity", true))
                        )
                        .client(client);
                String result = null;
                try {
                    result = HttpClientUtil.post(config);
                    JSONObject jsonObject = JSON.parseObject(result);
                    JSONObject data = jsonObject.getJSONObject("data");
                    index.set(index.get() + 1);
                    log.info("{}----处理成功-------------{}，结果：{}", index.get(), refCodes.get(refCodes.size() - 1), JSON.toJSONString(data));
                } catch (Exception e) {
                    log.error("处理失败-------------", e);
                }
            }
        });
        log.info("处理完成");
    }
}
