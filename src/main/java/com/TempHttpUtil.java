package com;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.SSLs;
import com.google.common.collect.Lists;
import com.lp.excel.TExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.client.HttpClient;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2021/03/31
 **/
@Slf4j
public class TempHttpUtil {

    public static void main(String[] args) throws Exception {
        HCB hcb = HCB.custom().ssl()
                .pool(100, 10)
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2)
                .retry(0);
        Header[] headers = HttpHeader.custom().contentType("application/json")
                .other("Token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiJjZTMyMzhkZDIzZDY2Njg5ODJhNzEzNTI3YmMyMzZlNSIsIkxPR0lOX1RJTUUiOiIxNjgxOTk3MTcyMTAyIiwiVE9LRU5fVFlQRSI6InBjIn0.fNjbnuubqjRKN5X-4xUMQ0z9KUYDvtoPgWq6shpLLh0")
                .build();
        HttpClient client = hcb.build();

        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "temp.xlsx");
        List<String> info = Lists.newArrayList();

        AtomicInteger index = new AtomicInteger(0);
        Lists.partition(aList, 1).forEach(it -> {
            List<String> refCodes = it.stream().map(item -> item.get(0)).collect(Collectors.toList());
            if (CollUtil.isNotEmpty(refCodes)) {
                HttpConfig config = HttpConfig.custom().headers(headers)
                        .url("https://saas-admin.yunlizhi.cn/tenant/list")
                        .encoding("utf-8")
                        .json(JSON.toJSONString(new JSONObject()
                                .fluentPut("tenantIds", refCodes))
                        )
                        .client(client);
                String result = null;
                try {
                    result = HttpClientUtil.post(config);
                    JSONObject jsonObject = JSON.parseObject(result);
                    JSONArray data = jsonObject.getJSONArray("data");
                    info.add(data.getJSONObject(0).getString("tenantName"));
                    index.set(index.get() + 1);
                } catch (Exception e) {
                    log.error("处理失败-------------", e);
                }
            }
        });
        log.info("'"+info.stream().collect(Collectors.joining("','"))+"'");
        log.info("处理完成");
    }
}
