package com;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.ImmutableList;
import com.lp.excel.TExcelUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;


/**
 * @author liupeng@newhope.cn
 * @date 2021/04/20
 **/
public class TestSqlUtil {

    private static final String SQL = "UPDATE stock_history set biz_type = ''ADJUST_OUT'', amount = -history_total, data_time = create_time where id = {0};";
    private static final String SQL_EXT = "update bms_plus_pay_bill set bill_details = ''{0}'' where id = {1};";

    public static void main(String[] args) throws Exception {
        List<List<String>> result = new ArrayList<>();
        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_data.xlsx");
        for (List<String> it : aList) {
            if (CollUtil.isEmpty(it)) {
                continue;
            }
            String bizId = it.get(0);
            if (StringUtils.isBlank(bizId)) {
                continue;
            }
            String bizStr = it.get(1);
            JSONObject json = JSON.parseObject(bizStr);
            JSONObject bill = json.getJSONArray("shippingOrderBills").getJSONObject(0);
            bill.put("billedAmount", 0);
            bill.put("adjustedAmount", -bill.getDoubleValue("auditedAmount"));
            JSONArray costs = bill.getJSONArray("costs");
            costs.add(new JSONObject()
                    .fluentPut("auditedAmount", 0)
                    .fluentPut("billedAmount", -bill.getDoubleValue("auditedAmount"))
                    .fluentPut("audited", false)
                    .fluentPut("type", "其他费")
                    .fluentPut("pictures", Lists.newArrayList())
                    .fluentPut("adjustReason", "运营核对账目提出申请审批通过做出调整")
            );
            result.add(ImmutableList.of(MessageFormat.format(SQL_EXT, JSON.toJSONString(json), bizId)));
        }

        TExcelUtils.getInstance().exportObjects2Excel(result, ROOT_PATH + "0907_sql.xlsx");
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Complaint {
        private String code;
        private String name;
        private String mobile;
        private String plateNumber;
        private String content;
        private String result;
    }

    public static Date randomDateTime(Long startTime) {
        long range = 5184000000L;
        long maxStartTime = 1648742400000L;
        if (startTime == null) {
            startTime = 1620467864000L;
        }
        if (startTime > maxStartTime) {
            startTime = maxStartTime;
        }
        long tempTime = startTime + range;
        long endTime = 1651334400000L;
        long date = RandomUtil.randomLong(startTime, tempTime < endTime ? tempTime : endTime);
        return new Date(date);
    }

    public static String getCode(Date datetime) {
        return "TS" + DateUtil.format(datetime, "yyyyMMddhhmmsss");
    }
}
