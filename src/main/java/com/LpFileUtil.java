package com;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liupeng@newhope.cn
 * @date 2022/10/20
 **/
public class LpFileUtil {

    public static void main(String[] args) {
        List<String> list = FileUtil.readUtf8Lines("lp.txt");
        String str = list.stream().collect(Collectors.joining());
        JSONArray array = JSONArray.parseArray(str);
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = array.getJSONObject(i);
            BigDecimal newMoney = jsonObject.getBigDecimal("moneyTax");
            BigDecimal oldMoney = jsonObject.getBigDecimal("oldMoneyTax");
            String oldBillCode = jsonObject.getString("oldBillCode");
            String billCode = jsonObject.getString("billCode");
            if (oldBillCode.startsWith("YS")) {
                System.out.println(newMoney);
            }
        }
    }
}
