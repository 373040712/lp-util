package com.find.tree;

import com.alibaba.fastjson.JSON;
import org.apache.commons.compress.utils.Lists;

import java.util.List;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/2/28 9:08
 */
public class TreeUtil<T> {

    public interface Convert<T, TreeNodeMap> {
        void convert(T object, TreeNodeMap treeNode);
    }

    /**
     * 树构建
     */
    public static <T> List<TreeNodeMap> build(List<T> list, Object parentId, Convert<T, TreeNodeMap> convert) {
        return build(list, parentId, TreeNodeConfig.getDefaultConfig(), convert);
    }

    /**
     * 树构建
     */
    public static <T> List<TreeNodeMap> build(List<T> list, Object parentId, TreeNodeConfig treeNodeConfig, Convert<T, TreeNodeMap> convert) {
        List<TreeNodeMap> treeNodes = Lists.newArrayList();
        for (T obj : list) {
            TreeNodeMap treeNode = new TreeNodeMap(treeNodeConfig);
            convert.convert(obj, treeNode);
            treeNodes.add(treeNode);
        }

        List<TreeNodeMap> finalTreeNodes = Lists.newArrayList();
        for (TreeNodeMap treeNode : treeNodes) {
            if (parentId.equals(treeNode.getParentId())) {
                finalTreeNodes.add(treeNode);
                innerBuild(treeNodes, treeNode);
            }
        }
        return finalTreeNodes;
    }

    private static void innerBuild(List<TreeNodeMap> treeNodes, TreeNodeMap parentNode) {
        for (TreeNodeMap childNode : treeNodes) {
            if (parentNode.getId().equals(childNode.getParentId())) {
                List<TreeNodeMap> children = parentNode.getChildren();
                if (children == null) {
                    children = Lists.newArrayList();
                    parentNode.setChildren(children);
                }
                children.add(childNode);
                childNode.setParentId(parentNode.getId());
                innerBuild(treeNodes, childNode);
            }
        }
    }

    public static void main(String[] args) {
        List<MenuEntity> menuEntityList = Lists.newArrayList();
        menuEntityList.add(new MenuEntity("1", "0", "系统管理", "sys", "/sys"));
        menuEntityList.add(new MenuEntity("11", "1", "用户管理", "user", "/sys/user"));
        menuEntityList.add(new MenuEntity("111", "11", "用户添加", "userAdd", "/sys/user/add"));
        menuEntityList.add(new MenuEntity("2", "0", "店铺管理", "store", "/store"));
        menuEntityList.add(new MenuEntity("21", "2", "商品管理", "shop", "/shop"));

        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        // 自定义属性名
        treeNodeConfig.setCodeKey("number");
        List<TreeNodeMap> treeNodes = TreeUtil.build(menuEntityList, "0", treeNodeConfig, new TreeUtil.Convert<MenuEntity, TreeNodeMap>() {
            @Override
            public void convert(MenuEntity object, TreeNodeMap treeNode) {
                treeNode.setId(object.getId());
                treeNode.setParentId(object.getPid());
                treeNode.setCode(object.getCode());
                treeNode.setName(object.getName());
                // 属性扩展
                treeNode.extra("extra1", "123");
            }
        });
        System.out.println(JSON.toJSONString(treeNodes));
    }
}
