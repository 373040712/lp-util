package com.find.html;

import com.find.html.parser.HtmlParser;
import com.find.html.parser.HtmlParserImpl;
import com.find.html.parser.ParseException;
import com.find.html.render.pdf.PdfRender;
import com.find.html.render.pdf.PdfRenderImpl;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;

import java.io.*;
import java.net.URI;
import java.net.URL;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/6 13:00
 */
public class HtmlToPdf {

    private HtmlParser parser;

    private PdfRender pdfRender;

    public static void main(String[] args) throws FileNotFoundException {
        HtmlToPdf.from(ResourceUtils.getURL("classpath:html/template_pdf.html")).to("template.pdf");
    }

    public HtmlToPdf() {
        this.parser = new HtmlParserImpl();
        this.pdfRender = new PdfRenderImpl(parser);
    }

    public static HtmlToPdf document(Document document) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.setDocument(document);
        return htmlToPdf;
    }

    public static HtmlToPdf html(String html) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.loadHtml(html);
        return htmlToPdf;
    }

    public static HtmlToPdf from(URL url) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.load(url);
        return htmlToPdf;
    }

    public static HtmlToPdf from(URI uri) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.load(uri);
        return htmlToPdf;
    }

    public static HtmlToPdf from(File file) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.load(file);
        return htmlToPdf;
    }

    public static HtmlToPdf from(Reader reader) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.load(reader);
        return htmlToPdf;
    }

    public static HtmlToPdf from(InputStream inputStream) {
        final HtmlToPdf htmlToPdf = new HtmlToPdf();
        htmlToPdf.parser.load(inputStream);
        return htmlToPdf;
    }

    public void to(OutputStream outputStream) {
        check();
        pdfRender.save(outputStream, true);
    }

    public void to(File file) {
        check();
        pdfRender.save(file);
    }

    public void to(String filename) {
        check();
        pdfRender.save(filename);
    }

    private void check() {
        if (parser.getDocument() == null) {
            throw new ParseException("There is no document when rendering");
        }
    }
}
