package com.find.html.parser;

import org.w3c.dom.Document;

import java.io.File;
import java.io.InputStream;
import java.io.Reader;
import java.net.URI;
import java.net.URL;

/**
 * @author admin
 */
public interface HtmlParser extends DocumentHolder {

    //注意渲染都需要url,否则丢失图片
    void load(URL url);

    void load(URI uri);

    void load(File file);

    void load(Reader reader);

    void load(InputStream inputStream);

    void loadHtml(String html);

    void loadURI(String uri);

    void setDocument(Document document);
}
