package com.find.html.parser;

import org.apache.xerces.parsers.DOMParser;
import org.cyberneko.html.HTMLConfiguration;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import java.io.*;
import java.net.URI;
import java.net.URL;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 14:26
 */
public class HtmlParserImpl implements HtmlParser {

    private DOMParser domParser;

    private Document document;

    public HtmlParserImpl() {
        try {
            domParser = new DOMParser(new HTMLConfiguration());
            //配置参考文档：http://nekohtml.sourceforge.net/settings.html
            domParser.setProperty("http://cyberneko.org/html/properties/names/elems", "lower");
        } catch (SAXNotRecognizedException e) {
            throw new ParseException("Can't create HtmlParserImpl", e);
        } catch (SAXNotSupportedException e) {
            throw new ParseException("Can't create HtmlParserImpl", e);
        }
    }

    @Override
    public void load(URL url) {
        loadURI(url.toExternalForm());
    }

    @Override
    public void load(URI uri) {
        loadURI(uri.toString());
    }

    @Override
    public void load(File file) {
        load(file.toURI());
    }

    @Override
    public void load(Reader reader) {
        try (Reader input = reader) {
            doLoad(new InputSource(input));
        } catch (IOException e) {
            throw new ParseException("SAXException while parsing HTML.", e);
        }
    }

    @Override
    public void load(InputStream inputStream) {
        try (InputStream input = inputStream) {
            doLoad(new InputSource(input));
        } catch (IOException e) {
            throw new ParseException("SAXException while parsing HTML.", e);
        }
    }

    @Override
    public void loadHtml(String html) {
        load(new StringReader(html));
    }

    @Override
    public void loadURI(String uri) {
        doLoad(new InputSource(uri));
    }

    @Override
    public void setDocument(Document document) {
        this.document = document;
    }

    private void doLoad(InputSource inputSource) {
        try {
            domParser.parse(inputSource);
            Document document = domParser.getDocument();
            if (document == null) {
                throw new ParseException("Document is null while parsing HTML.");
            }
            setDocument(document);
        } catch (SAXException e) {
            throw new ParseException("SAXException while parsing HTML.", e);
        } catch (IOException e) {
            throw new ParseException("IOException while parsing HTML.", e);
        }
    }

    @Override
    public Document getDocument() {
        return document;
    }
}
