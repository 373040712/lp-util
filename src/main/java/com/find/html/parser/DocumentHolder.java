package com.find.html.parser;

import org.w3c.dom.Document;

/**
 * @author admin
 */
public interface DocumentHolder {

	Document getDocument();
}
