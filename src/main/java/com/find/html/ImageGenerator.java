package com.find.html;

import com.find.html.image.LinkInfo;
import com.find.html.image.SynchronousHTMLEditorKit;
import com.google.common.collect.Lists;
import org.springframework.util.ResourceUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import static com.find.html.util.FormatNameUtil.getImageFormat;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 22:55
 */
public class ImageGenerator {

    private static final Dimension DEFAULT_SIZE = new Dimension(800, 800);

    private JEditorPane editorPane;

    public static void main(String[] args) throws FileNotFoundException {
        ImageGenerator imageGenerator = new ImageGenerator();
//        imageGenerator.setSize(new Dimension(550, 600));
        imageGenerator.loadUrl(ResourceUtils.getURL("classpath:html/template.html"));
        imageGenerator.saveAsImage("template_temp.png");
    }

    public ImageGenerator() {
        editorPane = createJEditorPane();
    }

    public void setSize(Dimension dimension) {
        editorPane.setPreferredSize(dimension);
    }

    public void loadUrl(URL url) {
        try {
            editorPane.setPage(url);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while loading %s", url), e);
        }
    }

    public void loadUrl(String url) {
        try {
            editorPane.setPage(url);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while loading %s", url), e);
        }
    }

    public void loadHtml(String html) {
        editorPane.setEditable(false);
        editorPane.setText(html);
        editorPane.setContentType("text/html");
        onDocumentLoad();
    }

    public String getLinksMapMarkup(String mapName) {
        final StringBuilder markup = new StringBuilder();
        markup.append("<map name=\"").append(mapName).append("\">\n");
        for (LinkInfo link : getLinks()) {
            final List<Rectangle> bounds = link.getBounds();
            for (Rectangle bound : bounds) {
                final int x1 = (int) bound.getX();
                final int y1 = (int) bound.getY();
                final int x2 = (int) (x1 + bound.getWidth());
                final int y2 = (int) (y1 + bound.getHeight());
                markup.append(String.format("<area href=\"%s\" coords=\"%s,%s,%s,%s\" shape=\"rect\"", link.getHref(), x1, y1, x2, y2));
                final String title = link.getTitle();
                if (title != null && !title.equals("")) {
                    markup.append(" title=\"").append(title.replace("\"", "&quot;")).append("\"");
                }
                markup.append(">\n");
            }
        }
        markup.append("</map>\n");
        return markup.toString();
    }

    public List<LinkInfo> getLinks() {
        return Lists.newArrayList();
    }

    public void saveAsHtmlWithMap(String file, String imageUrl) {
        saveAsHtmlWithMap(new File(file), imageUrl);
    }

    public void saveAsHtmlWithMap(File file, String imageUrl) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            writer.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n");
            writer.append("<html>\n<head></head>\n");
            writer.append("<body style=\"margin: 0; padding: 0; text-align: center;\">\n");
            final String htmlMap = getLinksMapMarkup("map");
            writer.write(htmlMap);
            writer.append("<img border=\"0\" usemap=\"#map\" src=\"");
            writer.append(imageUrl);
            writer.append("\"/>\n");
            writer.append("</body>\n</html>");
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while saving '%s' html file", file), e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
        }

    }

    public void saveAsImage(String file) {
        saveAsImage(new File(file));
    }

    public void saveAsImage(File file) {
        BufferedImage image = getBufferedImage();
        BufferedImage bufferedImageToWrite = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
        bufferedImageToWrite.createGraphics().drawImage(image, 0, 0, Color.WHITE, null);
        final String formatName = getImageFormat(file.getName());
        try {
            if (!ImageIO.write(bufferedImageToWrite, formatName, file)) {
                throw new IOException("No formatter for specified file type [" + formatName + "] available");
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("Exception while saving '%s' image", file), e);
        }
    }

    public Dimension getDefaultSize() {
        return DEFAULT_SIZE;
    }

    private BufferedImage getBufferedImage() {
        JFrame frame = new JFrame();
        frame.setPreferredSize(editorPane.getPreferredSize());
        frame.setUndecorated(true);
        frame.add(editorPane);
        frame.pack();

        Dimension prefSize = frame.getPreferredSize();
        BufferedImage img = new BufferedImage(prefSize.width, prefSize.height, BufferedImage.TYPE_INT_ARGB);
        Graphics graphics = img.getGraphics();
        frame.setVisible(true);
        frame.paint(graphics);
        frame.setVisible(false);
        frame.dispose();
        return img;
    }

    private JEditorPane createJEditorPane() {
        final JEditorPane editorPane = new JEditorPane();
        editorPane.setSize(getDefaultSize());
        editorPane.setEditable(false);
        editorPane.setEditorKitForContentType("text/html", new SynchronousHTMLEditorKit());
        editorPane.setContentType("text/html");
        editorPane.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals("page")) {
                    onDocumentLoad();
                }
            }
        });
        return editorPane;
    }

    private void onDocumentLoad() {
    }
}
