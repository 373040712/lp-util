package com.find.html.render;

import java.io.File;
import java.io.OutputStream;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 15:20
 */
public interface BaseRender {

    void save(OutputStream outputStream, boolean closeStream);

    void save(File file);

    void save(String file);
}
