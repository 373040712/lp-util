package com.find.html.render.pdf;

import com.find.html.parser.DocumentHolder;
import com.find.html.render.RenderException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;

/**
 * @author admin
 */
@Slf4j
public class PdfRenderImpl implements PdfRender {

    private DocumentHolder documentHolder;

    public PdfRenderImpl(DocumentHolder documentHolder) {
        this.documentHolder = documentHolder;
    }

    @Override
    public void save(String file) {
        save(new File(file));
    }

    @Override
    public void save(File file) {
        try {
            save(new FileOutputStream(file), true);
        } catch (FileNotFoundException e) {
            throw new RenderException(String.format("File not found %s", file.getAbsolutePath()), e);
        }
    }

    @Override
    public void save(OutputStream outputStream, boolean closeStream) {
        try {
            ITextRenderer renderer = new ITextRenderer();
            //System.setProperty("javax.xml.transform.TransformerFactory", "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
            //解决base64图片支持问题
            SharedContext sharedContext = renderer.getSharedContext();
            //单独的类；
            sharedContext.setReplacedElementFactory(new B64ImgReplacedElementFactory());
            sharedContext.getTextRenderer().setSmoothingThreshold(0);
            //将HTML的内容写入对象
            //如果是HTML的路径用这个方法renderer.setDocument(htmlPath);
            final Document document = documentHolder.getDocument();
            renderer.setDocument(document, document.getDocumentURI());
            //解决中文不显示的问题
            ITextFontResolver fontResolver = renderer.getFontResolver();
            String fontPath = ResourceUtils.getURL("classpath:font/simsun.ttc").getPath();
            fontResolver.addFont(fontPath, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            renderer.layout();
            renderer.createPDF(outputStream);
        } catch (DocumentException | IOException e) {
            throw new RenderException("DocumentException while rendering PDF", e);
        } finally {
            if (closeStream) {
                try {
                    outputStream.close();
                } catch (IOException ignore) {
                    log.warn("close outputStream error", ignore);
                }
            }
        }
    }
}
