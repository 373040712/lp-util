package com.find.html.render;

/**
 * @author admin
 */
public class RenderException extends RuntimeException {

	public RenderException(String message, Throwable cause) {
		super(message, cause);
	}
}
