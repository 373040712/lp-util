package com.find.html.render.image;

import com.find.html.render.BaseRender;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 15:44
 */
public interface ImageRender extends BaseRender {

    ImageRender setWidth(int width);

    ImageRender setHeight(int height);

    ImageRender setAutoSize(boolean autoSize);
}
