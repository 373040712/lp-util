package com.find.html.render.image;

import com.find.html.parser.DocumentHolder;
import com.find.html.render.RenderException;
import org.w3c.dom.Document;
import org.xhtmlrenderer.simple.Graphics2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;

import javax.imageio.ImageWriteParam;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import static com.find.html.util.FormatNameUtil.getImageFormat;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 15:45
 */
public class ImageRenderImpl implements ImageRender {

    public static final int DEFAULT_WIDTH = 1024;
    public static final int DEFAULT_HEIGHT = 768;

    private DocumentHolder documentHolder;

    private int width = DEFAULT_WIDTH;
    private int height = DEFAULT_HEIGHT;
    private boolean autoSize = true;

    private float writeCompressionQuality = 1.0f;
    private int writeCompressionMode = ImageWriteParam.MODE_COPY_FROM_METADATA;
    private String writeCompressionType = null;

    private BufferedImage bufferedImage;
    private int cacheImageType = -1;
    private Document cacheDocument;

    public ImageRenderImpl(DocumentHolder documentHolder) {
        this.documentHolder = documentHolder;
    }

    @Override
    public void save(String filename) {
        save(new File(filename));
    }

    @Override
    public void save(File file) {
        try {
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
            doSave(outputStream, file.getName(), true);
        } catch (IOException e) {
            throw new RenderException("IOException while rendering image to " + file.getAbsolutePath(), e);
        }
    }

    @Override
    public void save(OutputStream outputStream, boolean closeStream) {
        doSave(outputStream, null, closeStream);
    }

    private void doSave(OutputStream outputStream, String filename, boolean closeStream) {
        try {
            final String imageFormat = getImageFormat(filename);
            final FSImageWriter imageWriter = getImageWriter(imageFormat);
            final boolean isBMP = "bmp".equalsIgnoreCase(imageFormat);
            final BufferedImage bufferedImage = getBufferedImage(isBMP ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB);
            imageWriter.write(bufferedImage, outputStream);
        } catch (IOException e) {
            throw new RenderException("IOException while rendering image", e);
        } finally {
            if (closeStream) {
                try {
                    outputStream.close();
                } catch (IOException ignore) {
                    throw new RenderException("IOException while rendering image", ignore);
                }
            }
        }
    }

    private FSImageWriter getImageWriter(String imageFormat) {
        FSImageWriter imageWriter = new FSImageWriter(imageFormat);
        imageWriter.setWriteCompressionMode(writeCompressionMode);
        imageWriter.setWriteCompressionQuality(writeCompressionQuality);
        imageWriter.setWriteCompressionType(writeCompressionType);
        return imageWriter;
    }

    public BufferedImage getBufferedImage(int imageType) {
        final Document document = documentHolder.getDocument();
        if (bufferedImage != null || cacheImageType != imageType || cacheDocument != document) {
            cacheImageType = imageType;
            cacheDocument = document;
            Graphics2DRenderer renderer = new Graphics2DRenderer();
            renderer.setDocument(document, document.getDocumentURI());
            Dimension dimension = new Dimension(width, height);
            bufferedImage = new BufferedImage(width, height, imageType);

            if (autoSize) {
                // do layout with temp buffer
                Graphics2D graphics2D = (Graphics2D) bufferedImage.getGraphics();
                renderer.layout(graphics2D, new Dimension(width, height));
                graphics2D.dispose();

                Rectangle size = renderer.getMinimumSize();
                final int autoWidth = (int) size.getWidth();
                final int autoHeight = (int) size.getHeight();
                bufferedImage = new BufferedImage(autoWidth, autoHeight, imageType);
                dimension = new Dimension(autoWidth, autoHeight);
            }
            Graphics2D graphics2D = (Graphics2D) bufferedImage.getGraphics();
            renderer.layout(graphics2D, dimension);
            renderer.render(graphics2D);
            graphics2D.dispose();
        }
        return bufferedImage;
    }

    @Override
    public ImageRender setWidth(int width) {
        this.width = width;
        return this;
    }

    @Override
    public ImageRender setHeight(int height) {
        this.height = height;
        return this;
    }

    @Override
    public ImageRender setAutoSize(boolean autoSize) {
        this.autoSize = autoSize;
        return this;
    }
}
