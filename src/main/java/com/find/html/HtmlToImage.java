package com.find.html;

import com.find.html.parser.HtmlParser;
import com.find.html.parser.HtmlParserImpl;
import com.find.html.parser.ParseException;
import com.find.html.render.image.ImageRender;
import com.find.html.render.image.ImageRenderImpl;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/6 13:00
 */
public class HtmlToImage {

    public static void main(String[] args) throws IOException, URISyntaxException {
        URL url = ResourceUtils.getURL("classpath:html/template.html");
        HtmlToImage.from(url).config(700, 1000).to("template.png");

//        List<String> list = Files.readAllLines(Paths.get(url.toURI())).stream().map(i -> i.replace("刘小鹏", "xxx")).collect(Collectors.toList());
//        HtmlToImage.html(Strings.join(list, '\t')).config(550, 600).to("template.png");
    }

    private HtmlParser parser;

    private ImageRender imageRender;

    public HtmlToImage() {
        this.parser = new HtmlParserImpl();
        this.imageRender = new ImageRenderImpl(parser);
    }

    public static HtmlToImage document(Document document) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.setDocument(document);
        return htmlToImage;
    }

    public static HtmlToImage html(String html) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.loadHtml(html);
        return htmlToImage;
    }

    public static HtmlToImage from(URL url) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.load(url);
        return htmlToImage;
    }

    public static HtmlToImage from(URI uri) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.load(uri);
        return htmlToImage;
    }

    public static HtmlToImage from(File file) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.load(file);
        return htmlToImage;
    }

    public static HtmlToImage from(Reader reader) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.load(reader);
        return htmlToImage;
    }

    public static HtmlToImage from(InputStream inputStream) {
        final HtmlToImage htmlToImage = new HtmlToImage();
        htmlToImage.parser.load(inputStream);
        return htmlToImage;
    }

    public void to(OutputStream outputStream) {
        check();
        imageRender.save(outputStream, true);
    }

    public void to(File file) {
        check();
        imageRender.save(file);
    }

    public void to(String filename) {
        check();
        imageRender.save(filename);
    }

    public HtmlToImage autoSize(boolean autoSize) {
        imageRender.setAutoSize(autoSize);
        return this;
    }

    public HtmlToImage config(int width, int height) {
        imageRender.setWidth(width);
        imageRender.setHeight(height);
        return this;
    }

    private void check() {
        if (parser.getDocument() == null) {
            throw new ParseException("There is no document when rendering");
        }
    }
}
