package com.find.html.image;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.*;
import java.util.List;

/**
 * @author admin
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LinkInfo {

    private String href;

    private String title;

    private List<Rectangle> bounds;

}
