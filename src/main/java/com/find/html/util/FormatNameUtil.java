package com.find.html.util;

import com.google.common.collect.ImmutableSet;
import org.apache.logging.log4j.util.Strings;

import java.util.Set;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 23:13
 */
public class FormatNameUtil {

    private static final Set<String> IMG_TYPE = ImmutableSet.of(
            "gif", "jpg", "jpeg", "png", "bmp");

    private static final String DEFAULT_TYPE = "png";

    public static String getImageFormat(String filename) {
        if (Strings.isBlank(filename)) {
            return DEFAULT_TYPE;
        }
        final int dotIndex = filename.lastIndexOf('.');
        if (dotIndex < 0) {
            return DEFAULT_TYPE;
        }
        final String ext = filename.substring(dotIndex + 1);
        return IMG_TYPE.contains(ext) ? ext : DEFAULT_TYPE;
    }
}
