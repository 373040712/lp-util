package com;

import com.google.common.collect.ImmutableList;
import lombok.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author liupeng@newhope.cn
 * @date 2021/02/04
 **/
public class ABCTest {

    public static void main(String[] args) {
        BigDecimal amount = new BigDecimal(100);
        BigDecimal unAmount = amount;
        unAmount = unAmount.subtract(BigDecimal.TEN);
        System.out.println(amount);
        System.out.println(unAmount);

        List<UserTest> list = ImmutableList.of(
                UserTest.builder().id(1).index(10).build(),
                UserTest.builder().id(2).index(20).build(),
                UserTest.builder().id(3).build());
        List<List<UserTest>> result = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            list = filter(list);
            result.add(list);
        }
        for (List<UserTest> it : result) {
            it = sort(it);
        }
        System.out.println(list);
        System.out.println(result);
    }

    public static List<UserTest> sort(List<UserTest> stocks) {
        return stocks.stream().filter(it->it.getId()<3).collect(Collectors.toList());
    }

    public static List<UserTest> filter(List<UserTest> stocks) {
        stocks = stocks.stream().filter(it -> Objects.nonNull(it.getId())).collect(Collectors.toList());
        return stocks;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    static class UserTest {
        private Integer id;
        private Integer type;
        private Integer index;
        private Date date;
        private String key;
    }
}
