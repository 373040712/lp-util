package com.lp.cron;

import lombok.Data;

/**
 * @author liupeng@newhope.cn
 * @date 2022/11/10
 **/
@Data
public class TaskSchedule {

    /**
     * 所选作业类型:
     * 1  -> 每天
     * 2  -> 每周
     * 3  -> 每月
     */
    Integer type;

    /**
     * 一周的哪几天
     */
    Integer[] dayOf;

    /**
     * 秒
     */
    Integer second;

    /**
     * 分
     */
    Integer minute;

    /**
     * 时
     */
    Integer hour;
}
