package com.lp.table;

import com.google.common.base.Splitter;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liupeng@sinoiov.com
 * @Description
 * @Date 2019/9/5 14:45
 */
public class CacheKeyUtil {

    /**
     * redis缓存前缀，v2版本
     */
    private static final String CACHE_PREFIX = "tapV2";

    private static final String SEPARATOR = "-";

    public static String getKey(String... params) {
        Assert.notNull(params, "参数不能为空");
        List<String> list = new ArrayList<>(params.length + 1);
        list.add(CACHE_PREFIX);
        list.addAll(Arrays.asList(params));
        return list.stream().filter(i -> Strings.isNotBlank(i)).collect(Collectors.joining(SEPARATOR));
    }

    public static String getPreKey(String... params) {
        return getKey(params).concat(SEPARATOR).concat("*");
    }

    public static List<String> resolveKey(String key) {
        return Splitter.on(SEPARATOR).omitEmptyStrings().trimResults()
                .splitToList(key.replace(CACHE_PREFIX, ""));
    }
}
