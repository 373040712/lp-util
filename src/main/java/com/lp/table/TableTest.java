package com.lp.table;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

import java.util.List;
import java.util.Set;

import static com.lp.table.CacheKeyUtil.getKey;
import static com.lp.table.CacheKeyUtil.resolveKey;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/1/9 12:56
 */
public class TableTest {

    private static final int TRUCK_KEY_LENGTH = 2;

    private static final int BIZ_KEY_LENGTH = 4;

    public static void main(String[] args) {
        System.out.println(Boolean.TRUE.equals(null));
        Set<String> keys = Sets.newHashSet(getKey("A", "B"),
                getKey("B", "C"),
                getKey("A", "B", "1", "2"),
                getKey("B", "C", "2", "3"),
                getKey("A", "B", "4", "5"),
                getKey("B", "C", "5", "6"));
        Table<String, String, List<String>> table = HashBasedTable.create();
        for (String key : keys) {
            List<String> keyResolve = resolveKey(key);
            if (keyResolve.size() != BIZ_KEY_LENGTH) {
                continue;
            }
            String preKey = getKey(keyResolve.get(0), keyResolve.get(1));
            if (!keys.contains(preKey)) {
                continue;
            }
            table.put(preKey, key, keyResolve);
        }
        System.out.println(table.toString());
        System.out.println(table.row(getKey("A","B")));
    }
}
