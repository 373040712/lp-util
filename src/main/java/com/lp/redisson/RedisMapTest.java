package com.lp.redisson;

import org.redisson.Redisson;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.api.map.event.EntryEvent;
import org.redisson.api.map.event.EntryExpiredListener;
import org.redisson.config.Config;

/**
 * @author peng.liu
 * @date 2019/3/19 16:08
 */

public class RedisMapTest {

    public static void main(String[] args) throws InterruptedException {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.16.63.28:6379").setPassword("singheadredis").setDatabase(2);
        RedissonClient redisson = Redisson.create(config);

        RMapCache<String, String> map = redisson.getMapCache("myMap");
        map.addListener(new EntryExpiredListener<String, String>() {
            @Override
            public void onExpired(EntryEvent<String, String> event) {
                System.out.println(event.getKey());
                System.out.println(event.getValue());
            }
        });
//        map.fastPutAsync("test", "1234", 1, TimeUnit.SECONDS);
    }
}
