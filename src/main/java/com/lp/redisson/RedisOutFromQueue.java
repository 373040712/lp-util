package com.lp.redisson;

/**
 * @author peng.liu
 * @date 2019/3/11 23:39
 */

import org.redisson.Redisson;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Author:ZhuShangJin Date:2018/8/31
 */
public class RedisOutFromQueue {

    public static void main(String args[]) {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.16.63.28:6379").setPassword("singheadredis").setDatabase(2);
        RedissonClient redissonClient = Redisson.create(config);

        RBlockingQueue<CallCdr> blockingFairQueue = redissonClient.getBlockingQueue("delay_queue");
        RDelayedQueue<CallCdr> delayedQueue = redissonClient.getDelayedQueue(blockingFairQueue);
        while (true) {
            CallCdr callCdr = null;
            try {
                callCdr = blockingFairQueue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("订单取消时间：" + new SimpleDateFormat("hh:mm:ss").format(new Date()) + "==订单生成时间"
                    + callCdr.getPutTime());

        }

    }
}

