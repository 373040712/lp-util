package com.lp.redisson;

import org.redisson.Redisson;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.TimeUnit;

/**
 * @author peng.liu
 * @date 2019/3/19 16:26
 */

public class RedisMapProduce {

    public static void main(String[] args) throws InterruptedException {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://172.16.63.28:6379").setPassword("singheadredis").setDatabase(9);
        RedissonClient redisson = Redisson.create(config);

        RMapCache<String, String> map = redisson.getMapCache("myMap");
        String key = "test";
        for (int i = 0; i < 2000; i++) {
            long result = map.remainTimeToLive(key);
            if (result != -2 && result != -1) {
                map.fastPut(key, "" + i);
            } else {
                key = "test" + i;
                map.fastPut(key, "" + i, 1, TimeUnit.SECONDS);
            }
            System.out.println("_________________" + i);
        }
    }
}
