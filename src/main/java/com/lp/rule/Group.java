package com.lp.rule;

import java.util.List;

/**
 * @author liupeng@newhope.cn
 * @date 2023/07/03
 **/
public class Group {

    /**
     * 只支持运算符：||和&&
     */
    private String operator;
    /**
     * 规则条件集合
     */
    private List<Condition> conditionList;
    /**
     * 规则条件组集合
     */
    private List<Group> groupList;
}
