package com.lp.rule;

import java.util.List;

/**
 * @author liupeng@newhope.cn
 * @date 2023/07/03
 **/
public class Condition {
    /**
     * 条件属性编码
     */
    private String code;
    /**
     * 条件属性名称
     */
    private String name;
    /**
     * 条件属性函数
     */
    private String function;
    /**
     * 条件属性值集合
     */
    private List<Object> value;
}
