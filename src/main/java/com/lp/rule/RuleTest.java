package com.lp.rule;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorBoolean;
import com.googlecode.aviator.runtime.type.AviatorDouble;
import com.googlecode.aviator.runtime.type.AviatorObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author liupeng@newhope.cn
 * @date 2023/07/03
 **/
public class RuleTest {
    public static void main(String[] args) {
        AviatorEvaluator.addFunction(new MultiplyFunction());
        AviatorEvaluator.addFunction(new ContainFunction());

        Map<String, Object> params = new HashMap<>();
        params.put("a", 12.23);
        params.put("b", -2.3);
        System.out.println(AviatorEvaluator.execute("multiply(a, b)", params));

        Map<String, Object> env = new HashMap<String, Object>();
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(3);
        list.add(20);
        list.add(10);
        env.put("list", list);
        env.put("input", 10);
        env.put("s1", "abcdefg");
        env.put("s2", "cdx");
        env.put("s3", "cds");
        System.out.println(AviatorEvaluator.execute("(include(list,input) || string.contains(s1,s2)) && string.contains(s1,s3)", env));

        Expression expression = AviatorEvaluator.compile("(include(list,input) || string.contains(s1,s2)) && string.contains(s1,s3)", true);
        System.out.println(expression.getVariableFullNames());

        env.put("list", Lists.newArrayList(1, 2, 3, 4));
        env.put("p1", 1);
        env.put("p2", 5);
        env.put("p3", 2);

        Stopwatch stopwatch = Stopwatch.createStarted();
        System.out.println(AviatorEvaluator.execute("(IN(list,p1) || IN(list,p2)) && (IN(list,p3)) && (include(list,input) || string.contains(s1,s2)) && string.contains(s1,s3)", env));
        System.out.println(stopwatch.elapsed(TimeUnit.MILLISECONDS));
    }
}

class MultiplyFunction extends AbstractFunction {

    @Override
    public AviatorObject call(Map<String, Object> env, AviatorObject arg1, AviatorObject arg2) {
        double num1 = FunctionUtils.getNumberValue(arg1, env).doubleValue();
        double num2 = FunctionUtils.getNumberValue(arg2, env).doubleValue();
        return new AviatorDouble(num1 * num2);
    }

    @Override
    public String getName() {
        return "multiply";
    }
}

class ContainFunction extends AbstractFunction {

    @Override
    public AviatorObject call(Map<String, Object> env, AviatorObject arg1, AviatorObject arg2) {
        List<?> list = (List<?>) arg1.getValue(env);
        Object object = arg2.getValue(env);
        return AviatorBoolean.valueOf(list.contains(object));
    }

    @Override
    public String getName() {
        return "IN";
    }
}

