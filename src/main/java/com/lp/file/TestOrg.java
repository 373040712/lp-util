package com.lp.file;

import com.google.common.primitives.Ints;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author peng.liu
 * @date 2018/12/4 17:47
 */

public class TestOrg {

    public static void main(String[] args) throws IOException, URISyntaxException {
        URL url = ResourceUtils.getURL("classpath:org.txt");
        List<String> list = Files.readAllLines(Paths.get(url.toURI()));
        List<OrgEmp> orgEmps = list.stream().map(e -> {
            String[] line = e.split(",");
            return new OrgEmp(line[0], line[1], Ints.tryParse(line[2]));
        }).collect(Collectors.toList());
        orgEmps.stream().collect(Collectors.groupingBy(e -> e.getOrg())).entrySet()
                .forEach(e -> System.out
                        .println(e.getValue().stream().sorted(Comparator.comparing(OrgEmp::getMoney).reversed())
                                .limit(3).collect(Collectors.toList())));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    static class OrgEmp {
        String org;
        String emp;
        Integer money;
    }
}
