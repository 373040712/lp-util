package com.lp.file;

import com.google.common.primitives.Ints;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author peng.liu
 * @date 2018/12/4 17:47
 */

public class TestStu {

    public static void main(String[] args) throws IOException, URISyntaxException {
        URL url = ResourceUtils.getURL("classpath:student.txt");
        List<String> list = Files.readAllLines(Paths.get(url.toURI()));
        List<Student> students = list.stream().map(e -> {
            String[] line = e.split(",");
            return new Student(line[0], line[1], Ints.tryParse(line[2]));
        }).collect(Collectors.toList());
        students.stream().collect(Collectors.groupingBy(e -> e.getName())).entrySet().forEach(e -> System.out.println(
                e.getKey() + ":" + e.getValue().stream().mapToInt(i -> i.getScore()).summaryStatistics().getAverage()));
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    static class Student {
        String name;
        String subject;
        Integer score;
    }
}
