package com.lp.spring;

/**
 * @author peng.liu
 * @date 2019/2/28 10:34
 */

public interface MessageHandler {

    String show();
}
