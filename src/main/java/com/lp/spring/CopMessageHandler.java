package com.lp.spring;

import org.springframework.stereotype.Component;

/**
 * @author peng.liu
 * @date 2019/2/28 10:36
 */
@Component
public class CopMessageHandler implements MessageHandler{

    @Override
    public String show() {
        return "COP";
    }
}
