package com.lp.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author peng.liu
 * @date 2019/2/27 16:19
 */

public class TestLp {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguation.class);

        LPConfig ac = (LPConfig) context.getBean("lpConfig");
        System.out.println(ac.toString());
        AppConfig app = (AppConfig) context.getBean("appConfig");
        System.out.println(app);
    }
}
