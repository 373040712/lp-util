package com.lp.spring;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author peng.liu
 * @date 2019/2/28 11:32
 */
@Data
public class LPConfig {

    @Autowired
    private LPBase lpBase;

    @Override
    public String toString() {
        lpBase.show();
        return "LPConfig{" + "lpBase=" + lpBase.toString() + '}';
    }
}
