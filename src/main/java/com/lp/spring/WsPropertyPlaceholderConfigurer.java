package com.lp.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 重写Property加载
 *
 * @author ping
 * @date 2018/10/24
 * @since V1.0.1
 */
public class WsPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
            throws BeansException {
        Map<String, String> map = new HashMap<String, String>(1);
        map.put("server-id", "aaaaaaaaa");
        props.putAll(map);
        super.processProperties(beanFactory, props);
    }
}
