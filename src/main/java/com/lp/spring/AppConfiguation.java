package com.lp.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author peng.liu
 * @date 2019/2/27 16:20
 */
@Configuration
@ComponentScan(basePackages = "com.lp.spring")
public class AppConfiguation implements EnvironmentAware {

    @Value("${key1}")
    private String key1;

    private Environment env;

    @Bean
    public LPBase lpBase(){
        return new LPBase();
    }

    @Bean
    public AppConfig appConfig(List<MessageHandler> list) {
        AppConfig appConfig = new AppConfig();
        appConfig.setMessageHandler(list.get(0));
        return appConfig;
    }

    @Bean
    public LPConfig lpConfig() {
        LPConfig lpConfig = new LPConfig();
//        LPBase temp = lpBase();
//        if (lpBase.equals(temp)) {
//            System.out.println("参数注入和方法获取是同一个实例");
//        }
//        lpConfig.setLpBase(lpBase);
        return lpConfig;
    }

    @Bean
    public WsPropertyPlaceholderConfigurer properties() throws IOException {
        final WsPropertyPlaceholderConfigurer ppc = new WsPropertyPlaceholderConfigurer();
        ppc.setIgnoreResourceNotFound(true);
        final List<Resource> resourceLst = new ArrayList<Resource>();
        ClassPathResource temp = new ClassPathResource("spring/config.properties");
        if (temp.exists()) {
            resourceLst.add(temp);
            ppc.setLocations(resourceLst.toArray(new Resource[] {}));
        } else {
            ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
            ppc.setLocations(resolver.getResources("springl/*.properties"));
        }
        return ppc;
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.env = environment;
    }
}
