package com.lp.spring;

import org.springframework.stereotype.Component;

/**
 * @author peng.liu
 * @date 2019/2/28 10:35
 */
@Component
public class AopMessageHandler implements MessageHandler{

    @Override
    public String show() {
        return "AOP";
    }
}
