package com.lp.spring;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author peng.liu
 * @date 2019/2/27 16:22
 */
@Data
public class AppConfig {

    @Value("${key1:1}")
    private String key1;

    @Value("${key2}")
    private String key2;

    @Value("${server-id}")
    private String key3;

    private MessageHandler messageHandler;

    @Override
    public String toString() {
        return "AppConfig [key1=" + key1 + ", key2=" + key2 + ", key3=" + messageHandler.show() + "]";
    }
}
