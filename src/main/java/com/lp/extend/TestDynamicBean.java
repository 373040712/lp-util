package com.lp.extend;

import com.lp.common.pojo.DynamicBean;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * 动态类处理
 *
 * @author LP
 * @date 2017/11/24 19:20
 */

public class TestDynamicBean {

    public static void main(String[] args) throws ClassNotFoundException {
        TestDynamicBean testDynamicBean = new TestDynamicBean();
        HashMap returnMap = new HashMap();
        returnMap.put("name", "name");
        HashMap typeMap = new HashMap();
        typeMap.put("name", Class.forName("java.lang.String"));
        // map转换成实体对象
        DynamicBean bean = new DynamicBean(typeMap);
        // 赋值
        Set keys = typeMap.keySet();
        for (Iterator it = keys.iterator(); it.hasNext();) {
            String key = (String) it.next();
            bean.setValue(key, returnMap.get(key));
        }
        System.out.println(bean.getValue("name"));
    }
}
