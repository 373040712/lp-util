package com.lp.extend;

import com.google.common.base.Splitter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author LP
 * @date 2018/1/15 11:49
 */

public class TestRSAHelper {

    private final static Pattern CONNECT_PATTERN = Pattern.compile("[&|]");

    private final static Pattern IN_PATTERN = Pattern.compile("(\\[\\S+\\])");

    private final static Pattern OPERATOR_PATTERN = Pattern.compile("(\\s+\\S+\\s+)");

    public static void main(String[] args) {

        String template = "? in[{0}]sdg";
        Matcher inMatcher = IN_PATTERN.matcher(template);
        while (inMatcher.find()){
            System.out.println(inMatcher.group().trim());
        }

        String str = "xx.ss >= 100 && dd.ee <= 123 || ss.in in [1,2,3]";
        Iterable<String> array = Splitter.on(CONNECT_PATTERN).trimResults().omitEmptyStrings().split(str);
        for (String temp : array) {
            Matcher matcher = OPERATOR_PATTERN.matcher(temp);
            while (matcher.find()){
                System.out.println(matcher.group().trim());
            }
        }
    }
}
