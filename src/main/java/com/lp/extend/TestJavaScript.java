package com.lp.extend;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author LP
 * @date 2018/1/12 18:21
 */

public class TestJavaScript {

    public static void main(String[] args) throws Exception {
        TestJavaScript javaScript = new TestJavaScript();
        javaScript.testJS();
    }

    public void testJS() throws IOException, ScriptException {
        String json = "{\"Version\":\"120\",\"CreateDate\":\"2013-05-25T06:50:25.584Z\",\"Desc\":\"abcdef\"}";

        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine nashorn = scriptEngineManager.getEngineByName("nashorn");
        Path jsPath = Paths.get(System.getProperty("user.dir") + "/src/main/resources/jsonSQL.js");
        nashorn.eval(new String(Files.readAllBytes(jsPath)));
        Object eval = nashorn.eval("Query({\'id\':\'1\'},\'* where id=1\')");
        System.out.println(eval);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class Student {
        int id;
        String name;
    }
}
