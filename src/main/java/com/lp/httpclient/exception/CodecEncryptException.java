package com.lp.httpclient.exception;

/**
 * @author peng.liu
 * @date 2018/11/26 17:34
 */

public class CodecEncryptException extends Exception {
    private static final long serialVersionUID = -4903983876614965725L;
    private String message;

    public CodecEncryptException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
