package com.lp.httpclient.exception;

/**
 * @author peng.liu
 * @date 2018/11/26 17:41
 */

public enum ClientErrorCode {
    /**100001 未知的appId！**/
    UNKNOW_APPID(100001, "unknow appId!")
    ;

    private int code;
    private String message;

    private ClientErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ClientErrorCode getByCode(int code) {
        for (ClientErrorCode errorCode : ClientErrorCode.values()) {
            if (errorCode.getCode() == code) {
                return errorCode;
            }
        }
        return null;
    }
    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
