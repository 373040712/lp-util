package com.lp.httpclient.exception;

/**
 * @author peng.liu
 * @date 2018/11/26 17:35
 */

public class CodecHttpException extends Exception {
    private static final long serialVersionUID = -4903983876614965725L;
    private int errorCode;
    private String message;

    public CodecHttpException(String message) {
        this.message = message;
    }

    public CodecHttpException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public CodecHttpException(Throwable cause) {
        super(cause);
    }

    public CodecHttpException(ClientErrorCode errorCode) {
        this.errorCode = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    public CodecHttpException(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
