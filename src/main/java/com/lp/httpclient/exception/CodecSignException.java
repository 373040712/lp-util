package com.lp.httpclient.exception;

/**
 * @author peng.liu
 * @date 2018/11/26 17:44
 */

public class CodecSignException extends Exception {
    private static final long serialVersionUID = -4903983876614965725L;
    private int errorCode;
    private String message;

    public CodecSignException(String message) {
        this.message = message;
    }

    public CodecSignException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }

    public CodecSignException(Throwable cause) {
        super(cause);
    }

    public CodecSignException(ClientErrorCode errorCode) {
        this.errorCode = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    public CodecSignException(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
