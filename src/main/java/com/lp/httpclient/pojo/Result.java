package com.lp.httpclient.pojo;

import lombok.Data;

import static com.lp.httpclient.ClassUtil.STATUS_OK;

/**
 * @author peng.liu
 * @date 2018/12/7 12:36
 */
@Data
public class Result<T> {
    private T content;
    private String status;
    private String errorCode;
    private String errorMsg;

    public boolean isSuccess() {
        return STATUS_OK.equals(this.status);
    }
}
