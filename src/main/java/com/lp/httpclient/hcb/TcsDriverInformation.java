package com.lp.httpclient.hcb;

import lombok.Data;


@Data
public class TcsDriverInformation {

    /**
     *
     */
    private static final long serialVersionUID = -959446844802901033L;

    private String driverPhone;

    private String driverName;

    private String id;
}