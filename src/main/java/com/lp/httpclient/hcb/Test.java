package com.lp.httpclient.hcb;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.SSLs;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.lp.excel.TExcelUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.Header;
import org.apache.http.client.HttpClient;

import javax.ws.rs.HttpMethod;
import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Test {

    private static final String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiIzNzFkZjljMDhjZTM0MTBjYjMyNjgyNGFmOTViMGNmNyIsIkxPR0lOX1RJTUUiOiIxNjIyNDM4ODA3MTE5IiwiVE9LRU5fVFlQRSI6InBjIn0.HQM37eTWv2SPF0wMkNsJ9G5TBXAFdkRqnwAMJ0VXf68";
    private static final String tenantId = "T10000157";
    private static final String tenantName = "速必达冷链物流有限责任公司";
    private final static String ROOT_URL = "http://otms.yunlizhi.cn/";

//    private static final String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiJ0cG9rN2Q1ZDVkMTc0Yzg1OWRmMjQyZTYxNjY2YTA0YiIsIkxPR0lOX1RJTUUiOiIxNjIyNDI5MDU0OTg2IiwiVE9LRU5fVFlQRSI6InBjIn0.Ky-UuaXjlY_CgKnmWY1N0deJ6rHf1E6lyUfVg95mU9I";
//    private static final String tenantId = "T1020007";
//    private static final String tenantName = "运荔枝测试组";
//    private final static String ROOT_URL = "http://otms-ui.hope-saas-ui.dev.yunlizhi.net/";

    public final static String ROOT_PATH = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test"
            + File.separator + "resources" + File.separator;



    public static void main(String[] args) throws Exception {

        String path = ROOT_PATH + "车辆信息模板.xls";

        // 不基于注解,将Excel内容读至List<List<String>>对象内
        List<List<String>> lists = TExcelUtils.getInstance().readExcel2List(path, 1);
        System.out.println("读取Excel至String数组：");
        Map<String, Object> hashMap = new HashMap<String, Object>();

        for (List<String> list : lists) {
            insertOutCar(list);
        }


    }

    /**
     * 新增车辆
     *
     * @param list
     * @throws HttpProcessException
     */
    public static void insertOutCar(List<String> list) throws HttpProcessException {
        String url = ROOT_URL + "hope-saas-tcs-web/tcsCar/insertOut";
        Map<String, Object> param = new HashMap<>();
        Map<String, Object> map = new HashMap<>();
        map.put("tenantId", tenantId);
        map.put("tenantName", tenantName);
        map.put("lcensePlate", list.get(1));//车牌
        map.put("carSettingName", list.get(10));
//        if ("1".equals(list.get(47))) {
//            map.put("carSettingId", null);
//        } else {
//            map.put("carSettingId", list.get(47));
//        }
        map.put("brandModel", list.get(7));
        map.put("fuelTypeName", list.get(4));
        map.put("licensePlateType", list.get(2));
        map.put("carType", list.get(3));
        map.put("owningSideName", list.get(6));
//        map.put("carAllowWeight", list.get(6));
//        map.put("carGrossWeight", list.get(7));
        map.put("frameNumber", list.get(5));
//        map.put("carLength", new BigDecimal(list.get(8).replace("*", "")).divide(new BigDecimal(1000)));
//        map.put("carWide", new BigDecimal(list.get(9).replace("*", "")).divide(new BigDecimal(1000)));
//        map.put("carHeight", new BigDecimal(list.get(10).replace("*", "")).divide(new BigDecimal(1000)));
//        map.put("carVolume", NumberUtil.round(new BigDecimal(list.get(8).replace("*", ""))
//                .multiply(new BigDecimal(list.get(9).replace("*", "")))
//                .multiply(new BigDecimal(list.get(10).replace("*", "")))
//                .divide(new BigDecimal(1000000000)), 3));
        map.put("carriageBrand", null);
        map.put("isTemperatureControl", 1);
        map.put("gpsSystemName", list.get(8));
        map.put("advertisingAgency", null);
        // 辅助配置
        map.put("assistFixingsName", null);
//        map.put("refrigeratorBrand", list.get(18));
        map.put("passQualificationName", "无");
//        map.put("trafficInsuranceStartDate", DateUtil.parse(list.get(24)));
//        map.put("trafficInsuranceEndDate", DateUtil.parse(list.get(25)));
//        map.put("carShipInsuranceStartDate", DateUtil.parse(list.get(26)));
//        map.put("carShipInsuranceEndDate", DateUtil.parse(list.get(27)));
//        map.put("commercialInsuranceStartDate", DateUtil.parse(list.get(28)));
//        map.put("commercialInsuranceEndDate", DateUtil.parse(list.get(29)));
        // 年审
        map.put("annualReviewStatus", 1);
        map.put("licensePlateTypeCode", 2);


//        map.put("advertisementName", list.get(0));

//        map.put("carAge", 1);
//        map.put("carLabelTypeName", 1);
//
//        map.put("engineNumber",1);
//        map.put("etcCode",1);
//        map.put("transportPermitNo",1);
//
//        map.put("frequency",1);
//        map.put("fuelConsumption",1);
//
//        map.put("intoCity",1);
//        map.put("isTrailer",1);
//
//
//        map.put("loadAccessories",1);
//        map.put("maxTemperature",1);
//        map.put("minTemperature",1);
//        map.put("oilCard",1);
//
//        map.put("registerDate",1);
//        map.put("temperatureRange",1);
//
//
//        map.put("vehicleLicenseCopyUrl",1);
//        map.put("vehicleLicenseUrl",1);
//        map.put("isColdCar",1);
//
//
//
//
        map.put("passQualificationCode", 2);

        map.put("annualReviewDate", 32);
//
//
        map.put("cooperationType", 0);
//
//
//        map.put("carSettingId",1);


        Map<String, Object> supplierMap = new HashMap<>();
        supplierMap.put("tenantName", tenantName);
        supplierMap.put("tenantId", tenantId);
        supplierMap.put("id", "8cc28c7865554caea2db23b590058221");
        supplierMap.put("code", "YLGYS20210324000013");
        supplierMap.put("supplierName", "速必达冷链物流有限责任公司");
        supplierMap.put("supplierType", 0);
//        supplierMap.put("contact","27832");
        supplierMap.put("phone", null);
        supplierMap.put("bindStatus", "1");
//        supplierMap.put("bindTenantId","T10000084051");
//        supplierMap.put("bindTenantName","87232");
        map.put("supplier", supplierMap);
        param.put("carInformation", map);
        doHandler(param, url, HttpMethod.POST);
    }

    /**
     * 新增承运商
     *
     * @param list
     * @throws HttpProcessException
     */
    public static void addSupplier(List<String> list) throws HttpProcessException {
        String url = ROOT_URL + "tms_plus_waybill/transport_supplier/insert";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tenantId", tenantId);
        map.put("tenantName", tenantName);
        map.put("supplierName", list.get(0));
        map.put("supplierType", list.get(1));
        map.put("cooperationTypeCode", list.get(2));
        map.put("cooperationStartTime", list.get(3));
        map.put("joinBid", list.get(4));
        doHandler(map, url, HttpMethod.POST);
    }

    /**
     * 删除自有司机method
     *
     * @param list
     * @throws HttpProcessException
     */
    public static void delOwnerDriver(List<String> list) throws HttpProcessException {
        String url = ROOT_URL + "hope-saas-tcs-web/tcsDriverInformation/deleteById?id=" + list.get(0);
        Map<String, Object> map = new HashMap<String, Object>();
        doHandler(map, url, HttpMethod.GET);
    }

    /**
     * 新增自有司机method
     *
     * @param list
     * @throws HttpProcessException
     */
    public static void addOwnerDriver(List<String> list) throws HttpProcessException {
        String url = ROOT_URL + "hope-saas-tcs-web/tcsDriver/insert";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("tenantId", tenantId);
        map.put("tenantName", tenantName);
        map.put("driverName", list.get(0));
        map.put("driverPhone", list.get(1));
        map.put("identityCard", list.get(2));
        map.put("drivingLicense", list.get(3));
        doHandler(map, url, HttpMethod.POST);
    }

    public static String doHandler(Map<String, Object> map, String url, String method) throws HttpProcessException {
        // 插件式配置生成HttpClient时所需参数（超时、连接池、ssl、重试）
        HCB hcb = HCB.custom().timeout(1000) // 超时
                .pool(100, 10) // 启用连接池，每个路由最大创建10个链接，总连接数限制为100个
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2) // 设置ssl版本号，默认SSLv3，也可以调用sslpv("TLSv1.2")
                .ssl() // https，支持自定义ssl证书路径和密码，ssl(String keyStorePath, String keyStorepass)
                .retry(5) // 重试5次
                ;

        HttpClient client = hcb.build();
        // 插件式配置Header（各种header信息、自定义header）
        Header[] headers = HttpHeader.custom().userAgent("javacl")
                .contentType("application/json;charset=UTF-8")
                .other("Token", token)
                .build();
        // 插件式配置请求参数（网址、请求参数、编码、client）
        HttpConfig config = HttpConfig.custom().headers(headers) // 设置headers，不需要时则无需设置
                .url(url) // 设置请求的url
//                .map(map) // 设置请求参数，没有则无需设置
                .json(JSON.toJSONString(map))
                .encoding("utf-8") // 设置请求和返回编码，默认就是Charset.defaultCharset()
                .client(client); // 如果只是简单使用，无需设置，会自动获取默认的一个client对象
        String result = null; // post请求
        if (Objects.equals(HttpMethod.POST, method)) {
            result = HttpClientUtil.post(config);
        } else if (Objects.equals(HttpMethod.GET, method)) {
            result = HttpClientUtil.get(config);
        } else if (Objects.equals(HttpMethod.PUT, method)) {
            result = HttpClientUtil.put(config);
        } else if (Objects.equals(HttpMethod.DELETE, method)) {
            result = HttpClientUtil.delete(config);
        }
        System.out.println(result);
        return result;
    }

    /**
     * 新增外请司机method,先判断是否存在司机信息，如果存在则直接绑定，不存在，则新增
     *
     * @param list
     * @throws HttpProcessException
     */
    public static void findOutDriver(List<String> list) throws HttpProcessException {
        String url = ROOT_URL + "hope-saas-tcs-web/tcsDriver/selectListByPhone?phone=" + list.get(0);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("driverName", list.get(1));
        map.put("driverPhone", list.get(0));
        map.put("identityCard", checkData(list, 2));
        map.put("drivingLicense", checkData(list, 3));
        map.put("tenantId", tenantId);
        map.put("tenantName", tenantName);
        String result2 = doHandler(map, url, HttpMethod.GET);
        JSONObject jsonObject = JSON.parseObject(result2);
        Object data = jsonObject.get("data");
        if (data != null) {
            List<TcsDriverInformation> tcsDriverInformations = JSONArray.parseArray(data.toString(), TcsDriverInformation.class);
            if (CollectionUtils.isNotEmpty(tcsDriverInformations)) {
                insertDataOut(tcsDriverInformations.get(0));
                return;
            }
        }
        insertData(list);
        System.out.println(result2);
    }

    public static void insertDataOut(TcsDriverInformation information) throws HttpProcessException {
        String url = ROOT_URL + "hope-saas-tcs-web/tcsDriver/insertTenant";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("driverId", information.getId());
        map.put("driverName", information.getDriverName());
        map.put("phone", information.getDriverPhone());
        String result2 = doHandler(map, url, HttpMethod.POST);
        System.out.println(information.getDriverPhone() + "：" + result2);
    }

    // 司机刷数据
    public static void insertData(List<String> list) throws HttpProcessException {
        String url = ROOT_URL + "hope-saas-tcs-web/tcsDriver/insertOut";
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("driverPhone", list.get(0));
        map.put("driverName", list.get(1));
        map.put("identityCard", checkData(list, 2));
        map.put("drivingLicense", checkData(list, 3));
        map.put("tenantId", tenantId);
        map.put("tenantName", tenantName);
        String postResult = doHandler(map, url, HttpMethod.POST);
        System.out.println(list.get(0) + "：" + postResult);
    }

    public static String checkData(List<String> list, int index) {
        if (list.size() >= 4) {
            return list.get(index);
        }
        if (list.size() == 3) {
            if (index == 2) {
                return list.get(2).length() > 3 ? list.get(2) : "";
            } else if (index == 3) {
                return list.get(2).length() == 2 ? list.get(2) : "";
            }
        }
        return null;
    }
}
