package com.lp.httpclient.hcb;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author peng.liu
 * @date 2018/10/18 11:49
 */

public class TestCleanUtil {

    public static void main(String[] args) throws HttpProcessException {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sid", "2892961576");
        map.put("st", "248C2D5CBB25C7D2");
        map.put("bizType", "304");

        List<String> list = Lists.newArrayList("1402015", "1903164", "1915913", "1938836", "1954101", "1954400",
                "3226570", "3423697", "3508018", "3896874", "3987092", "3998529", "4256408", "4956586", "5483150",
                "8151243", "8224170", "8879827", "8923678", "10958614", "12021612", "12076957", "12953844");
        for (String empId : list) {
            map.put("empId", empId);
            String url = "http://crmop-admin.56qq.com/web/customer/bind-relation/clear";
            HttpConfig config = HttpConfig.custom().url(url).map(map).encoding("utf-8");

            String result = HttpClientUtil.post(config);
            System.out.println(result);
        }
    }
}
