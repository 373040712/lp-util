package com.lp.httpclient.hcb;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;

import java.security.MessageDigest;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static com.google.common.base.Charsets.UTF_8;

/**
 * 风控请求认证
 * 
 * @author peng.liu
 * @date 2019/5/7 16:47
 */
public class ShieldHttpUtil {

    private static Map<String, String> header = Maps.newHashMap();

    private static String secret = "";

    public ShieldHttpUtil() {
        this("dev");
    }

    public ShieldHttpUtil(String env) {
        header.put("Accept-Language", "zh-CN");
        header.put("Connection", "Keep-Alive");
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36");
        header.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        header.put("X-Requested-With", "shield");
        header.put("X-Shield-Version", "2.0.0");
        header.put("X-Shield-ContentKey", "data");
        if (Objects.equals(env, "qa")) {
            header.put("Host", "shield.qa-bj.56qq.com");
            header.put("X-Shield-AppKey", "758a46f8f0af484bb11fd4a6ac62d6ab");
            secret = "f7193a9a6e21d02d52628774ee7f91c8cccc502783a87822";
        } else {
            header.put("Host", "shield.dev-ag.56qq.com");
            header.put("X-Shield-AppKey", "3e245ad110c14b8299132ddae4cbaea3");
            secret = "7205c8c346fa237fbc009e030d1f21c9e24a320d54d27870";
        }
    }

    public static void main(String[] args) throws Exception {
        ShieldHttpUtil shieldHttpUtil = new ShieldHttpUtil();
        String code = "lp_xx";
        String path = "/gateway/v2/verify/" + code + "/sync";
        Map<String, Object> param = ImmutableMap.of("deviceId", "5c80e068", "bankCode", "BOC", "totalAmount", "0.01",
                "memberId", "100010400121", "userMobile", "13408663242");

        System.out.println("----------------------开始-------------------");
        int total = 10;
        ExecutorService executor =
                new ThreadPoolExecutor(10, 10, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
        CountDownLatch latch = new CountDownLatch(total);
        while ((total--) > 0) {
            executor.execute(() -> {
                try {
                    System.out.println(shieldHttpUtil.pushEvent(path, param));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                latch.countDown();
            });
        }
        latch.await();
        System.out.println("----------------------完成-------------------");
        System.exit(0);
    }

    public String pushEvent(String path, Map<String, Object> param) throws Exception {
        HttpConfig config = HttpConfig.custom().url(getUrl(path)).headers(getHeader(param)).map(getParam(param))
                .encoding(UTF_8.name());
        return HttpClientUtil.post(config);
    }

    private String getUrl(String path) {
        return "http://" + header.get("Host") + path;
    }

    private Header[] getHeader(Map<String, Object> param) throws Exception {
        String paramStr = getParamStr(param);
        String prefix = StringUtils.isBlank(paramStr) ? "" : paramStr + "&";
        String content = prefix + "secret=" + secret;
        header.put("X-Shield-Sign", getMD5(content));
        HttpHeader httpHeader = HttpHeader.custom();
        for (Map.Entry<String, String> entry : header.entrySet()) {
            httpHeader.other(entry.getKey(), entry.getValue());
        }
        return httpHeader.build();
    }

    private Map<String, Object> getParam(Map<String, Object> param) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("data", getParamStr(param));
        return result;
    }

    private String getParamStr(Map<String, Object> param) {
        if (MapUtils.isEmpty(param)) {
            return StringUtils.EMPTY;
        }
        List<String> list = param.keySet().stream().sorted()
                .map(e -> MessageFormat.format("{0}={1}", e, param.get(e).toString())).collect(Collectors.toList());
        return String.join("&", list);
    }

    private String getMD5(String content) throws Exception {
        // 生成一个MD5加密计算摘要
        MessageDigest md = MessageDigest.getInstance("MD5");
        // 计算md5函数
        md.update(content.getBytes("UTF-8"));
        byte[] bytes = md.digest();
        String msg = toHexString(bytes);
        return msg.toUpperCase();
    }

    private String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int b : bytes) {
            int tmp = b & 0xFF;
            String digit = Integer.toHexString(tmp);
            if (digit.length() == 1) {
                sb.append("0");
            }
            sb.append(digit);
        }
        return sb.toString();
    }
}
