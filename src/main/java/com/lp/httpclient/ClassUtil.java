package com.lp.httpclient;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.util.TypeUtils;
import com.lp.httpclient.pojo.Result;
import org.apache.logging.log4j.util.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * @author peng.liu
 * @date 2018/12/7 11:40
 */

public class ClassUtil {

    public static final String CONTENT_FIELD = "content";
    public static final String STATUS_FIELD = "status";
    public static final String ERRORCODE_FIELD = "errorCode";
    public static final String ERRORMSG_FIELD = "errorMsg";
    public static final String STATUS_OK = "OK";

    public static <T> Result<T> get(String input, Class<T> clazz) {
        if (Strings.isBlank(input)) {
            return new Result<>();
        }
        JSONObject jsonObject = JSONObject.parseObject(input);
        Result<T> result = new Result<>();
        result.setStatus(jsonObject.getString(STATUS_FIELD));
        result.setErrorCode(jsonObject.getString(ERRORCODE_FIELD));
        result.setErrorMsg(jsonObject.getString(ERRORMSG_FIELD));
        if (STATUS_OK.equals(jsonObject.getString(STATUS_FIELD))) {
            if (clazz != null) {
                result.setContent(jsonObject.getObject(CONTENT_FIELD, clazz));
            }
        }
        return result;
    }

    public static <T> Result<List<T>> list(String input, Class<T> clazz) {
        if (Strings.isBlank(input)) {
            return new Result<>();
        }
        JSONObject jsonObject = JSONObject.parseObject(input);
        Result<List<T>> result = new Result<>();
        result.setStatus(jsonObject.getString(STATUS_FIELD));
        result.setErrorCode(jsonObject.getString(ERRORCODE_FIELD));
        result.setErrorMsg(jsonObject.getString(ERRORMSG_FIELD));
        if (STATUS_OK.equals(jsonObject.getString(STATUS_FIELD))) {
            if (clazz != null) {
                JSONArray array = jsonObject.getJSONArray(CONTENT_FIELD);
                if (array != null) {
                    result.setContent(castList(array, clazz));
                }
            }
        }
        return result;
    }

    private static <T> List<T> castList(JSONArray array, Class<T> clazz) {
        List<T> list = new ArrayList<>();
        for (Object obj : array) {
            if (obj != null) {
                if (obj instanceof JSONObject) {
                    JSONObject json = (JSONObject) obj;
                    list.add(JSON.toJavaObject(json, clazz));
                } else if (obj instanceof JSONArray) {
                    List<T> l = castList(array, clazz);
                    if (l != null && !l.isEmpty()) {
                        list.addAll(l);
                    }
                } else {
                    list.add(TypeUtils.castToJavaBean(obj, clazz));
                }
            }
        }
        return list;
    }
}
