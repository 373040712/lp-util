package com.lp.httpclient.server;

import com.google.common.collect.Maps;
import com.lp.httpclient.RSASignHttpUtil;
import org.junit.Test;

import java.util.Map;

/**
 * @author peng.liu
 * @date 2018/12/5 11:01
 */

public class WorkerbeeTest {

    private static RSASignHttpUtil devHttpUtil = new RSASignHttpUtil(true, "hive",
            "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMZ7DDwnNvVPZp9ta7Dy/g5voO1JhHCsMoEB9FJUxHoLoC/f4sNsLIHMTOLtr5CuawQ4J2Zjv8iwwLEcUUDUlCtUCvepDwDass6FUQJTinXy1hzXYjdwT3nzwIMiJD1dtaMBRUMqO7SyeooqZL9+qGwcY+d+/525Jy4aIMP21e1NAgMBAAECgYEAnQwf0jj1M4VwHhXmcEzU2XdaTmctdDnMfGefesW38Rnh1f/A3gBp9wpAjA6mLETWSLBl/I5Q4et5jKprYHDUC02jZiZnTpmsv/OPD5gzb2ZRLTD5I9OaTswjF2OnAD6m2hiR8dSHVMKtkNXjHQf2VGBTjfnqscByKB19+G1SaAECQQD8aBWt8G/ng0OKpnivQYwl/X7yWbsHbviq2f1ebONQmtqAxHjhYAMmrwI82gUFXUEBjjzY4d3Vrkc7S/hQSDPhAkEAyU5tB8Ugi5OSt+0b5EAHc5Y/42wQ7FJ5Ccee7aqxx2b2okAgbbNI6qXqVHGYiIWO/eLJd3hy+nA/+HjnPZqm7QJBAKbPYwSBUnN+nLSVME0i1KyGy87+3kwbWtwPFRPCgrhSnvVa40iPW4XFQ4VT0N2qs7uDdVMgcqs1cn4Nxx3HmgECQGG2SqAB5Fk7qXTTh71UAwns/VfjhZdgNNY1agRM3Xw/kymQXO1Cn/sdy89vUC28Pnsn0MjH02+SesqdzQiO/8UCQQDEd7suPe7QFYNnOgjLTLLK9DJujsN8WGqO9IoD6eIZoPQQShQJ+aUfhBaA/PW6ZQLkbobQMh9TTpkjrKCniyi2");

    private static RSASignHttpUtil httpUtil = new RSASignHttpUtil(true, "hive",
            "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAJvh2YfnSHeEwzk1HriQJHsL8Z4T6C7QvHYXfUP9Xs1uxOdffh5+fOiRGkGYfK0j1cB73LAPQkFjytpmMuUb6yiIATdQVSbIyCr9ZSxxtYep2k7RUmPdh0oXrsEB94ygcfsUxfaleFuoBfQjS/GZHzgJ6H5gQrH9ytylncmMuXcZAgMBAAECgYA+CEJossFDzrFhsqHnEHV0AezE22cqOibFV9OhmtJ/LfVtnbKyr+NFKXkpYWU8AWaukpvf9DKJkwzGnKTkR5fsheK0D1n5i/c7jvbUyiJspgYXOotpWVHxEJxdgWToKk1BlW4sPLLxjXC5kOnU2h66zIA+oqS4q0SNsW45qU/QAQJBAPwVhZWNSUHPNq7jrIW1akVRWa7ZBTyKTY5txM1zZ4ynTk9Cg9/hCWdVLHWkYMqsLLsni7ODgu1SOCN7GyMwFSkCQQCeTcGSb9O7Q6OE+wYJnPJG0x1Vpe9jJ5qCNiq3KZZSZ0ygm55Tk7nnxmKEyW7X/p4zeYka93Sj0PoLcRmbYyBxAkBRZP2aGWGjDy4/8CGflsE2mprTmx1Wu+1o5Mab8/xmbQsbCoYcPsRTiivt3GKobkmPMKiqEPewe1DHHBPaG8dpAkADe66ArFk6S5izelp9p6hlCowgsE/+HIb3yn1SHmYiVgeHmST4rc1vXhwY093Oftm4RDpSD5PPbu+xhMGNe7bhAkBzjfEoAw9Vk4jGdwZ+ByCXQRFcnP6mecAk3A+e8mWCToUAAEEWkmyyRwZtjJuGr/RdEKKWcWvRe27tpSG27L2Y");


    @Test
    public void testCrmop() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("requestId", "1221212");
        param.put("data", "{'uid':'20028022'}");

        System.out.println(devHttpUtil.signPost("http://workerbee.dev-ag.56qq.com/server/crmop/userSearch", param));
    }

    @Test
    public void testAvatarTag() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("requestId", "661111");
        param.put("data", "{'userType':1}");

        System.out.println(devHttpUtil.signPost("http://workerbee.dev-ag.56qq.com/server/avatarTag/listField", param));
    }

    private static RSASignHttpUtil aiocHttpUtil = new RSASignHttpUtil(true, "aioc",
            "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANKv12+wq/b1tdWwcoKILPISHX5hg2w2sp6UyEGtckY+8cAEvBA+YrCluCwlVAEr2DD29z20YebtjGePc2truEeFXZnMqmmxS79exfqImikOkCJUt1HXBacKPdn6y25Q/uZyVjROX6p9x+hFp+haiw4Iw4GG1Ydjr/Lprd5/MasBAgMBAAECgYEAzfEeWEnMvlkNxfR06k0sCe9VLx0odtW8obBzU2e0hR+v9W9rWUFx/JPwxA403/Q4nhw6LvGspStcjDo3qzAfbtnArriF9gjTfFFbMiLRDCFnixQnqHSFuLhx964wFJtMEBksPsIpoJTqhnKpQIhG+zpqWIIE5CgyNfAGPPlCIMECQQD/fhV9tdOmle6WA9Gv7Hyqf5wLKV+z9gIRz8WXJfuGMvdHN/E17h902DLSahxr8GgXy1/p27K/0IZEImRb/EtZAkEA0xr5aXeosFy8Y2anHjUYtr0ys0AzlfqXbTpZM/oHUzlSBne/w1XJXYX31pBdeadMUjTPTVuyVykPykwOu+Hv6QJANlAraWn58HP3IFT3gpqXdfdiyMym1674iajNrHAapFC5WJ5/3KMedMxxIWIFYLzepBBL938I5NYDdQCKrTNZIQJAM9JzV4igSGr7dAtHCwtNvc5EG+yuvSp+8blZHIFMOwnfMOdEk9cDT4oLhQh7adm/yo5cNUlFRlA2KBrzovubwQJBAJufEsviVRtoDrMOXbIBGd7mBqsV52eRE22IB4hyfpVykq3Q4VUfpQyjFLFeXkplnXafI+ueGVHV9z2/9/W3Ilo=");

    @Test
    public void testProdXiaoShouYi() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("requestId", "1233445556");

        System.out
                .println(aiocHttpUtil.signPost("http://workerbee.56qq.com/server/salesEasy/querySkillsetList", param));
    }
}
