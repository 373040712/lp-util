package com.lp.httpclient.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lp.excel.TExcelUtils;
import com.lp.httpclient.RSASignHttpUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author peng.liu
 * @date 2018/12/6 18:33
 */
@Slf4j
public class AvatarTest {

    public final static String ROOT_PATH = System.getProperty("user.dir") + File.separator + "src" + File.separator
            + "main" + File.separator + "resources" + File.separator;

    public final static String SQL_TEMPLATE =
            " UPDATE driver SET extend_info = JSON_SET(extend_info, \'$.newCarSalePlateNumber\', \'%s\') WHERE driver_id=%d";

    public final static String REMOVE_SQL_TEMPLATE =
            " UPDATE driver SET extend_info = JSON_REMOVE(extend_info, \'$.newCarSalePlateNumber\') WHERE driver_id=%d";

    private static RSASignHttpUtil httpUtil = new RSASignHttpUtil(true, "dac",
            "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJJXQEH6iUt6nlTAGQe3LhuTj6vWRyR0ky+3oSynlxu441D3M5HjjxuP1YSHkQyy7JC05xr+dFpEPHlcNfBorIu5P0S7DhBKi8IWpv/RNZy2nhLxx3pkENwvMALkv9/vAhxwysVqt/fuREqWoEVOmS1iR6QNVV2BSyPfqxqf+II7AgMBAAECgYAw+axSqHM8ZHwKpwN5do+iwP6rJppGlBESPiVdmXTT3UuONDz5DSw+sCNoKCqeEl9jindxrEjqUL3ga8DNhWqBETZJhhUoZIkYzpfSPFVXXvpwHvBisshHc15Zgtp2fF5xPZEPZUkJ2qMYI+EdmUZfzZOoPxgAJ8EVU0MxrxcX4QJBAOMez/wFOzH+C1MOon0q8wWKDA55KhvE9BXuEjSSJ7O0/C9rlNbx744am+tS4Kr8rHJSz8jYb/D/AfzVzzcbZPECQQCk8upTIaQuuXfJObIZfRYG0oLlxkIlUOlCH45fkpuRQQnYXim+wZKKMaglFYOcnsRg0YAXti/9ye5orjq0LGnrAkAaqe1k+wA+GEMgFyHAbCBn0BMmR9dHt1GomXCNt3LtyBurPYgVzAWBu2H+uJYjvNJKAarfO3m441dKZZeUMqUhAkEAlKB9G6L4eVGM6bgfDnBzZHpSkA/wkgcx5u7gLqW/n0ZnRJq0Krno/LR5udCE4p78e/DJ5pO28or9Uz2Kj8ZlOQJBAJIKjMcUJw6rsJ7sdBhw3akSsLi+WSOd39SFRdh5sXB//6iBtuAl2nWDLkkDu9vWGoNC9WEAXND8eafgoZqubfk=");

    private static RSASignHttpUtil devHttpUtil = new RSASignHttpUtil();

    @Test
    public void doDevReload() throws Exception {
        System.out.println(
                devHttpUtil.doPost("http://avatar-api.dev-ag.56qq.com/web/query/reload-mappings", Maps.newHashMap()));
    }

    @Test
    public void doReload() throws Exception {
        System.out.println(httpUtil.doPost("http://avatar-api.56qq.com/web/query/reload-mappings", Maps.newHashMap()));
    }

    @Test
    public void doNewCarUpdate() throws Exception {
        List<List<String>> list = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "/data/prd-new-car.xlsx");
//        List<UserCar> userCars = list.stream().map(
//                e -> new UserCar(Longs.tryParse(e.get(0)), e.get(1), e.size() > 2 ? Longs.tryParse(e.get(2)) : null))
//                .collect(Collectors.toList());
//        List<UserCar> userCars = Lists.newArrayList();
//        List<UserCar> removeList =
//                userCars.stream().filter(e -> Objects.nonNull(e.getRuid())).collect(Collectors.toList());
//        List<UserCar> updateList =
//                userCars.stream().filter(e -> Objects.nonNull(e.getUid()) && StringUtils.isNotBlank(e.getPlateNumber()))
//                        .collect(Collectors.toList());
//        if (CollectionUtils.isNotEmpty(removeList)) {
//            // System.out.println("移除操作");
//            // List<List<UserCar>> pageList = Lists.partition(removeList, 100);
//            // for (List<UserCar> tempList : pageList) {
//            // Map<String, Object> param = Maps.newHashMap();
//            // String multiSql = String.join(";", tempList.stream()
//            // .map(e -> String.format(REMOVE_SQL_TEMPLATE,
//            // e.getRuid())).collect(Collectors.toList()));
//            // param.put("multiSql", multiSql);
//            // System.out.println(httpUtil.signPost("http://avatar-api.56qq.com/server/sql/update",
//            // param));
//            // }
//        }
//        if (CollectionUtils.isNotEmpty(updateList)) {
//            System.out.println("更新操作");
//            List<List<UserCar>> pageList = Lists.partition(updateList, 100);
//            for (List<UserCar> tempList : pageList) {
//                Map<String, Object> param = Maps.newHashMap();
//                String multiSql = String.join(";",
//                        tempList.stream().map(e -> String.format(SQL_TEMPLATE, e.getPlateNumber(), e.getUid()))
//                                .collect(Collectors.toList()));
//                param.put("multiSql", multiSql);
//                System.out.println(httpUtil.signPost("http://avatar-api.56qq.com/server/sql/update", param));
//            }
//        }
    }


    @Test
    public void doInsert() throws Exception {
        String type = "driver";
        int maxIndex = 1;
        String sql = "INSERT ignore INTO " + type
                + " (driver_id,username,type,status,register_time,real_name,ic_no,avatar_url,mobile,auth_status,last_login_time,province_id,city_id,county_id,register_location,position_info,extend_info) VALUES (%s)";
        for (int i = 0; i < maxIndex; i++) {
            String filePath = "/data/" + type + "/" + type + i + ".xlsx";
            List<List<String>> list = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + filePath, 0, 2000, 0);
            List<List<List<String>>> pageList = Lists.partition(list, 100);
            for (List<List<String>> tempList : pageList) {
                Map<String, Object> param = Maps.newHashMap();
                String multiSql = String.join(";",
                        tempList.stream().map(e -> String.format(sql, Joiner.on(",").join(getSqlField(e))))
                                .collect(Collectors.toList()));
                param.put("multiSql", multiSql);
                System.out.println(httpUtil.signPost("http://avatar-api.56qq.com/server/sql/update", param));
            }
        }
    }

    private List<String> getSqlField(List<String> e) {
        e.remove(0);
        e.remove(e.size() - 1);
        return e.stream().map(i -> {
            if (i.equalsIgnoreCase("NULL")) {
                return i;
            } else {
                return "'" + i + "'";
            }
        }).collect(Collectors.toList());
    }

    @Test
    public void fixNewIndex() throws Exception {
        String template = " UPDATE consignor SET update_time = now() WHERE user_id=%d";

        List<List<String>> list = TExcelUtils.getInstance().readExcel2List(
                ROOT_PATH + "/temp/consignor_index_20181211_consignor_a65dbcbd-e01a-49fb-b18b-6276bd492667.xlsx", 1);
        List<String> tempList =
                list.stream().map(e -> String.format(template, Long.parseLong(e.get(0)))).collect(Collectors.toList());
        Map<String, Object> param = Maps.newHashMap();
        String multiSql = String.join(";", tempList);
        param.put("multiSql", multiSql);
        System.out.println(httpUtil.signPost("http://avatar-api.56qq.com/server/sql/update", param));
    }

    @Test
    public void onlineLoad() throws Exception {
        doSearchOrDownload("consignor_index_20181211", "consignor",
                "{\"query\":{\"bool\":{\"must_not\":[{\"exists\":{\"field\":\"extendInfo.memberType\"}},{\"exists\":{\"field\":\"extendInfo.kill\"}}]}},\"_source\":[\"userId\"],\"from\":0,\"size\":1000}",
                true, true, null);
    }

    // @Test
    // public void onlinePreLoad() throws Exception {
    // List<List<String>> list = TExcelUtils.getInstance()
    // .readExcel2List(ROOT_PATH + "/temp/consignor_index_20181211_consignor_2.xlsx", 1);
    // List<String> userIds = list.stream().map(e -> e.get(0)).collect(Collectors.toList());
    // List<String> userIdStr =
    // Lists.partition(userIds, 1000).stream().map(e -> String.join(",",
    // e)).collect(Collectors.toList());
    //
    // String tempalte =
    // "{\"query\":{\"bool\":{\"must\":[{\"terms\":{\"userId\":[%s]}}]}},\"_source\":[\"userId\",\"source\"],\"from\":0,\"size\":10000}";
    //
    // int index = 11;
    // for (String str : userIdStr) {
    // doSearchOrDownload("consignor_index_20180601", "consignor", String.format(tempalte, str),
    // true, true,
    // index);
    // index++;
    // }
    // }

    private void doSearchOrDownload(String index, String type, String queryJson, boolean online, boolean download,
            Integer fileIndex) throws Exception {
        Preconditions.checkArgument(StringUtils.isNotBlank(index));
        Preconditions.checkArgument(StringUtils.isNotBlank(type));
        Preconditions.checkArgument(StringUtils.isNotBlank(queryJson));

        JSONObject jsonObject = JSONObject.parseObject(queryJson);
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("index", index);
        paramMap.put("type", type);
        paramMap.put("queryJSON", JSON.toJSONString(jsonObject));
        String output;
        if (online) {
            output = httpUtil.signPost("http://avatar-api.56qq.com/server/query/origin", paramMap);
        } else {
            output = devHttpUtil.signPost("http://avatar-api.dev-ag.56qq.com/server/query/origin", paramMap);
        }
        if (!download) {
            log.info("查询结果：{}", output);
            return;
        }
        JSONObject result = JSONObject.parseObject(output);
        if (!Objects.equals(result.getString("status"), "OK")) {
            log.error("请求失败，错误：{}", output);
            return;
        }
        JSONObject content = result.getJSONObject("content");
        JSONArray jsonArray = content.getJSONArray("datas");
        int size = jsonArray.size();
        log.info("数据总数，tc：{}", content.getIntValue("tc"));
        log.info("导出总数，tc：{}", size);
        if (size <= 0) {
            return;
        }
        List<String> header = jsonArray.getJSONObject(0).keySet().stream().collect(Collectors.toList());
        List<List<String>> list = Lists.newArrayList();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject data = jsonArray.getJSONObject(i);
            list.add(header.stream().map(e -> {
                Object object = data.get(e);
                return Objects.isNull(object) ? StringUtils.EMPTY : object.toString();
            }).collect(Collectors.toList()));
        }
        StringBuilder name = new StringBuilder();
        name.append(index).append("_").append(type).append("_")
                .append(Objects.isNull(fileIndex) ? UUID.randomUUID() : fileIndex).append(".xlsx");
        TExcelUtils.getInstance().exportObjects2Excel(list, header,
                ROOT_PATH + File.separator + "temp" + File.separator + name.toString());
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class UserCar {

        private Long uid;

        private String plateNumber;

        private Long ruid;
    }
}
