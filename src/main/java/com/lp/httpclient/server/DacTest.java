package com.lp.httpclient.server;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.lp.httpclient.RSASignHttpUtil;
import org.junit.Test;

import java.util.Map;

/**
 * @author peng.liu
 * @date 2018/11/26 19:46
 */

public class DacTest {

    private static RSASignHttpUtil httpUtil = new RSASignHttpUtil(true, "crmop",
            "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAI1JjusGy0Y8/feSzeKSipGA5eHEtWtR8GV9gl27Q0/po4GCvu16LhO2TH+Mqjkzgot+uCPxucUjMUItZpQjoEF9UFu/O+rM5Stcn5Qn6fu/+uzodXvkt90nQ0Z7g0o4IwKuJF3sIl1jOCUKafFmnM5ETot1cMzAeLUh2t7QOLM1AgMBAAECgYByRcJQGQij4ITQjm3zCO8GTCih98V77aPvkhm00dbZdXCHOUZPMHi//LvWleC/rwew52bru4CzD4mxqqLCwiXEWB5LsAYOIEaQjZA/bK2jQ+ecUvKQlLmo3wtjdAVEKT3fUI+qB9QuCVEYpbY4L8JWWLyw1Pocn0FvrY6dNiGiqQJBAOkQnngr9HvziORvrAG+f8FCoQAINk96mQ8ecKlCXat/WjUpFlHKmIDh/TGZQHrjIiyI7KrukPtujaV4UlPRG/MCQQCbMN/qC3DfIA2cEUg5vAQ42ts4UvtEks+A2+LL7F2jR84mv1JYsoTJnOt0BDHkRAp849YrWSAgvCao+IZWzAY3AkEAq5Pfhgqd7w6HrdyEXbhXpvgdPL/O884GljnLqN/Bw7p6t34q/0XN5bv5QSVYcd96zhxzVxdQRrnJr5KX0Tq1fwJADNxNAgE2tlbaqd9Iw/gPxYJlflJqHjGoXn5aWhnZXKmbrNxX/5+zn12QqYmbdNYqpFThL7BwGCZfccs+nrzDmwJAS+HgJQ6BLBy+Jp7jel/wtXxM5hHIQr7RurwsaAaBCKDPn3PjRJXzUHZYtl9REOYKeQETAeAtJ7Jt5mCMN+fLhw==");

    private static RSASignHttpUtil devHttpUtil = new RSASignHttpUtil();


    private static RSASignHttpUtil ccHttpUtil = new RSASignHttpUtil(false, "aios",
            "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANAOzgguPWjo8tMwqTdRNX0dd2HKmMoDfcxBOTsxy/COnBPeDnZhj00r0U1jN96x8JwtxRHkYqasJGgLdwTp5Bn8qWadtJFVgJmD06mi9ftfq/UWhS7uEh8nu4OxY6Hv5BIZi2JaxmB9kuk22RhCPoh6PAOZa/En96Xade6JqiNHAgMBAAECgYEAuC2S55z/UcJ9SsTV1xk1j3cpOlFdZd8nWkwWvLb9QQsNUW0OQm9kqrzyQF5nk+TTCX5e6NLTHZZfeBdGGfxupZUpzPFd205QAlhhajjRKKP11mc20hnSj1tSDTpl7J5ChSB1LTkPB7KNYObhXAOvkKYtDsKhX06wfRm+Hbdax8ECQQD1fS0OrzZ5F137QzvymBAPkWLZ64rayYDB9DmqZu60VJB/3xL/F1ajIDx+/HZq2DA1VIlslI9ePIOup5HCQeenAkEA2PdX2KxiUnB1I49D6dBB/7mosmdbe/9VJwmfRNfOTa/oGhR1dLhS45Kumw4N6nbcIRDhphiURdwbEfSxbv9bYQJAftztiAUsFW/p9YtNU5UZ17G+b/e1jlNL4u+fTVxcR3zcH/jcDE+3Sh3Cpa06VQTcBPNnAWHUpVKIip22SbldSQJAC3fHZOR3rfPTvTVtWYZwdpvRltxvsXYlghK4IGNnvCN4GSjLrIexmeW+5OMxSRtPN1A1HRpfe/HOXHyahEtrAQJBAND4bPEvd8OC2lnkwWUycnCz0U7KL5pcNqFnkG9zGtOdmMXAl++qPVAnrgqp4dlFqzX4Wzeb7SFyFhgKl9H+hJU=");

    @Test
    public void testPush() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("bizDept", "cc");
        param.put("bizFunction", "员工信息报表");
        param.put("fileName", "测试.xml");
        param.put("fileStoreId", "1234554321");
        param.put("fileSize", "100");
        param.put("dataCount", "1000");
        param.put("clientIp", "127.0.0.1");
        param.put("operatorId", "1154322");
        param.put("remark", "测试");

        System.out.println(ccHttpUtil.signPost("http://dac.dev-ag.56qq.com/server/export-action/push", param));
    }

    @Test
    public void testAddConfig() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("appId", "gs-uoperation");
        param.put("publicKey",
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC61V+F7ffyyUoWyZz7AXeyoC2XKrzhc6QDnr7Opjox5IGBCTumiGOpIySKCJffnMp+0wCEiwEpdA8j1luKD8m2IeGKUWeONwiYtxmA7rzUiocYARRBfVJifntKvWeCD/Q0pvAsrLx3d3vxz2ONHrv38QXORb4Y5W/guTGuzDQntwIDAQAB");

        System.out.println(httpUtil.doPost("http://dac.56qq.com/web/add/public-key", param));
    }

    @Test
    public void testDevAddConfig() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("appId", "gs-uoperation");
        param.put("publicKey",
                "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCEmMpMlRzb0R/0335vvGefrWdYTEzlobfJOzPhDayGzw709ihqgnnJHW9iZC3ksYHXvvh8l/nvGYchKLO4xo0A03FA49eaFfpD7fgSoGH1NUTa7VExsorDEgGDx8n+FYSrQTCoMdvOME50UTMwN88sW5Hv6KSz43lgjsDTTLHgBQIDAQAB");

        System.out.println(devHttpUtil.doPost("http://dac.dev-ag.56qq.com/web/add/public-key", param));
    }

    @Test
    public void testSearch() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        // param.put("bizDept", "etc");
        param.put("bizSys", "finance-loan");
        // param.put("operatorId","1077579");wherr
        // param.put("operatorAccount","xxx");
        // param.put("operatorStartTime", "2018-11-27 0:0:0");
        // param.put("operatorEndTime", "2018-11-28 0:0:0");

        JSONObject result =
                JSONObject.parseObject(httpUtil.doPost("http://dac.56qq.com/web/export-action/search", param))
                        .getJSONObject("content");
        System.out.println("总条数：" + result.get("total"));
        JSONArray jsonArray = result.getJSONArray("records");
        jsonArray.stream().forEach(e -> System.out.println(e));
    }
}
