package com.lp.httpclient.server;

import com.google.common.collect.Maps;
import com.lp.httpclient.RSASignHttpUtil;
import org.junit.Test;

import java.util.Map;

/**
 * @author peng.liu
 * @date 2018/11/27 9:41
 */

public class HermesTest {

    private static final String HOST = "http://hermes-admin.56qq.com";

    private static RSASignHttpUtil httpUtil = new RSASignHttpUtil(true, null, null);

    @Test
    public void setSwitchFlag() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("key", "UAGENT_SERVER_CHECK_KEY");
        param.put("flag", "true");

        System.out.println(httpUtil.doPost(HOST + "/web/hermes/config/set-switch-flag", param));
    }
}
