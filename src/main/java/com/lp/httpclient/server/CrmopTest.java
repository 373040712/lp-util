package com.lp.httpclient.server;

import com.google.common.collect.Maps;
import com.lp.httpclient.RSASignHttpUtil;
import org.junit.Test;

import java.util.Map;

/**
 * @author peng.liu
 * @date 2018/12/21 10:12
 */

public class CrmopTest {

    private static RSASignHttpUtil httpUtil = new RSASignHttpUtil(true, "workerbee",
            "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOTTtGpJZOfphlYAYGTFU6VujWlez7FXmGDBnZ2HzkYPCU1ocFIEQE6mAlCJWjKDRpt0gxejaqgLdWUjbBab/c9VyosXNhcyjVUoAz40O3qGzbhUWvkuhfevBLqxCriWcV5Pgcjjhdn1pD37vnmy8YnCu++fwnGAgunybuklOKalAgMBAAECgYBgqDhmSRhRiegPvdr51gxuLdOX9c+fZpPcEwzj9PCh0Uopo3sGefs7/Y2uvUYXXjJeCgox2vp7f1OYZC0NI9lChbS9oT+vWqXf8d50rNPyjDv/IbB+gJe+/koRwDV1X9KkOnzD76qg48VEqHjiD05O3/bAIBwwWrWNa0SbdnotIQJBAPm9iHSxRVdZpnE17UtGvhX8zHoPXhvYe5ecu3jUSbvb0OH1xq/KtVwyci33p/jnSfx2NC8p5ptyjWxDYifyLx0CQQDqj/rzQZy52mRR45af6b8cjn4khQgCIapiZOFR1e3uXVOs/pKW4MjcJ6eOSYTsjlbozAY54AyxwmTlEXOiKZcpAkB8iWhHkLLcaUdXwyWO2TsnKcYpxAoQ5rZTF4RhL7OJK4eRdoda2EHyNPG9PHeV5nh9ndBOCJ4HvGsLptqGwv19AkEAsg82e271YIiO2zMfZql0sGLjEyoBmEyiIStfeFYj1mz2Vj6yXOKq16UqpvLcew7ansIyf8C7Ij3lhZan5xQXcQJBAPA2s5Nvlk6vhIDHxLSXLwALvbcqhtGXZzeMnXrauvyXO4KKz+8zpm1oPeHwUwdAh95h3ohjxMEiaHdJNhv1S9A=");

    private static RSASignHttpUtil devHttpUtil = new RSASignHttpUtil();

    @Test
    public void testSearchBaseUser() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("uid", "8319496");

        System.out.println(httpUtil.signPost("http://crmop.56qq.com/server/user/search", param));
    }

    @Test
    public void testGetDetail() throws Exception {
        Map<String, Object> param = Maps.newHashMap();
        param.put("userId", "8319496");

        System.out.println(httpUtil.doPost("http://crmop.56qq.com/mobile/consignor/detail", param));
    }
}
