package com.lp.httpclient.server;

import com.google.common.collect.Maps;
import com.lp.httpclient.RSASignHttpUtil;
import org.junit.Test;

import java.util.Map;

/**
 * @author peng.liu
 * @date 2018/12/26 10:56
 */

public class AIOSApiTest {

    private static RSASignHttpUtil devHttpUtil = new RSASignHttpUtil(false, "optimus",
            "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMq5w9dEmt6tORGrrGY8iI6mV0WvhH+oAIaKckY8bkD2w9f7IcnON58TLD+XeeO6q0+cX5R3xU9OAGaUqdMyIhV1Zg5pOGZpPdHyF9dvfHJW5MSi9h7FZpEWpTTuOF42N8UMFICxXXWPPV2wEXmoVYjvWLDcojmNWf57+ynomi1dAgMBAAECgYB18P1vEArJ4h2Bmk8Ca9VGqJ+9ew+q9SLZI1K4ag/625W04fYWYlMZyR3FIWtYghGIj4FL7yJrsQWqqHLk3BXXPPCY6R+wLGweqarjBz31ZZHkqim1af6Rma2I7K5Moc/MXVZiXLB60SwkTykLiD1SyagqeeiYiN3+U8Fu5nukQQJBAOsldjPfNySs+xlvDEv+S1Ji9gaWPQ05J/GuKWHWwQL8YuItu7LemutZ6ugv+tAs8i6qwt1UkoHB+nwf3Fso3U8CQQDctEE7837l1upAekyumtuv6Z7wgFPj36mnLE/NwZeBdpabiplAHV3n1+qzmEP3JUrBJJwEHfAxlFCVZcJhbxeTAkEA6Yy3riWR23eDnmtRR1k27okj2r71vGB8b+qPZ4GpCpIqlILf3nSCYidYzWx6LLG3iOpJyRLqwL4tT4NFlbUsuQJAL+avgeOHVzsd/ICOxBa4N6Xs6SXxDzQsDix/5Zhu2Zpzda7PNCsrhnZGalRR7AIcB296rSoSR7B5DPPZDGygIQJBAMKCQ1UF7d0JE1ufK0SNaGxyW5zVoxUUfTKW2aoGWWXIJy5Ik67Tjc0iMoo6+GFF6U4mJvXu5JxL4uyg8Df4GkI=");

    @Test
    public void apiServerTest() throws Exception {
        Map<String, Object> param = Maps.newHashMap();

        System.out.println(devHttpUtil.signPost("http://aios-api.dev-ag.56qq.com/server/ot/call-task/list", param));
    }
}
