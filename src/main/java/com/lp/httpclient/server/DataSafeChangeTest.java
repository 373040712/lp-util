package com.lp.httpclient.server;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.lp.httpclient.server.DataSafeChangeTest.PlainType.*;

/**
 * @author peng.liu
 * @date 2019/3/20 15:25
 */

public class DataSafeChangeTest {

    private static final String ENV = "dev";

    private static final String LOGIN_URL =
            "/common/web/login.do?username=peng.liu@56qq.com&password=liupengLSM920225&domainId=1&appClientId=212";

    private static final String CRYPTO_URL = "/crypto/init-mask";

    private static final Map<String, Object> PARAMS = Maps.newHashMap();

    @Before
    public void before() throws HttpProcessException {
        String result = HttpClientUtil.get(HttpConfig.custom().url(getLoginUrl()).encoding("utf-8"));
        JSONObject jsonObject = JSON.parseObject(result);
        PARAMS.put("sid", JSONPath.eval(jsonObject, "$.content.id"));
        PARAMS.put("st", JSONPath.eval(jsonObject, "$.content.token"));
    }

    @Test
    public void starkTest() throws Exception {
        List<InitData> list = Lists.newArrayList();
        list.add(new InitData("enterprise", "id").setFields(Lists.newArrayList(new MaskData("mobile", MOBILE),
                new MaskData("phone", TEL), new MaskData("email", EMAIL), new MaskData("address", ADDR),
                new MaskData("address_ext", ADDR))));
        list.add(new InitData("enterprise_contact", "id")
                .setFields(Lists.newArrayList(new MaskData("contact_phone", MOBILE))));
        list.add(new InitData("enterprise_visit_record", "id")
                .setFields(Lists.newArrayList(new MaskData("address", ADDR))));
        doInitCrypto("stark-admin", list);
    }

    @Test
    public void hermesTest() throws Exception {
        List<InitData> list = Lists.newArrayList();
        list.add(new InitData("agent_account", "id")
                .setFields(Lists.newArrayList(new MaskData("father_phone", MOBILE), new MaskData("son_phone", MOBILE),
                        new MaskData("bank_card_no", BANK_CARD), new MaskData("detail_address", ADDR))));
        list.add(
                new InitData("agent_apply_record", "id").setFields(Lists.newArrayList(new MaskData("mobile", MOBILE))));
        list.add(new InitData("business_opportunity_record", "id")
                .setFields(Lists.newArrayList(new MaskData("user_real_phone", MOBILE))));
        doInitCrypto("hermes-admin", list);
    }

    private void doInitCrypto(String appId, List<InitData> list) throws HttpProcessException {
        Header[] headers = HttpHeader.custom().contentType("application/x-www-form-urlencoded").build();
        HttpConfig httpConfig = HttpConfig.custom().headers(headers).url(getCryptoUrl(appId)).encoding("utf-8");
        for (InitData item : list) {
            Map<String, Object> paramMap = toParamMap(item);
            String result = HttpClientUtil.post(httpConfig.map(paramMap));
            System.out.println(result);
            httpConfig.map().clear();
        }
    }

    /**
     * json对象铺平
     *
     * @param initData
     * @return
     */
    public Map<String, Object> toParamMap(InitData initData) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("tableName", initData.getTableName());
        map.put("primaryKey", initData.getPrimaryKey());
        List<MaskData> fields = initData.getFields();
        if (CollectionUtils.isEmpty(fields)) {
            return map;
        }
        for (int i = 0; i < fields.size(); i++) {
            MaskData maskData = fields.get(i);
            if (StringUtils.isNotBlank(maskData.getName())) {
                map.put("fields[" + i + "].name", maskData.getName());
            }
            if (StringUtils.isNotBlank(maskData.getJsonClass())) {
                map.put("fields[" + i + "].jsonClass", maskData.getJsonClass());
            }
            if (Objects.nonNull(maskData.getPlainType())) {
                map.put("fields[" + i + "].plainType", maskData.getPlainType().toString());
            }
        }
        return map;
    }

    private String getCryptoUrl(String appId) {
        StringBuilder url = new StringBuilder();
        url.append(getUrl(appId)).append(CRYPTO_URL);
        url.append("?sid=").append(PARAMS.get("sid"));
        url.append("&st=").append(PARAMS.get("st"));
        return url.toString();
    }

    private String getLoginUrl() {
        return getUrl("sso") + LOGIN_URL;
    }

    private String getUrl(String appId) {
        StringBuilder host = new StringBuilder();
        host.append("http://").append(appId);
        if (ENV.equals("dev")) {
            host.append(".");
            host.append("dev-ag");
        } else if (ENV.equals("qa")) {
            host.append(".");
            host.append("qa-sh");
        } else if (!ENV.equals("prod")) {
            throw new RuntimeException("请配置环境");
        }
        host.append(".56qq.com");
        return host.toString();
    }

    @Data
    @Accessors(chain = true)
    class InitData {
        private String tableName;

        private List<MaskData> fields;

        private String primaryKey;

        public InitData(String tableName, String primaryKey) {
            this.tableName = tableName;
            this.primaryKey = primaryKey;
        }
    }

    @Data
    @Accessors(chain = true)
    class MaskData {

        private String name;

        private PlainType plainType;

        private String jsonClass;

        public MaskData(String name, PlainType plainType) {
            this.name = name;
            this.plainType = plainType;
        }

        public MaskData(String name, String jsonClass) {
            this.name = name;
            this.jsonClass = jsonClass;
        }
    }

    @Getter
    @AllArgsConstructor
    public enum PlainType {
        GENERAL("通用"), ADDR("个人详细地址"), BANK_CARD("银行卡号"), DRIVER_LICENSE_NO("驾驶证档案编号"), EMAIL("邮箱账号"), ENGINE_NO(
                "发动机号"), ETC_CARD("ETC卡号"), ID_CARD("身份证号码"), MOBILE("手机号码"), PLATE_NO(
                        "车牌号码"), TAXPAYER_NO("纳税人号码"), TEL("固定电话"), USERNAME("系统账号"), VIN("车辆识别号（VIN）");

        String desc;
    }
}
