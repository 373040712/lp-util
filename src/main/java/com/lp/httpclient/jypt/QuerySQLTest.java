package com.lp.httpclient.jypt;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.RegExUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author zhangpeng01@sinoiov.com
 * @Date 2019/12/10 19:24
 */
public class QuerySQLTest {

    private final static String COOKIE = "sso_session=sg2bo4bura7vbdramdaz2qi5nwg2ekca; csrftoken=tIt1LvCCt6yFENIZISF7gOZofbPGLiS7uhr3StV004B1J8MlBKMQrMk2nZqFbthO; SinDB=0fdnjuoxr8ju411960w6ewrmdkipfl9u";

    private final static String TOKEN = "tIt1LvCCt6yFENIZISF7gOZofbPGLiS7uhr3StV004B1J8MlBKMQrMk2nZqFbthO";


    final static RestTemplate REST_TEMPLATE = new RestTemplate();

    final static String URL = "http://172.90.4.54:8007/sql_query/";

    public static void main(String[] args) {

        String dbId = "103";
        String cookieVal = "sso_session=3jlo3l0t680wgy00zfkro45lew2904sx; csrftoken=ro1GSqJKqG5PFpbIACFXXaFpF1DRUpbS3GCcjPHUu1H1cuasFoe5gTN9bF3rCGka; SinDB=ykmcqvi3wmqw9ckiaxyoywfye1yc9t3j";
        String token = "ro1GSqJKqG5PFpbIACFXXaFpF1DRUpbS3GCcjPHUu1H1cuasFoe5gTN9bF3rCGka";
        String sql = "SELECT\n" +
                "\ti.`id`,\n" +
                "\ti.`open_invoice_no`,\n" +
                "\ti.`status`,\n" +
                "\tr.order_no,\n" +
                "\ti.create_time invoice_create_time,\n" +
                "        o.status,\n" +
                "\tr.create_time from tb_invoice i inner join invoice_order_relationship r on i.open_invoice_no = r.open_invoice_no\n" +
                "\tinner join order_info o on o.order_no = r.order_no \n" +
                "\twhere i.`status` in ('INVOICE_POSTED', 'INOVCE_COMPLETE') and o.`status` in ('WAIT_INVOICE')  order by i.create_time desc limit %s,10;";

        List<String> allVals = doSQLExecute(dbId, cookieVal, token, sql);
        write(allVals);
    }

    private static List<String> doSQLExecute(String dbId, String cookieVal, String token, String sql) {
        String headerFormat = null;

        String formatSQL = RegExUtils.replaceAll(sql, "[\t|\n]+", " ");

        int i = 0;
        HttpHeaders header = new HttpHeaders();
        header.add("X-CSRFToken", token);
        header.add("Content-Type", "application/json");
        header.add("Cookie", cookieVal);
        List<String> allVals = new ArrayList<>();
        while (true) {
            Map<String, Object> queryData = new HashMap<>();
            queryData.put("db_id", dbId);
            queryData.put("current_page", 1);
            queryData.put("query_sql", String.format(formatSQL, i * 10));
            i++;
            System.out.println("当前第[" + i + "]页");
            HttpEntity<String> httpEntity = new HttpEntity<String>(JSON.toJSONString(queryData), header);
            String querySQLRespJson = null;
            try {
                querySQLRespJson = REST_TEMPLATE.postForObject(URL, httpEntity, String.class);
            } catch (RestClientException e) {
                e.printStackTrace();
            }
            if (querySQLRespJson == null) {
                break;
            }
            QuerySQLResp querySQLResp = null;
            try {
                querySQLResp = JSON.parseObject(querySQLRespJson, QuerySQLResp.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (querySQLResp == null || !querySQLResp.hasRows()) {
                break;
            }

            if (headerFormat == null) {
                headerFormat = convertFieldHeader(querySQLResp.getField());
            }
            List<String> jsonList = querySQLResp.convertToJSON(headerFormat);
            allVals.addAll(jsonList);
        }
        return allVals;
    }

    static void write(List<String> allVals) {
        File dir = new File(System.getProperty("user.dir"), File.separator + "json");
        if (!dir.exists()) {
            dir.mkdirs();
        }


        File file = new File(dir, File.separator + String.valueOf(System.currentTimeMillis()) + ".json");
        String val = String.join(",", allVals);
        InputStream is;
        BufferedOutputStream bufferedOutputStream;
        try {
            is = new ByteArrayInputStream(val.getBytes());
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            FileCopyUtils.copy(is, bufferedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String convertFieldHeader(String[] fields) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        int i = 0;
        for (String field : fields) {
            i++;
            stringBuilder.append("\"").append(field).append("\"").append(":").append("\"%s").append("\"");
            if (i != fields.length) {
                stringBuilder.append(",");
            }
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }


    static class QuerySQLResp {

        private String[] field;
        private String[][] result;
        private Boolean status;
        private Integer effectRow;

        public String[] getField() {
            return field;
        }

        public void setField(String[] field) {
            this.field = field;
        }

        public String[][] getResult() {
            return result;
        }

        public void setResult(String[][] result) {
            this.result = result;
        }

        public Boolean getStatus() {
            return status;
        }

        public void setStatus(Boolean status) {
            this.status = status;
        }

        public Integer getEffectRow() {
            return effectRow;
        }

        public void setEffectRow(Integer effectRow) {
            this.effectRow = effectRow;
        }

        public Boolean hasRows() {
            return this.result != null && this.result.length != 0;
        }

        public List<String> convertToJSON(String header) {
            List<String> jsons = new ArrayList<>();
            for (String[] data : this.result) {
                jsons.add(String.format(header, data));
            }
            return jsons;
        }
    }

}
