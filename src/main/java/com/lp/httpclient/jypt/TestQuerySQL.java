package com.lp.httpclient.jypt;

import com.alibaba.fastjson.JSON;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.apache.commons.lang3.RegExUtils;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2019/12/26 17:57
 */
public class TestQuerySQL {

    private final static int DB_JYPT_TRANSPORT = 108;

    private final static String URL = "http://172.90.4.54:8007/sql_query/";

    private final static String COOKIE = "sso_session=sg2bo4bura7vbdramdaz2qi5nwg2ekca; csrftoken=tIt1LvCCt6yFENIZISF7gOZofbPGLiS7uhr3StV004B1J8MlBKMQrMk2nZqFbthO; SinDB=0fdnjuoxr8ju411960w6ewrmdkipfl9u";

    private final static String TOKEN = "tIt1LvCCt6yFENIZISF7gOZofbPGLiS7uhr3StV004B1J8MlBKMQrMk2nZqFbthO";

    private final static String SQL_LIMIT = " limit %s,10;";

    private final static int MAX_PN = 1;

    private final static String SQL = "SELECT `id`, `import_no`, `company_name`, `import_status`, `import_error_msg`, `first_instance_status`, `first_instance_error_msg`, `second_instance_status`, `second_instance_error_msg`, `order_no`, `waybill_no` FROM import_order WHERE import_status='SUCCESS' AND first_instance_status ='ERROR' AND second_instance_status='SUCCESS' ORDER BY id";

    public static void main(String[] args) throws HttpProcessException {
        TestQuerySQL querySQL = new TestQuerySQL();
        querySQL.query(DB_JYPT_TRANSPORT, SQL);
    }

    public void query(int dbId, String sql) throws HttpProcessException {
        String formatSQL = RegExUtils.replaceAll(sql, "[\t|\n]+", " ") + SQL_LIMIT;
        HttpHeader header = HttpHeader.custom().contentType(MediaType.APPLICATION_JSON_VALUE)
                .cookie(COOKIE).other("X-CSRFToken", TOKEN);

        int pn = 0;
        while (true) {
            Map<String, Object> queryData = new HashMap<>(3);
            queryData.put("db_id", dbId);
            queryData.put("current_page", 1);
            queryData.put("query_sql", String.format(formatSQL, pn * 10));
            pn++;
            System.out.println(JSON.toJSONString(queryData));
            System.out.println("当前第[" + pn + "]页");
            HttpConfig config = HttpConfig.custom().url(URL).headers(header.build()).encoding("utf-8").json(JSON.toJSONString(queryData));
            String querySQLRespJson = HttpClientUtil.post(config);
            System.out.println(querySQLRespJson);
            if (pn > MAX_PN) {
                break;
            }
        }
    }
}
