package com.lp.httpclient.jypt;

import com.alibaba.fastjson.JSON;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.base.Splitter;
import com.lp.geohash.LatLngUtil;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2019/12/26 17:52
 */
public class TestTransportUtil {

    public static void main(String[] args) throws FileNotFoundException {
        TestTransportUtil test = new TestTransportUtil();
        test.readTextFile("tt.properties");
    }

    public void readTextFile(String fileName) throws FileNotFoundException {
        URL url = ResourceUtils.getURL("classpath:" + fileName);
        if (url == null) {
            return;
        }

        Map<String, Object> queryData = new HashMap<>(3);
        queryData.put("bizType", "WAYBILL");
        queryData.put("toRealStatus", 1);
        queryData.put("source", "APPEAL");
        queryData.put("pwd", "f293c146-8b88-4be1-981f-a7301ad2ee6f");

        try (Stream<String> stream = Files.lines(Paths.get(url.toURI()))) {
            stream.forEach(i -> {
                List<String> list = Splitter.on("\t").omitEmptyStrings().trimResults().splitToList(i);
                String bizNo = list.get(1);
                double distance = LatLngUtil.getDistance(Double.parseDouble(list.get(2)), Double.parseDouble(list.get(3)),
                        Double.parseDouble(list.get(4)), Double.parseDouble(list.get(5)));
                int tt = Integer.parseInt(list.get(6));
                double result = (distance / tt);
                if (result < 24) {
                    System.out.println("异常" + list + "_" + distance + "_" + result);

                    queryData.put("bizNo", bizNo);
                    HttpHeader header = HttpHeader.custom()
                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                            .other("_ticketObject", "24786AD36CAE8567C58FCCB428A526E0");

                    HttpConfig config = HttpConfig.custom()
                            .url("https://console.56hygj.com/tap/console/authenticity/result/modify")
                            .headers(header.build())
                            .encoding("utf-8")
                            .json(JSON.toJSONString(queryData));
                    try {
                        HttpClientUtil.post(config);
                    } catch (HttpProcessException e) {
                        e.printStackTrace();
                    }
                    System.out.println(bizNo);
                } else {
                    System.out.println("正常" + list + "_" + distance + "_" + result);
                }
            });
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
