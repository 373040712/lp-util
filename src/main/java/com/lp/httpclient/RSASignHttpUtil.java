package com.lp.httpclient;

import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;

import java.util.*;

import static com.google.common.base.Charsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.EMPTY;

/**
 * hcb验签请求
 *
 * @author peng.liu
 * @date 2018/11/26 17:25
 */
@Slf4j
public class RSASignHttpUtil {

    private static final String KEY_ALGORITHM = "RSA";

    private String ssoUrl =
            "http://sso-gy.qa-ag.56qq.com/common/web/login.do?username=peng.liu@56qq.com&password=liupengLSM920225&domainId=1&appClientId=212";
    private String appId = "crmop";
    private String privateKey =
            "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAODTYVkoOgGyJnMfhTeKBy6JiLvSw0EL8gPnPuvHDzmPTKazbceAuJ8S36C0PRXEiSA3hGzpfwTik80Xi5Azg5niR2NN1OgdKXKqNz/XDVl4AZa4Az+YUb2AR1m7GQc6nEl76QHkh1SxlBBnJ3Iwfo7+2Ggu68ZObNvL6iEHrb8nAgMBAAECgYA5ij/+9omqB3NJwYZxyXS/F1oUYAOy8SC2mQeEhoTCAYfnnkJ5bdtScx0i3x5HJO7HfQ36ZeQpVufm9KC2fkiuregYhEGFCT4Z2fXcc+ejWeActyRkYqNKEOucWquQOJf5OahDYEsdfWzSEsL56em7QXnW9YKtKgvxAsSDkuUn2QJBAPMVJ8w3Flw63WpqI2DoyMUMQQJCxWz8lcDWLTzXNHy2PVPHTC3N0ejpLc+XB3+mc+0uNZ15WgtlPUxJkkXJVR0CQQDsxdyBoZWy500h5A9S7jn0JzgkczWzX5WLP7QVAd/dgSoP12VFcB3Alr8BiCJVFzbQM7UHGMpcMjtD8TsNCMYTAkEA4LUyyAY4hmPoIKIjOEeYqQct5VAiPyCzssKKqQ5rt8mjoZm/oyv7mqpLrfX4eZk+2wd3iq4OfjBFXQs5y4dVRQJBAMLGIZch7Zu2WuU+TrzR7CrE+JJDlDFyt/pM9T8yw43b61YMwupUSKVdXNNNwbBt4FF1t72WzyD40XgawZD/XrECQF42q1WV2+iiMrj3PBtgKQX5gCxcXG9PlZtmtwLPqxarUKVrJ16zg+UxEEv1GlO1kiEqgDtAknLDEmmcO6ViW9I=";

    public RSASignHttpUtil() {}

    public RSASignHttpUtil(boolean onlineFlag, String code, String value) {
        if (onlineFlag) {
            ssoUrl = "http://sso.56qq.com/common/web/login.do?username=peng.liu@56qq.com&password=liupengLSM920225&domainId=1&appClientId=273";
        }
        if (StringUtils.isNotBlank(code)) {
            appId = code;
        }
        if (StringUtils.isNotBlank(value)) {
            privateKey = value;
        }

    }

    public String doPost(String url, Map<String, Object> param) throws Exception {
        return doPost(url, param, true);
    }

    public String doPost(String url, Map<String, Object> param, boolean needLogin) throws Exception {
        if (needLogin) {
            initLogin(param);
        }
        HttpConfig config = HttpConfig.custom().url(url).map(param).encoding(UTF_8.name());
        return HttpClientUtil.post(config);
    }

    public String signPost(String url, Map<String, Object> param) throws Exception {
        HttpConfig config =
                HttpConfig.custom().url(url).headers(buildSignHeaders(param)).map(param).encoding(UTF_8.name());
        return HttpClientUtil.post(config);
    }

    private Header[] buildSignHeaders(Map<String, Object> param) throws Exception {
        String sign = RSAHelper.sign(joinParams(param), privateKey, UTF_8.name());
        return HttpHeader.custom().other(HeaderName.SIGN.getKey(), sign)
                .other(HeaderName.SIGN_TYPE.getKey(), KEY_ALGORITHM).other(HeaderName.APP_ID.getKey(), appId)
                .other(HeaderName.RANDOM_PARAM.getKey(), String.valueOf(System.currentTimeMillis())).build();
    }

    private String joinParams(Map<String, Object> paramMap) {
        if (MapUtils.isEmpty(paramMap)) {
            return EMPTY;
        }
        List<String> keys = new ArrayList<>(paramMap.keySet());
        // 参数排序
        Collections.sort(keys);
        StringBuilder sb = new StringBuilder();
        for (String key : keys) {
            Object value = paramMap.get(key);
            if (Objects.isNull(value)) {
                continue;
            }
            sb.append(key).append("=").append(value.toString()).append("&");
        }
        String result = sb.toString();
        return result.length() > 0 ? result.substring(0, result.length() - 1) : EMPTY;
    }

    private void initLogin(Map<String, Object> param) throws Exception {
        String result = HttpClientUtil.get(HttpConfig.custom().url(ssoUrl).encoding(UTF_8.name()));
        JSONObject jsonObject = JSONObject.parseObject(result).getJSONObject("content");
        param.put("sid", jsonObject.get("id"));
        param.put("st", jsonObject.get("token"));
    }

    @Getter
    @AllArgsConstructor
    @SuppressWarnings("all")
    public enum HeaderName {

        SIGN("x-sign"), SIGN_TYPE("x-sign-type"), APP_ID("x-ap-id"), RANDOM_PARAM("_rdp");

        private String key;
    }
}
