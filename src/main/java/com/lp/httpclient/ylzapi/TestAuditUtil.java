package com.lp.httpclient.ylzapi;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.SSLs;
import com.google.common.collect.Lists;
import com.lp.excel.TExcelUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.client.HttpClient;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2021/03/31
 **/
@Slf4j
public class TestAuditUtil {

    public static void main(String[] args) throws Exception {
        HCB hcb = HCB.custom().ssl()
                .pool(100, 10)
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2)
                .retry(5);
        Header[] headers = HttpHeader.custom().contentType("application/json")
                .other("Token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiJjZjBkYjJhOWVlY2U0NjdjOGU3MmJmZDQ1M2YyNTdiOCIsIkxPR0lOX1RJTUUiOiIxNjM3MjM0ODcyODU0IiwiVE9LRU5fVFlQRSI6InBjIn0.DzV2qeW50nxAGDHqh5eiK4SL3s49UGVaw4U1h_vUivc")
                .build();
        HttpClient client = hcb.build();

        Date limitDate = DateUtil.parseDateTime("2021-10-01 00:00:00");
        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "audit1.xlsx");
        Lists.partition(aList, 1000).forEach(it -> {
            List<String> waybills = it.stream()
//                    .filter(item -> DateUtil.parseDateTime(item.get(1)).after(limitDate))
                    .map(item -> item.get(0))
                    .collect(Collectors.toList());
            if (CollUtil.isNotEmpty(waybills)) {
                HttpConfig config = HttpConfig.custom().headers(headers)
                        .url("http://otms-web-plus.yunlizhi.io/tms-plus/waybill/manual/audit")
                        .encoding("utf-8")
                        .json(JSON.toJSONString(new JSONObject()
                                .fluentPut("waybillCodes", waybills)
                                .fluentPut("auditStatus", "AUDITED")
                                .fluentPut("auditSource", "TMS")
                                .fluentPut("onlyStatus", true))
                        )
                        .client(client);
                String result = null;
                try {
                    result = HttpClientUtil.post(config);
                    JSONObject jsonObject = JSON.parseObject(result);
                    JSONObject data = jsonObject.getJSONObject("data");
                    System.out.println("处理成功-------------");
                } catch (Exception e) {
                    System.out.println("处理失败-------------");
                }
            }
        });
    }
}
