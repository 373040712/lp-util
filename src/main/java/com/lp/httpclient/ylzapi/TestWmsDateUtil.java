package com.lp.httpclient.ylzapi;

import com.alibaba.fastjson.JSON;
import com.lp.excel.TExcelUtils;
import lombok.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2022/03/04
 **/
public class TestWmsDateUtil {

    public static void main(String[] args) throws Exception {
        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_data.xlsx");
        Set<DataItem> jsonSet = aList.stream().filter(it -> it.size()>0).map(it -> DataItem.builder()
                .goods_code(it.get(0))
                .shipper_name(it.get(1))
                .inc_day(Integer.parseInt(it.get(2)))
                .build()).collect(Collectors.toSet());
        System.out.println(JSON.toJSON(jsonSet));
    }


    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode
    public static class DataItem {
        private String goods_code;
        private String shipper_name;
        private Integer inc_day;
    }
}
