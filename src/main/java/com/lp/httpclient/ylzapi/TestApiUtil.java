package com.lp.httpclient.ylzapi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.SSLs;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.lp.excel.TExcelUtils;
import com.lp.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.apache.http.Header;
import org.apache.http.client.HttpClient;

import java.util.*;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;
import static java.util.stream.Collectors.toList;

/**
 * @author liupeng@newhope.cn
 * @date 2021/03/31
 **/
@Slf4j
public class TestApiUtil {

    private static final Map<String, String> T_API = new HashMap<String, String>() {
        {
            put("1007", "tcs_web");
            put("1009", "tcs_plus");
            put("1019", "tms_web_v1");
            put("1022", "tms_v1_plus");
            put("1010", "tms_web");
            put("1003", "tms_plus");
            put("1021", "app_workspace");
        }
    };

    public static void main(String[] args) throws Exception {
        HCB hcb = HCB.custom().ssl()
                .pool(100, 10)
                .sslpv(SSLs.SSLProtocolVersion.TLSv1_2)
                .retry(5);

        Header[] headers = HttpHeader.custom().contentType("application/json")
                .other("Token", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiJjZjBkYjJhOWVlY2U0NjdjOGU3MmJmZDQ1M2YyNTdiOCIsIkxPR0lOX1RJTUUiOiIxNjE5NDIwMjY4MjEwIiwiVE9LRU5fVFlQRSI6InBjIn0.KtOcMkCbmqetdEd4ceUdANDWwk6oi7H-kH4b9DBETG0")
                .build();

        HttpClient client = hcb.build();

        List<ApiVo> list = T_API.entrySet().stream().map(it -> {
            ApiVo apiVo = ApiVo.builder().appId(it.getKey()).appName(it.getValue()).items(Lists.newArrayList()).build();
            try {
                HttpConfig config = HttpConfig.custom().headers(headers)
                        .url("http://otms-web-plus.yunlizhi.io/bfs_app_stat/admin/api-stats/content")
                        .encoding("utf-8")
                        .json("{\"nodeName\":\"" + it.getKey() + "\"}")
                        .client(client);
                String result = HttpClientUtil.post(config);
                JSONObject jsonObject = JSON.parseObject(result);
                JSONObject data = jsonObject.getJSONObject("data");
                if (data == null) {
                    return apiVo;
                }
                List<ApiItemVo> items = new ArrayList<>();
                JSONArray unused = data.getJSONArray("unused");
                for (int i = 0; i < unused.size(); i++) {
                    items.add(ApiItemVo.builder().api(unused.getString(i)).build());
                }
                JSONObject used = data.getJSONObject("used");
                for (Map.Entry<String, Object> entry : used.entrySet()) {
                    items.add(ApiItemVo.builder().api(entry.getKey())
                            .count(Integer.parseInt(((List<String>) entry.getValue()).get(0).split("=")[1].trim()))
                            .dubbo(entry.getKey().startsWith("com"))
                            .used(true).build());
                }
                apiVo.setItems(items);
                return apiVo;
            } catch (HttpProcessException e) {
                log.error(e.getMessage());
                return apiVo;
            }
        }).collect(toList());

        String tempPath = ROOT_PATH + "api_template.xlsx";
        for (int i = 0; i < list.size(); i++) {
            ApiVo apiVo = list.get(i);
            List<ApiItemVo> items = apiVo.getItems().stream().sorted(Comparator.comparing(ApiItemVo::isUsed)
                    .thenComparing(ApiItemVo::isDubbo)
                    .thenComparing(ApiItemVo::getCount).reversed())
                    .collect(toList());
            Map<String, String> data = new HashMap<>();
            data.put("title", apiVo.getAppId() + "：" + apiVo.getAppName());
            data.put("info", String.format("api接口总数：%d，api接口使用数：%d，dubbo接口使用数：%d", items.size(),
                    items.stream().filter(ApiItemVo::isUsed).count(), items.stream().filter(it -> it.isDubbo() && it.isUsed()).count()));
            // 基于模板导出Excel
            TExcelUtils.getInstance().exportObjects2Excel(tempPath, 0, items, data, ApiItemVo.class, false,
                    ROOT_PATH + "API_" + apiVo.getAppName() + ".xlsx");
        }
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ApiVo {
        private String appId;
        private String appName;
        private List<ApiItemVo> items;
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ApiItemVo {
        @Excel(title = "API", order = 1)
        private String api;
        @Excel(title = "是否使用", order = 2)
        private boolean used;
        @Excel(title = "是否dubbo", order = 3)
        private boolean dubbo;
        @Excel(title = "累计调用次数", order = 4)
        private int count;
    }
}
