package com.lp.httpclient.ylzapi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import com.lp.excel.TExcelUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2022/02/24
 **/
public class TestWmsJsonUtil {

    private static final String CUR_JSON = "[{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"a8c04de7d6de8184ce8d35e35cf35fb7\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin7@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"10650061cafc4de5a00f30215d2a028e\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"4150541d3a77596fc9c4817dc119bcc9\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin20@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"5374637ec025030ada95f845057a36f8\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin4@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"934e9af651a84678b3429910a11ad911\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"ae6353b66d0b120e7a1341a1efd45c50\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin13@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"5a3bffa00032fac353c8dd83a04e4bd0\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"f25771c8317b792439ecc6601a72c2ce\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"shenlw1@penaviconb.com\"],\"shipperId\":\"7bc8f071fd756a9d40edb8a50704e9d0\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin11@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"a10a2e9f78b14843bbb68ba98e8a2caf\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"lvweiqiang@sh-qianlong.com\"],\"shipperId\":\"f279935228ca972629718fc0c9864a91\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"4de240c5874eb037b6880272067da5eb\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin6@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"f902888d58f05f03a9458c732d1069e1\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"2498959026@qq.com\"],\"shipperId\":\"867148034ff345e68f884fa3d793b6b4\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin15@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"03e2df17822052141674b2375673921d\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"duancq@newhope.cn\",\"luoqin9@newhope.cn\"],\"shipperId\":\"e25043529f024aa8b76973090c6789bc\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin10@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"82bf7379f09645cd8f2bf427ee48166f\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"rockylilei@163.com\"],\"shipperId\":\"24766a92f4064f329ee17be2fe6f24c0\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"2325550744@qq.com\"],\"shipperId\":\"9c5b5b0c8e0e4bbbf5882c973f3e29a1\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"zhanghejun@newhope.cn\"],\"shipperId\":\"55ff9553962f1f6d722340e228332994\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin17@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"c0972db6fca4cd459bd0514ac213625e\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"b69723c93cdee6c8be290cefac42ba94\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"5b469eded2baa1fd2032b99a13eb75f5\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"yujiao_ylz@newhope.cn\"],\"shipperId\":\"3bcfe254a455b5c18491cbf1f7ecdfe3\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"751316468@qq.com\"],\"shipperId\":\"1249bd12019c3539a708766238c04bb5\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"afeac5a7c01c9ea04ef37435b4610fd8\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"27c86b88561b96d0ad8aa824cfa65c10\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"shijiajck@126.com\"],\"shipperId\":\"0ea859015d36eec6066b050a298d8a26\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin19@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"12756b18f13f26b6caba85f719c1c646\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"227f69a6d73071e8a5bc573e514eea7f\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin5@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"36f5a084b4bb4490bdecd67f30e4a41d\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"9289196@qq.com\"],\"shipperId\":\"497c91325374e13e38b4cf8f374dd0dd\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"6a5c5fffe72afee136a1f163eb2ff94e\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin16@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"074bd0c9fa666058eccaaa6961725b71\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"sh_logistics@pmidistribution.com.cn\"],\"shipperId\":\"1232fbde53a7b64e787d4809bbe7ba84\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin14@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"6557482dd1f14abab1bbfdc6d34f627b\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"6f0c3ec940a896e4dbb1fa78c4d5936b\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"465f1bffdb51d73065ec97698e33fa30\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin12@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"7e38e84583a740a6b527e984ed3e3fb2\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"zjy_nj@163.com\"],\"shipperId\":\"1f9d9b37deaa09f1a2943c8a39a3b5ef\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin8@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"6c4a0f48bf10441a9d85f84fbe5e0fd9\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"d5bf5df61c87f9bb3cbb9cf290cb8d14\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin18@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"7dae78c09ed317c69a56613c18ef4ca8\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"zhangyanyan@newhope.cn\"],\"shipperId\":\"e15d7705c3efdbdc4673aa4a8cb81bfc\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"8f92a7f8ed85ffa857f618920d964f07\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin2@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"242603ca4f60bf31d76a20c927897b0b\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"eltonz@163.com\"],\"shipperId\":\"8ecade914c5e45deebef22f83f9ffdd0\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"luoqin3@newhope.cn\",\"duancq@newhope.cn\"],\"shipperId\":\"7f3bb2b25c3d443783c7c9f469973ded\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"trevor@daotone.cn\"],\"shipperId\":\"285ce195b2ff72b21fd5c28663bdfeaa\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"guhr@newhope.cn\"],\"shipperId\":\"00afd273332da49548c5f7168319012f\",\"warnStatus\":[\"SECOND\",\"STOP_WARNING\"]},{\"emails\":[\"wind.wang@sf-dsc.com\",\"yong.cai@dkl-cn.com\"],\"shipperId\":\"6c72e6560f774dd0acccd677d27af83c\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"gary.qian@innocentdrinks.com\",\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"1a729a4edcdab4d488a3e244e6fc65d8\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"yuyoushangmao@126.com\",\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"b63177f4841f452210d83530cd5d5a5f\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"dingj@xianglanch.com\",\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"6d578b226929fccfc8a65b8897e2e68d\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\",\"grace@croissanterie.cn\"],\"shipperId\":\"6b34f929b39641eb22df0697a86a41d4\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"hly@honsea.com\",\"jiaxin.chen@dkl-cn.com\"],\"shipperId\":\"6b6b9a00c6df8b3fe4147b805f858f7e\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"WolfGuo@catchy.net.cn\",\"jiaxin.chen@dkl-cn.com\",\"jerryghw@catchy.net.cn\"],\"shipperId\":\"e3001844227d271bb41d695a6b88b3c3\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"jiaxin.chen@dkl-cn.com\",\"orders@jjw-cn.com\"],\"shipperId\":\"ace61356a39e4f939a227b500c21da6f\",\"warnStatus\":[\"STOP_WARNING\"]},{\"emails\":[\"sunjiaqi@andros.com.cn\",\"zhicheng.mai@dkl-cn.com\"],\"shipperId\":\"c881a6a1c65542caa25f497b46ef57d2\",\"warnStatus\":[\"SECOND\",\"FIRST\",\"STOP_WARNING\"]},{\"emails\":[\"orders@jjw-cn.com\",\"zhicheng.mai@dkl-cn.com\"],\"shipperId\":\"5e0c51ce6e3949d098c822f544f52bc9\",\"warnStatus\":[\"SECOND\",\"FIRST\",\"STOP_WARNING\"]}]";

    private static final String CONFIG_STR = "HZ1619524434242,HZ1566894479302,HZ1561113922408,HZ1561025125596,JUNLAO,HZ1561027273522,HZ1560521072546,HZ1561521820592,HZ1561107250868,HZ1568891137050,HZ1577865967358,HZ1598844999410,HZ1560686221824,HZ1608260760694,LSCY001,HZ1620464289438,YJCY001,HZ1628581035058,YLYX,HBS001";

    private static final String ALL_EMAIL = "shixiaojian@newhope.cn";
    private static final String BL_EMAIL = "jijian@newhope.cn";

    public static void main(String[] args) throws Exception {
        List<DataItem> oldDataList = JSONObject.parseArray(CUR_JSON, DataItem.class);
        Map<String, DataItem> oldDataMap = oldDataList.stream().collect(Collectors.toMap(DataItem::getShipperId, Function.identity(), (v1, v2) -> v2));
        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_data.xlsx");
        Map<String, String> aMap = aList.stream().filter(it -> CollectionUtils.isNotEmpty(it)).collect(Collectors.toMap(it -> it.get(0), it -> it.get(1)));
        List<List<String>> bList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_base.xlsx");
        Map<String, String> bMap = bList.stream().filter(it -> CollectionUtils.isNotEmpty(it)).collect(Collectors.toMap(it -> it.get(0), it -> it.get(1)));
        Set<String> cSet = Arrays.stream(CONFIG_STR.split(",")).collect(Collectors.toSet());

        List<DataItem> result = new ArrayList<>();
        aMap.entrySet().forEach(it -> {
            DataItem item = oldDataMap.remove(it.getKey());
            if (Objects.nonNull(item)) {
                System.out.println(it.getKey() + ", " + it.getValue());
            }
            if (item == null) {
                item = DataItem.builder()
                        .shipperId(it.getKey())
                        .build();
            }
            item.setWarnStatus(Sets.newHashSet("SECOND", "STOP_WARNING"));
            item.setEmails(Arrays.stream(bMap.get(it.getValue()).split(",")).collect(Collectors.toSet()));

            item.getEmails().add(ALL_EMAIL);
            if(cSet.contains(it.getValue())){
                item.getEmails().add(BL_EMAIL);
            }
            result.add(item);
        });
        result.addAll(oldDataMap.values());
        System.out.println(JSON.toJSON(result));
    }

    @Builder
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DataItem {
        private String shipperId;
        private Set<String> warnStatus;
        private Set<String> emails;
    }
}
