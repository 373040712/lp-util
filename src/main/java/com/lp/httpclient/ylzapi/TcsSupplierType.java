package com.lp.httpclient.ylzapi;

import com.baomidou.mybatisplus.core.enums.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 承运商类型
 *
 * @author JinGuiBo
 * @date 2021-03-22 15:04
 **/
@AllArgsConstructor
@Getter
public enum TcsSupplierType implements IEnum<Integer> {
    COMPANY(0, "公司"),
    PERSONAL(1, "个体"),
    DEPEND(2, "挂靠"),
    MOTORCADE(3, "车队"),
    ;

    private Integer code;
    private String desc;

    @Override
    public Integer getValue() {
        return code;
    }

    public static TcsSupplierType parse(String code) {
        for (TcsSupplierType type : TcsSupplierType.values()) {
            if (type.desc.equals(code)) {
                return type;
            }
        }
        return null;
    }
}
