package com.lp.httpclient.ylzapi;

import com.lp.excel.TExcelUtils;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Objects;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;


public class CarPicDownloadMetaDataFromExcel {


    public static void main(String[] args) throws Exception {
        System.out.println("=======>外请车处理=======>");
        handler(ROOT_PATH + "/excel/wqcar.xlsx","/Users/jianghao/wq/");

        System.out.println("=======>自有车处理=======>");
        handler(ROOT_PATH + "/excel/zycar.xlsx","/Users/jianghao/zy/");
    }

    private static void handler(String dataPath, String outDir) throws Exception {
        List<List<String>> lines = TExcelUtils.getInstance().readExcel2List(dataPath);
        lines.stream().peek(line-> {
            try {
                handleLine(line,outDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).forEach(l-> System.out.println(l+"  已经处理"));
    }

    private static void handleLine(List<String> line,String outDir) throws IOException {
        if(Objects.isNull(line) || line.isEmpty()) {
            return ;
        }
        doWork(WorkContent.builder().plateNumber(getLineData(line,0))
                .url01(getLineData(line,1))
                .url02(getLineData(line,2)).build(),outDir,2,0);
    }

    private static String getLineData(List<String> lines,int index) {
        if(index<=lines.size()-1)
        {
            return lines.get(index);
        }
        return null;
    }

    private static void doWork(WorkContent content,String outDir,int max,int index) throws IOException {
        if(index>=max) {
            return;
        }
        File file = new File(String.format(outDir+"%s.png",getFileName(content,index)));
        String readPath = index==0?content.getUrl01():content.getUrl02();
        System.out.println("文件存储路径: "+file.getPath() + "  读取: "+readPath );
        if(StringUtils.isBlank(readPath)) {
            return;
        }

        readPath +="?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVU0VSX0NPREUiOiIzYzZmZDFlNWUwNjI0MjVlODczZWY1MDMwOTRiNDBkOSIsIkxPR0lOX1RJTUUiOiIxNjIwODE3MjM4Nzg1IiwiVE9LRU5fVFlQRSI6InBjIn0.y4DQYjmuadT_Gdde92W3bHs_XJWSaD-KJmOUZe0tqbk";

        byte[] bytes = IOUtils.toByteArray(new URL(readPath));
        if(Objects.isNull(bytes) || bytes.length<=0){
            return;
        }
        FileUtils.writeByteArrayToFile(file,bytes);
        doWork(content,outDir,max,++index);
    }

    private static String getFileName(WorkContent content, int index) {
        return  String.format("%s%s",content.getPlateNumber(),index ==0?"正":"副");
    }

    @Data
    @Builder
    public static class WorkContent {
        private String plateNumber;
        private String url01;
        private String url02;
    }

}
