package com.lp.lock;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2019/12/25 15:04
 */
public class LockTest {

    private Map<String, Integer> result;

    public void init() {
        synchronized (this) {
            Map<String, Integer> temp = new HashMap<>();
            int i=100000;
            while (i-->0){

            }
            temp.put("a", 1);
            result = temp;
        }
    }

    public void refresh() {
        init();
    }

    public Map<String, Integer> getResult() {
        if (result == null) {
            init();
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        LockTest lockTest = new LockTest();
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("1");
                    System.out.println(lockTest.getResult());
                }
            }).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("2");
                    lockTest.refresh();
                }
            }).start();
        }
        System.in.read();
    }
}
