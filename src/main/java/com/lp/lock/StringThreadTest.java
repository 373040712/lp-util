package com.lp.lock;

import org.junit.Test;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/28 11:29
 */
public class StringThreadTest {

    private static final int THREAD_COUNT = 5;

    @Test
    public void testStringThread() {
        Thread[] threads = new Thread[THREAD_COUNT];
        for (int i = 0; i < THREAD_COUNT; i++) {
            threads[i] = new Thread(new StringThread("192.168.1.1"));
        }

        for (int i = 0; i < THREAD_COUNT; i++) {
            threads[i].start();
        }

        for (;;);
    }

}