package com.lp.interview;

/**
 * @author peng.liu
 * @date 2019/6/4 11:22
 */

public class NumberCache {

    public static void main(String[] args) {
        //Integer、Short、Byte、Character、Long这几个类的valueOf方法的实现是类似的。
        //在[-128,127]从缓存中获得，不在new新对象
        Integer i1 = 100;
        Integer i2 = 100;
        Integer i3 = 200;
        Integer i4 = 200;
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);

        //Double、Float的valueOf方法的实现是类似的。
        //new新对象
        Double i11 = 100.0;
        Double i22 = 100.0;
        Double i33 = 200.0;
        Double i44 = 200.0;
        System.out.println(i11 == i22);
        System.out.println(i33 == i44);

        //获得final对象
        Boolean ii1 = false;
        Boolean ii2 = false;
        Boolean ii3 = true;
        Boolean ii4 = true;
        System.out.println(ii1==ii2);
        System.out.println(ii3==ii4);
    }
}
