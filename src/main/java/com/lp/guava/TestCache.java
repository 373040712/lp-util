package com.lp.guava;

import com.google.common.base.Ticker;
import com.google.common.cache.*;
import com.lp.guava.pojo.EmployeeInfo;
import org.apache.commons.collections4.MapUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author peng.liu
 * @date 2017/10/16 12:27
 */

public class TestCache {

    /**
     * 基于容量的回收
     * <p>
     * 缓存将尝试回收最近没有使用或总体上很少使用的缓存项。
     * <p>
     * CacheBuilder.weigher(Weigher)指定一个权重函数，并且用CacheBuilder.maximumWeight(long)指定最大总重。
     */
    LoadingCache<String, EmployeeInfo> SIZE_CACHE =
            CacheBuilder.newBuilder().maximumSize(1000).build(new CacheLoader<String, EmployeeInfo>() {
                @Override
                public EmployeeInfo load(String key) throws Exception {
                    return createEmp(key);
                }
            });

    /**
     * 基于定时回收。
     * <p>
     * expireAfterAccess(long, TimeUnit)：缓存项在给定时间内没有被读/写访问，则回收。 请注意这种缓存的回收顺序和基于大小回收一样。
     * <p>
     * expireAfterWrite(longTimeUnit)：缓存项在给定时间内没有被写访问（创建或覆盖），则回收。
     * 如果认为缓存数据总是在固定时候后变得陈旧不可用，这种回收方式是可取的。
     */
    LoadingCache<String, EmployeeInfo> TIME_CACHE = CacheBuilder.newBuilder().expireAfterAccess(10, TimeUnit.SECONDS)
            .build(new CacheLoader<String, EmployeeInfo>() {
                @Override
                public EmployeeInfo load(String key) throws Exception {
                    return createEmp(key);
                }
            });

    /**
     * 基于引用回收
     * <p>
     * CacheBuilder.weakKeys()：使用弱引用存储键。 CacheBuilder.weakValues()：使用弱引用存储值。
     * <p>
     * CacheBuilder.softValues()：使用软引用存储值
     */
    LoadingCache<String, EmployeeInfo> REFERENCE_CACHE =
            CacheBuilder.newBuilder().weakValues().build(new CacheLoader<String, EmployeeInfo>() {
                @Override
                public EmployeeInfo load(String key) throws Exception {
                    return createEmp(key);
                }
            });


    private static final Cache<String, Optional<String>> DRIVER_CAPTAIN_CACHE = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build();

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Map<String,Object> contentMap = new HashMap<>();
        contentMap.put("xxx",null);
        if(MapUtils.isNotEmpty(contentMap)){
            System.out.println("aaaa");
        }
        if(contentMap.containsKey("xxx")){
            System.out.println((String)contentMap.get("xxx"));
            System.out.println(contentMap.keySet());
        }
        Optional<String> optional = DRIVER_CAPTAIN_CACHE.get("123", () -> getString());
        DRIVER_CAPTAIN_CACHE.invalidate("123");
        Optional<String> optional1 = DRIVER_CAPTAIN_CACHE.get("123", () -> getString());
//        TestCache testCache = new TestCache();
//        System.out.println(200_0 > 300);
////        testCache.testStatistics();
//        testCache.getEmp("123");
    }

    public static Optional<String> getString(){
        System.out.println("xxxxxxxxxxxxxxx");
        return Optional.empty();
    }

    public void getEmp(String key) throws ExecutionException, InterruptedException {
        // 配置CacheBuilder的字符串
        String spec = "concurrencyLevel=10,expireAfterAccess=1s,softValues";
        // 解析字符串，创建CacheBuilderSpec实例
        CacheBuilderSpec cacheBuilderSpec = CacheBuilderSpec.parse(spec);
        // 通过CacheBuilderSpec实例构造CacheBuilder实例
        CacheBuilder cacheBuilder = CacheBuilder.from(cacheBuilderSpec);
        // ticker：设置缓存条目过期时间
        // removalListener：监听缓存条目的移除
        // 自定义ticker
        TestTicker testTicker = new TestTicker();

        LoadingCache<String, EmployeeInfo> cache =
                cacheBuilder.ticker(testTicker).removalListener(new RemovalListener<String, EmployeeInfo>() {
                    public void onRemoval(RemovalNotification<String, EmployeeInfo> notification) {
                        System.out.println("移除key：" + notification.getKey());
                    }
                }).build(new CacheLoader<String, EmployeeInfo>() {
                    public EmployeeInfo load(String key) throws Exception {
                        return createEmp(key);
                    }
                });

        System.out.println("++++++++" + cache.get(key).getEmployeeId());
        System.out.println("--------" + cache.getUnchecked(key).getEmployeeId());
        // 模拟时间流逝
        testTicker.addElapsedTime(TimeUnit.NANOSECONDS.convert(1, TimeUnit.HOURS));
        System.out.println(cache.get(key));
    }

    public void testStatistics() throws ExecutionException {
        LoadingCache<String, EmployeeInfo> sizeCahce = CacheBuilder.newBuilder().recordStats().maximumSize(1000)
                .build(new CacheLoader<String, EmployeeInfo>() {
                    @Override
                    public EmployeeInfo load(String key) throws Exception {
                        return createEmp(key);
                    }
                });
        sizeCahce.get("123");
        sizeCahce.get("123");
        sizeCahce.get("123");
        sizeCahce.get("123");

        CacheStats cacheStats = sizeCahce.stats();
        // 缓存命中率；
        System.out.println(cacheStats.hitRate());

        System.out.println(cacheStats.hitCount());

        // 加载新值的平均时间，单位为纳秒；
        System.out.println(cacheStats.averageLoadPenalty());

        // 缓存项被回收的总数，不包括显式清除。
        System.out.println(cacheStats.evictionCount());

        // asMap视图提供了缓存的ConcurrentMap形式，但asMap视图与缓存的交互需要注意：
        System.out.println(sizeCahce.asMap());
    }

    public EmployeeInfo createEmp(String key) {
        EmployeeInfo emp = new EmployeeInfo();
        emp.setEmployeeId(Long.valueOf(key));
        System.out.println("初始化" + key);
        return emp;
    }

    private static class TestTicker extends Ticker {
        private long start = Ticker.systemTicker().read();
        private long elapsedNano = 0;

        @Override
        public long read() {
            return start + elapsedNano;
        }

        public void addElapsedTime(long elapsedNano) {
            this.elapsedNano = elapsedNano;
        }
    }
}
