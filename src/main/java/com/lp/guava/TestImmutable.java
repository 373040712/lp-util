package com.lp.guava;

/**
 * @author LiuPeng
 * @date 2017/9/13 10:23
 */

public class TestImmutable {
    // Collection JDK ImmutableCollection
    // List JDK ImmutableList
    // Set JDK ImmutableSet
    // SortedSet/NavigableSet JDK ImmutableSortedSet
    // Map JDK ImmutableMap
    // SortedMap JDK ImmutableSortedMap
    // Multiset Guava ImmutableMultiset
    // SortedMultiset Guava ImmutableSortedMultiset
    // Multimap Guava ImmutableMultimap
    // ListMultimap Guava ImmutableListMultimap
    // SetMultimap Guava ImmutableSetMultimap
    // BiMap Guava ImmutableBiMap
    // ClassToInstanceMap Guava ImmutableClassToInstanceMap
    // Table Guava ImmutableTable

    public static void main(String[] args) {

    }
}
