package com.lp.guava;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.lp.guava.pojo.EmployeeInfo;

import javax.annotation.Nullable;
import java.util.Comparator;
import java.util.List;

/**
 * @author LiuPeng
 * @date 2017/9/12 10:00
 */

public class TestOrdering {

    public static void main(String[] args) {
        TestOrdering testOrdering = new TestOrdering();
        testOrdering.list();
    }

    public void list() {
        // 实现自定义的排序器时，除了用上面的from方法，也可以跳过实现Comparator，而直接继承Ordering：
        Ordering<String> byLengthOrdering = new Ordering<String>() {
            public int compare(String left, String right) {
                return Ints.compare(left.length(), right.length());
            }
        };

        List<EmployeeInfo> emps = listEmp();

        // 按对象的字符串形式做字典排序[lexicographical ordering]
        List<EmployeeInfo> tempEmps = Ordering.usingToString().nullsLast().sortedCopy(emps);
        System.out.println(tempEmps.toString());

        // 把给定的Comparator转化为排序器
        Ordering<EmployeeInfo> ordering2 = Ordering.from(new Comparator<EmployeeInfo>() {
            public int compare(EmployeeInfo o1, EmployeeInfo o2) {
                return Longs.compare(o1.getUserId(), o2.getUserId());
            }
        });
        System.out.println(ordering2.sortedCopy(emps));

        // 对可排序类型做自然排序，如数字按大小，日期按先后排序
        Ordering<EmployeeInfo> ordering1 =
                Ordering.natural().nullsFirst().onResultOf(new Function<EmployeeInfo, Long>() {
                    @Nullable
                    public Long apply(@Nullable EmployeeInfo input) {
                        return input.getUserId();
                    }
                });
        System.out.println(ordering1.sortedCopy(emps));

        // 获取可迭代对象中最大的k个元素。
        System.out.println(ordering1.greatestOf(emps, 2));
        System.out.println(ordering1.leastOf(emps, 1));

        // 判断可迭代对象是否已按排序器排序：允许有排序值相等的元素。
        System.out.println(ordering1.isOrdered(emps));
        System.out.println(ordering1.isStrictlyOrdered(ordering1.sortedCopy(emps)));

        // 返回迭代器中最小的元素。如果可迭代对象中没有元素，则抛出NoSuchElementException。
        System.out.println(ordering1.max(emps));
        System.out.println(ordering1.min(emps));
    }

    private List<EmployeeInfo> listEmp() {
        List<EmployeeInfo> emps = Lists.newArrayList();
        EmployeeInfo emp1 = new EmployeeInfo();
        emp1.setUserId(1L);
        emp1.setWorkNo("321");
        emp1.setName("C123");

        EmployeeInfo emp2 = new EmployeeInfo();
        emp2.setUserId(2L);
        emp2.setWorkNo("213");
        emp2.setName("A123");

        EmployeeInfo emp3 = new EmployeeInfo();
        emp3.setUserId(3L);
        emp3.setWorkNo("123");
        emp3.setName("B123");

        EmployeeInfo emp4 = new EmployeeInfo();

        emps.add(emp1);
        emps.add(emp2);
        emps.add(emp3);
        emps.add(emp4);
        return emps;
    }
}
