package com.lp.guava;

import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.*;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * @author peng.liu
 * @date 2017/10/16 17:22
 */

public class TestConcurrency {

    /**
     * ListenableFuture可以允许你注册回调方法(callbacks)，在运算（多线程执行）完成的时候进行调用, 或者在运算（多线程执行）完成后立即执行
     * 
     * @param args
     */
    public static void main(String[] args) {
        TestConcurrency testConcurrency = new TestConcurrency();
        testConcurrency.testBatchFuture();
        // testConcurrency.testFuture();
        // testConcurrency.testListenableFuture();
        // testConcurrency.testFutures();
    }

    public void testListenableFuture() {
        ListeningExecutorService threadPool = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));
        ListenableFuture<Integer> future = threadPool.submit(new Callable<Integer>() {
            public Integer call() throws Exception {
                return new Random().nextInt(100);
            }
        });
        Futures.addCallback(future, new FutureCallback<Integer>() {
            @Override
            public void onSuccess(@Nullable Integer result) {
                System.out.println("成功");
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println("失败");
            }
        }, threadPool);
    }

    /**
     * transform：对于ListenableFuture的返回值进行转换。
     *
     * allAsList：对多个ListenableFuture的合并，返回一个当所有Future成功时返回多个Future返回值组成的List对象。注：
     * 当其中一个Future失败或者取消的时候，将会进入失败或者取消。
     *
     * successfulAsList：和allAsList相似，唯一差别是对于失败或取消的Future返回值用null代替。不会进入失败或者取消流程。
     *
     * immediateFuture/immediateCancelledFuture： 立即返回一个待返回值的ListenableFuture。
     *
     * makeChecked: 将ListenableFuture 转换成CheckedFuture。
     *
     * CheckedFuture 是一个ListenableFuture ，其中包含了多个版本的get
     * 方法，方法声明抛出检查异常.这样使得创建一个在执行逻辑中可以抛出异常的Future更加容易
     *
     * JdkFutureAdapters.listenInPoolThread(future): guava同时提供了将JDK Future转换为ListenableFuture的接口函数。
     *
     * AsyncFunction<A, B> 中提供一个方法ListenableFuture<B> apply(A input)，它可以被用于异步变换值。
     */
    public void testFutures() {
        ListeningExecutorService threadPool = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(10));
        List<ListenableFuture<Integer>> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            ListenableFuture<Integer> future = threadPool.submit(new Callable<Integer>() {
                public Integer call() throws Exception {
                    return new Random().nextInt(100);
                }
            });
            list.add(future);
        }

        ListenableFuture<List<Integer>> successfulQueries =
                Futures.successfulAsList(list.toArray(new ListenableFuture[0]));
        Futures.addCallback(successfulQueries, new FutureCallback<List>() {
            @Override
            public void onSuccess(@Nullable List result) {
                System.out.println(JSONObject.toJSONString(result));
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println(t.getMessage());
            }
        }, threadPool);

        final ListenableFuture<String> transform =
                Futures.transformAsync(successfulQueries, new AsyncFunction<List<Integer>, String>() {
                    @Override
                    public ListenableFuture<String> apply(@Nullable List<Integer> input) throws Exception {
                        return Futures.immediateFuture(input.size() + "");
                    }
                }, threadPool);
        Futures.addCallback(transform, new FutureCallback<String>() {
            @Override
            public void onSuccess(@Nullable String result) {
                System.out.println(result);
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println(t.getMessage());
            }
        }, threadPool);
    }

    public void testFuture() {
        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        Future<Integer> future = threadPool.submit(new Callable<Integer>() {
            public Integer call() throws Exception {
                return new Random().nextInt(100);
            }
        });
        try {
            Thread.sleep(500);// 可能做一些事情
            System.out.println(future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * 1.CompletionService.take 会获取并清除已经完成Task的结果，如果当前没有已经完成Task时，会阻塞。
     * 
     * 2.“先创建一个装Future类型的集合，用Executor提交的任务返回值添加到集合中，最后遍历集合取出数据” -- 这种方法通常是按照Future加入的顺序。
     * 
     * 两个方法最大的差别在于遍历 Future 的顺序，相对来说， CompletionService 的性能更高。
     * 考虑如下场景：多线程下载，结果用Future返回。第一个文件特别大，后面的文件很小。用方法1，能很快知道已经下载完文件的结果(不是第一个)；而用方法2，必须等第一个文件下载结束后，
     * 才会获得其他文件的下载结果。
     */
    public void testBatchFuture() {
        ExecutorService threadPool = Executors.newCachedThreadPool();
        CompletionService<Integer> cs = new ExecutorCompletionService<Integer>(threadPool);
//        for (int i = 0; i < 5; i++) {
//            final int taskID = i;
//            cs.submit(new Callable<Integer>() {
//                public Integer call() throws Exception {
//                    return taskID;
//                }
//            });
//        }
        // 可能做一些事情
        while (true)
            try {
                Future<Integer> future = cs.take();
                if(future==null){
                    return;
                }
                Integer temp = future.get();
                if (temp == null) {
                    return;
                }
                System.out.println(temp.intValue());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
    }
}
