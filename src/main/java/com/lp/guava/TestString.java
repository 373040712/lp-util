package com.lp.guava;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

/**
 * @author peng.liu
 * @date 2017/11/2 18:06
 */

public class TestString {

    private final static String SEPARATOR = "/";

    /**
     * 支持通配符配置
     */
    private final static String PLACEHOLDER = "*";

    public static void main(String[] args) {
        System.out.println(String.format("sssss%s",1234));
        System.out.println(
                Joiner.on(",").appendTo(new StringBuilder("123"), Lists.newArrayList("a", "b", "c")).toString());
        TestString testString = new TestString();
        // testString.testCharMatcher();
        // testString.testSplitter();
        // testString.testJoiner();
        System.out.println(testString.isMatch(Lists.newArrayList("/server", "/server/*/query"), "/server/123/list"));


    }

    private boolean isMatch(List<String> apiConfig, String uri) {
        ApiConfig apiConfig1 = JSONObject.parseObject("{\"include\":[\"/server/api/business\"]}", ApiConfig.class);
        List<String> uriList = Splitter.on(SEPARATOR).omitEmptyStrings().splitToList(uri);
        int uriSize = uriList.size();
        return apiConfig.stream().anyMatch(e -> {
            if (!e.contains(PLACEHOLDER)) {
                return uri.startsWith(e);
            }
            List<String> tempList = Splitter.on(SEPARATOR).omitEmptyStrings().splitToList(e);
            int tempSize = tempList.size();
            if (uriSize < tempSize) {
                return false;
            }
            for (int i = 0; i < tempSize; i++) {
                if (Objects.equals(tempList.get(i), PLACEHOLDER)) {
                    continue;
                }
                if (!Objects.equals(tempList.get(i), uriList.get(i))) {
                    return false;
                }
            }
            return true;
        });
    }

    // 方法 描述
    // collapseFrom(CharSequence, char) 把每组连续的匹配字符替换为特定字符。如WHITESPACE.collapseFrom(string, ‘
    // ‘)把字符串中的连续空白字符替换为单个空格。
    // matchesAllOf(CharSequence) 测试是否字符序列中的所有字符都匹配。
    // removeFrom(CharSequence) 从字符序列中移除所有匹配字符。
    // retainFrom(CharSequence) 在字符序列中保留匹配字符，移除其他字符。
    // trimFrom(CharSequence) 移除字符序列的前导匹配字符和尾部匹配字符。
    // replaceFrom(CharSequence, CharSequence) 用特定字符序列替代匹配字符。
    private void testCharMatcher() {
        // 移除control字符
        String string = "   12as2\t34\t5AD    B<J?ad  %*CHas   ";
        String temp = CharMatcher.anyOf("as").removeFrom(string);
        System.out.println(temp);

        String noControl = CharMatcher.javaIsoControl().removeFrom(string);
        System.out.println(noControl);

        // 只保留数字字符
        String theDigits = CharMatcher.digit().retainFrom(string);
        System.out.println(theDigits);

        // 去除两端的空格，并把中间的连续空格替换成单个空格
        String spaced = CharMatcher.whitespace().trimAndCollapseFrom(string, ' ');
        System.out.println(spaced);

        // 用*号替换所有数字
        String noDigits = CharMatcher.javaDigit().replaceFrom(string, "*");
        System.out.println(noDigits);

        // 只保留数字和小写字母
        String lowerAndDigit = CharMatcher.javaDigit().or(CharMatcher.javaLowerCase()).retainFrom(string);
        System.out.println(lowerAndDigit);

        // 只保留范围内字符
        String azStr = CharMatcher.inRange('a', 'd').retainFrom(string);
        System.out.println(azStr);

    }

    private void testSplitter() {
        System.out.println(Splitter.on(',').trimResults().omitEmptyStrings().split("foo,bar,,   qux"));
    }

    private void testJoiner() {
        Joiner joiner = Joiner.on("; ").skipNulls();
        System.out.println(joiner.join("Harry", null, "Ron", "Hermione"));
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    static class ApiConfig {

        private List<String> include = Lists.newArrayList();

        private List<String> exclude = Lists.newArrayList();
    }
}
