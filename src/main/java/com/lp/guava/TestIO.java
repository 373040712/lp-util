package com.lp.guava;

import com.google.common.collect.ImmutableList;
import com.google.common.io.*;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

/**
 * 把已经打开的流（比如InputStream）包装为源或汇听起来是很有诱惑力的，但是应该避免这样做。
 * 源与汇的实现应该在每次openStream()方法被调用时都创建一个新的流。
 * 始终创建新的流可以让源或汇管理流的整个生命周期，并且让多次调用openStream()返回的流都是可用的。
 * 此外，如果你在创建源或汇之前创建了流，你不得不在异常的时候自己保证关闭流，这压根就违背了发挥源与汇API优点的初衷。
 *
 * @author LP
 * @date 2017/11/22 20:19
 */

public class TestIO {
    private String rootPath = System.getProperty("user.dir") + "\\src\\main\\resources\\";

    public static void main(String[] args) throws IOException {
        TestIO testIO = new TestIO();
        testIO.testSourceAndSink();
    }

    /**
     * Guava中的Resources.toByteArray(URL)和Files.toByteArray(File)做了同样的事情，只不过数据源一个是URL，一个是文件
     * <p>
     * FileWriteMode.APPEND 原文件追加
     *
     * @throws IOException
     */
    private void testSourceAndSink() throws IOException {
        //字节流
        ByteSource byteSource = Files.asByteSource(new File(rootPath + "io-test.txt"));
        byte[] buffer = byteSource.read();
        System.out.println("ByteSource: " + new String(buffer));
        ByteSink byteSink = Files.asByteSink(new File(rootPath + "io-test-byte-w.txt"));
        byteSink.write(buffer);
        //byteSink.writeFrom(InputStream);

        buffer = ByteStreams.toByteArray(new FileInputStream(new File(rootPath + "io-test.txt")));
        System.out.println("ByteStreams: " + new String(buffer));
        ByteStreams.copy(new FileInputStream(rootPath + "io-test.txt"), new FileOutputStream(rootPath + "io-test-byte-w2.txt"));

        //字符流
        CharSource charSource = Files.asCharSource(new File(rootPath + "io-test.txt"), Charset.defaultCharset());
        ImmutableList<String> lines = charSource.readLines();
        System.out.println("CharSource: " + lines);
        CharSink charSink =
                Files.asCharSink(new File(rootPath + "io-test-w.txt"), Charset.defaultCharset());
        charSink.writeLines(lines);
        //charSink.writeFrom(Readable);

        String str = CharStreams.toString(new FileReader(rootPath + "io-test.txt"));
        System.out.println("CharStreams: " + str);

        List<String> temps = CharStreams.readLines(new FileReader(rootPath + "io-test.txt"));
        System.out.println("CharStreams: " + temps);
        FileWriter fw = new FileWriter(rootPath + "io-test-2.txt");
        CharStreams.copy(new FileReader(rootPath + "io-test.txt"), fw);
        fw.flush();
    }

}
