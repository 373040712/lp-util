package com.lp.guava.eventBus;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.util.Date;
import java.util.concurrent.Executors;

/**
 * Created by Administrator on 2017-11-22 0022.
 */
public class TestEventBus {

    public static void siginalThreadConsumer() {

        EventBus bus = new EventBus("iamzhongyong");
        SFEventListener sf = new SFEventListener();
        YTOEventListener yto = new YTOEventListener();
        bus.register(sf);
        bus.register(yto);
        bus.register(new Object() {

            @Subscribe
            public void lister(DeadEvent event) {
                System.out.printf("%s=%s from dead events%n", event.getSource().getClass(), event.getEvent());
            }
        });

        SignEvent sign1 = new SignEvent("SF", "比熊啊", new Date());
        bus.post(sign1);
        SignEvent sign2 = new SignEvent("YTO", "你妹的", new Date());
        bus.post(sign2);
    }

    public static void multiThread() {
        EventBus bus = new AsyncEventBus(Executors.newFixedThreadPool(3));
        SFEventListener sf = new SFEventListener();
        YTOEventListener yto = new YTOEventListener();
        bus.register(sf);
        bus.register(yto);
        bus.register(new Object() {

            @Subscribe
            public void lister(DeadEvent event) {
                System.out.printf("%s=%s from dead events%n", event.getSource().getClass(), event.getEvent());
            }
        });

        SignEvent sign1 = new SignEvent("SF", "比熊啊", new Date());
        bus.post(sign1);
        SignEvent sign2 = new SignEvent("YTO", "你妹的", new Date());
        bus.post(sign2);
        bus.post(new Object());
    }

    public static void main(String[] args) {
//        TestEventBus.siginalThreadConsumer();
        TestEventBus.multiThread();
    }
}
