package com.lp.guava.eventBus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 一个事件的定义（任何对象都可以是事件）
 *
 * Created by Administrator on 2017-11-23 0023.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignEvent {

    private String companyName;
    private String signName;
    private Date signDate;

    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append("物流公司：").append(this.companyName);
        sb.append("签收人：").append(signName).append(",签收日期：").append(signDate);
        return sb.toString();
    }
}
