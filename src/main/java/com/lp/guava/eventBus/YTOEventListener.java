package com.lp.guava.eventBus;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;

/**
 * Created by Administrator on 2017-11-23 0023.
 */
public class YTOEventListener {

    @Subscribe
    @AllowConcurrentEvents
    public void consign(SignEvent signEvent) {
        if (signEvent.getCompanyName().equalsIgnoreCase("YTO")) {
            System.out.println("YTO。。。开始发货");
            System.out.println(signEvent.getMessage());
        }
    }

    @Subscribe
    @AllowConcurrentEvents
    public void delivery(SignEvent signEvent) {
        if (signEvent.getCompanyName().equalsIgnoreCase("YTO")) {
            System.out.println("YTO。。。开始投递");
        }
    }
}
