package com.lp.guava;

import com.google.common.collect.Range;
import com.google.common.primitives.Ints;

/**
 * @author LP
 * @date 2017/11/22 18:02
 */

public class TestRange {

    public static void main(String[] args) {
        TestRange testRange = new TestRange();
        testRange.testRange();
    }

    // (a..b) open(C, C)
    // [a..b] closed(C, C)
    // [a..b) closedOpen(C, C)
    // (a..b] openClosed(C, C)
    // (a..+∞) greaterThan(C)
    // [a..+∞) atLeast(C)
    // (-∞..b) lessThan(C)
    // (-∞..b] atMost(C)
    // (-∞..+∞) all()
    private void testRange() {
        // 字典序在"left"和"right"之间的字符串，闭区间
        Range.closed("left", "right");

        // 严格小于4.0的double值
        Range.lessThan(4.0);

        // 区间运算
        Range.closed(1, 3).contains(2);// return true
        Range.closed(1, 3).contains(4);// return false
        Range.lessThan(5).contains(5); // return false
        Range.closed(1, 4).containsAll(Ints.asList(1, 2, 3)); // return true

        // 查询运算
        Range.closedOpen(4, 4).isEmpty(); // returns true
        Range.openClosed(4, 4).isEmpty(); // returns true
        Range.closed(4, 4).isEmpty(); // returns false
        Range.open(4, 4).isEmpty(); // Range.open throws IllegalArgumentException
        Range.closed(3, 10).lowerEndpoint(); // returns 3
        Range.open(3, 10).lowerEndpoint(); // returns 3
        Range.closed(3, 10).lowerBoundType(); // returns CLOSED
        Range.open(3, 10).upperBoundType(); // returns OPEN

        // 相连[isConnected]
        Range.closed(3, 5).isConnected(Range.open(5, 10)); // returns true
        Range.closed(0, 9).isConnected(Range.closed(3, 4)); // returns true
        Range.closed(0, 5).isConnected(Range.closed(3, 9)); // returns true
        Range.open(3, 5).isConnected(Range.open(5, 10)); // returns false
        Range.closed(1, 5).isConnected(Range.closed(6, 10)); // returns false

        // 交集[intersection]
        Range.closed(3, 5).intersection(Range.open(5, 10)); // returns (5, 5]
        Range.closed(0, 9).intersection(Range.closed(3, 4)); // returns [3, 4]
        Range.closed(0, 5).intersection(Range.closed(3, 9)); // returns [3, 5]
        Range.open(3, 5).intersection(Range.open(5, 10)); // throws IAE
        Range.closed(1, 5).intersection(Range.closed(6, 10)); // throws IAE

        // 跨区间[span]
        Range.closed(3, 5).span(Range.open(5, 10)); // returns [3, 10)
        Range.closed(0, 9).span(Range.closed(3, 4)); // returns [0, 9]
        Range.closed(0, 5).span(Range.closed(3, 9)); // returns [0, 9]
        Range.open(3, 5).span(Range.open(5, 10)); // returns (3, 10)
        Range.closed(1, 5).span(Range.closed(6, 10)); // returns [1, 10]

    }
}
