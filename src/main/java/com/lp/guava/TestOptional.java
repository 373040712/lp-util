package com.lp.guava;

import com.google.common.base.Optional;

/**
 * Created by Administrator on 2017-8-29 0029.
 */
public class TestOptional {

    public static void main(String[] args) {
        TestOptional testOptional = new TestOptional();
        // testOptional.createOptional();
        testOptional.getOptional();
    }

    public void createOptional() {
        Integer temp = null;
        // 创建引用缺失的Optional实例
        Optional<Integer> possible1 = Optional.absent();
        // 创建指定引用的Optional实例，若引用为null则表示缺失
        Optional<Integer> possible2 = Optional.fromNullable(temp);
        // 创建指定引用的Optional实例，若引用为null则快速失败
        Optional<Integer> possible = Optional.of(temp);

    }

    public void getOptional() {
        // 创建引用缺失的Optional实例
        Optional<Integer> possible1 = Optional.absent();
        // 如果Optional包含非null的引用（引用存在），返回true
        System.out.println(possible1.isPresent());
        // 返回Optional所包含的引用，若引用缺失，返回指定的值
        System.out.println(possible1.or(6));
        // 返回Optional所包含的引用，若引用缺失，返回null
        System.out.println(possible1.orNull());
        // 返回Optional所包含的引用，若引用缺失，返回null
        System.out.println(possible1.asSet());
        // 返回Optional所包含引用的单例不可变集，如果引用存在，返回一个只有单一元素的集合，如果引用缺失，返回一个空集合。
        System.out.println(possible1.get());
    }
}
