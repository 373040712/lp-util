package com.lp.guava;

import com.google.common.collect.AbstractIterator;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.PeekingIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author peng.liu
 * @date 2017/10/13 17:05
 */

public class TestPeekingIterator {

    public static void main(String[] args) {
        TestPeekingIterator testPeekingIterator = new TestPeekingIterator();
        testPeekingIterator.doTest();
    }

    public void doTest() {
        List<String> list = new ArrayList<String>();
        list.add("123");
        list.add("345");
        list.add("678");

        List<String> result = Lists.newArrayList();
        PeekingIterator<String> iter = Iterators.peekingIterator(list.iterator());
        while (iter.hasNext()) {
            String current = iter.next();
            while (iter.hasNext() && iter.peek().equals(current)) {
                // 跳过重复的元素
                iter.next();
            }
            result.add(current);
        }
    }

    /**
     * 你实现了computeNext()方法，来计算下一个值。如果循环结束了也没有找到下一个值，请返回endOfData()表明已经到达迭代的末尾。
     * 
     * @param in
     * @return
     */
    public Iterator<String> skipNulls(final Iterator<String> in) {
        return new AbstractIterator<String>() {
            protected String computeNext() {
                while (in.hasNext()) {
                    String s = in.next();
                    if (s != null) {
                        return s;
                    }
                }
                return endOfData();
            }
        };
    }

}
