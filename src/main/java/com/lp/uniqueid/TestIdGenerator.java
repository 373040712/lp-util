package com.lp.uniqueid;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author LP
 * @date 2018/3/7 15:41
 */

public class TestIdGenerator {

    public static void main(String[] args) throws UnknownHostException {
        InetAddress ip = InetAddress.getLocalHost();
        System.out.println(ip.getAddress());
        System.out.println(System.currentTimeMillis());
        final IdGenerator idg = IdGenerator.INSTANCE;
        System.out.println(idg.nextId());
    }
}
