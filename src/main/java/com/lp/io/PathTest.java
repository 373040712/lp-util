package com.lp.io;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 20:10
 */
public class PathTest {

    /**
     * Path就是取代File的
     *
     * @throws URISyntaxException
     */
    public void testPath() throws URISyntaxException {
        //在d盘找a.txt文件
        Paths.get("d:", "a.txt");

        //在项目根目录找a.txt文件
        Paths.get("a.txt");

        //根据字符串格式的路径获取：使用Paths工具类中的get方法去获取，方法的参数是1个或1个以上的String，
        // Paths会自动根据参数中的字符串对路径做拼接。另外，两个点".."表示的是路径向上回退一级。
        //System.getProperty("user.dir")=="D:\workspace\my\lp-util"
        Paths.get(System.getProperty("user.dir"), "..", "data");

        //PathTest.class.getClassLoader().getResource()=="D:\workspace\my\lp-util\target\classes"
        Paths.get(PathTest.class.getClassLoader().getResource("tt_sql.properties").toURI());

        //FileSystems构造：
        FileSystems.getDefault().getPath("C:/", "access.log");

        //File和Path之间的转换，File和URI之间的转换
        File file = new File("C:/my.ini");
        Path p1 = file.toPath();
        p1.toFile();
        file.toURI();
    }
}
