package com.lp.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 21:13
 */
public class FileTest {

    /**
     * 创建目录和文件
     */
    public void createFile() {
        try {
            Files.createDirectories(Paths.get("C://TEST"));
            if (!Files.exists(Paths.get("C://TEST"))) {
                Files.createFile(Paths.get("C://TEST/test.txt"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件复制
     */
    public void copyFile() {
        try {
            Files.createDirectories(Paths.get("C://TEST"));
            if (!Files.exists(Paths.get("C://TEST"))) {
                Files.createFile(Paths.get("C://TEST/test.txt"));
            }
//          Files.createDirectories(Paths.get("C://TEST/test2.txt"));
            Files.copy(Paths.get("C://my.ini"), System.out);
            Files.copy(Paths.get("C://my.ini"), Paths.get("C://my2.ini"), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(System.in, Paths.get("C://my3.ini"), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
