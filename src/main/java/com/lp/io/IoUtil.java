package com.lp.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/9 20:41
 */
public class IoUtil {

    public static List<String> readFile(String filename) {
        List<String> list = new ArrayList<>();
        try {
            Path path = Paths.get(PathTest.class.getClassLoader().getResource(filename).toURI());
            //如果指定的字符编码不对，可能会抛出异常 MalformedInputException
            BufferedReader reader = Files.newBufferedReader(path, Charset.forName("GBK"));
            String str;
            while ((str = reader.readLine()) != null) {
                list.add(str);
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void writeFile(String filename) {
        try {
            Path path = Paths.get(PathTest.class.getClassLoader().getResource(filename).toURI());
            BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName("GBK"));
            writer.write("测试文件写操作");
            writer.flush();
            writer.close();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static List<Path> listDir() {
        List<Path> list = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(System.getProperty("user.dir")))) {
            for (Path e : stream) {
                list.add(e.getFileName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Path> listDirOther() {
        List<Path> list = new ArrayList<>();
        try (Stream<Path> stream = Files.list(Paths.get(System.getProperty("user.dir")))) {
            Iterator<Path> ite = stream.iterator();
            while (ite.hasNext()) {
                Path pp = ite.next();
                list.add(pp.getFileName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<Path> listDeepDir() {
        List<Path> result = new LinkedList<Path>();
        try {
            Files.walkFileTree(Paths.get(System.getProperty("user.dir"), "src/main/java"), new FindJavaVisitor(result));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static class FindJavaVisitor extends SimpleFileVisitor<Path> {

        private List<Path> result;

        public FindJavaVisitor(List<Path> result) {
            this.result = result;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
            if (file.toString().endsWith(".java")) {
                result.add(file.getFileName());
            }
            return FileVisitResult.CONTINUE;
        }
    }
}
