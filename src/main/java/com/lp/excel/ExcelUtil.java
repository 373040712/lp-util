package com.lp.excel;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.lp.excel.handler.HandlerTool;
import com.lp.excel.pojo.ExcelHeader;
import com.lp.excel.pojo.StringRow;
import com.lp.excel.pojo.StringWorksheet;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author LiuPeng
 * @date 2017/3/27 16:13
 */

public class ExcelUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil.class);

    private static final int MAX_ROW_NUM = 60000;

    private static final String DT_FORMATE = "yyyy-MM-dd HH:mm:ss";


    private static StringTemplateGroup stGroup;

    static {
        stGroup = new StringTemplateGroup("stringTemplate");
        // 解决可能发生的中文乱码
        stGroup.setFileCharEncoding("UTF-8");
    }

    /**
     * 导出一个excel压缩文件，可以控制一个excel只有一个sheet，一个sheet最多可以60000行数据
     *
     * @param outputStream
     * @param paramList
     * @param fileName
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws IOException
     */
    public static void exportExcelZip(OutputStream outputStream, List paramList, String fileName)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, IOException {
        exportExcelZip(outputStream, paramList, fileName, 1);
    }

    /**
     * 导出一个excel压缩文件，可以控制一个excel的sheet个数，一个sheet最多可以60000行数据
     *
     * @param outputStream
     * @param paramList
     * @param fileName
     * @param sheetSize
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws IOException
     */
    public static void exportExcelZip(OutputStream outputStream, List paramList, String fileName, int sheetSize)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, IOException {
        exportExcelZip(outputStream, paramList, fileName, sheetSize, MAX_ROW_NUM);
    }

    /**
     * 导出一个excel压缩文件，可以控制一个excel的sheet个数，一个sheet最多可以多少行数据
     *
     * @param outputStream
     * @param paramList
     * @param fileName
     * @param sheetSize
     * @param maxRowNum
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws IOException
     */
    public static void exportExcelZip(OutputStream outputStream, List paramList, String fileName, int sheetSize,
            int maxRowNum)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, IOException {
        if (CollectionUtils.isEmpty(paramList)) {
            return;
        }
        long startTime = System.currentTimeMillis();
        LOGGER.debug("正在生成zip文件");
        if (StringUtils.isBlank(fileName)) {
            fileName = "temp";
        }
        ZipOutputStream zipOut = new ZipOutputStream(outputStream);
        byte[] buffer = new byte[2048];

        // 用于存放生成的文件名称
        int totalRowNum = paramList.size();
        int fileRowNum = sheetSize * maxRowNum;
        int totalFile = totalRowNum % fileRowNum == 0 ? (totalRowNum / fileRowNum) : (totalRowNum / fileRowNum + 1);
        for (int i = 0; i < totalFile; i++) {
            int start = i * fileRowNum;
            int end = Math.min(start + fileRowNum, totalRowNum);
            List tempList = paramList.subList(start, end);
            File fileTemp = new File(fileName + "-" + i + ".xls");
            exportExcelFile(fileTemp, tempList, maxRowNum);
            compressExcelFile(zipOut, buffer, fileTemp, true);
        }
        zipOut.flush();
        zipOut.close();

        long endTime = System.currentTimeMillis();
        LOGGER.debug("生成zip文件完成");
        LOGGER.debug("用时=" + (endTime - startTime) + "毫秒");
    }

    /**
     * 压缩excel文件输出到压缩流
     *
     * @param zipOut
     * @param buffer
     * @param file
     * @param deleteFile
     * @throws IOException
     */
    public static void compressExcelFile(ZipOutputStream zipOut, byte[] buffer, File file, boolean deleteFile)
            throws IOException {
        if (file == null) {
            return;
        }
        if (buffer == null) {
            buffer = new byte[2048];
        }
        FileInputStream fis = new FileInputStream(file);
        zipOut.putNextEntry(new ZipEntry(file.getName()));
        // 读取文件的内容,打包到zip流
        int len;
        while ((len = fis.read(buffer)) > 0) {
            zipOut.write(buffer, 0, len);
        }
        zipOut.flush();
        zipOut.closeEntry();
        fis.close();
        if (deleteFile) {
            file.delete();
        }
    }

    /**
     * 导出一个excel文件，可以控制每个sheet最多可以60000行数据
     *
     * @param file
     * @param dataList
     * @throws IOException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void exportExcelFile(File file, List dataList)
            throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        exportExcelFile(file, dataList, MAX_ROW_NUM);
    }

    /**
     * 导出一个excel文件，可以控制每个sheet最多可以多少行数据
     *
     * @param file
     * @param dataList
     * @param maxRowNum
     * @throws IOException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public static void exportExcelFile(File file, List dataList, int maxRowNum)
            throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (file == null) {
            return;
        }
        OutputStream outputTemp = new FileOutputStream(file);
        exportExcel(outputTemp, dataList, maxRowNum);
        outputTemp.flush();
        outputTemp.close();
    }

    /**
     * 导出一个excel文件流，可以控制每个sheet最多可以60000行数据
     *
     * @param outputStream
     * @param dataList
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     */
    public static void exportExcel(OutputStream outputStream, List dataList)
            throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        exportExcel(outputStream, dataList, MAX_ROW_NUM);
    }

    /**
     * 导出一个excel文件流，可以控制每个sheet最多可以多少行数据
     *
     * @param outputStream
     * @param dataList
     * @param maxRowNum
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static void exportExcel(OutputStream outputStream, List dataList, int maxRowNum)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (CollectionUtils.isEmpty(dataList)) {
            return;
        }
        long startTime = System.currentTimeMillis();
        LOGGER.debug("正在生成excel文件");
        PrintWriter writer = new PrintWriter(outputStream);
        exportExcelHead(writer);
        exportExcelBody(writer, dataList, 1, maxRowNum);
        exportExcelTail(writer);
        long endTime = System.currentTimeMillis();
        LOGGER.debug("生成excel文件完成");
        LOGGER.debug("用时=" + (endTime - startTime) + "毫秒");
    }

    public static void exportEmptyExcel(OutputStream outputStream) {
        PrintWriter writer = new PrintWriter(outputStream);
        exportExcelHead(writer);
        exportExcelTail(writer);
    }

    public static void exportEmptyExcelZip(OutputStream outputStream, String fileName)
            throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, IOException {
        ZipOutputStream zipOut = new ZipOutputStream(outputStream);
        byte[] buffer = new byte[2048];
        File fileTemp = new File(fileName + ".xls");
        exportExcelFile(fileTemp, null);
        compressExcelFile(zipOut, buffer, fileTemp, true);
        zipOut.flush();
        zipOut.close();
    }

    public static void exportExcelHead(PrintWriter writer) {
        // 写入excel文件头部信息
        StringTemplate head = stGroup.getInstanceOf("excel/head");
        writer.print(head.toString());
        writer.flush();
        LOGGER.debug("正在生成excel文件的头部");
    }

    public static int exportExcelBody(PrintWriter writer, List dataList, int sheetIndex, int maxRowNum)
            throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        if (CollectionUtils.isEmpty(dataList)) {
            return 0;
        }
        long startTime = System.currentTimeMillis();
        if (sheetIndex <= 0) {
            sheetIndex = 1;
        }
        if (maxRowNum > MAX_ROW_NUM) {
            maxRowNum = MAX_ROW_NUM;
        }
        Class<?> pojoClass = dataList.get(0).getClass();
        List<ExcelHeader> headerList = HandlerTool.getHeaderList(pojoClass);
        List<String> title = Lists.transform(headerList, new Function<ExcelHeader, String>() {
            @Override
            public String apply(ExcelHeader excelHeader) {
                return excelHeader.getTitle();
            }
        });
        int columnLength = headerList.size();
        int totalRowNum = dataList.size();
        int sheets = totalRowNum % maxRowNum == 0 ? (totalRowNum / maxRowNum) : (totalRowNum / maxRowNum + 1);

        // 写入excel文件数据信息
        for (int i = 0; i < sheets; i++) {
            LOGGER.debug("正在生成excel文件的sheet" + sheetIndex);
            StringTemplate body = stGroup.getInstanceOf("excel/body");
            StringWorksheet stringWorksheet = new StringWorksheet();
            stringWorksheet.setTitle(title);
            stringWorksheet.setSheet(Integer.toString(sheetIndex));
            stringWorksheet.setColumnNum(columnLength);
            stringWorksheet.setRowNum(maxRowNum + 1L);
            List<StringRow> rows = new ArrayList<>(totalRowNum < maxRowNum ? totalRowNum : maxRowNum);
            int startIndex = i * maxRowNum;
            int endIndex = Math.min((i + 1) * maxRowNum - 1, totalRowNum - 1);
            for (int j = startIndex; j <= endIndex; j++) {
                StringRow stringRow = new StringRow();
                List<String> result = new ArrayList<String>(columnLength);
                for (int n = 0; n < columnLength; n++) {
                    Object value = BeanUtils.getProperty(dataList.get(j), headerList.get(n).getFiled());
                    if (value == null) {
                        result.add("");
                    } else {
                        if (value instanceof Date) {
                            result.add(new DateTime(value).toString(DT_FORMATE));
                        } else {
                            result.add(value.toString());
                        }
                    }
                }
                stringRow.setResult(result);
                rows.add(stringRow);
            }
            stringWorksheet.setRows(rows);
            body.setAttribute("worksheet", stringWorksheet);
            writer.print(body.toString());
            writer.flush();
            rows.clear();
            rows = null;
            stringWorksheet = null;
            body = null;
            System.gc();
            long endTime = System.currentTimeMillis();
            LOGGER.debug("生成excel文件的sheet" + sheetIndex + "完成");
            LOGGER.debug("用时=" + (endTime - startTime) + "毫秒");
            ++sheetIndex;
        }
        return sheetIndex;
    }

    public static void exportExcelTail(PrintWriter writer) {
        // 写入excel文件尾部
        writer.print("</Workbook>");
        writer.flush();
        writer.close();
        LOGGER.debug("正在生成excel文件的尾部");
    }

    public static void main(String[] args) {
        Field[] fieldlist = StringWorksheet.class.getDeclaredFields();
        for (int i = 0; i < fieldlist.length; i++) {
            Field fld = fieldlist[i];
            System.out.println("name = " + fld.getName());
            System.out.println("decl class = " + fld.getGenericType());
            System.out.println("type = " + fld.getType());
            System.out.println("-----");
        }
    }
}
