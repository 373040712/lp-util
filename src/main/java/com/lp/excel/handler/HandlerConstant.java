package com.lp.excel.handler;


/**
 * <p>
 * Excel模板自定义属性,不区分大小写
 * </p>
 * </br>
 */
public class HandlerConstant {

    // 数据插入起始坐标点
    public static final String DATA_INIT_INDEX = "$data_index";

    // 默认样式
    public static final String DEFAULT_STYLE = "$default_style";

    // 当前标记行样式
    public static final String APPOINT_LINE_STYLE = "$appoint_line_style";

    // 单数行样式
    public static final String SINGLE_LINE_STYLE = "$single_line_style";

    // 双数行样式
    public static final String DOUBLE_LINE_STYLE = "$double_line_style";

    // 序号列坐标点
    public static final String SERIAL_NUMBER = "$serial_number";

}
