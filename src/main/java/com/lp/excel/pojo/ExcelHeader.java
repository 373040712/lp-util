package com.lp.excel.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 功能说明: 用来存储Excel标题的对象，通过该对象可以获取标题和方法的对应关系
 */
@Data
@AllArgsConstructor
public class ExcelHeader implements Comparable<ExcelHeader> {
    /**
     * excel的标题名称
     */
    private String title;
    /**
     * 每一个标题的顺序
     */
    private int order;
    /**
     * 注解域
     */
    private String filed;
    /**
     * 属性类型
     */
    private Class<?> filedClazz;

    public int compareTo(ExcelHeader o) {
        return order - o.order;
    }

}
