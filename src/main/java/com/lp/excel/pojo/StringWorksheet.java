package com.lp.excel.pojo;

import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * sheet
 *
 * @author LiuPeng
 * @date 2017/3/27 10:27
 */
@Data
public class StringWorksheet {

    private String sheet;

    private long columnNum;

    private Long rowNum;

    private Long[] rowNums;

    private long[] rowNumss;

    private HashSet<Long> rowNumsss;

    private Set<?> rowNumssss;

    private List<String> title;

    private List<StringRow> rows;

}
