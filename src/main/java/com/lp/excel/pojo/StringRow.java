package com.lp.excel.pojo;

import lombok.Data;

import java.util.List;

/**
 * 行
 *
 * @author LiuPeng
 * @date 2017/3/27 10:26
 */
@Data
public class StringRow {

    private List<String> result;

}
