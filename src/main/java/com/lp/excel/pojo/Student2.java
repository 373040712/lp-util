package com.lp.excel.pojo;



import com.lp.excel.annotation.Excel;

import java.util.Date;


public class Student2 {

    @Excel(title = "学号", order = 1)
    private Long id;

    @Excel(title = "姓名", order = 2)
    private String name;

    @Excel(title = "入学日期", order = 3)
    private Date date;

    @Excel(title = "班级", order = 4)
    private Integer classes;

    @Excel(title = "是否开除", order = 5)
    private String expel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getClasses() {
        return classes;
    }

    public void setClasses(Integer classes) {
        this.classes = classes;
    }

    public String getExpel() {
        return expel;
    }

    public void setExpel(String expel) {
        this.expel = expel;
    }

    @Override
    public String toString() {
        return "Student2{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", date=" + date +
                ", classes=" + classes +
                ", expel='" + expel + '\'' +
                '}';
    }
}
