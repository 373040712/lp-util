package com.lp.ztest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/4/8 16:39
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserInfo {

    private String adbc;

    private String id;
}
