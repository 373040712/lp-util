package com.lp.ztest;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author peng.liu
 * @date 2019/4/23 11:27
 */
@Component
public class AbcLp extends Hellolp {
    @PostConstruct
    public final void abcInit() {
        System.out.println("AbcLp");
    }

    public void test() {
        System.out.println("xxxxxx");
    }

}
