package com.lp.ztest;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author liupeng@newhope.cn
 * @date 2022/01/06
 **/
public class AbcTest {

    public static void main(String[] args) {
        List<UserInfo> list = new ArrayList<>();
        list.add(new UserInfo("b2", "2"));
        list.add(new UserInfo("c1", "3"));
        list.add(new UserInfo("b", "1"));
        list.add(new UserInfo("a", "1"));
        list.add(new UserInfo("b1", "2"));
        list.add(new UserInfo("c", "1"));
        list.add(new UserInfo("d", "4"));
        List<UserInfo> unique = list.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(UserInfo::getId))), ArrayList::new)
        );
        System.out.println(unique);
        int index = IntStream.range(0, unique.size())
                .filter(i -> unique.get(i).getId().equals("1")).findFirst().orElse(-1);
        unique.add(0, unique.remove(index));
        System.out.println(unique);
    }
}
