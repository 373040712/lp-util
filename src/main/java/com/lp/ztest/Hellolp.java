package com.lp.ztest;

import javax.annotation.PostConstruct;

/**
 * @author peng.liu
 * @date 2019/4/23 11:26
 */

public abstract class Hellolp {
    @PostConstruct
    public final void helloInit() {
        System.out.println("Hellolp");
    }
}
