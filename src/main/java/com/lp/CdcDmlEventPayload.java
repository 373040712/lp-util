package com.lp;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 参考 https://wiki.ops.yunlizhi.cn/pages/viewpage.action?pageId=2523984
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CdcDmlEventPayload {
    private Action action;
    private Long timestamp;
    private JSONObject data;
    private JSONObject old;
    private Meta meta;

    public enum Action {
        INSERT,
        UPDATE,
        DELETE
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Meta {
        private String database;
        private String table;
        private Long xid;
        private Integer xoffset;
        private Boolean commit;
    }
}
