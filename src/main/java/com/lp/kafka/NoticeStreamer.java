package com.lp.kafka;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindowedKStream;
import org.apache.kafka.streams.kstream.TimeWindows;

import java.time.Duration;
import java.util.Properties;

/**
 * @author peng.liu
 * @date 2019/3/12 16:08
 */

public class NoticeStreamer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "notice-application");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "10.6.1.180:9096,10.6.1.183:9096,10.6.1.186:9096");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> kStream = builder.stream("ws_notice");
        kStream.foreach((key, value) -> System.out.println(value));

        TimeWindowedKStream<String, String> windowedKStream =
                kStream.groupByKey().windowedBy(TimeWindows.of(Duration.ofSeconds(30)));
        windowedKStream.count().toStream().to("ws_notice");
        KafkaStreams streams = new KafkaStreams(builder.build(), props);
        streams.start();
    }
}

