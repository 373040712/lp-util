package com.lp.datetime;

import org.joda.time.DateTime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author LP
 * @date 2018/7/2 16:29
 */

public class TestTime {
    public static void main(String[] args) throws InterruptedException {
        System.out.println(new Date(2019,9,1));
        DateTime dateTime = new DateTime(new Date(1545816119000L).getTime());
        System.out.println(dateTime.toString("yyyy-MM-dd HH:mm:ss"));
        TestTime test = new TestTime();
        test.doDateTime();
        test.doTimeDifference();
        test.doClock();
        test.doPeriod();
        test.doYear();
        test.doMonth();
    }

    private void doDateTime() {
        /**
         * LocalDate类是Java
         * 8中日期时间功能里表示一个本地日期的类，它的日期是无时区属性的。可以用来表示生日、节假日期等等。这个类用于表示一个确切的日期，而不是这个日期所在的时间
         */
        LocalDate localDate = LocalDate.now();
        LocalDate localDate2 = LocalDate.of(2015, 12, 31);

        /**
         * LocalTime类是Java
         * 8中日期时间功能里表示一整天中某个时间点的类，它的时间是无时区属性的（早上10点等等）。比如你需要描述学校几点开学，这个时间不涉及在什么城市，这个描述是对任何国家城市都适用的，
         * 此时使用无时区的LocalTime就足够了
         */
        LocalTime localTime = LocalTime.now();
        LocalTime localTime2 = LocalTime.of(21, 30, 59, 11001);

        /**
         * LocalDateTime类是Java 8中日期时间功能里，用于表示当地的日期与时间的类，它的值是无时区属性的。你可以将其视为Java
         * 8中LocalDate与LocalTime两个类的结合。 LocalDateTime类的值是不可变的，所以其计算方法会返回一个新的LocalDateTime实例。
         */
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime localDateTime2 = LocalDateTime.of(2015, 11, 26, 13, 55, 36, 123);

        /**
         * ZonedDateTime类是Java 8中日期时间功能里，用于表示带时区的日期与时间信息的类。可以用于表示一个真实事件的开始时间，如某火箭升空时间等等。
         * ZonedDateTime 类的值是不可变的，所以其计算方法会返回一个新的ZonedDateTime 实例。
         */
        ZonedDateTime dateTime = ZonedDateTime.now();
        ZoneId zoneId = ZoneId.of("UTC+1");
        ZonedDateTime dateTime2 = ZonedDateTime.of(2015, 11, 30, 23, 45, 59, 1234, zoneId);


        DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
        String formattedDate = formatter.format(LocalDate.now());
        System.out.println(formattedDate);
    }

    /**
     * When you write code to specify an amount of time, use the class or method that best meets
     * your needs: the Duration class, Period class, or the ChronoUnit.between method. A Duration
     * measures an amount of time using time-based values (seconds, nanoseconds). A Period uses
     * date-based values (years, months, days).
     * 
     * The ChronoUnit enum, discussed in the The Temporal Package, defines the units used to measure
     * time. The ChronoUnit.between method is useful when you want to measure an amount of time in a
     * single unit of time only, such as days or seconds. The between method works with all
     * temporal-based objects, but it returns the amount in a single unit only. The following code
     * calculates the gap, in milliseconds, between two time-stamps:
     */
    private void doTimeDifference() {
        LocalDate today = LocalDate.now();
        System.out.println("Today : " + today);
        LocalDate birthDate = LocalDate.of(1993, Month.OCTOBER, 19);
        System.out.println("BirthDate : " + birthDate);
        Period p = Period.between(birthDate, today);
        System.out.printf("年龄 : %d 年 %d 月 %d 日", p.getYears(), p.getMonths(), p.getDays());


        /**
         * 一个Duration对象表示两个Instant间的一段时间，一个Duration实例是不可变的，当创建出对象后就不能改变它的值了.
         *
         * 瞬时实例。
         */
        Instant inst1 = Instant.now();
        System.out.println("Inst1 : " + inst1);
        Instant inst2 = inst1.plus(Duration.ofSeconds(10));
        System.out.println("Inst2 : " + inst2);
        System.out.println("Difference in milliseconds : " + Duration.between(inst1, inst2).toMillis());
        System.out.println("Difference in seconds : " + Duration.between(inst1, inst2).getSeconds());


        LocalDate startDate = LocalDate.of(1993, Month.OCTOBER, 19);
        System.out.println("开始时间  : " + startDate);
        LocalDate endDate = LocalDate.of(2017, Month.JUNE, 16);
        System.out.println("结束时间 : " + endDate);
        long daysDiff = ChronoUnit.DAYS.between(startDate, endDate);
        System.out.println("两天之间的差在天数   : " + daysDiff);
    }

    private void doClock() throws InterruptedException {
        Clock systemDefaultClock = Clock.systemDefaultZone();
        System.out.println("Current DateTime with system default clock: " + LocalDateTime.now(systemDefaultClock));
        Clock systemUTCClock = Clock.systemUTC();

        System.out.println("Current DateTime with UTC clock: " + LocalDateTime.now(systemUTCClock));
        System.out.println("Current Instant  in UTC: " + Instant.now(systemUTCClock));

        // Clock ticks for every second.
        Clock tickClock = Clock.tick(Clock.systemDefaultZone(), Duration.ofSeconds(1));
        System.out.println("Tick Clock precision at seconds level " + LocalTime.now(tickClock));
        Thread.sleep(1000); // Delay by 1 second
        System.out.println("Tick Clock precision at seconds level " + LocalTime.now(tickClock));

        /**
         * Applications are required to be tested for certain future dated/past dated scenarios. To
         * facilitate such scenarios, we can use Offset Clock.
         */
        // Current Date - UTC clock : 2016-04-16T16:02:32.768
        Clock offsetClock = Clock.offset(Clock.systemUTC(), Duration.ofDays(10));
        System.out.println("Offset Clock future dated ( +10 days ) : " + LocalDateTime.now(offsetClock));
        Thread.sleep(1000); // Delay by 1 second
        System.out.println(
                "Offset Clock future dated ( +10 days ) after 1 Sec Delay : " + LocalDateTime.now(offsetClock));
    }

    /**
     * Period is the amount of time in different unit like year, month or days. An example of period
     * can be like 1 year 5 month 10 days.
     */
    private void doPeriod() {
        LocalDate start = LocalDate.now();
        System.out.println("Period.between:" + Period.between(start, LocalDate.MAX).getDays());
        System.out.println("Period.ofDays:" + Period.ofDays(5));
    }

    private void doYear() {
        System.out.println("Year.now():" + Year.now());
        System.out.println("Year.MAX_VALUE:" + Year.MAX_VALUE);
        System.out.println("Year.isLeap(2014):" + Year.isLeap(2014));
        System.out.println("Year.isLeap(2016):" + Year.isLeap(2016));

        System.out.println("YearMonth.now():" + YearMonth.now());
        System.out.println("getMonthValue():" + YearMonth.parse("2014-09").getMonthValue());
        System.out.println("getYear():" + YearMonth.parse("2014-09").getYear());
        System.out.println("isLeapYear():" + YearMonth.parse("2014-09").isLeapYear());
    }

    private void doMonth() {
        MonthDay mday = MonthDay.now();
        System.out.println(mday.getDayOfMonth());
        System.out.println(mday.getMonth());
        System.out.println(mday.atYear(2014));

        System.out.println(Month.MARCH);
        System.out.println(Month.MARCH.getValue());
        System.out.println(Month.of(3));
        System.out.println(Month.valueOf("MARCH"));
    }
}
