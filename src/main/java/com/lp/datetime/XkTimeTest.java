package com.lp.datetime;

import com.xkzhangsan.time.calculator.DateTimeCalculatorUtil;
import com.xkzhangsan.time.converter.DateTimeConverterUtil;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/2/28 8:26
 */
public class XkTimeTest {
    public static void main(String[] args) {
        DateTimeConverterUtil.toDate(LocalDateTime.now());
        DateTimeCalculatorUtil.startTimeOfDate(new Date());
    }
}
