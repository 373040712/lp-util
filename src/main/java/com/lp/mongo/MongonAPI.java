package com.lp.mongo;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

/**
 * @author peng.liu
 * @date 2019/1/21 0:44
 */

public class MongonAPI {

    public static void main(String[] args) {
        // 连接到 mongodb 服务，默认连接到localhost服务器，端口号为27017
        MongoClient mongoClient = new MongoClient();
        // 连接到数据库
        MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
        // 获取集合
        MongoCollection<Document> collection = mongoDatabase.getCollection("user");
        // 创建文档
        Document document = new Document("name", "张三").append("sex", "男").append("age", 18);
        collection.insertOne(document);
    }
}
