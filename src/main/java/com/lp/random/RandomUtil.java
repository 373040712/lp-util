package com.lp.random;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/11/28 22:14
 */
public class RandomUtil {

    public static int inRange(int min, int max) {
        Random r = new Random();
        return r.ints(min, (max + 1)).findFirst().getAsInt();
    }

    public static int threadInRange(int min, int max) {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        return r.nextInt(min, max);
    }
}
