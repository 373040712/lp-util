package com.lp.rabbitmq.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 基础扩展信息实体
 *
 * @author 刘胜-Felix
 * @date 2018年10月2018/10/18日17时12分
 * @since V1.1.0
 */
@Data
public class BaseExtDTO implements Serializable {
    private static final long serialVersionUID = 6261630494564697371L;
}
