package com.lp.rabbitmq.pojo;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息类型枚举
 *
 * @author 刘胜-Felix
 * @date 2018年10月2018/10/18日14时03分
 * @since V1.1.0
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum MessageTypeEnum {
    /**
     * 暂停或者终止外呼任务
     */
    STOP_OR_CLOSE_OC_TASK(1, "暂停或者终止外呼任务"),
    /**
     * 添加任务执行成员
     */
    ADD_OC_TASK_USER(2, "添加任务执行成员"),
    /**
     * 移除执行组成员
     */
    REMOVE_OC_TASK_EXECUTE_USER(3, "移除任务执行组成员"),
    /**
     * 任务明细执行状态变更
     */
    TASK_DATA_EXECUTE_STATUS_UPDATE(4, "任务执行状态变更"),
    /**
     * 增加用户任务执行明细
     */
    ADD_USER_EXECUTE_TASK_DATA(5, "增加用户任务执行明细"),
    /**
     * 移除用户任务执行明细
     */
    REMOVE_USER_EXECUTE_TASK_DATA(6, "移除用户任务执行明细");
    private Integer value;
    private String desc;
}
