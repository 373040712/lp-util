package com.lp.rabbitmq.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 增加用户任务明细
 *
 * @author 刘胜-Felix
 * @date 2018年10月2018/10/18日15时01分
 * @since V1.1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AddUserTaskDataExtDTO extends BaseExtDTO implements Serializable {
    private static final long serialVersionUID = -8206204227752887972L;
    /**
     * 对应任务id
     */
    @JSONField(serializeUsing = ToStringSerializer.class)
    private Long taskId;
    /**
     * 任务明细的ID列表
     */
    private List<Long> taskDataIds;
    /**
     * 未执行总数
     */
    private Long unExecutedCount;
    /**
     * 分配总数
     */
    private Long distributionCount;
    /**
     * 已拨打的数量
     */
    private Long dialedCount;

}
