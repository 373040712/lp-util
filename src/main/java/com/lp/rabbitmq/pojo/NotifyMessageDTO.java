package com.lp.rabbitmq.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 通知消息实体
 *
 * @author 刘胜-Felix
 * @date 2018年10月2018/10/18日14时02分
 * @since V1.1.0
 */
@Data
public class NotifyMessageDTO<T extends BaseExtDTO> implements Serializable {
    private static final long serialVersionUID = 7369630175054858739L;

    /**
     * to : 对应的员工用户ID source : 消息的来源 type : 消息类型 msg : 消息说明 version : 消息的版本 time : 时间 ext : {}
     */
    private List<Long> to;
    private String source;
    private MessageTypeEnum type;
    private String msg;
    private Long version;
    private Long time;
    private T ext;

    public NotifyMessageDTO(MessageTypeEnum type) {
        this.type = type;
    }

    public NotifyMessageDTO(List<Long> to, MessageTypeEnum type, String msg, T ext) {
        this.to = to;
        this.type = type;
        this.msg = msg;
        this.ext = ext;
    }
}
