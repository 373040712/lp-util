package com.lp.rabbitmq;

/**
 * @author peng.liu
 * @date 2019/3/13 9:54
 */

import com.alibaba.fastjson.JSONObject;
import com.lp.rabbitmq.pojo.AddUserTaskDataExtDTO;
import com.lp.rabbitmq.pojo.MessageTypeEnum;
import com.lp.rabbitmq.pojo.NotifyMessageDTO;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

public class Producer {

    public static void main(String[] args) throws IOException, TimeoutException {
        // 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();

        // 设置RabbitMQ相关信息
        factory.setHost("sa-all-dev-rabbitmq.56qq.com");
        factory.setUsername("oper");
        factory.setPassword("r8l&vhtI!O3t");
        factory.setVirtualHost("/oper");
        factory.setPort(5673);

        // 创建一个新的连接
        Connection connection = factory.newConnection();

        // 创建一个通道
        Channel channel = connection.createChannel();

        // 发送消息到队列中
        for (int i = 0; i < 100; i++) {
            String message = getMessage((long) i);
            channel.basicPublish("aios.exchange", "aios.notice.message.*", null, message.getBytes("UTF-8"));
            System.out.println("Producer Send +'" + message + "'");
        }
        // 关闭通道和连接
        channel.close();
        connection.close();
    }

    private static String getMessage(Long total) {
        NotifyMessageDTO<AddUserTaskDataExtDTO> notifyMessageDTO =
                new NotifyMessageDTO<>(MessageTypeEnum.ADD_USER_EXECUTE_TASK_DATA);
        AddUserTaskDataExtDTO addUserTaskDataExtDTO = new AddUserTaskDataExtDTO();
        addUserTaskDataExtDTO.setTaskId(123L);
        addUserTaskDataExtDTO.setTaskDataIds(Arrays.asList(123L, 234L));
        addUserTaskDataExtDTO.setDistributionCount(total);
        notifyMessageDTO.setExt(addUserTaskDataExtDTO);
        notifyMessageDTO.setTo(Arrays.asList(20006047L));
        notifyMessageDTO.setMsg(String.format("当前有%s条任务数据分配至你", addUserTaskDataExtDTO.getTaskDataIds().size()));
        notifyMessageDTO.setVersion(10L);
        notifyMessageDTO.setTime(System.currentTimeMillis());
        notifyMessageDTO.setSource("aios");
        return JSONObject.toJSONString(notifyMessageDTO);
    }
}
