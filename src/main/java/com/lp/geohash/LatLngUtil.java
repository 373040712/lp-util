package com.lp.geohash;


import com.google.common.base.Strings;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 经度纬度检查
 *
 * @author LP
 * @date 2018/4/2 20:04
 */
public class LatLngUtil {

    private static final double EARTH_RADIUS = 6378137;

    private static final Pair<Double, Double> CHINA_LAT = new ImmutablePair<>(3.0, 54.0);

    private static final Pair<Double, Double> CHINA_LNG = new ImmutablePair<>(73.0, 136.0);

    private final static String MAX_DISTANCE_STR = "Infinity";

    private final static double MIN_DISTANCE = 0.01;

    public static boolean isLegalLatAndLng(Double lat, Double lng) {
        if (lat == null || lng == null) {
            return false;
        }
        boolean latFlag = lat >= CHINA_LAT.getLeft() && lat <= CHINA_LAT.getRight();
        boolean lngFlag = lng >= CHINA_LNG.getLeft() && lng <= CHINA_LNG.getRight();
        return latFlag && lngFlag;
    }

    public static double getDistance(double lat1, double lng1, double lat2, double lng2) {
        try {
            double result;
            lat1 = lat1 * Math.PI / 180.0;
            lat2 = lat2 * Math.PI / 180.0;
            double a = lat1 - lat2;
            double b = (lng1 - lng2) * Math.PI / 180.0;
            double sa2, sb2;
            sa2 = Math.sin(a / 2.0);
            sb2 = Math.sin(b / 2.0);
            result = 2 * EARTH_RADIUS * Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1) * Math.cos(lat2) * sb2 * sb2));
            return new BigDecimal(result).setScale(2, RoundingMode.HALF_UP).doubleValue();
        } catch (Exception e) {
            throw e;
        }
    }

    public static Double getDistance(String distance) {
        if (Strings.isNullOrEmpty(distance)) {
            return null;
        }
        if (MAX_DISTANCE_STR.equals(distance) || !NumberUtils.isNumber(distance)) {
            return null;
        }
        double temp = new BigDecimal(distance).setScale(2, RoundingMode.HALF_UP).doubleValue();
        return temp < MIN_DISTANCE ? MIN_DISTANCE : temp;
    }
}
