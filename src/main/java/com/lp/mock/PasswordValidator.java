package com.lp.mock;

import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017-9-26 0026.
 */
@Component
public class PasswordValidator {

    public boolean verifyPassword(String password) {
        System.out.println(password);
        if (password == null || password.length() < 6) {
            return false;
        }
        return "123456".equals(password);
    }
}
