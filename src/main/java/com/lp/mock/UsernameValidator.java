package com.lp.mock;

import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2017-9-26 0026.
 */
@Component
public class UsernameValidator {

    public boolean verifyUsername(String username) {
        System.out.println(username);
        if (username == null || username.length() == 0) {
            return false;
        }
        return "lp".equals(username);
    }
}
