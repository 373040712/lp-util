package com.lp.mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author peng.liu
 * @date 2017/9/26 16:19
 */

@Component
public class LoginPresenter {

    @Autowired
    private UsernameValidator usernameValidator;

    @Autowired
    private PasswordValidator passwordValidator;

    @Autowired
    private UserManager mUserManager;

    public void login(String username, String password) {
        if (!usernameValidator.verifyUsername(username)) {
            return;
        }
        //假设我们对密码强度有一定要求，使用一个专门的validator来验证密码的有效性
        if (!passwordValidator.verifyPassword(password)) {
            return;
        }

        mUserManager.performLogin(username, password);
    }

    public void loginCallback(String username, String password) {
        if (!usernameValidator.verifyUsername(username)) {
            return;
        }
        //假设我们对密码强度有一定要求，使用一个专门的validator来验证密码的有效性
        if (!passwordValidator.verifyPassword(password)) {
            return;
        }

        //login的结果将通过callback传递回来。
        mUserManager.performLogin(username, password, new Callback() {

            public void onSuccess(Object data) {
                System.out.println(data);
            }

            public void onFailure(int code, String msg) {
                System.out.println(msg);
            }
        });
    }
}
