package com.lp.mock;

/**
 * Created by Administrator on 2017-9-26 0026.
 */
public interface Callback {

    void onSuccess(Object data);

    void onFailure(int code, String msg);
}
