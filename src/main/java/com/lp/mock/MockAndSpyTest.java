package com.lp.mock;

import com.google.common.collect.Lists;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * @author peng.liu
 * @date 2019/5/6 17:37
 */

public class MockAndSpyTest {

    @Test
    public void whenCreateMock_thenCreated() {
        List mockedList = mock(ArrayList.class);
        mockedList.add("one");
        verify(mockedList).add("one");
        assertEquals(0, mockedList.size());
    }

    @Test
    public void whenCreateSpy_thenCreate() {
        List spyList = Mockito.spy(new ArrayList());
        spyList.add("one");
        verify(spyList).add("one");
        assertEquals(1, spyList.size());
    }

    /**
     * 为Mock对象配置简单的返回值
     */
    @Test
    public final void whenMockReturnBehaviorIsConfigured_thenBehaviorIsVerified() {
        final MyList listMock = Mockito.mock(MyList.class);
        when(listMock.add(anyString())).thenReturn(false);
        final boolean added = listMock.add(randomAlphabetic(6));
        assertThat(added, is(false));
    }

    /**
     * 另一种配置返回值的方法
     */
    @Test
    public final void whenMockReturnBehaviorIsConfigured2_thenBehaviorIsVerified() {
        final MyList listMock = Mockito.mock(MyList.class);
        doReturn(false).when(listMock).add(anyString());
        final boolean added = listMock.add(randomAlphabetic(6));
        assertThat(added, is(false));
    }

    /**
     * 为mock对象的指定方法配置抛出异常
     */
    @Test(expected = IllegalStateException.class)
    public final void givenMethodIsConfiguredToThrowException_whenCallingMethod_thenExceptionIsThrown() {
        final MyList listMock = Mockito.mock(MyList.class);
        when(listMock.add(anyString())).thenThrow(IllegalStateException.class);
        listMock.add(randomAlphabetic(6));
    }

    /**
     * 为返回值为空的方法配置行为（抛出异常）
     */
    @Test(expected = NullPointerException.class)
    public final void whenMethodHasNoReturnType_whenConfiguringBehaviorOfMethod_thenPossible() {
        final MyList listMock = Mockito.mock(MyList.class);
        doThrow(NullPointerException.class).when(listMock).clear();
        listMock.clear();
    }

    /**
     * 为多次调用配置行为，第一次调用不抛出异常
     */
    @Test
    public final void givenBehaviorIsConfiguredToThrowExceptionOnSecondCall_whenCallingOnlyOnce_thenNoExceptionIsThrown() {
        final MyList listMock = Mockito.mock(MyList.class);
        when(listMock.add(anyString())).thenReturn(false).thenThrow(IllegalStateException.class);
        listMock.add(randomAlphabetic(6));
    }

    /**
     * 第二次调用时抛出异常
     */
    @Test(expected = IllegalStateException.class)
    public final void givenBehaviorIsConfiguredToThrowExceptionOnSecondCall_whenCallingTwice_thenExceptionIsThrown() {
        final MyList listMock = Mockito.mock(MyList.class);
        when(listMock.add(anyString())).thenReturn(false).thenThrow(IllegalStateException.class);
        listMock.add(randomAlphabetic(6));
        listMock.add(randomAlphabetic(6));
    }

    /**
     * 配置mock对象调用真实的方法
     */
    @Test
    public final void whenMockMethodCallIsConfiguredToCallTheRealMethod_thenRealMethodIsCalled() {
        final MyList listMock = Mockito.mock(MyList.class);
        when(listMock.size()).thenCallRealMethod();
        assertThat(listMock.size(), equalTo(1));
    }

    /**
     * 为mock方法配置自定义的回答
     */
    @Test
    public final void whenMockMethodCallIsConfiguredWithCustomAnswer_thenRealMethodIsCalled() {
        final MyList listMock = Mockito.mock(MyList.class);
        doAnswer(invocation -> "Always the same").when(listMock).get(anyInt());
        final String element = listMock.get(1);
        assertThat(element, is(equalTo("Always the same")));
    }

    @Test
    public void testVerify() {
        // 验证Mock对象的简单调用
        List<String> mockedList1 = mock(MyList.class);
        mockedList1.size();
        verify(mockedList1).size();

        // 验证Mock对象的调用次数
        List<String> mockedList2 = mock(MyList.class);
        mockedList2.size();
        verify(mockedList2, times(1)).size();

        // 验证整个Mock对象的都没有被调用
        List<String> mockedList3 = mock(MyList.class);
        verifyZeroInteractions(mockedList3);

        // 验证Mock对象的某个方法没有被调用
        List<String> mockedList4 = mock(MyList.class);
        verify(mockedList4, times(0)).size();

        // 验证Mock对象的没有意料之外的调用--此案例会失败
        List<String> mockedList5 = mock(MyList.class);
        mockedList5.size();
        mockedList5.clear();
        verify(mockedList5).size();
        verifyNoMoreInteractions(mockedList5);

        // 验证调用顺序
        List<String> mockedList6 = mock(MyList.class);
        mockedList6.size();
        mockedList6.add("a parameter");
        mockedList6.clear();

        InOrder inOrder = Mockito.inOrder(mockedList6);
        inOrder.verify(mockedList6).size();
        inOrder.verify(mockedList6).add("a parameter");
        inOrder.verify(mockedList6).clear();

        // 验证调用没有发生
        List<String> mockedList7 = mock(MyList.class);
        mockedList7.size();
        verify(mockedList7, never()).clear();

        // 验证某个方法至少或者之多被调用的次数
        List<String> mockedList8 = mock(MyList.class);
        mockedList8.clear();
        mockedList8.clear();
        mockedList8.clear();

        verify(mockedList8, atLeast(1)).clear();
        verify(mockedList8, atMost(10)).clear();

        // 准确验证方法调用时的参数
        List<String> mockedList9 = mock(MyList.class);
        mockedList9.add("test");
        verify(mockedList9).add("test");

        // 使用flexible和any参数
        List<String> mockedList10 = mock(MyList.class);
        mockedList10.add("test");
        verify(mockedList10).add(anyString());

        // 使用参数捕获器
        List<String> mockedList = mock(MyList.class);
        mockedList.addAll(Lists.newArrayList("someElement"));
        ArgumentCaptor<List> argumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(mockedList).addAll(argumentCaptor.capture());
        List<String> capturedArgument = argumentCaptor.<List<String>>getValue();
        assertThat(capturedArgument, CoreMatchers.hasItem("someElement"));
    }

    public static class MyList extends AbstractList<String> {
        @Override
        public String get(final int index) {
            return null;
        }

        @Override
        public int size() {
            return 0;
        }
    }
}
