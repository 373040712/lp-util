package com.lp.mock;

import org.springframework.stereotype.Component;

/**
 * @author peng.liu
 * @date 2017/9/26 16:16
 */
@Component
public class UserManager {

    public void performLogin(String username, String password) {
        // TODO: 2017/9/26 测试
    }

    public void performLogin(String username, String password, Callback callback) {
        if ("lp".equals(username) && "123456".equals(password)) {
            callback.onSuccess("成功");
        } else {
            callback.onFailure(500, "失败");
        }
    }
}
