package com.lp.tree;

/**
 * Created by Administrator on 2017-10-18 0018.
 */
public interface ITreeNode {

    String getNodeId();

    String getParentNodeId();
}
