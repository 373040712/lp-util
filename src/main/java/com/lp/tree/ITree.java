package com.lp.tree;

import com.lp.tree.impl.TreeNode;

import java.util.List;

/**
 * Created by Administrator on 2017-10-18 0018.
 */
public interface ITree {

    List<TreeNode> getRoot();

    TreeNode getTreeNode(String nodeId);

    ITreeNode getTreeNodeObject(String nodeId);
}
