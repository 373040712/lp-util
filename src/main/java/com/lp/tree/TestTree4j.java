package com.lp.tree;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.lp.common.pojo.Org;
import com.lp.tree.impl.Tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017-10-18 0018.
 */
public class TestTree4j {

    public static void main(String[] args) {
        Tree tree = new Tree(genFayOrgList());
        SimplePropertyPreFilter filter = new SimplePropertyPreFilter(); // 构造方法里，也可以直接传需要序列化的属性名字
        filter.getExcludes().add("parent");
        filter.getExcludes().add("children");
        System.out.println(JSONObject.toJSONString(tree.getRoot(), filter));
    }

    public static List<ITreeNode> genFayOrgList() {
        List<ITreeNode> list = new ArrayList<ITreeNode>();

        Org org = new Org("2", "1", "北京市", "110000", "2");
        list.add(org);
        org = new Org("3", "2", "市辖区", "110100", "3");
        list.add(org);
        org = new Org("4", "3", "东城区", "110101", "4");
        list.add(org);
        org = new Org("5", "3", "东城区", "110102", "4");
        list.add(org);
        org = new Org("6", "3", "东城区", "110105", "4");
        list.add(org);
        org = new Org("7", "3", "东城区", "110106", "4");
        list.add(org);
        org = new Org("8", "3", "东城区", "110107", "4");
        list.add(org);
        org = new Org("9", "3", "东城区", "110108", "4");
        list.add(org);
        org = new Org("10", "3", "东城区", "110109", "4");
        list.add(org);
        org = new Org("11", "3", "东城区", "110111", "4");
        list.add(org);
        org = new Org("12", "3", "东城区", "110112", "4");
        list.add(org);
        org = new Org("13", "3", "东城区", "110113", "4");
        list.add(org);
        org = new Org("14", "3", "东城区", "110114", "4");
        list.add(org);
        org = new Org("15", "3", "东城区", "110115", "4");
        list.add(org);
        org = new Org("16", "3", "东城区", "110116", "4");
        list.add(org);
        org = new Org("17", "3", "东城区", "110117", "4");
        list.add(org);
        org = new Org("18", "2", "县", "110200", "3");
        list.add(org);
        org = new Org("19", "18", "密云县", "110228", "4");
        list.add(org);
        org = new Org("20", "18", "延庆县", "110229", "4");
        list.add(org);
        return list;
    }
}
