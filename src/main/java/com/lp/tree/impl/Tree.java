package com.lp.tree.impl;


import com.lp.tree.ITree;
import com.lp.tree.ITreeNode;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by Administrator on 2017-10-18 0018.
 */
public class Tree implements ITree {

    private HashMap<String, TreeNode> treeNodeMap = new LinkedHashMap<String, TreeNode>();

    private List<TreeNode> rootList = new ArrayList<TreeNode>();

    public Tree(Collection<ITreeNode> list) {
        initTree(list);
    }

    private void initTree(Collection<ITreeNode> treeNodes) {
        for (ITreeNode temp : treeNodes) {
            TreeNode treeNode = new TreeNode(temp);
            treeNodeMap.put(treeNode.getNodeId(), treeNode);
        }
        for (TreeNode temp : treeNodeMap.values()) {
            if (StringUtils.isBlank(temp.getParentNodeId())) {
                rootList.add(temp);
            } else {
                TreeNode parentTreeNode = treeNodeMap.get(temp.getParentNodeId());
                if (parentTreeNode != null) {
                    temp.setParent(parentTreeNode);
                    parentTreeNode.addChild(temp);
                } else {
                    rootList.add(temp);
                }
            }
        }
    }


    public List<TreeNode> getRoot() {
        return this.rootList;
    }

    public TreeNode getTreeNode(String nodeId) {
        return treeNodeMap.get(nodeId);
    }

    public ITreeNode getTreeNodeObject(String nodeId) {
        return treeNodeMap.get(nodeId).getNodeObject();
    }
}
