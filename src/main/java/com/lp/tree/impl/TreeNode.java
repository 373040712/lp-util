package com.lp.tree.impl;

import com.lp.tree.ITreeNode;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017-10-18 0018.
 */
@Data
public class TreeNode implements ITreeNode {

    private TreeNode parent;

    private ITreeNode nodeObject;

    private List<TreeNode> children = new ArrayList<TreeNode>();


    public TreeNode(ITreeNode treeNode) {
        this.nodeObject = treeNode;
    }

    public void addChild(TreeNode treeNode) {
        this.children.add(treeNode);
    }

    public void removeChild(TreeNode treeNode) {
        this.children.remove(treeNode);
    }

    public String getNodeId() {
        return this.nodeObject.getNodeId();
    }

    public String getParentNodeId() {
        return this.nodeObject.getParentNodeId();
    }
}
