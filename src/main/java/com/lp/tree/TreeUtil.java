package com.lp.tree;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author peng.liu
 * @date 2017/10/19 14:55
 */

public class TreeUtil {

    public static List<Node> getTreeRoot(Collection<Node> nodes) {
        List<Node> rootList = new ArrayList<Node>();
        HashMap<String, Node> nodeMap = new LinkedHashMap<String,Node>();
        for (Node temp : nodes) {
            nodeMap.put(temp.getNodeId(), temp);
        }
        for (Node temp : nodeMap.values()) {
            if (StringUtils.isBlank(temp.getParentNodeId())) {
                rootList.add(temp);
            } else {
                Node parentNode = nodeMap.get(temp.getParentNodeId());
                if (parentNode != null) {
                    parentNode.addChild(temp);
                } else {
                    rootList.add(temp);
                }
            }
        }
        return rootList;
    }
}
