package com.lp.tree;

import java.util.List;

/**
 * @author peng.liu
 * @date 2017/10/19 14:50
 */

public interface Node {

    /**
     * 节点id
     *
     * @return
     */
    String getNodeId();

    /**
     * 节点父id
     *
     * @return
     */
    String getParentNodeId();

    /**
     * 获得孩子节点
     * 
     * @return
     */
    List<Node> getChildren();

    /**
     * 增加孩子节点
     *
     * @return
     */
    void addChild(Node node);

    /**
     * 移除孩子节点
     *
     * @return
     */
    void removeChild(Node node);
}
