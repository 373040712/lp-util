package com.lp.xpath;

import com.lp.xpath.json2xml.JsonXmlReader;
import com.lp.xpath.model.JXDocument;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.InputSource;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.List;

/**
 * @author LP
 * @date 2018/1/12 15:45
 */

public class JXDocumentTest {

    public static void main(String[] args) throws Exception {
        JXDocumentTest jxDocument = new JXDocumentTest();
        // jxDocument.testSel();
        jxDocument.testJsonSel();
    }

    public void testSel() throws Exception {
        String html =
                "<html><body><script>console.log('aaaaa')</script><div class='test'>搜易贷致力于普惠金融，专注于互联网投资理财与小额贷款，搭建中国最大、用户体验最好的个人及中小企业的互联网信贷平台</div><div class='xiao'>Two</div></body></html>";
        JXDocument underTest = new JXDocument(html);
        String xpath = "//script[1]/text()";
        List<Object> res = underTest.sel(xpath);
        System.out.println(StringUtils.join(res, ","));
    }

    public void testJsonSel() throws Exception {
        String json =
                "{\"root\":{\"sendInfo\":{\"frequentlySendRoute\":\"22\",\"lastSendTime\":1515666511139,\"sendDensity\":334,\"sendSatisfaction\":1,\"sendLostDays\":12345,\"frequentlySendCargoType\":\"type\"},\"avatarUrl\":\"http://www.baidu.com\",\"registerTime\":\"2018-01-11T10:28:31.000Z\",\"companyInfo\":{\"companyName\":\"四川省有限公司\",\"companyAddress\":\"四川省成都市武侯区天府大道\"},\"authStatus\":1,\"mobile\":\"13242425475\",\"icNo\":\"565161\",\"updateTime\":\"2018-01-11T05:58:35.000Z\",\"type\":1,\"userId\":25558,\"domainId\":1,\"uid\":\"1_25558\",\"realName\":\"abc\",\"lastLoginTime\":\"2018-01-11T10:28:31.000Z\",\"bindInfo\":{\"bindEmployeeName\":\"test\",\"isBinding\":true,\"bindEmployeeUserId\":445,\"bindEmployeeWorkNo\":\"324324\"},\"registerLocation\":{\"lon\":33,\"lat\":47},\"insuranceInfo\":{\"isBuyInsuranceLast90Days\":true,\"userType\":22,\"isBuyVouchLast90Days\":false},\"feeInfo\":{\"finishStatus\":1,\"openFeeStatus\":2},\"timestamp\":\"2018-01-12T08:08:00.687Z\",\"positionInfo\":{\"address\":\"test\",\"countyId\":32,\"lastUploadPosition\":{\"lon\":22,\"lat\":45},\"cityId\":12,\"provinceId\":12},\"version\":\"1\",\"id\":18,\"username\":\"2122\",\"status\":1}}";
        String xml = convertToXml(json);
        System.out.println(xml);
        JXDocument underTest = new JXDocument(convertToXml(json));
        String xpath = "//root/uid/text()|//root/positioninfo/provinceid[text()>10]/text()";
        List<Object> res = underTest.sel(xpath);
        System.out.println(StringUtils.join(res, ","));
    }

    private String convertToXml(final String json) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        InputSource source = new InputSource(new StringReader(json));
        Result result = new StreamResult(out);
        transformer.transform(new SAXSource(new JsonXmlReader(), source), result);
        return new String(out.toByteArray());
    }
}
