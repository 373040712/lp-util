package com.lp.executor.impl;

import com.google.common.collect.ImmutableSet;
import com.lp.executor.AbstractPackageService;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

import static com.lp.executor.Util.*;
import static java.util.concurrent.CompletableFuture.allOf;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/8 18:26
 */
public class MultiRepoPackageService extends AbstractPackageService {

    public static void main(final String... args) throws Exception {
        final boolean result = new MultiRepoPackageService().pack(oid, pid);
        logger.log("allocated: %s", result);
    }

    @Override
    protected Collection<String> getRepos() {
        return ImmutableSet.of("010", "020", "021", "023");
    }

    @Override
    protected void startPick(String pid) {
        final CompletableFuture[] queries = new CompletableFuture[repos.size()];
        final Iterator<String> iter = repos.iterator();
        for (int i = 0; i < queries.length; i++) {
            queries[i] = startPickFromOneRepo(iter.next(), pid);
        }

        allOf(queries).whenCompleteAsync(this::completeAllocate);
    }
}