package com.lp.executor.impl;

import com.google.common.collect.ImmutableSet;
import com.lp.executor.AbstractPackageService;

import java.util.Collection;

import static com.lp.executor.Util.*;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/8 18:23
 */
public class SingleRepoPackageService extends AbstractPackageService {

    private final String repo = "021";

    public static void main(String... args) throws Exception {
        final boolean result = new SingleRepoPackageService().pack(oid, pid);
        logger.log("allocated: %s", result);
    }

    @Override
    protected Collection<String> getRepos() {
        return ImmutableSet.of(repo);
    }

    @Override
    protected void startPick(String pid) {
        startPickFromOneRepo(repo, pid).whenComplete(this::completeAllocate);
    }
}