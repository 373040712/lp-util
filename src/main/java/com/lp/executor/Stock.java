package com.lp.executor;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/8 18:20
 */
class Stock {
    String repo;
    int count;

    public Stock(String repo, int count) {
        this.repo = repo;
        this.count = count;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Stock{");
        sb.append("repo='").append(repo).append('\'');
        sb.append(", count=").append(count);
        sb.append('}');
        return sb.toString();
    }
}
