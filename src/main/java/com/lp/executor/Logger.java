package com.lp.executor;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/8 18:11
 */
public class Logger {

    private static final Logger INSTANCE = new Logger();

    private long started;

    private Logger() {
        started = System.nanoTime();
    }

    static Logger getInstance() {
        return INSTANCE;
    }

    Logger start() {
        started = System.nanoTime();
        return this;
    }

    public void log(String s, Object... args) {
        if (args == null) {
            args = new Object[0];
        }

        //+ "\t<<<%s>>>%n";
        final String formatS = "[%4dms] " + s + "%n";
        final int argLength = args.length + 2;


        final Object[] args2 = new Object[argLength];
        args2[0] = (System.nanoTime() - started) / 1_000_000;
        System.arraycopy(args, 0, args2, 1, args.length);
        args2[argLength - 1] = Thread.currentThread().getName();

        System.out.format(formatS, args2);
    }
}
