package com.lp.executor;

/**
 * https://www.jianshu.com/p/d70a18fc4154
 * <p>
 * 这是一个虚构的订单分拣的例子：
 * <p>
 * 一个订单只包括一个商品，数量不限（>=1）
 * <p>
 * 4个仓库中心（010，020，021，023），包含每个商品在具体哪个货架多少数量等明细
 * <p>
 * 仓库不总是可用的（譬如错峰放假、运送区域不支持等）
 * 有一个全局的库存汇总（无明细，可快速查询全国某商品的总数）
 * <p>
 * 为了提高分库扣减库存的有效性
 * <p>
 * 先查询总库，只有总库存数量足够，才开始进行分库库存分拣
 * 在分拣的同时，订单可能被撤销。一旦撤销，本次分拣结束
 * <p>
 * 为使代码简单
 * <p>
 * 库存分拣只是查询分库库存
 * <p>
 * 库存回撤没有任何实现
 * <p>
 * 实现是有状态的，所以不是线程安全的
 *
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/8 18:07
 */
public interface PackageService {

    boolean pack(String oid, String pid);

}
