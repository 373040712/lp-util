package com.lp.executor;

import static com.lp.executor.Util.*;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/8 18:13
 */
public class EventService {

    public void listenOrderCancel(String order) {
        delay(2000, 300);
        logger.log("cancelled with no reason");
    }
}
