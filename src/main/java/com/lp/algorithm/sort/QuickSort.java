package com.lp.algorithm.sort;

import org.junit.Test;

/**
 * 快速排序
 * 
 * @author peng.liu
 * @date 2019/6/3 16:50
 */
public class QuickSort {

    public void quickSort(int[] arr, int start, int end) {
        if (start > end) {
            return;
        }
        int tmp = arr[start];
        int i = start;
        int j = end;
        while (i < j) {
            while (tmp <= arr[j] && i < j) {
                j--;
            }
            while (tmp >= arr[i] && i < j) {
                i++;
            }
            // 如果满足条件则交换
            if (i < j) {
                int t = arr[j];
                arr[j] = arr[i];
                arr[i] = t;
            }
        }
        arr[start] = arr[i];
        arr[i] = tmp;

        // 递归调用左半数组
        quickSort(arr, start, j - 1);
        // 递归调用右半数组
        quickSort(arr, j + 1, end);
    }

    @Test
    public void test() {
        int[] a = {49, 38, 65, 97, 76, 13, 27, 50};
        quickSort(a, 0, a.length - 1);
        System.out.println("排好序的数组：");
        for (int e : a) {
            System.out.print(e + " ");
        }
    }
}
