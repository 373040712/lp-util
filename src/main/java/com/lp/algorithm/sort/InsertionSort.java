package com.lp.algorithm.sort;

import org.junit.Test;

/**
 * 插入排序
 * 
 * @author peng.liu
 * @date 2019/6/3 17:25
 */
public class InsertionSort {

    public void insertionSort(int[] array) {
        if (array.length == 0) {
            return;
        }
        int current;
        for (int i = 0; i < array.length - 1; i++) {
            current = array[i + 1];
            int preIndex = i;
            while (preIndex >= 0 && current < array[preIndex]) {
                array[preIndex + 1] = array[preIndex];
                preIndex--;
            }
            array[preIndex + 1] = current;
        }
    }

    @Test
    public void test() {
        int[] a = {49, 38, 65, 97, 76, 13, 27, 50};
        insertionSort(a);
        System.out.println("排好序的数组：");
        for (int e : a) {
            System.out.print(e + " ");
        }
    }
}
