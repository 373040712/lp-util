package com.lp.compress;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;

/**
 * @author peng.liu
 * @date 2019/6/17 18:01
 */

public class AESUtils {

    /**
     * 加密
     * 
     * @param content 待加密内容
     * @param password 加密密钥
     * @return
     */
    public static byte[] encrypt(String content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            byte[] byteContent = content.getBytes("utf-8");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] result = cipher.doFinal(byteContent);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     * 
     * @param content 待解密内容
     * @param password 解密密钥
     * @return
     */
    public static byte[] decrypt(byte[] content, String password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128, new SecureRandom(password.getBytes()));
            SecretKey secretKey = kgen.generateKey();
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] result = cipher.doFinal(content);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 编码函数
    public static String encode(String content, String key) throws Exception {
        byte[] encrypt = encrypt(content, key);
        return Base64.encode(encrypt);
    }

    // 解码函数
    public static String decode(String encode, String key) throws Exception {
        byte[] encrypt = Base64.decode(encode);
        byte[] content = decrypt(encrypt, key);
        return new String(content);
    }

    // 0-正常使用
    public static void main(String[] args) throws Exception {
        String content = "xflush3.0个人感觉最大的特点就是监控配置非常灵活，从日志的格式定义、收集、配置，都可以自定义；这样对于老应用的打点日志，不需要关心规则的定义就可以平滑的接入该平台。 本篇幅以AE detail的应用为例，介绍一部分业务监控规则的配置方式以及遇到的一些坑。xflush3.0个人感觉最大的特点就是监控配置非常灵活，从日志的格式定义、收集、配置，都可以自定义；这样对于老应用的打点日志，不需要关心规则的定义就可以平滑的接入该平台。";
        String password = "12345678";

        System.out.println("加密前1：" + content);
        byte[] encryptResult1 = encrypt(content, password); // 普通加密
        byte[] decryptResult1 = decrypt(encryptResult1, password); // 普通解密
        System.out.println("解密后1：" + new String(decryptResult1));

        System.out.println("\n加密前2：" + content);
        String encryptResult2 = encode(content, password); // 先编码再加密
        System.out.println("加密后2：" + encryptResult2);
        String decryptResult2 = decode(encryptResult2, password); // 先解码再解密
        System.out.println("解密后2：" + decryptResult2);
    }
}
