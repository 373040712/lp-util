package com.lp.compress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author peng.liu
 * @date 2019/6/16 13:57
 */
public class GzipUtils {

    /**
     * 对字符串进行gzip压缩
     *
     * @param data
     * @return
     * @throws IOException
     */
    public static String compress(String data) throws IOException {
        if (null == data || data.length() <= 0) {
            return data;
        }
        // 创建一个新的byte数组输出流
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // 使用默认缓冲区大小创建新的输出流
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        // 将b.length个字节写入此输出流
        gzip.write(data.getBytes());
        gzip.flush();
        gzip.close();

        // 使用指定的charsetName，通过解码字节将缓冲区内容转换为字符串
        return out.toString("ISO-8859-1");
    }

    /**
     * 对字符串进行解压缩
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String unCompress(String data) throws Exception {
        if (null == data && data.length() <= 0) {
            return data;
        }
        // 创建一个新的byte数组输出流
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // 创建一个byte数组输入流
        ByteArrayInputStream in = new ByteArrayInputStream(data.getBytes("ISO-8859-1"));
        // 创建gzip输入流
        GZIPInputStream gzip = new GZIPInputStream(in);
        byte[] buf = new byte[1024];
        int len = 0;
        while ((len = gzip.read(buf)) >= 0) {
            out.write(buf, 0, len);
        }
        // 使用指定的 charsetName，通过解码字节将缓冲区内容转换为字符串
        return out.toString("UTF-8");
    }

    public static void main(String[] args) throws Exception {
        // URL url = ResourceUtils.getURL("classpath:demo.txt");
        // List<String> list = Files.readAllLines(Paths.get(url.toURI()));
        // StringBuilder sb = new StringBuilder();
        // for (int i = 0; i < 1000; i++) {
        // for (int j = 0; j < list.size(); j++) {
        // sb.append(list.get(j).trim());
        // }
        // }
        StringBuilder sb = new StringBuilder();
        sb.append("xflush3.0个人感觉最大的特点就是监控配置非常灵活，从日志的格式定义、收集、配置，都可以自定义；这样对于老应用的打点日志，不需要关心规则的定义就可以平滑的接入该平台。 本篇幅以AE detail的应用为例，介绍一部分业务监控规则的配置方式以及遇到的一些坑。xflush3.0个人感觉最大的特点就是监控配置非常灵活，从日志的格式定义、收集、配置，都可以自定义；这样对于老应用的打点日志，不需要关心规则的定义就可以平滑的接入该平台。");
        System.out.println("原来：" + sb.toString().length());
        long start = System.currentTimeMillis();
        String temp = compress(sb.toString());
        System.out.println("压缩：" + temp.length());
        System.out.println(temp);
        long end = System.currentTimeMillis();
        System.out.println("时间：" + (end - start));
        String result = unCompress(temp);
        System.out.println("解压：" + result.length());
        System.out.println(result);
        System.out.println("时间：" + (System.currentTimeMillis() - end));
    }
}
