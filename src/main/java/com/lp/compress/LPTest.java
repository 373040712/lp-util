package com.lp.compress;

import com.alibaba.fastjson.JSONObject;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import org.springframework.util.ResourceUtils;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author peng.liu
 * @date 2019/6/16 15:08
 */

public class LPTest {

    public static void main(String[] args) throws Exception {
        URL url = ResourceUtils.getURL("classpath:demo.txt");
        List<String> list = Files.readAllLines(Paths.get(url.toURI()));
        StringBuilder sb = new StringBuilder();
        list.forEach(it->sb.append(it.trim()).append("\r\n"));
        System.out.println(sb.toString());

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ruleScript",sb.toString());
        System.out.println(jsonObject.toJSONString());
//        for (int i = 0; i < 1000; i++) {
//            for (int j = 0; j < list.size(); j++) {
//                sb.append(list.get(j).trim());
//            }
//        }
//        String str = sb.toString();
//        gzip(str);
//        lz4(str);
    }

    private static void gzip(String str) throws Exception {
        System.out.println("GZIP压缩开始------------------");
        System.out.println("原来：" + str.length());
        byte[] data = str.getBytes("UTF-8");
        System.out.println("原来byte：" + data.length);

        long start = System.currentTimeMillis();
        String temp = GzipUtils.compress(str);
        System.out.println("压缩：" + temp.length());
        System.out.println("压缩byte：" + temp.getBytes("UTF-8").length);
        long end = System.currentTimeMillis();
        System.out.println("压缩时间：" + (end - start));

        String result = GzipUtils.unCompress(temp);
        System.out.println("解压结果：" + result.equals(str));
        System.out.println("解压时间：" + (System.currentTimeMillis() - end));
    }

    private static void lz4(String str) throws Exception {
        System.out.println("LZ4压缩开始------------------");
        System.out.println("原来：" + str.length());
        byte[] data = str.getBytes("UTF-8");
        System.out.println("原来byte：" + data.length);

        LZ4Factory factory = LZ4Factory.fastestInstance();
        final int decompressedLength = data.length;
        long start = System.currentTimeMillis();
        LZ4Compressor compressor = factory.fastCompressor();
        int maxCompressedLength = compressor.maxCompressedLength(decompressedLength);
        byte[] compressed = new byte[maxCompressedLength];
        int compressedLength = compressor.compress(data, 0, decompressedLength, compressed, 0, maxCompressedLength);
        System.out.println("压缩：" + new String(compressed).length());
        System.out.println("压缩byte：" + compressed.length);
        long end = System.currentTimeMillis();
        System.out.println("压缩时间：" + (end - start));

        LZ4FastDecompressor decompressor = factory.fastDecompressor();
        byte[] restored = new byte[decompressedLength];
        int compressedLength2 = decompressor.decompress(compressed, 0, restored, 0, decompressedLength);
        System.out.println("解压结果：" + new String(restored).equals(str));
        System.out.println("解压时间：" + (System.currentTimeMillis() - end));

    }
}
