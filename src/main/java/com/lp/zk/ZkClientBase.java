package com.lp.zk;

import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.apache.commons.lang3.StringUtils;

/**
 * @author peng.liu
 * @date 2019/3/5 15:17
 */

public class ZkClientBase {

    /** zookeeper地址 */
    private static final String CONNECT_ADDR = "172.17.33.217:2181";
    /** session超时时间 */
    private static final int SESSION_OUTTIME = 10000;

    private static ZkClient zkc = new ZkClient(new ZkConnection(CONNECT_ADDR), SESSION_OUTTIME);


    public static void main(String[] args) throws Exception {
        zkc.deleteRecursive("/a");
        create("/b/c", "123", true, true);
        create("/b/c", "345", true, true);
        create("/a", "123", false, true);
        create("/a/b/c/d", "123", false, true);
        System.out.println("");
    }

    public static void create(String path, String data, boolean isEphemeral, boolean isSequential) {
        if (StringUtils.isBlank(path)) {
            return;
        }
        String prePath = path.substring(0, path.lastIndexOf("/"));
        if (StringUtils.isNotBlank(prePath)) {
            zkc.createPersistent(prePath, true);
        }
        if (zkc.exists(path)) {
            zkc.writeData(path, data);
            return;
        }
        if (isEphemeral) {
            if (isSequential) {
                zkc.createEphemeralSequential(path, data);
            } else {
                zkc.createEphemeral(path, data);
            }
        } else {
            if (isSequential) {
                zkc.createPersistentSequential(path, data);
            } else {
                zkc.createPersistent(path, data);
            }
        }
    }
}

