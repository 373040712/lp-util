package com.lp.function;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.lp.common.pojo.Account;
import com.lp.common.pojo.Moon;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

/**
 * @author peng.liu
 * @date 2017/11/16 14:05
 */

public class TestJava8 {

    public static void main(String[] args) {
        TestJava8 testJava8 = new TestJava8();
        // testJava8.testCommon();
        // testJava8.testMinMax();
        // testJava8.testCount();
        // testJava8.testString();
        // testJava8.testThreadLocal();
        // testJava8.testDesignMode();
        testJava8.testDistinct();
    }

    public void testCommon() {
        List<Integer> list = Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 9, 9);
        list.parallelStream().forEach(System.out::print);
        System.out.println();
        list.parallelStream().forEachOrdered(System.out::print);

        System.out.println("-------------" + list.stream().filter(e -> e > 100).collect(toMap(e -> e, e -> e)));
        Function<Integer, Integer> maxInt = e -> e;
        Optional<Integer> temp1 = list.stream().collect(Collectors.maxBy(Comparator.comparing(maxInt)));
        Optional<Integer> temp2 = list.stream().max(Comparator.comparing(maxInt));
        Optional<Integer> temp3 = list.stream().reduce((x, y) -> x > y ? x : y);
        System.out.println("result: " + temp1.get());
        System.out.println("result: " + temp2.get());
        System.out.println("result: " + temp3.get());

        double averaging = list.stream().collect(Collectors.averagingInt(e -> e));
        System.out.println("result: " + averaging);

        IntSummaryStatistics summaryStatistics = list.stream().mapToInt(e -> e).summaryStatistics();
        System.out.printf("Max: %d, Min: %d, Ave: %f, Sum: %d", summaryStatistics.getMax(), summaryStatistics.getMin(),
                summaryStatistics.getAverage(), summaryStatistics.getSum());
        System.out.println();

        String strInt = list.stream().map(e -> e.toString()).collect(Collectors.joining(",", "[", "]"));
        System.out.println("result: " + strInt);

        Function<Integer, String> intStr = (Integer e) -> e % 2 != 0 ? "odd" : "even";
        // mapping 允许在收集器的容器上执行类似 map 的操作
        Map<String, List<String>> intMap =
                list.stream().collect(groupingBy(intStr, mapping(e -> e.toString() + "lp", toList())));
        System.out.println("result: " + intMap);

        Map<String, String> strMap = new HashMap<>();
        intMap.forEach((key, value) -> strMap.put(key, value.stream().peek(e -> System.out.println("stream: " + e))
                .collect(Collectors.joining(",", "[", "]"))));
        System.out.println("result: " + strMap);
    }

    public void testDesignMode() {
        Moon moon = new Moon();
        moon.startSpying(name -> {
            if (name.contains("Apollo")) {
                System.out.println("We made it!");
            }
        });
        moon.startSpying(name -> {
            if (name.contains("Apollo")) {
                System.out.println("They're distracted, lets invade earth!");
            }
        });
        moon.land("An asteroid");
        moon.land("Apollo 11");
    }

    public void testThreadLocal() {
        ThreadLocal<Integer> thisInt = ThreadLocal.withInitial(() -> new Random().nextInt());
        System.out.println(thisInt.get());
    }

    /**
     * 取集合最小值
     */
    public void testMinMax() {
        Account a1 = new Account();
        Account a2 = new Account();
        Account a3 = new Account();
        Account a4 = new Account();
        Account a5 = new Account();
        Account a6 = new Account();
        a1.setId(1);
        a2.setId(2);
        a3.setId(3);
        a4.setId(4);
        List<Account> accounts = Lists.newArrayList(a1, a2, a3, a4);
        System.out.println(accounts.stream().sorted(Comparator.comparing(Account::getId).reversed()).collect(toList()));
        System.out.println(accounts.stream().count());
        System.out.println(accounts.stream().map(Account::getId).reduce((s1, s2) -> s1 + s2).get());

        Account min = accounts.stream().min(Comparator.comparing(e -> e.getId())).get();
        Account max = accounts.stream().max(Comparator.comparing(e -> e.getId())).get();
        System.out.println(min);
        System.out.println(max);
    }

    public void testCount() {
        Account a1 = new Account();
        Account a2 = new Account();
        Account a3 = new Account();
        Account a4 = new Account();
        a1.setId(1);
        a2.setId(2);
        a3.setId(3);
        a4.setId(4);
        List<Account> accounts = Lists.newArrayList(a1, a2, a3, a4);
        int count = accounts.stream().map(e -> e.getId()).reduce(0, (x, y) -> x + y);
        System.out.println(count);

        System.out.println(accounts.stream().count());
    }

    public void testString() {
        String str = "aADN87&*nsd";
        long total = str.chars().filter(t -> (t >= 'a' && t <= 'z')).count();
        System.out.println(total);
    }

    public void testMap() {
        Account a1 = new Account();
        Account a2 = new Account();
        Account a3 = new Account();
        Account a4 = new Account();
        a1.setId(1);
        a2.setId(1);
        a3.setId(3);
        a4.setId(4);
        List<Account> accounts = Lists.newArrayList(a1, a2, a3, a4);
        System.out.println(accounts.stream().collect(toMap(e -> e.getId(), e -> e.getDate())));
    }

    public void testFlatMapToInt() {
        int[][] data = {{1, 2}, {3, 4}, {5, 6}};
        IntStream is1 = Arrays.stream(data).flatMapToInt(row -> Arrays.stream(row));
        System.out.println(is1.sum());

        int[] l1 = {4, 8, 9};
        IntDemoPerson p1 = new IntDemoPerson("Ram", l1);
        int[] l2 = {2, 7, 8};
        IntDemoPerson p2 = new IntDemoPerson("Shyam", l2);
        List<IntDemoPerson> list = Arrays.asList(p1, p2);
        IntStream is2 = list.stream().flatMapToInt(row -> Arrays.stream(row.getLuckyNum()));
        System.out.println(is2.max().getAsInt());
    }

    public void testDistinct() {
        Account a0 = new Account();
        Account a1 = new Account();
        Account a2 = new Account();
        Account a3 = new Account();
        Account a4 = new Account();
        Account a5 = new Account();
        Account a6 = new Account();
        a0.setId(2);
        a1.setId(1);
        a2.setId(2);
        a3.setId(3);
        a4.setId(4);
        a5.setId(4);
        a6.setId(4);
        List<Account> accounts = Lists.newArrayList(a0, a1, a2, a3, a4, a5, a6);
        List<Account> tempList = accounts.stream().filter(e -> Objects.nonNull(e.getId())).collect(collectingAndThen(
                toCollection(() -> new TreeSet<>(Comparator.comparing(Account::getId))), ArrayList::new));
        System.out.println(JSONObject.toJSONString(tempList));
    }

    class IntDemoPerson {
        private String name;
        private int[] luckyNum;

        public IntDemoPerson(String name, int[] luckyNum) {
            this.name = name;
            this.luckyNum = luckyNum;
        }

        public String getName() {
            return name;
        }

        public int[] getLuckyNum() {
            return luckyNum;
        }
    }
}
