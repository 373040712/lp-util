package com.lp.function.common;

import java.util.Objects;

/**
 * @author LP
 * @date 2018/6/29 15:35
 */
@FunctionalInterface
public interface DataReceiver<T> extends DataCombiner<T> {

    default void receive(TaskHandler handler, T t) {
        Objects.requireNonNull(handler);
        handler.get(combine(t));
    }
}
