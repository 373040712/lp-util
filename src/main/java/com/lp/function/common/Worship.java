package com.lp.function.common;

import java.util.Objects;

/**
 * the result will be chained
 * 
 * @author LP
 * @date 2018/6/29 14:26
 */
@FunctionalInterface
public interface Worship {

    void chant(String name);

    default Worship again(Worship w) {
        return (name) -> {
            Objects.requireNonNull(w);
            chant(name);
            w.chant(name);
        };
    }
}
