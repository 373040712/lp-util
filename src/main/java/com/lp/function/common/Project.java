package com.lp.function.common;

import lombok.Data;

/**
 * @author LP
 * @date 2018/6/29 15:02
 */
@Data
public class Project {
    private String pname;
    private String teamLead;
    private String location;

    public Project(String pname, String teamLead) {
        this.pname = pname;
        this.teamLead = teamLead;
    }
    // getters and setters
}
