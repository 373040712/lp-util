package com.lp.function.common;

/**
 * @author LP
 * @date 2018/6/29 14:52
 */
@FunctionalInterface
public interface ExtraInfoProvider<R> {

    R provideMore(R r);

}
