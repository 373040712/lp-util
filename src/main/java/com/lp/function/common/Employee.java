package com.lp.function.common;

import lombok.Data;

/**
 * @author LP
 * @date 2018/6/29 15:02
 */
@Data
public class Employee {
    private int id;
    private String name;

    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }
    // getters and setters
}
