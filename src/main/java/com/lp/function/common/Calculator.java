package com.lp.function.common;

/**
 * @author LP
 * @date 2018/6/29 14:12
 */
@FunctionalInterface
public interface Calculator {
    long calculate(long num1, long num2);
}
