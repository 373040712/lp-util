package com.lp.function.common;

/**
 * @author LP
 * @date 2018/7/2 11:14
 */

public class Person {
    private Integer id;
    private String name;

    public Person(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
