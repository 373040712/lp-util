package com.lp.function.common;

/**
 * @author LP
 * @date 2018/6/29 14:18
 */

public class Utility {

    public Utility(String taskName) {
        System.out.println(taskName);
    }

    public static long add(long num1, long num2) {
        return num1 + num2;
    }

    public static long multiply(long num1, long num2) {
        return num1 * num2;
    }
}
