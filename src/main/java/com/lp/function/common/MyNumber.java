package com.lp.function.common;

/**
 * @author LP
 * @date 2018/6/29 14:15
 */

public class MyNumber {
    private long firstNum;
    private long secondNum;

    public MyNumber(long firstNum, long secondNum) {
        this.firstNum = firstNum;
        this.secondNum = secondNum;
    }

    public long process(Calculator calc) {
        return calc.calculate(this.firstNum, this.secondNum);
    }
}
