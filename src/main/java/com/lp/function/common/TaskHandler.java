package com.lp.function.common;

/**
 * @author LP
 * @date 2018/6/29 14:20
 */
@FunctionalInterface
public interface TaskHandler {

    void get(String tname);

}
