package com.lp.function;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/2/26 22:50
 */
public interface GreetingService {

    void sayMessage(String message);

}
