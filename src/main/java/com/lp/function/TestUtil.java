package com.lp.function;

import com.lp.function.common.Book;
import com.lp.function.common.Person;
import com.lp.function.common.Rectangle;
import com.lp.function.common.Student;

import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * Java 8 Concat Streams, Lists, Sets, Arrays Example
 *
 * 对流对象Stream进行惰性求值，返回值仍然是一个Stream对象。
 * 
 * 对流对象Stream进行及早求值，返回值不在是一个Stream对象。
 * 
 * @author LP
 * @date 2018/6/29 17:02
 */

public class TestUtil {

    private static final List<Integer> VALID_PWD_CHARS = new ArrayList<>();
    static {
        IntStream.rangeClosed('0', '9').forEach(VALID_PWD_CHARS::add); // 0-9
        IntStream.rangeClosed('A', 'Z').forEach(VALID_PWD_CHARS::add); // A-Z
        IntStream.rangeClosed('a', 'z').forEach(VALID_PWD_CHARS::add); // a-z
        IntStream.rangeClosed('!', '*').forEach(VALID_PWD_CHARS::add); // !-*
    }

    public static void main(String[] args) {
        TestUtil test = new TestUtil();
//        test.doConcatStreams();
//        test.doDistinctStreams();
//        test.doDistinctByProperty();
//        test.doStreams();
//        test.doRandomAndSecureRandom();
//        test.doSortedStreams();
//        test.doReduceStreams();
        test.doListToMap();
        test.doSummaryStatistics();
        test.doParallelStreams();
        test.doFlatMap();
        test.doCollectors();
    }

    private void doConcatStreams() {
        Stream<String> s1 = Stream.of("AA", "BB", "CC");
        Stream<String> s2 = Stream.of("AA", "BB", "DD");
        Stream<String> s = Stream.concat(s1, s2);
        s.forEach(e -> System.out.print(e + " "));

        // Remove duplicates using distinct()
        s1 = Stream.of("AA", "BB", "CC");
        s2 = Stream.of("AA", "BB", "DD");
        System.out.println("\nRemove duplicates using distinct()");
        s = Stream.concat(s1, s2).distinct();
        s.forEach(e -> System.out.print(e + " "));
    }

    private void doDistinctStreams() {
        List<String> list = Arrays.asList("AA", "BB", "CC", "BB", "CC", "AA", "AA");
        long l = list.stream().distinct().count();
        System.out.println("No. of distinct elements:" + l);
        String output = list.stream().distinct().collect(Collectors.joining(","));
        System.out.println(output);
    }

    private void doDistinctByProperty() {
        List<Book> list = new ArrayList<>();
        {
            list.add(new Book("Core Java", 200));
            list.add(new Book("Core Java", 300));
            list.add(new Book("Learning Freemarker", 150));
            list.add(new Book("Spring MVC", 200));
            list.add(new Book("Hibernate", 300));
        }
        list.stream().filter(distinctByKey(b -> b.getName()))
                .forEach(b -> System.out.println(b.getName() + "," + b.getPrice()));
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private void doStreams() {
        Predicate<Integer> p = num -> num % 2 == 0;
        List<Integer> list = Arrays.asList(3, 5, 6);
        System.out.println("allMatch:" + list.stream().allMatch(p));
        System.out.println("anyMatch:" + list.stream().anyMatch(p));
        System.out.println("noneMatch:" + list.stream().noneMatch(p));

        int sum = list.stream().collect(Collectors.summingInt(i -> i));
        System.out.println("Sum: " + sum);

        List<Integer> list1 = Arrays.asList(4, 5, 6);
        System.out.println("---concat Demo---");
        Stream<Integer> resStream = Stream.concat(list.stream(), list1.stream());
        resStream.forEach(s -> System.out.print(s + " "));

        Predicate<Integer> p1 = num -> num % 2 == 0;
        System.out.println("Count: " + list.stream().filter(p1).count());

        List<Integer> list2 = Arrays.asList(3, 4, 6, 6, 4);
        System.out.println("Distinct Count: " + list2.stream().distinct().count());

        Predicate<Integer> p2 = num -> num % 2 == 0;
        System.out.println("---filter Demo---");
        list.stream().filter(p2).forEach(e -> System.out.print(e + " "));

        List<String> list3 = Arrays.asList("G", "B", "F", "E");
        String any = list3.stream().findAny().get();
        System.out.println("FindAny: " + any);
        String first = list3.stream().findFirst().get();
        System.out.println("FindFirst: " + first);

        Integer[][] data = {{1, 2}, {3, 4}, {5, 6}};
        Arrays.stream(data).flatMap(row -> Arrays.stream(row)).filter(num -> num % 2 == 1)
                .forEach(s -> System.out.print(s + " "));

        Integer[] data1 = {1, 2, 3, 4, 5, 6, 7};
        System.out.println("---forEach Demo---");
        Arrays.stream(data1).filter(num -> num % 2 == 1).forEach(s -> System.out.print(s + " "));
        System.out.println("\n---forEachOrdered Demo---");
        Arrays.stream(data1).filter(num -> num % 2 == 1).forEachOrdered(s -> System.out.print(s + " "));

        String str = "Hello World!";
        System.out.println("---generate Demo---");
        Stream<String> stream = Stream.generate(str::toString).limit(5);
        stream.forEach(s -> System.out.println(s));

        System.out.println("---iterate Demo---");
        Stream<Integer> stream1 = Stream.iterate(1, n -> n * 2).limit(5);
        stream1.forEach(s -> System.out.print(s + " "));

        System.out.println("---map Demo---");
        list.stream().map(i -> i * i).forEach(s -> System.out.print(s + " "));

        List<String> list4 = Arrays.asList("G", "B", "F", "E");
        String max = list4.stream().max(Comparator.comparing(String::valueOf)).get();
        System.out.println("Max:" + max);
        String min = list4.stream().min(Comparator.comparing(String::valueOf)).get();
        System.out.println("Min:" + min);

        // peek: 生成一个包含原Stream的所有元素的新Stream，同时会提供一个消费函数（Consumer实例），新Stream每个元素被消费的时候都会执行给定的消费函数；
        List<String> list5 = Arrays.asList("A", "B", "C");
        System.out.println("---peek Demo---");
        list5.stream().peek(s -> System.out.println(s + s)).collect(Collectors.toList());

        int[] array = {3, 5, 10, 15};
        System.out.println("---reduce Demo---");
        int sumA = Arrays.stream(array).reduce(0, (x, y) -> x + y);
        System.out.println("Sum:" + sumA);

        System.out.println("---skip Demo---");
        Arrays.stream(array).skip(2).forEach(s -> System.out.println(s + " "));

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "BBBB");
        map.put(2, "AAAA");
        map.put(3, "CCCC");
        System.out.println("---Sort by Map Value---");
        map.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue))
                .forEach(e -> System.out.println("Key: " + e.getKey() + ", Value: " + e.getValue()));

        List<String> listB = Arrays.asList("A", "B", "C", "D");
        Object[] arrayB = listB.stream().toArray();
        System.out.println("Length of array: " + arrayB.length);

        System.out.println("--Using LongStream.rangeClosed--");
        LongStream.rangeClosed(13, 15).map(n -> n * n).forEach(s -> System.out.print(s + " "));
        System.out.println("\n--Using LongStream.range--");
        LongStream.range(13, 15).map(n -> n * n).forEach(s -> System.out.print(s + " "));
        System.out.println("\n--Sum of range 1 to 10--");
        System.out.print(LongStream.rangeClosed(1, 10).sum());
        System.out.println("\n--Sorted number--");
        LongStream.of(13, 4, 15, 2, 8).sorted().forEach(s -> System.out.print(s + " "));
    }

    private void doRandomAndSecureRandom() {
        int passwordLength = 8;
        System.out.println("---Generated Password---");
        for (int i = 0; i < 5; i++) {
            new Random().ints(passwordLength, 0, VALID_PWD_CHARS.size()).map(VALID_PWD_CHARS::get)
                    .forEach(s -> System.out.print((char) s));
            System.out.println();
        }

        for (int i = 0; i < 5; i++) {
            new SecureRandom().ints(passwordLength, 0, VALID_PWD_CHARS.size()).map(VALID_PWD_CHARS::get)
                    .forEach(s -> System.out.print((char) s));
            System.out.println();
        }
    }

    private void doSortedStreams() {
        List<Student> list = new ArrayList<Student>();
        list.add(new Student(1, "Mahesh", 12));
        list.add(new Student(2, "Suresh", 15));
        list.add(new Student(3, "Nilesh", 10));

        System.out.println("---Natural Sorting by Name---");
        List<Student> slist = list.stream().sorted().collect(Collectors.toList());
        slist.forEach(e -> System.out.println("Id:" + e.getId() + ", Name: " + e.getName() + ", Age:" + e.getAge()));

        System.out.println("---Natural Sorting by Name in reverse order---");
        slist = list.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        slist.forEach(e -> System.out.println("Id:" + e.getId() + ", Name: " + e.getName() + ", Age:" + e.getAge()));

        System.out.println("---Sorting using Comparator by Age---");
        slist = list.stream().sorted(Comparator.comparing(Student::getAge)).collect(Collectors.toList());
        slist.forEach(e -> System.out.println("Id:" + e.getId() + ", Name: " + e.getName() + ", Age:" + e.getAge()));

        System.out.println("---Sorting using Comparator by Age with reverse order---");
        slist = list.stream().sorted(Comparator.comparing(Student::getAge).reversed()).collect(Collectors.toList());
        slist.forEach(e -> System.out.println("Id:" + e.getId() + ", Name: " + e.getName() + ", Age:" + e.getAge()));
    }

    private void doReduceStreams() {
        List<Integer> list = Arrays.asList(23, 43, 12, 25);
        IntSummaryStatistics stats = list.stream().collect(Collectors.summarizingInt(i -> i + i));
        System.out.println("Sum:" + stats.getSum());

        int[] array = {23, 43, 56, 97, 32};
        Arrays.stream(array).reduce((x, y) -> x + y).ifPresent(s -> System.out.println(s));
        Arrays.stream(array).reduce(Integer::sum).ifPresent(s -> System.out.println(s));
    }

    private void doListToMap() {
        List<Person> list = new ArrayList<>();
        list.add(new Person(100, "Mohan"));
        list.add(new Person(200, "Sohan"));
        list.add(new Person(300, "Mahesh"));
        Map<Integer, String> map = list.stream().collect(Collectors.toMap(Person::getId, Person::getName));
        map.forEach((x, y) -> System.out.println("Key: " + x + ", value: " + y));

        /**
         * Key: 100, value: Mohan, Sohan
         * 
         * Key: 300, value: Mahesh
         */
        List<Person> list1 = new ArrayList<>();
        list1.add(new Person(100, "Mohan"));
        list1.add(new Person(100, "Sohan"));
        list1.add(new Person(300, "Mahesh"));
        Map<Integer, String> map1 =
                list1.stream().collect(Collectors.toMap(Person::getId, Person::getName, (x, y) -> x + ", " + y));
        map1.forEach((x, y) -> System.out.println("Key: " + x + ", value: " + y));
    }

    private void doSummaryStatistics() {
        System.out.println("--DoubleSummaryStatistics--");
        List<Rectangle> list = Rectangle.getRectangle();
        DoubleSummaryStatistics dstats = list.stream().collect(Collectors.summarizingDouble(Rectangle::getWidth));
        System.out.println("Max:" + dstats.getMax() + ", Min:" + dstats.getMin());
        System.out.println("Count:" + dstats.getCount() + ", Sum:" + dstats.getSum());
        System.out.println("Average:" + dstats.getAverage());

        System.out.println("--IntSummaryStatistics--");
        list = Rectangle.getRectangle();
        IntSummaryStatistics istats = list.stream().collect(Collectors.summarizingInt(Rectangle::getLength));
        System.out.println("Max:" + istats.getMax() + ", Min:" + istats.getMin());
        System.out.println("Count:" + istats.getCount() + ", Sum:" + istats.getSum());
        System.out.println("Average:" + istats.getAverage());

        System.out.println("--LongSummaryStatistics--");
        list = Rectangle.getRectangle();
        LongSummaryStatistics lstats = list.stream().collect(Collectors.summarizingLong(Rectangle::getId));
        System.out.println("Max:" + lstats.getMax() + ", Min:" + lstats.getMin());
        System.out.println("Count:" + lstats.getCount() + ", Sum:" + lstats.getSum());
        System.out.println("Average:" + lstats.getAverage());
    }

    private void doParallelStreams() {
        int[] intNum1 = {3, 4, 2, 5, 1, 6, 3};
        IntBinaryOperator intOpt = (i1, i2) -> i1 * i2;
        System.out.println("parallel prefix for complete array");
        Arrays.parallelPrefix(intNum1, intOpt);
        IntConsumer intCon = i -> System.out.print(i + " ");
        Arrays.stream(intNum1).forEach(intCon);

        System.out.println("\nparallel prefix for array from index 1 to 4");
        int[] intNum2 = {3, 4, 2, 5, 1, 6, 3};
        Arrays.parallelPrefix(intNum2, 1, 4, intOpt);
        Arrays.stream(intNum2).forEach(intCon);

        double[] dbNum1 = {3.2, 4.1, 2.2, 5.4, 1.2, 6.4, 3.2};
        DoubleBinaryOperator dbOpt = (d1, d2) -> d1 + d2;
        System.out.println("parallel prefix for complete array");
        Arrays.parallelPrefix(dbNum1, dbOpt);
        DoubleConsumer dbCon = d -> System.out.print(d + " ");
        Arrays.stream(dbNum1).forEach(dbCon);

        System.out.println("\nparallel prefix for array from index 1 to 4");
        double[] dbNum2 = {3.2, 4.1, 2.2, 5.4, 1.2, 6.4, 3.2};
        Arrays.parallelPrefix(dbNum2, 1, 4, dbOpt);
        Arrays.stream(dbNum2).forEach(dbCon);

        int[] num1 = {3, 6, 2, 10, 4, 1, 7};
        System.out.println("--Sort complete Integer array--");
        Arrays.parallelSort(num1);
        IntConsumer printInt = i -> System.out.print(i + " ");
        Arrays.stream(num1).forEach(printInt);
        System.out.println("\n--Sort Integer array from index 1 to 5--");
        int[] num2 = {3, 6, 2, 10, 4, 1, 7};
        Arrays.parallelSort(num2, 1, 5);
        Arrays.stream(num1).forEach(printInt);

        double[] db1 = {3.5, 1.2, 6.7, 8.9, 0.6, 2.3, 5.5};
        System.out.println("\n--Sort complete Double array--");
        Arrays.parallelSort(db1);
        DoubleConsumer printDB = d -> System.out.print(d + " ");
        Arrays.stream(db1).forEach(printDB);
        System.out.println("\n--Sort Double array from index 1 to 5--");
        double[] db2 = {3.5, 1.2, 6.7, 8.9, 0.6, 2.3, 5.5};
        Arrays.parallelSort(db2, 1, 5);
        Arrays.stream(db2).forEach(printDB);
    }

    private void doFlatMap() {
        /** 获取单词，并且去重 **/
        List<String> list = Arrays.asList("hello welcome", "world hello", "hello world", "hello world welcome");
        System.out.println(list.toString());

        // map和flatmap的区别
        list.stream().map(item -> Arrays.stream(item.split(" "))).distinct().collect(Collectors.toList())
                .forEach(System.out::println);
        System.out.println("---------- ");
        list.stream().flatMap(item -> Arrays.stream(item.split(" "))).distinct().collect(Collectors.toList())
                .forEach(System.out::println);

        // 实际上返回的类似是不同的
        List<Stream<String>> listResult =
                list.stream().map(item -> Arrays.stream(item.split(" "))).distinct().collect(Collectors.toList());
        List<String> listResult2 =
                list.stream().flatMap(item -> Arrays.stream(item.split(" "))).distinct().collect(Collectors.toList());

        System.out.println("---------- ");

        // 也可以这样
        list.stream().map(item -> item.split(" ")).flatMap(Arrays::stream).distinct().collect(Collectors.toList())
                .forEach(System.out::println);

        System.out.println("================================================");

        /** 相互组合 **/
        List<String> list2 = Arrays.asList("hello", "hi", "你好");
        List<String> list3 = Arrays.asList("zhangsan", "lisi", "wangwu", "zhaoliu");

        list2.stream().map(item -> list3.stream().map(item2 -> item + " " + item2)).collect(Collectors.toList())
                .forEach(System.out::println);
        list2.stream().flatMap(item -> list3.stream().map(item2 -> item + " " + item2)).collect(Collectors.toList())
                .forEach(System.out::println);

        // 实际上返回的类似是不同的
        List<Stream<String>> list2Result = list2.stream().map(item -> list3.stream().map(item2 -> item + " " + item2))
                .collect(Collectors.toList());
        List<String> list2Result2 = list2.stream().flatMap(item -> list3.stream().map(item2 -> item + " " + item2))
                .collect(Collectors.toList());
    }

    private void doCollectors() {
        Student s1 = new Student(1, "Ram", 18);
        Student s2 = new Student(2, "Shyam", 22);
        Student s3 = new Student(3, "Mohan", 19);
        Student s4 = new Student(4, "Mahesh", 20);
        Student s5 = new Student(5, "Krishna", 21);
        List<Student> list = Arrays.asList(s1, s2, s3, s4, s5);
        // partition of Student on the basis of age
        System.out.println("----Partition of Student on the basis of age >20 ----");
        Map<Boolean, List<Student>> stdByClass = list.stream().collect(Collectors.partitioningBy(s -> s.getAge() > 20));

        stdByClass.forEach((k, v) -> System.out.println("Key:" + k + "  "
                + ((List<Student>) v).stream().map(s -> s.getName()).collect(Collectors.joining(","))));
    }
}
