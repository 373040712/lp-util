package com.lp.function;

import com.lp.function.common.*;

/**
 * Java Functional Interface
 *
 * @author LP
 * @date 2018/6/29 14:11
 */

public class TestFunctionalInterface {
    public static void main(String[] args) {
        GreetingService greetService = message -> System.out.println("Hello " + message);
        greetService.sayMessage("xxx");

        TestFunctionalInterface test = new TestFunctionalInterface();
        test.doCommon();
        test.doMethodReference();
        test.doConstructorReference();
        test.doDefaultMethod();
        test.doGenericDefaultMethod();
        test.doInterfaceInheritance();
    }

    private void doCommon() {
        Calculator calc = (n1, n2) -> n1 + n2;

        System.out.println(calc.calculate(30, 50));

        System.out.println(new MyNumber(30, 50).process(calc));

        System.out.println(new MyNumber(30, 50).process((n1, n2) -> n1 * n2));
    }

    private void doMethodReference() {
        Calculator calc = Utility::add;

        System.out.println(calc.calculate(30, 50));

        System.out.println(new MyNumber(30, 50).process(Utility::add));
    }

    /**
     * As we know that constructor has no return type. So we will create a functional interface with
     * abstract method that has no return type but with same number of arguments and type as
     * constructor. Find a functional interface.
     */
    private void doConstructorReference() {
        TaskHandler taskHandler = Utility::new;
        taskHandler.get("Task 1");
    }

    private void doDefaultMethod() {
        Worship worship = (name) -> System.out.println(name);

        worship.again(worship).again(worship).chant("Ram");
    }

    private void doGenericDefaultMethod() {
        DataCombiner<Project> dataCombiner = (Project p) -> {
            if (p.getLocation() == null) {
                return p.getPname() + " : " + p.getTeamLead();
            } else {
                return p.getPname() + " : " + p.getTeamLead() + " : " + p.getLocation();
            }
        };

        InfoProvider<Employee, Project> infoProvider = (Employee emp) -> {
            if (emp.getId() > 100) {
                return new Project("ABCD", emp.getName());
            } else {
                return new Project("PQRS", emp.getName());
            }
        };

        ExtraInfoProvider<Project> extraInfoProvider = (Project p) -> {
            if (p.getPname().equals("PQRS")) {
                p.setLocation("Noida");
            }
            return p;
        };

        String s =
                infoProvider.addMore(extraInfoProvider).addCombiner(dataCombiner).combine(new Employee(50, "Mahesh"));

        System.out.println(s);
    }

    private void doInterfaceInheritance() {
        DataReceiver<Employee> dataReceiver = (Employee emp) -> emp.getId() + "-" + emp.getName();

        TaskHandler tskHandler = (res) -> System.out.println(res);

        dataReceiver.receive(tskHandler, new Employee(101, "Krishna"));
    }
}
