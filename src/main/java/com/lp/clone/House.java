package com.lp.clone;

import com.google.common.collect.Lists;

import java.util.ArrayList;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/11 22:11
 */
public class House implements Cloneable {

    private ArrayList<String> list = new ArrayList<>();

    private User user = new User("LP");

    private ArrayList<User> users = Lists.newArrayList(new User("LA"), new User("LB"));

    public static void main(String[] args) {
        House houseA = new House();
        House houseB = houseA.clone();
        System.out.println(houseA == houseB);
        System.out.println(houseA.list == houseB.list);
        System.out.println(houseA.user == houseB.user);
        System.out.println(houseA.users == houseB.users);
    }

    @Override
    public House clone() {
        House house = null;
        try {
            house = (House) super.clone();
            this.list = (ArrayList<String>) this.list.clone();
            this.user = this.user.clone();
            this.users = (ArrayList<User>) this.users.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return house;
    }
}
