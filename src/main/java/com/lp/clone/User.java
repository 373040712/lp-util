package com.lp.clone;

/**
 * @author liupeng@sioniov.com
 * @version 1.0
 * @date 2020/3/11 22:13
 */
public class User implements Cloneable {

    private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public User clone() {
        User user = null;
        try {
            user = (User) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return user;
    }
}
