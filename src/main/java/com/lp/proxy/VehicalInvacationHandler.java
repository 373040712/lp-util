package com.lp.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/15 20:08
 */
public class VehicalInvacationHandler implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("test");
        return null;
    }
}
