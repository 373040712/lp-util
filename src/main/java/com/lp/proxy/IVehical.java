package com.lp.proxy;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/15 20:07
 */
public interface IVehical {
    void run();
}
