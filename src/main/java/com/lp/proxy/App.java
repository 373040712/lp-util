package com.lp.proxy;

import java.lang.reflect.Proxy;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/10/15 20:09
 */
public class App {
    public static void main(String[] args) {
        IVehical vehical = (IVehical) Proxy.newProxyInstance(IVehical.class.getClassLoader(),
                IVehical.class.getInterfaces(),
                new VehicalInvacationHandler());
        vehical.run();
    }
}
