package com.lp.json;


import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 代理级别DTO
 *
 * @author peng.liu
 * @date 2018/10/23 16:41
 */
@Data
public class AgentLevelDTO implements Serializable {

    private static final long serialVersionUID = 2333470365337794223L;

    /**
     * 代理index
     */
    private Integer id;
    /**
     * 代理名称
     */
    private String name;
    /**
     * 是否启用
     */
    private Boolean enable;
    /**
     * 是否需要审核，默认需要审核
     */
    private Boolean needAudit;
    /**
     * 是否需要合同，默认不需要签合同
     */
    private Boolean needContract;
    /**
     * 押金配置
     */
    private DepositConfig depositConfig;
    /**
     * 免押试用期配置
     */
    private FreeDepositExp freeDepositExp;
    /**
     * 业绩提成配置
     */
    private AchievementConfig achievementConfig;
    /**
     * 业务配置
     */
    private Business business;
    /**
     * 映射etc代理级别
     */
    private String etcLevelMapping;

    @Data
    public class DepositConfig {
        /**
         * 是否需要押金，默认不需要
         */
        private Boolean needDeposit;
        /**
         * 押金套餐编码
         */
        private String depositComboCode;
        /**
         * 押金套餐名称
         */
        private String depositComboName;

        public DepositConfig() {
            needDeposit = false;
            depositComboCode = StringUtils.EMPTY;
            depositComboName = StringUtils.EMPTY;
        }
    }

    @Data
    public class FreeDepositExp {
        /**
         * 是否免押体验，默认没有
         */
        private Boolean hasFreeDeposit;
        /**
         * 免押时间，默认当前时间
         */
        private Date freeDepositTime;
        /**
         * 免押天数，默认0
         */
        private Integer freeDepositDay;

        public FreeDepositExp() {
            hasFreeDeposit = false;
            freeDepositDay = 0;
            freeDepositTime = new Date();
        }
    }

    @Data
    public class AchievementConfig {
        /**
         * 是否有业绩, 默认没有
         */
        private Boolean hasAchievement;
        /**
         * 是否归属自己,不然默认归属父级，默认归属自己
         */
        private Boolean belongToMe;

        public AchievementConfig() {
            hasAchievement = false;
            belongToMe = true;
        }
    }

    @Data
    public class Business {
        /**
         * 能否售卖套餐，默认不能
         */
        private Boolean canSellCombo;
        /**
         * 能否提交商机，默认不能
         */
        private Boolean canSubmitOpportunity;
        /**
         * 能否开办事员，默认不能
         */
        private Boolean canOpenSubordinate;

        public Business() {
            canSellCombo = false;
            canSubmitOpportunity = false;
            canOpenSubordinate = false;
        }
    }

    public AgentLevelDTO() {
        needAudit = true;
        needContract = false;
        depositConfig = new DepositConfig();
        freeDepositExp = new FreeDepositExp();
        achievementConfig = new AchievementConfig();
        business = new Business();
    }
}
