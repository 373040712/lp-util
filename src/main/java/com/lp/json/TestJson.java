package com.lp.json;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lp.common.pojo.Score;
import com.lp.common.pojo.TBaseEmployeeDTO;
import lombok.Data;
import org.apache.commons.beanutils.BeanMap;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author peng.liu
 * @date 2017/9/30 17:10
 */

public class TestJson {

    public static void main(String[] args) throws IOException {
        TestJson testJson = new TestJson();
        com.lp.common.pojo.User user = new com.lp.common.pojo.User();
        user.setName("菠萝大象");
        user.setGender(com.lp.common.pojo.Gender.MALE);
        List<com.lp.common.pojo.Account> accounts = new ArrayList<com.lp.common.pojo.Account>();
        com.lp.common.pojo.Account account = new com.lp.common.pojo.Account();
        account.setId(1);
        account.setBalance(BigDecimal.valueOf(1900.2));
        account.setCardId("423335533434");
        account.setDate(new Date());
        accounts.add(account);
        account = new com.lp.common.pojo.Account();
        account.setId(2);
        account.setBalance(BigDecimal.valueOf(5000));
        account.setCardId("625444548433");
        account.setDate(new Date());
        accounts.add(account);
        user.setAccounts(accounts);
        List<Score> scoreList = new ArrayList<Score>();
        Score score = new Score();
        score.setName("abc");
        score.setNum(12);
        scoreList.add(score);
        score = new Score();
        score.setName("qqq");
        score.setNum(45);
        scoreList.add(score);
        user.setScore(score);
        user.setScores(scoreList);

        // testJson.doJsonObject(user);
        // testJson.doGsonObject(user);
        // testJson.doJacksonObject(user);
         testJson.doJson();
        // testJson.doComplexJson();

    }

    private void doJsonObject(com.lp.common.pojo.User user) {
        String json = JSONObject.toJSONString(user);
        System.out.println(json);
        com.lp.common.pojo.User temp = JSONObject.parseObject(json, com.lp.common.pojo.User.class);
        System.out.println(user);
    }

    private void doGsonObject(com.lp.common.pojo.User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        System.out.println(json);
        Type type = new TypeToken<com.lp.common.pojo.User<com.lp.common.pojo.Account>>() {}.getType();
        com.lp.common.pojo.User temp = gson.fromJson(json, type);
        System.out.println(user);
    }

    private void doJacksonObject(com.lp.common.pojo.User user) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(user);
        System.out.println(json);
        com.lp.common.pojo.User temp =
                mapper.readValue(json, new TypeReference<com.lp.common.pojo.User<com.lp.common.pojo.Account>>() {});

        Map<?, ?> tempMap = new BeanMap(temp);
        Map<String, Object> mapInfo = mapper.readValue(json, Map.class);
        System.out.println(user);
    }

    private void doJson() {
        String str =
                "[{\"name\":\"最近30日发货流失天数\",\"refPath\":\"sendInfo.sendLostDays\",\"type\":\"Number\",\"code\":\"EQ\",\"operator\":\"==\",\"values\":[\"abc\",2]}]";
        List<com.lp.common.pojo.TaskConditionInputDTO> list =
                JSONObject.parseArray(str, com.lp.common.pojo.TaskConditionInputDTO.class);
        System.out.println(list);

        String strTest =
                "{\"content\":{\"pn\":1,\"ps\":100,\"datas\":[{\"id\":1795,\"userId\":1074161,\"name\":\"张文川\",\"workNo\":\"66779\",\"organizationId\":336,\"organizationName\":\"技术部\",\"organizationPath\":\"董事会/技术部\",\"birthDate\":495475200000,\"entryDate\":1465142400000,\"mobile\":\"13666272285\",\"icno\":\"51302919850914005X\",\"status\":1,\"adAccount\":\"zwc\",\"alias\":\"张文川\",\"email\":\"wenchuan.zhang@56qq.com\"}],\"tc\":1},\"status\":\"OK\"}";
        JSONObject jsonObject = JSONObject.parseObject(strTest);
        Map<String, Object> dataMap = JSONObject.toJavaObject(jsonObject, Map.class);
        TbasePageDTO<TBaseEmployeeDTO> page = jsonObject.getJSONObject("content")
                .toJavaObject(new com.alibaba.fastjson.TypeReference<TbasePageDTO<TBaseEmployeeDTO>>() {});
        System.out.println(page.getDatas().toString());
    }

    private void doComplexJson() {
        String str =
                "{\"and\":{\"tag\":[{\"id\":1,\"name\":\"标签名称\",\"category\":1}],\"field\":[{\"id\":1,\"name\":\"属性名称\",\"type\":\"String\",\"operator\":\"EQ\",\"value\":[1,2],\"valueDesc\":[\"值1描述,数组形式\",\"值2描述,数组形式\"]}]},\"or\":{\"tag\":[{\"id\":1,\"name\":\"标签名称\",\"category\":1}],\"field\":[{\"id\":1,\"name\":\"属性名称\",\"type\":\"String\",\"operator\":\"EQ\",\"value\":[1,2],\"valueDesc\":[\"值1描述,数组形式\",\"值2描述,数组形式\"]}]}}";
        com.lp.common.pojo.RuleDTO ruleDTO = JSONObject.parseObject(str, com.lp.common.pojo.RuleDTO.class);
        com.lp.common.pojo.RuleDTO.ConditionDTO.FieldInputDTO fieldInputDTO = ruleDTO.getAnd().getField().get(0);
        System.out.println(fieldInputDTO.getId());

        String strEs =
                "{\"and\":{\"field\":[{\"mapping\":\"lastLoginTime\",\"operator\":\"GE\",\"value\":[\"2017-01-01 0:0:0\"]}]},\"or\":{\"field\":[{\"mapping\":\"extendInfo.loginDaysLast30Days\",\"operator\":\"GE\",\"value\":[5]}],\"tag\":[{\"and\":{\"field\":[{\"mapping\":\"status\",\"operator\":\"EQ\",\"value\":[true]},{\"mapping\":\"extendInfo.loginDaysLast30Days\",\"operator\":\"GE\",\"value\":[20]}]},\"or\":{\"field\":[{\"mapping\":\"provinceId\",\"operator\":\"IN\",\"value\":[41,42,43]},{\"mapping\":\"positionInfo.lastUploadProvinceId\",\"operator\":\"IN\",\"value\":[41,42,43]}]},\"userType\":1},{\"and\":{\"field\":[{\"mapping\":\"status\",\"operator\":\"EQ\",\"value\":[true]},{\"mapping\":\"extendInfo.loginDaysLast30Days\",\"operator\":\"GE\",\"value\":[10]}]},\"or\":{\"field\":[{\"mapping\":\"provinceId\",\"operator\":\"IN\",\"value\":[41,42,43]},{\"mapping\":\"positionInfo.lastUploadProvinceId\",\"operator\":\"IN\",\"value\":[41,42,43]}]},\"userType\":1}]},\"userType\":1}";
        com.lp.common.pojo.RuleEsDTO ruleEsDTO = JSONObject.parseObject(strEs, com.lp.common.pojo.RuleEsDTO.class);
        System.out.println(ruleEsDTO);
    }

    @Data
    private static class TbasePageDTO<T> implements Serializable {

        private static final long serialVersionUID = 1714706181058060411L;

        private List<T> datas;

        private int tc;
    }
}
