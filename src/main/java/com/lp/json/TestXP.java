package com.lp.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.util.Strings;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author peng.liu
 * @date 2018/10/25 14:38
 */

public class TestXP {

    public static void main(String[] args) {
//        String strTest =
//                "{\"content\":{\"pn\":1,\"ps\":100,\"datas\":{\"id\":1795,\"userId\":1074161,\"name\":\"张文川\",\"workNo\":\"66779\",\"organizationId\":336,\"organizationName\":\"技术部\",\"organizationPath\":\"董事会/技术部\",\"birthDate\":495475200000,\"entryDate\":1465142400000,\"mobile\":\"13666272285\",\"icno\":\"51302919850914005X\",\"status\":1,\"adAccount\":\"zwc\",\"alias\":\"张文川\",\"email\":\"wenchuan.zhang@56qq.com\"},\"tc\":1},\"status\":\"OK\"}";
//        JSONObject jsonObject = JSONObject.parseObject(strTest);
//        Map<String, Object> dataMap = JSONObject.toJavaObject(jsonObject, Map.class);
//        Map<String, Object> result = Maps.newHashMap();
//        initProperties(result, StringUtils.EMPTY, dataMap);
//        System.out.println(result);

        String str = "ASSET_TYPE_CODE.ab_cLog.acb_er.ABC";

        System.out.println(lineToHump(str));
    }

    private static Pattern linePattern = Pattern.compile("_(\\w)");

    private static final String PROPERTY_DELIMITER = ".";

    public static String lineToHump(String str) {
        return String.join(PROPERTY_DELIMITER, Arrays.stream(str.split("\\"+PROPERTY_DELIMITER)).map(e -> {
            Matcher matcher = linePattern.matcher(e.toLowerCase());
            StringBuffer sb = new StringBuffer();
            while (matcher.find()) {
                matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
            }
            matcher.appendTail(sb);
            return sb.toString();
        }).collect(Collectors.toList()));
    }

    private static void initProperties(Map<String, Object> result, String prefix, Map<String, Object> dataMap) {
        dataMap.entrySet().stream().forEach(e -> {
            String key = Strings.isBlank(prefix) ? e.getKey() : prefix + "." + e.getKey();
            Object object = e.getValue();
            if (object instanceof JSONObject) {
                initProperties(result, key, JSONObject.toJavaObject((JSON) object, Map.class));
            } else {
                result.put(key, object);
            }
        });
    }
}
