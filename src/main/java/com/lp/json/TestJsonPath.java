package com.lp.json;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

/**
 * @author LP
 * @date 2018/1/15 16:29
 */

public class TestJsonPath {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        String json =
                "{\"sendInfo\":{\"frequentlySendRoute\":\"22\",\"lastSendTime\":1515666511139,\"sendDensity\":334,\"sendSatisfaction\":1,\"sendLostDays\":12345,\"frequentlySendCargoType\":\"type\"},\"avatarUrl\":\"http://www.baidu.com\",\"registerTime\":\"2018-01-11T10:28:31.000Z\",\"companyInfo\":{\"companyName\":\"四川省有限公司\",\"companyAddress\":\"四川省成都市武侯区天府大道\"},\"mostBuyInsuranceCargoTypes\":\"蔬菜,水果,苹果,桃子,设备,生鲜\",\"authStatus\":1,\"mobile\":\"13242425475\",\"icNo\":\"565161\",\"updateTime\":\"2018-01-11T05:58:35.000Z\",\"type\":1,\"userId\":25558,\"domainId\":1,\"uid\":\"1_25558\",\"realName\":\"abc\",\"lastLoginTime\":\"2018-01-11T10:28:31.000Z\",\"bindInfo\":{\"bindEmployeeName\":\"test\",\"isBinding\":true,\"bindEmployeeUserId\":445,\"bindEmployeeWorkNo\":\"324324\"},\"registerLocation\":{\"lon\":33,\"lat\":47},\"insuranceInfo\":{\"isBuyInsuranceLast90Days\":true,\"userType\":22,\"isBuyVouchLast90Days\":false},\"feeInfo\":{\"finishStatus\":1,\"openFeeStatus\":2},\"@timestamp\":\"2018-01-12T08:08:00.687Z\",\"positionInfo\":{\"address\":\"test\",\"countyId\":32,\"lastUploadPosition\":{\"lon\":22,\"lat\":45},\"cityId\":12,\"provinceId\":12},\"@version\":\"1\",\"id\":18,\"username\":\"2122\",\"status\":1}";

        Configuration.setDefaults(new Configuration.Defaults() {

            private final JsonProvider jsonProvider = new JacksonJsonProvider();
            private final MappingProvider mappingProvider = new JacksonMappingProvider();

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });

        ReadContext ctx = JsonPath.parse(json);
        System.out.println(System.currentTimeMillis() - start);
        Object object = ctx.read(
                "$[?(@.sendInfo.sendLostDays >= 0 && @.sendInfo.sendLostDays <= 20000 && @.companyInfo.companyName == '四川省有限公司' && @.positionInfo.cityId in [11,12,13] && @.mostBuyInsuranceCargoTypes =~/.['苹果']/i)]");
        if (object instanceof Collection) {
            List list = (List) object;
            System.out.println(list);
        } else {
            System.out.println(object);
        }
        System.out.println(System.currentTimeMillis() - start);
    }


}
