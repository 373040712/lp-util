package com.lp.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author peng.liu
 * @date 2018/7/24 19:13
 */

public class JsonToMapTest {

    private static final String PARENT_SEPARATOR = ".";

    public static void main(String[] args) {
        JSONObject.parseObject("xxx", String.class);

        String str = "{\"0\":\"zhangsan\",\"1\":\"lisi\",\"2\":\"wangwu\",\"3\":\"maliu\"}";
        // 第一种方式
        Map maps = (Map) JSON.parse(str);
        System.out.println("这个是用JSON类来解析JSON字符串!!!");
        for (Object map : maps.entrySet()) {
            System.out.println(((Map.Entry) map).getKey() + "     " + ((Map.Entry) map).getValue());
        }
        // 第二种方式
        Map mapTypes = JSON.parseObject(str);
        System.out.println("这个是用JSON类的parseObject来解析JSON字符串!!!");
        for (Object obj : mapTypes.keySet()) {
            System.out.println("key为：" + obj + "值为：" + mapTypes.get(obj));
        }
        // 第三种方式
        Map mapType = JSON.parseObject(str, Map.class);
        System.out.println("这个是用JSON类,指定解析类型，来解析JSON字符串!!!");
        for (Object obj : mapType.keySet()) {
            System.out.println("key为：" + obj + "值为：" + mapType.get(obj));
        }
        // 第四种方式
        /**
         * JSONObject是Map接口的一个实现类
         */
        Map json = (Map) JSONObject.parse(str);
        System.out.println("这个是用JSONObject类的parse方法来解析JSON字符串!!!");
        for (Object map : json.entrySet()) {
            System.out.println(((Map.Entry) map).getKey() + "  " + ((Map.Entry) map).getValue());
        }
        // 第五种方式
        /**
         * JSONObject是Map接口的一个实现类
         */
        JSONObject jsonObject = JSONObject.parseObject(str);
        System.out.println("这个是用JSONObject的parseObject方法来解析JSON字符串!!!");
        for (Object map : json.entrySet()) {
            System.out.println(((Map.Entry) map).getKey() + "  " + ((Map.Entry) map).getValue());
        }
        // 第六种方式
        /**
         * JSONObject是Map接口的一个实现类
         */
        Map mapObj = JSONObject.parseObject(str, Map.class);
        System.out.println("这个是用JSONObject的parseObject方法并执行返回类型来解析JSON字符串!!!");
        for (Object map : json.entrySet()) {
            System.out.println(((Map.Entry) map).getKey() + "  " + ((Map.Entry) map).getValue());
        }
        String strArr = "{{\"0\":\"zhangsan\",\"1\":\"lisi\",\"2\":\"wangwu\",\"3\":\"maliu\"},"
                + "{\"00\":\"zhangsan\",\"11\":\"lisi\",\"22\":\"wangwu\",\"33\":\"maliu\"}}";
        // JSONArray.parse()
        System.out.println(json);
    }

    /**
     * json对象铺平
     * 
     * @param json
     * @param parentKey
     * @return
     */
    public Map<String, Object> jsonOfData2Map(JSONObject json, Optional<String> parentKey) {
        Map<String, Object> map = new HashMap<>((int) (json.size() * 1.2));
        String key;
        Object value;
        for (Object k : json.keySet()) {
            value = json.get(k);
            // 尝试拼接父节点的key以保证唯一性
            key = parentKey.map(p -> p + PARENT_SEPARATOR + k.toString()).orElseGet(k::toString);
            if (value instanceof JSONObject) {
                // 递归获取所有叶子节点的键值对
                map.putAll(jsonOfData2Map(((JSONObject) value), Optional.of(key)));
            } else if (Objects.nonNull(value)) {
                // 如果为jsonArray，目前只支持一层jsonArray，不进行深度转换
                // 值不为空才添加以节省内存空间
                map.put(key, value);
            }
        }
        return map;
    }
}
