package com.lp.common.service;

/**
 * @author LP
 * @date 2017/11/22 14:53
 */
@FunctionalInterface
public interface LandingObserver {

    void observeLanding(String name);

}
