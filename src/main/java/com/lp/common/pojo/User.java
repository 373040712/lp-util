package com.lp.common.pojo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author peng.liu
 * @date 2017/9/30 17:13
 */
@Data
@ToString
public class User<T> extends BaseUser{

    private String name;

    private Gender gender;

    private List<T> accounts;

    private List<Score> scores;

    private Score score;
}
