package com.lp.common.pojo;

import com.lp.tree.ITreeNode;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Org implements ITreeNode {

    private String uuid;
    private String parentId;
    private String name;
    private String code;
    private String type;


    public String getNodeId() {
        return this.uuid;
    }

    public String getParentNodeId() {
        return this.parentId;
    }
}
