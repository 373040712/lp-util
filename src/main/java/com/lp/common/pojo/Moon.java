package com.lp.common.pojo;

import com.lp.common.service.LandingObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LP
 * @date 2017/11/22 14:54
 */

public class Moon {

    private final List<LandingObserver> observers = new ArrayList<>();

    public void land(String name) {
        for (LandingObserver observer : observers) {
            observer.observeLanding(name);
        }
    }

    public void startSpying(LandingObserver observer) {
        observers.add(observer);
    }
}
