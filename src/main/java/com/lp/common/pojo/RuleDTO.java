package com.lp.common.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 标签规则DTO
 *
 * @author LP
 * @date 2018/5/24 19:46
 */
@Data
public class RuleDTO implements Serializable {

    private static final long serialVersionUID = -8263858394775319555L;

    /**
     * 满足所有条件
     */
    private ConditionDTO and;

    /**
     * 满足其中条件
     */
    private ConditionDTO or;

    @Data
    public class ConditionDTO implements Serializable {

        private static final long serialVersionUID = 4131972597386787963L;

        /**
         * 条件包含标签
         */
        private List<TagInputDTO> tag;

        /**
         * 条件包含属性
         */
        private List<FieldInputDTO> field;

        @Data
        public class FieldInputDTO implements Serializable {

            private static final long serialVersionUID = -7685815405140868215L;

            /**
             * 属性id
             */
            private Long id;
            /**
             * 属性名称
             */
            private String name;
            /**
             * 属性类型
             */
            private String type;
            /**
             * 操作符
             */
            private String operator;
            /**
             * 输入值
             */
            private List<Object> values;
            /**
             * 输入值映射值
             */
            private List<Object> valuesDesc;
        }

        @Data
        public class TagInputDTO implements Serializable {

            private static final long serialVersionUID = -2699462701356176381L;

            /**
             * 标签id
             */
            private Long id;
            /**
             * 标签名称
             */
            private String name;
            /**
             * 标签分类:1=平台，2=油品，3=保险
             */
            private Integer category;
        }

    }
}
