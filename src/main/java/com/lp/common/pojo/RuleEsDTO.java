package com.lp.common.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 规则用于ES查询
 *
 * @author LP
 * @date 2018/5/25 10:34
 */
@Data
@Accessors(chain = true)
public class RuleEsDTO implements Serializable {

    private static final long serialVersionUID = 589162492404010067L;

    /**
     * 用户类型：1=货主，2=司机
     */
    private Integer userType;

    /**
     * 满足所有条件
     */
    private Condition and;

    /**
     * 满足其中条件
     */
    private Condition or;

    public Condition generateCondition() {
        return new Condition();
    }

    @Data
    @Accessors(chain = true)
    public class Condition implements Serializable {

        private static final long serialVersionUID = 8207603370772007494L;

        /**
         * 标签规则
         */
        private List<RuleEsDTO> tag;

        /**
         * 条件包含属性
         */
        private List<Field> field;

        public Field generateField() {
            return new Field();
        }

        @Data
        @Accessors(chain = true)
        public class Field implements Serializable {

            private static final long serialVersionUID = 4221169486107767460L;

            /**
             * 属性es字段映射
             */
            private String mapping;
            /**
             * 操作符
             */
            private String operator;
            /**
             * 输入值
             */
            private List<Object> value;
        }
    }

}
