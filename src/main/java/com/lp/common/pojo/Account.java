package com.lp.common.pojo;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author peng.liu
 * @date 2017/9/30 17:12
 */
@Data
@ToString
public class Account {

    private Integer id;

    private String cardId;

    private BigDecimal balance;

    private Date date;
}
