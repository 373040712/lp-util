package com.lp.common.pojo;

import lombok.ToString;

/**
 * @author peng.liu
 * @date 2017/9/30 17:12
 */
@ToString
public enum Gender {
    MALE, FEMALE
}
