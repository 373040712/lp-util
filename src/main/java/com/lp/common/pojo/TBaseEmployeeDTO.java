package com.lp.common.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 员工信息DTO
 *
 * @author jian.zhang5
 * @date 2018/6/8 14:45
 * @since V1.0
 */
@Data
@EqualsAndHashCode(of={"userId"})
public class TBaseEmployeeDTO implements Serializable {
    /**
     * 员工记录id
     */
    private Long id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 工号
     */
    private String workNo;
    /**
     * 姓名
     */
    private String name;
    /**
     * 在职状态
     */
    private Integer status;
    /**
     * 电话
     */
    private String mobile;
    /**
     * 所属组织机构id
     */
    private Long organizationId;
    /**
     * 最终所属组织机构名称
     */
    private String organizationName;
    /**
     * 所属组织机构全路径名称,用'/'隔开
     */
    private String organizationPath;
    /**
     * 邮箱
     */
    private String email;
}
