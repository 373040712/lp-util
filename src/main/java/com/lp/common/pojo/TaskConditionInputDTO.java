package com.lp.common.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 任务条件输入
 *
 * @author LP
 * @date 2018/1/17 16:27
 */
@Data
public class TaskConditionInputDTO implements Serializable {

    private static final long serialVersionUID = 330402737417053549L;

    /**
     * 属性名称
     */
    private String name;
    /**
     * 对应json字段
     */
    private String refPath;
    /**
     * 属性类型
     */
    private String type;
    /**
     * 操作符编码
     */
    private String code;
    /**
     * 操作符
     */
    private String operator;
    /**
     * 输入值
     */
    private List<Object> values;
}
