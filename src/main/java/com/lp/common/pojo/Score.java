package com.lp.common.pojo;

import lombok.Data;
import lombok.ToString;

/**
 * @author peng.liu
 * @date 2017/9/30 17:25
 */
@Data
@ToString
public class Score {

    private Integer num;

    private String name;
}
