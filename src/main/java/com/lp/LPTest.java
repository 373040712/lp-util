package com.lp;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;

import java.text.MessageFormat;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;


/**
 * @author peng.liu
 * @date 2019/6/4 22:21
 */
public class LPTest {

    private static final Pattern PARAM_PATTERN = Pattern.compile("[#]\\{(.*?)\\}");

    static String point = ".";

    public static void main(String[] args) throws Exception {
        User user1 = User.builder().id(1).date(new DateTime(2020,8,10,1,1).toDate()).build();
        JSONObject ext = new JSONObject().fluentPut("test",user1);
        User xx = ext.getObject("test",User.class);
        System.out.println(xx);
        Byte t = 2;
        System.out.println(t.equals(new Byte("2")));
        System.out.println(JSON.parseArray("[12]", Long.class));
        List<User> temp = Lists.newArrayList(
                User.builder().id(1).date(new DateTime(2020,8,10,1,1).toDate()).build(),
                User.builder().id(2).date(new DateTime(2020,6,10,1,1).toDate()).build(),
                User.builder().id(2).date(new DateTime(2020,7,10,2,1).toDate()).build()
        );
        System.out.println(temp.stream().sorted(Comparator.comparing(User::getId).thenComparing(User::getDate).reversed()).collect(toList()));
        System.out.println(JSONObject.parseObject("{}"));
        System.out.println(Collections.emptyMap().get(null));
        System.out.println(JSON.parse(""));
        ;
        String day = DateTime.now().toString("yyyyMMdd");
        System.out.println(day);
        System.out.println("123345".split("\\" + point)[0]);
        System.out.println(JSONObject.parseObject("{'abc':123}", Object.class));
        String PARAM_CODE_FORMAT = "#'{'{0}'}'";
        String value = "#{dateTime}-#{dateTime}";
        value = value.replace(MessageFormat.format(PARAM_CODE_FORMAT, "dateTime"), "111111");
        System.out.println(value);
        Matcher matcher = PARAM_PATTERN.matcher("#{124}");
        while (matcher.find()) {
            System.out.println(matcher.group(1));
        }
        System.out.println(NumberUtils.isDigits("232"));
        List<User> list = Lists.newArrayList(
                User.builder().id(1).type(1).index(1).build(),
                User.builder().id(1).type(1).index(1).build(),
                User.builder().id(1).type(1).index(2).build(),
                User.builder().id(1).type(1).index(3).build(),
                User.builder().id(1).type(2).index(1).build(),
                User.builder().id(1).type(2).index(2).build(),
                User.builder().id(1).type(2).index(2).build(),
                User.builder().id(1).type(2).index(3).build(),
                User.builder().id(2).type(1).index(1).build(),
                User.builder().id(2).type(1).index(1).build(),
                User.builder().id(2).type(2).index(1).build(),
                User.builder().id(null).type(2).index(1).build());
        Map<Integer, User> userMap = list.stream().collect(toMap(User::getId, Function.identity(),(v1,v2)->v2));
        userMap.put(2,new User());
        Map<Integer, List<User>> userGroupMap = list.stream().collect(Collectors.groupingBy(item->{
            if(item.getId()==null){
                return -1;
            }else{
                return item.getId();
            }
        }));
//        System.out.println(list.stream().filter(it -> it.getId() > 100).findFirst().orElse(null));
        int retry = 2;
        while (retry > 0) {
            Iterator<User> it = list.iterator();
            while (it.hasNext()) {
                User user = it.next();
                it.remove();
            }
            if (CollectionUtils.isEmpty(list)) {
                break;
            }
            retry--;
        }
        System.out.println(list);
        Map<Integer, Map<Integer, List<Integer>>> resultMap = list.stream().collect(Collectors.groupingBy(User::getId,
                Collectors.groupingBy(User::getType, Collectors.mapping(User::getIndex, toList()))));
        System.out.println(resultMap.get(null));
        System.out.println(resultMap);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class User {
        private Integer id;
        private Integer type;
        private Integer index;
        private Date date;
        private String key;
    }
}
