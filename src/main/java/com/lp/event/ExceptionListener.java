package com.lp.event;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/11/4 18:42
 */
@Slf4j
public class ExceptionListener {

    @Subscribe
    public void m1(final String event) {
        log.info("Received event [{}] and will take m1", event);
    }

    @Subscribe
    public void m2(final String event) {
        log.info("Received event [{}] and will take m2", event);
    }

    @Subscribe
    public void m3(final String event) {
        throw new RuntimeException();
    }
}

