package com.lp.es.common.annotation;


import com.lp.es.common.enums.Index;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * ES搜索类注解, 用于指定搜索的用户类型.
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/02
 **/
@Target(TYPE)
@Retention(RUNTIME)
public @interface Search {

    Index value();

}
