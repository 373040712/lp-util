package com.lp.es.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * ES搜索排序后的返回值注解, 用于指定排序返回值的映射字段
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/02
 **/
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface SearchField {

    /**
     * 对应ES返回字段的名称, 缺省设置与类字段名一致.
     * <p>
     * 如果返回字段包含多层级(JSON), 则以点(.)相隔, 如:
     * <pre>
     *     "extendInfo": {
     *       "sendTimesLast30Days": 12,
     *       "sendTimesLast60Days": 12,
     *       "sendTimesLast90Days": 12,
     *       "frequentlySendPosition": {
     *         "lon": 32.32,
     *         "lat": 78.33
     *       }
     *     }
     * </pre>
     * 要取常发货地址的经度的话, 则value = "extendInfo.frequentlySendPosition.lon"
     * </p>
     */
    String value() default "";
}
