package com.lp.es.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * ES搜索排序后的返回值注解, 用于指定排序返回值的映射字段
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/02
 **/
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface SortedValue {

    /**
     * 对应ES排序返回值的名称, 缺省设置与类字段名一致.
     * <p>
     * Avatar返回排序值时没有层级关系, 以键值对的方式返回, 其key等于调用方设置的排序字段名, 比如:
     * <pre>
     *     esQueryParam.addSort(Sort.builder().name("extendInfo.lastSendTime").build());
     * </pre>
     * 则返回键值对为: extendInfo.lastSendTime = xxx, 要获取该排序返回值, 仅需作简单的字段映射:
     * <pre>
     *     //@SortedValue("extendInfo.lastSendTime")
     *     //private Date sortValueOfLastSendTime;
     * </pre>
     * </p>
     */
    String value() default "";
}
