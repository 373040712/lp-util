package com.lp.es.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 条件枚举
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Condition {
    MUST, MUST_NOT, SHOULD
}
