package com.lp.es.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * ES 索引配置
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum Index {

    CRM_CUSTOMER("crm-customer", "customer");

    private String indexAlias;

    private String indexType;

    public static String[] getAllIndicesAlias() {
        return Arrays.stream(Index.values()).map(Index::getIndexAlias).toArray(String[]::new);
    }
}
