package com.lp.es.common.util;

/**
 * @author liupeng2@tinman.cn
 * @date 2020/07/03
 **/
public class Const {

    public static final String TYPE_GEO = "geo_point";
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_GEO_POINT_ATTRIBUTE = "geo_point_attr";

    public static final Integer MAX_PS = 1000;
    public static final Integer DEFAULT_PS = 10;
    public static final Integer DEFAULT_PN = 1;
    public static final String PROPERTY_ID = "id";

    public static final String PROPERTIES_KEY = "properties";
    public static final String PROPERTY_TYPE_KEY = "type";
    public static final String PROPERTY_INDEX_KEY = "index";
    public static final String PROPERTY_NAME_DELIMITER = ".";
    public static final String PROPERTY_LON_KEY = "lon";
    public static final String PROPERTY_LAT_KEY = "lat";

    public static final String TYPE_NESTED_KEY = "nested";

    public static final String SORT_VALUE_PREFIX = "$SORT#";
    public static final String EMPTY = "";
}
