package com.lp.es.common.util;


import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.lp.es.common.annotation.Search;
import com.lp.es.common.annotation.SearchField;
import com.lp.es.common.annotation.SortedValue;
import com.lp.es.common.dto.Property;
import com.lp.es.common.dto.QueryHit;
import com.lp.es.common.enums.Index;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.lp.es.common.util.Const.*;


/**
 * ES 工具
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/03
 **/
@Slf4j
public class EsUtil {

    /**
     * ES 属性集合
     */
    private static final Map<String, Map<String, Property>> ALL_INDEX_PROPERTIES_MAP = new ConcurrentHashMap<>();

    /**
     * 刷新是ES索引所有属性缓存
     *
     * @param aliases
     * @param sourceMap
     */
    public static void refreshPropertyCache(Set<String> aliases, Map<String, Map> sourceMap) {
        Map<String, Property> propertiesMap = new LinkedHashMap<>();
        extractProperties(sourceMap, propertiesMap, null, false, null);
        aliases.forEach(alias -> {
            Map<String, Property> dataMap = ALL_INDEX_PROPERTIES_MAP.computeIfAbsent(alias, it -> new HashMap<>());
            dataMap.putAll(propertiesMap);
        });
    }

    /**
     * 获得缓存map
     *
     * @return
     */
    public static Map<String, Map<String, Property>> getAllPropertyMap() {
        return ALL_INDEX_PROPERTIES_MAP;
    }

    /**
     * 获得ES索引属性
     *
     * @param index
     * @return
     */
    public static Map<String, Property> getPropertyMap(String index) {
        Map<String, Property> properties = ALL_INDEX_PROPERTIES_MAP.get(index);
        return properties == null ? Collections.emptyMap() : properties;
    }

    /**
     * 获得索引，属性信息
     *
     * @param index
     * @param name
     * @return
     */
    public static Property getProperty(String index, String name) {
        return getPropertyMap(index).get(name);
    }

    /**
     * 获得索引所有属性名称集合
     *
     * @param index
     * @return
     */
    public static Set<String> getPropertyNames(String index) {
        return getPropertyMap(index).keySet();

    }

    /**
     * 结果对象解析获得索引以及查询属性
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> Pair<Index, Set<String>> resolveDataClass(Class<T> clazz) {
        Search search = clazz.getAnnotation(Search.class);
        Set<String> includeFields = Sets.newHashSet();
        ReflectionUtils.doWithFields(clazz, field -> {
            SearchField searchField = field.getAnnotation(SearchField.class);
            if (Objects.isNull(searchField)) {
                return;
            }
            if (Objects.nonNull(field.getAnnotation(SortedValue.class))) {
                throw new RuntimeException(String.format("@%s不能与@%s同时使用！[%s#%s]，请检查一下哟 ^_^",
                        SearchField.class.getSimpleName(), SortedValue.class.getSimpleName(),
                        clazz.getName(), field.getName()));
            }
            includeFields.add(Strings.isBlank(searchField.value()) ? field.getName() : searchField.value());
        });
        if (includeFields.isEmpty()) {
            throw new RuntimeException(String.format("clazz【%s】至少有1个属性必须包含@SearchField【%s】，请检查一下哟 ^_^",
                    clazz.getName(), SearchField.class.getName()));
        }
        return new ImmutablePair<>(search.value(), includeFields);
    }

    /**
     * 获得指定对象结果集
     *
     * @param queryHits
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> convertToDataList(List<QueryHit> queryHits, Class<T> clazz) {
        List<T> dataList = Lists.newArrayListWithCapacity(queryHits.size());
        for (QueryHit queryHit : queryHits) {
            try {
                dataList.add(convertHitToData(queryHit, clazz));
            } catch (Exception e) {
                log.error("初始化对象失败，错误-{}", e.getMessage(), e);
                throw new RuntimeException("初始化对象失败，请联系研发哥哥哟 o(╥﹏╥)o");
            }
        }
        return dataList;
    }

    /**
     * 查询结果转map
     *
     * @param queryHit
     * @return
     */
    public static Map<String, Object> convertHitToMap(QueryHit queryHit) {
        if (MapUtils.isEmpty(queryHit.getObject())) {
            return Collections.emptyMap();
        }
        Map<String, Object> result = new HashMap<>();
        assembleDataMap(queryHit.getObject(), null, result);
        return result;
    }

    private static void assembleDataMap(Map<String, Object> object, String parentKey, Map<String, Object> dataMap) {
        object.entrySet().forEach(it -> {
            String tempKey = getParentKey(parentKey, it.getKey());
            if (it.getValue() instanceof Map) {
                assembleDataMap((Map<String, Object>) it.getValue(), tempKey, dataMap);
            } else if (it.getValue() instanceof List) {
                List<Map<String, Object>> list = (List<Map<String, Object>>) it.getValue();
                list.forEach(item -> assembleDataMap(item, tempKey, dataMap));
            } else {
                Object obj = dataMap.get(tempKey);
                if (obj != null) {
                    if (obj instanceof List) {
                        List list = (List) obj;
                        list.add(it.getValue());
                    } else {
                        List list = Lists.newArrayList(obj, it.getValue());
                        obj = list;
                    }
                } else {
                    obj = it.getValue();
                }
                dataMap.put(tempKey, obj);
            }
        });
    }

    private static <T> T convertHitToData(QueryHit queryHit, Class<T> clazz) throws Exception {
        T data = clazz.newInstance();
        boolean searchMapFlag = MapUtils.isNotEmpty(queryHit.getObject());
        boolean sortMapFlag = MapUtils.isNotEmpty(queryHit.getSortValues());
        ReflectionUtils.doWithFields(clazz, field -> {
            if (searchMapFlag) {
                SearchField searchField = field.getAnnotation(SearchField.class);
                if (Objects.nonNull(searchField)) {
                    setClassField(field, data, queryHit.getObject(), listPath(searchField, field));
                    return;
                }
            }
            if (sortMapFlag) {
                SortedValue sortedValue = field.getAnnotation(SortedValue.class);
                if (Objects.nonNull(sortedValue)) {
                    setClassField(field, data, queryHit.getSortValues(), listPath(sortedValue, field));
                    return;
                }
            }
        });
        return data;
    }

    private static void extractProperties(Map<String, Map> sourceMap, Map<String, Property> propertiesMap,
                                          String parentKey, boolean nestedFlag, String nested) {
        if (Objects.isNull(sourceMap)) {
            return;
        }
        for (Map.Entry<String, Map> entry : sourceMap.entrySet()) {
            Map entryValue = entry.getValue();
            if (Objects.nonNull(entryValue.get(PROPERTIES_KEY))) {
                if (!nestedFlag && Objects.equals(TYPE_NESTED_KEY, entryValue.get(PROPERTY_TYPE_KEY))) {
                    extractProperties((Map<String, Map>) entryValue.get(PROPERTIES_KEY), propertiesMap,
                            getParentKey(parentKey, entry.getKey()), true, getParentKey(parentKey, entry.getKey()));
                } else {
                    extractProperties((Map<String, Map>) entryValue.get(PROPERTIES_KEY), propertiesMap,
                            getParentKey(parentKey, entry.getKey()), nestedFlag, nested);
                }
            } else {
                Property property = new Property();
                property.setName(getParentKey(parentKey, entry.getKey()));
                property.setType((String) entryValue.get(PROPERTY_TYPE_KEY));
                property.setIndex(Objects.nonNull(entryValue.get(PROPERTY_INDEX_KEY)) ? (Boolean) entryValue.get(PROPERTY_INDEX_KEY) : true);
                propertiesMap.put(property.getName(), property);
                if (nestedFlag && Strings.isNotBlank(nested)) {
                    property.setNestedName(nested);
                }
                // 因为ES针对geo_point的mapping不会返回其lon和lat属性，而用户在设置includeFields时可能会这样设置，所以这里做个特殊处理，将lon和lat也加入到properties中
                if (property.isGeoType()) {
                    Property lonProperty = new Property();
                    lonProperty.setName(property.getName() + PROPERTY_NAME_DELIMITER + PROPERTY_LON_KEY);
                    lonProperty.setType(TYPE_GEO_POINT_ATTRIBUTE);
                    lonProperty.setIndex(true);
                    propertiesMap.put(lonProperty.getName(), lonProperty);

                    Property latProperty = new Property();
                    latProperty.setName(property.getName() + PROPERTY_NAME_DELIMITER + PROPERTY_LAT_KEY);
                    latProperty.setType(TYPE_GEO_POINT_ATTRIBUTE);
                    latProperty.setIndex(true);
                    propertiesMap.put(latProperty.getName(), latProperty);
                }
            }
        }
    }

    private static String getParentKey(String parentKey, String key) {
        return (Objects.isNull(parentKey) ? EMPTY : parentKey + PROPERTY_NAME_DELIMITER) + key;
    }

    private static List<String> listPath(SearchField annotation, Field field) {
        String name = Strings.isBlank(annotation.value()) ? field.getName() : annotation.value();
        return Splitter.on(PROPERTY_NAME_DELIMITER).splitToList(name);
    }

    private static List<String> listPath(SortedValue annotation, Field field) {
        String name = Strings.isBlank(annotation.value()) ? field.getName() : annotation.value();
        return Splitter.on(PROPERTY_NAME_DELIMITER).splitToList(name);
    }

    private static <T> void setClassField(Field field, T target, Map<String, Object> dataMap, List<String> paths) {
        if (CollectionUtils.isEmpty(paths)) {
            return;
        }
        List<String> currPaths = new ArrayList<>(paths);
        Object value = dataMap.get(currPaths.remove(0));
        if (Objects.isNull(value)) {
            return;
        }
        if ((value instanceof Map) && CollectionUtils.isNotEmpty(currPaths)) {
            setClassField(field, target, (Map<String, Object>) value, currPaths);
        } else if ((value instanceof List) && CollectionUtils.isNotEmpty(currPaths)) {
            List<Map<String, Object>> tValue = (List<Map<String, Object>>) value;
            tValue.forEach(it -> setClassField(field, target, it, currPaths));
        } else {
            setField(field, target, value);
        }
    }

    private static <T> void setField(Field field, T target, Object value) {
        field.setAccessible(true);
        Type type = field.getType();
        if (Long.class.equals(type)) {
            ReflectionUtils.setField(field, target, Long.valueOf(value.toString()));
        } else if (Integer.class.equals(type)) {
            ReflectionUtils.setField(field, target, Integer.valueOf(value.toString()));
        } else if (Double.class.equals(type)) {
            ReflectionUtils.setField(field, target, Double.valueOf(value.toString()));
        } else if (Float.class.equals(type)) {
            ReflectionUtils.setField(field, target, Float.valueOf(value.toString()));
        } else if (Date.class.equals(type)) {
            if (value instanceof Long) {
                // 转换时间戳
                ReflectionUtils.setField(field, target, new Date((Long) value));
            } else if (value instanceof String) {
                // 否则按字符串格式转换
                ReflectionUtils.setField(field, target, DateUtil.parse(value.toString(), "yyyy-MM-dd HH:mm:ss"));
            }
        } else if (String.class.equals(type)) {
            ReflectionUtils.setField(field, target, value.toString());
        } else if (BigDecimal.class.equals(type)) {
            if (value instanceof Integer) {
                ReflectionUtils.setField(field, target, new BigDecimal((Integer) value));
            } else if (value instanceof Long) {
                ReflectionUtils.setField(field, target, new BigDecimal((Long) value));
            } else if (value instanceof Double) {
                ReflectionUtils.setField(field, target, new BigDecimal((Double) value));
            } else if (value instanceof Float) {
                ReflectionUtils.setField(field, target, new BigDecimal((Float) value));
            } else {
                ReflectionUtils.setField(field, target, value);
            }
        } else if (Boolean.class.equals(type)) {
            if (value instanceof Number) {
                ReflectionUtils.setField(field, target, new Boolean(((Number) value).intValue() > 0));
            } else {
                ReflectionUtils.setField(field, target, value);
            }
        } else if (List.class.equals(type) || Set.class.equals(type)) {
            //统一用list，不支持set
            Type genericType = field.getGenericType();
            Class<?> genericClazz = Object.class;
            // 如果是泛型参数的类型
            if (genericType instanceof ParameterizedType) {
                ParameterizedType pt = (ParameterizedType) genericType;
                // 得到泛型里的class类型对象
                genericClazz = (Class<?>) pt.getActualTypeArguments()[0];
            }
            List result = new ArrayList<>();
            if (value instanceof List) {
                result.addAll(JSONObject.parseArray(JSON.toJSONString(value), genericClazz));
            } else {
                result.add(JSONObject.parseObject(JSON.toJSONString(value), genericClazz));
            }
            if (Set.class.equals(type)) {
                Set set = (Set) ReflectionUtils.getField(field, target);
                if (CollectionUtils.isEmpty(set)) {
                    set = new HashSet();
                }
                set.addAll(result);
                ReflectionUtils.setField(field, target, set);
            } else {
                List list = (List) ReflectionUtils.getField(field, target);
                if (CollectionUtils.isEmpty(list)) {
                    list = new ArrayList();
                }
                list.addAll(result);
                ReflectionUtils.setField(field, target, list);
            }
        } else {
            // double等精度类型必须定义为BigDecimal
            ReflectionUtils.setField(field, target, JSONObject.parseObject(JSON.toJSONString(value), type));
        }
    }
}
