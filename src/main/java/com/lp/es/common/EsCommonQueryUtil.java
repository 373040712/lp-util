package com.lp.es.common;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.lp.es.common.dto.Property;
import com.lp.es.common.dto.QueryHit;
import com.lp.es.common.dto.QueryParam;
import com.lp.es.common.dto.QueryResult;
import com.lp.es.common.dto.query.GeoBoundingBox;
import com.lp.es.common.dto.query.GroupQuery;
import com.lp.es.common.dto.query.Query;
import com.lp.es.common.enums.Condition;
import com.lp.es.common.enums.Index;
import com.lp.es.common.util.EsUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.util.Strings;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetMappingsRequest;
import org.elasticsearch.client.indices.GetMappingsResponse;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.*;

import static com.lp.es.common.util.Const.MAX_PS;
import static com.lp.es.common.util.Const.PROPERTIES_KEY;
import static org.elasticsearch.client.RequestOptions.DEFAULT;

/**
 * @author liupeng2@tinman.cn
 * @date 2020/07/06
 **/
@Slf4j
public class EsCommonQueryUtil {

    private RestHighLevelClient restHighLevelClient;

    public EsCommonQueryUtil(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    public void reloadMappings() {
        try {
            //获得索引别名
            GetMappingsRequest getMappingsRequest = new GetMappingsRequest().indices(Index.getAllIndicesAlias());
            GetMappingsResponse getMappingsResponse = restHighLevelClient.indices().getMapping(getMappingsRequest, DEFAULT);
            getMappingsResponse.mappings().entrySet().forEach(it -> {
                try {
                    EsUtil.refreshPropertyCache(Sets.newHashSet(Index.getAllIndicesAlias()), (Map<String, Map>) it.getValue().sourceAsMap().get(PROPERTIES_KEY));
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage());
                }
            });
        } catch (Exception e) {
            log.error("获取ElasticSearch Property发生异常，Error：{}", e.getMessage(), e);
        }
    }

    public QueryResult<QueryHit> commonQuery(QueryParam param) {
        Assert.notNull(param, "查询参数不能为空");
        //检查查询参数
        checkQueryParam(param);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().from(param.getStart()).size(param.getEnd())
                .fetchSource(param.getIncludeFields().toArray(new String[0]), null);
        //组装查询条件
        assembleQueries(param, searchSourceBuilder);
        //组装排序条件
        assembleSorts(param, searchSourceBuilder);
        SearchRequest searchRequest = new SearchRequest().indices(param.getIndex().getIndexAlias())
                .types(param.getIndex().getIndexType()).source(searchSourceBuilder);
        //查询结果
        SearchResponse response;
        try {
            log.info("******ES：索引-{}，类型-{}，查询语句-{}", param.getIndex().getIndexAlias(),
                    param.getIndex().getIndexType(), searchSourceBuilder.toString());
            response = restHighLevelClient.search(searchRequest, DEFAULT);
        } catch (IOException e) {
            log.error("******ES：索引-{}，类型-{}，查询语句-{}，查询失败原因-{}", param.getIndex().getIndexAlias(),
                    param.getIndex().getIndexType(), searchSourceBuilder.toString(), e.getMessage(), e);
            throw new RuntimeException("查询出错了，请联系研发哥哥哟 o(╥﹏╥)o");
        }
        SearchHits searchHits = response.getHits();
        long total = searchHits.getTotalHits();
        //组装查询结果
        QueryResult result = new QueryResult();
        result.setTc(total);
        List<QueryHit> dataList = new ArrayList<>((int) total);
        searchHits.forEach(hit -> {
            QueryHit queryHit = new QueryHit();
            queryHit.setObject(hit.getSourceAsMap());
            if (ArrayUtils.isNotEmpty(hit.getSortValues())) {
                Map<String, Object> sortValues = Maps.newLinkedHashMap();
                for (int i = 0; i < param.getSorts().size(); i++) {
                    sortValues.put(param.getSorts().get(i).getName(), hit.getSortValues()[i]);
                }
                queryHit.setSortValues(sortValues);
            }
            dataList.add(queryHit);
        });
        result.setData(dataList);
        return result;
    }

    public <T> QueryResult<T> commonQuery(QueryParam param, Class<T> clazz) {
        Pair<Index, Set<String>> pair = EsUtil.resolveDataClass(clazz);
        //设置查询索引
        param.setIndex(pair.getLeft());
        //设置查询字段
        param.setIncludeFields(pair.getRight());
        //查询结果
        QueryResult<QueryHit> result = commonQuery(param);
        //组装结果
        QueryResult<T> tResult = new QueryResult<T>();
        tResult.setTc(result.getTc());
        //结果记录
        List<QueryHit> queryHits = result.getData();
        if (CollectionUtils.isNotEmpty(queryHits)) {
            //获得对象结果集
            tResult.setData(EsUtil.convertToDataList(queryHits, clazz));
        }
        return tResult;
    }

    private void assembleQueries(QueryParam param, SearchSourceBuilder searchSourceBuilder) {
        if (CollectionUtils.isEmpty(param.getQueries()) && CollectionUtils.isEmpty(param.getGroupQueries())) {
            return;
        }
        BoolQueryBuilder boolBuilder = QueryBuilders.boolQuery();
        buildQuery(boolBuilder, param.getQueries(), true);
        buildGroupQuery(boolBuilder, param.getGroupQueries(), null);
        searchSourceBuilder.query(boolBuilder);
    }

    private void buildQuery(BoolQueryBuilder boolBuilder, List<Query> queries, boolean checkNested) {
        if (CollectionUtils.isEmpty(queries)) {
            return;
        }
        queries.forEach(query -> {
            QueryBuilder queryBuilder = getBuildQuery(query);
            if (queryBuilder == null) {
                return;
            }
            NestedQueryBuilder nestedBuilder = null;
            if (checkNested && StringUtils.isNotBlank(query.getProperty().getNestedName())) {
                nestedBuilder = QueryBuilders.nestedQuery(query.getProperty().getNestedName(), queryBuilder, ScoreMode.None);
            }
            appendQueryByCondition(boolBuilder, Objects.isNull(nestedBuilder) ? queryBuilder : nestedBuilder, query.getCondition());
        });
    }

    private void buildGroupQuery(BoolQueryBuilder boolBuilder, List<GroupQuery> groupQueries, String nestedName) {
        if (CollectionUtils.isEmpty(groupQueries)) {
            return;
        }
        for (GroupQuery groupQuery : groupQueries) {
            if (CollectionUtils.isEmpty(groupQuery.getQueries()) && CollectionUtils.isEmpty(groupQuery.getGroupQueries())) {
                continue;
            }
            //获得nested类型那么
            String tempNestedName = Strings.isNotBlank(groupQuery.getNestedName()) ? groupQuery.getNestedName() : nestedName;
            BoolQueryBuilder groupBoolQuery = QueryBuilders.boolQuery();
            //组装查询
            buildQuery(groupBoolQuery, groupQuery.getQueries(), Strings.isBlank(tempNestedName));
            //组装子查询
            buildGroupQuery(groupBoolQuery, groupQuery.getGroupQueries(), tempNestedName);
            //嵌套查询
            NestedQueryBuilder nestedBuilder = null;
            if (StringUtils.isNotBlank(tempNestedName) && !Objects.equals(tempNestedName, nestedName)) {
                nestedBuilder = QueryBuilders.nestedQuery(tempNestedName, groupBoolQuery, ScoreMode.None);
            }
            //添加条件
            appendQueryByCondition(boolBuilder, Objects.isNull(nestedBuilder) ? groupBoolQuery : nestedBuilder, groupQuery.getCondition());
        }
    }

    private QueryBuilder getBuildQuery(Query query) {
        QueryBuilder queryBuilder = null;
        Property property = query.getProperty();
        if (Objects.nonNull(query.getExists())) {
            if (query.getExists()) {
                queryBuilder = QueryBuilders.existsQuery(query.getName());
            } else {
                queryBuilder = QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(query.getName()));
            }
        } else {
            if (property.isTextType()) {
                queryBuilder = QueryBuilders.constantScoreQuery(QueryBuilders.matchPhraseQuery(query.getName(),
                        IterableUtils.get(query.getIn(), 0)));
            } else if (property.isGeoType() && Objects.nonNull(query.getGeo())) {
                queryBuilder = QueryBuilders.geoDistanceQuery(query.getName()).point(query.getGeo().getLat(),
                        query.getGeo().getLng()).distance(query.getGeo().getDistance(), DistanceUnit.METERS);
            } else if (Objects.nonNull(query.getGte()) || Objects.nonNull(query.getLte())
                    || Objects.nonNull(query.getGt()) || Objects.nonNull(query.getLt())) {
                RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(query.getName());
                if (Objects.nonNull(query.getGt())) {
                    rangeQueryBuilder.gt(query.getGt());
                } else if (Objects.nonNull(query.getGte())) {
                    rangeQueryBuilder.gte(query.getGte());
                }
                if (Objects.nonNull(query.getLt())) {
                    rangeQueryBuilder.lt(query.getLt());
                } else if (Objects.nonNull(query.getLte())) {
                    rangeQueryBuilder.lte(query.getLte());
                }
                queryBuilder = QueryBuilders.constantScoreQuery(rangeQueryBuilder);
            } else if (StringUtils.isNotBlank(query.getWildcard())) {
                queryBuilder = QueryBuilders.constantScoreQuery(QueryBuilders.wildcardQuery(query.getName(),
                        query.getWildcard()));
            } else if (CollectionUtils.isNotEmpty(query.getIn())) {
                queryBuilder = QueryBuilders.constantScoreQuery(QueryBuilders.termsQuery(query.getName(),
                        query.getIn().toArray()));
            } else if (CollectionUtils.isNotEmpty(query.getNin())) {
                queryBuilder = QueryBuilders.boolQuery().mustNot(QueryBuilders.termsQuery(query.getName(),
                        query.getNin().toArray()));
            } else if (property.isGeoType() && Objects.nonNull(query.getGeoBoundingBox())) {
                GeoBoundingBox boundingBox = query.getGeoBoundingBox();
                queryBuilder = QueryBuilders.geoBoundingBoxQuery(query.getName()).setCorners(
                        new GeoPoint(boundingBox.getTopLeftLat(), boundingBox.getTopLeftLng()),
                        new GeoPoint(boundingBox.getBottomRightLat(), boundingBox.getBottomRightLng()));
            }
        }
        return queryBuilder;
    }

    private void appendQueryByCondition(BoolQueryBuilder boolBuilder, QueryBuilder query, Condition condition) {
        if (Objects.equals(Condition.MUST, condition)) {
            boolBuilder.must(query);
        } else if (Objects.equals(Condition.MUST_NOT, condition)) {
            boolBuilder.mustNot(query);
        } else if (Objects.equals(Condition.SHOULD, condition)) {
            boolBuilder.should(query);
        }
    }

    private void assembleSorts(QueryParam param, SearchSourceBuilder searchSourceBuilder) {
        if (CollectionUtils.isEmpty(param.getSorts())) {
            return;
        }
        param.getSorts().forEach(s -> {
            if (Objects.nonNull(s.getGeo())) {
                searchSourceBuilder.sort(SortBuilders.geoDistanceSort(s.getName(), s.getGeo().getLat(), s.getGeo().getLng())
                        .order(s.getAsc() ? SortOrder.ASC : SortOrder.DESC));
            } else {
                searchSourceBuilder.sort(SortBuilders.fieldSort(s.getName()).order(s.getAsc() ? SortOrder.ASC : SortOrder.DESC));
            }
        });
    }

    private void checkQueryParam(QueryParam param) {
        if (Objects.isNull(param.getIndex())) {
            throw new RuntimeException("索引信息不能为空哟 ^_^");
        }
        Map<String, Property> properties = EsUtil.getPropertyMap(param.getIndex().getIndexAlias());
        if (MapUtils.isEmpty(properties)) {
            throw new RuntimeException("未获取到对应的ES Mapping Properties，请联系研发哥哥哟 ^_^");
        }
        if (CollectionUtils.isNotEmpty(param.getQueries())) {
            param.getQueries().forEach(query -> validateAndInitQuery(query, properties, null));
        }
        if (CollectionUtils.isNotEmpty(param.getGroupQueries())) {
            param.getGroupQueries().forEach(groupQuery -> validateGroupQuery(groupQuery, properties, null));
        }
        if (CollectionUtils.isNotEmpty(param.getSorts())) {
            param.getSorts().forEach(it -> {
                Property property = properties.get(it.getName());
                if (Objects.isNull(property) || !property.getIndex() || property.isGeoTypeAttr()
                        || Strings.isNotBlank(property.getNestedName())) {
                    throw new RuntimeException(String.format("字段【%s】不支持排序，请检查一下哟 ^_^", it.getName()));
                }
                if (property.isGeoType()) {
                    if (Objects.isNull(it.getGeo()) || Objects.isNull(it.getGeo().getLat()) || Objects.isNull(it.getGeo().getLng())) {
                        throw new RuntimeException(String.format("GEO字段【%s】排序需要地理信息参数，请检查一下哟 ^_^", it.getName()));
                    }
                } else {
                    //清空非geo类型排序geo数据
                    it.setGeo(null);
                }
            });
        }
        if (CollectionUtils.isNotEmpty(param.getIncludeFields())) {
            Set<String> paths = EsUtil.getPropertyNames(param.getIndex().getIndexAlias());
            param.getIncludeFields().forEach(it -> {
                if (paths.stream().noneMatch(i -> i.startsWith(it))) {
                    throw new RuntimeException(String.format("返回字段【%s】不存在，请检查一下哟 ^_^", it));
                }
            });
        }
        if (param.getPs() > MAX_PS) {
            throw new RuntimeException(String.format("每页最多支持【%d】条，请检查一下哟 ^_^", MAX_PS));
        }
    }

    private void validateAndInitQuery(Query query, Map<String, Property> properties, String nestedName) {
        Property property = properties.get(query.getName());
        if (Objects.isNull(property) || !property.getIndex()) {
            throw new RuntimeException(String.format("字段【%s】不支持搜索，请检查一下哟 ^_^", query.getName()));
        }
        if (property.isGeoTypeAttr()) {
            throw new RuntimeException(String.format("字段【%s】不支持此种查询，请使用GEO方式哟 ^_^", query.getName()));
        } else if (property.isGeoType()) {
            if (Objects.isNull(query.getGeo()) && Objects.isNull(query.getGeoBoundingBox())) {
                throw new RuntimeException(String.format("GEO字段【%s】查询需要地理位置参数，请检查一下哟 ^_^", query.getName()));
            }
        } else if (property.isTextType()) {
            if (CollectionUtils.isEmpty(query.getIn())) {
                throw new RuntimeException(String.format("字段【%s】未设置查询值，请检查一下哟 ^_^", query.getName()));
            }
        } else {
            if (Objects.isNull(query.getGte()) && Objects.isNull(query.getLte()) && Objects.isNull(query.getGt())
                    && Objects.isNull(query.getLt()) && CollectionUtils.isEmpty(query.getNin())
                    && CollectionUtils.isEmpty(query.getIn()) && Objects.isNull(query.getExists())) {
                throw new RuntimeException(String.format("字段【%s】未设置查询值，请检查一下哟 ^_^", query.getName()));
            }
        }
        if (Strings.isNotBlank(nestedName)) {
            if (Strings.isBlank(property.getNestedName())) {
                throw new RuntimeException(String.format("字段【%s】不支持nested查询，请检查一下哟 ^_^", query.getName()));
            }
            if (!nestedName.equals(property.getNestedName())) {
                throw new RuntimeException(String.format("字段【%s】不支持nested查询类型【%s】，请检查一下哟 ^_^",
                        query.getName(), nestedName));
            }
        }
        //设置属性值
        query.setProperty(property);
    }

    private void validateGroupQuery(GroupQuery groupQuery, Map<String, Property> properties, String nestedName) {
        if (CollectionUtils.isNotEmpty(groupQuery.getQueries())) {
            groupQuery.getQueries().forEach(q -> validateAndInitQuery(q, properties,
                    Strings.isNotBlank(groupQuery.getNestedName()) ? groupQuery.getNestedName() : nestedName));
        }
        if (CollectionUtils.isNotEmpty(groupQuery.getGroupQueries())) {
            groupQuery.getGroupQueries().forEach(q -> validateGroupQuery(q, properties,
                    Strings.isNotBlank(q.getNestedName()) ? q.getNestedName() :
                            (Strings.isNotBlank(groupQuery.getNestedName()) ? groupQuery.getNestedName() : nestedName)
                    )
            );
        }
    }
}
