package com.lp.es.common.dto.query;

import com.lp.es.common.dto.Property;
import com.lp.es.common.enums.Condition;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 条件类
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Query<T> implements Serializable {

    private static final long serialVersionUID = 8122436505339374687L;
    /**
     * 属性名称
     */
    private String name;
    @Singular("in")
    private List<T> in;
    @Singular("nin")
    private List<T> nin;
    private T gte;
    private T gt;
    private T lte;
    private T lt;
    private String wildcard;
    private GeoField geo;
    private GeoBoundingBox geoBoundingBox;
    @Builder.Default
    private Condition condition = Condition.MUST;
    private Boolean exists;
    private Property property;
}
