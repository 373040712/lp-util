package com.lp.es.common.dto;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.lp.es.common.dto.query.GroupQuery;
import com.lp.es.common.dto.query.Query;
import com.lp.es.common.dto.query.Sort;
import com.lp.es.common.enums.Index;
import lombok.Data;

import java.io.Serializable;
import java.util.*;

import static com.lp.es.common.util.Const.*;

/**
 * ES 通用搜索DTO
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/02
 **/
@Data
public class QueryParam implements Serializable {

    private static final long serialVersionUID = 7850045449596016002L;

    /**
     * 索引配置
     */
    private Index index = Index.CRM_CUSTOMER;

    /**
     * 查询属性，默认只查id
     */
    private Set<String> includeFields = Sets.newHashSet(PROPERTY_ID);

    /**
     * 普通查询
     */
    private List<Query> queries;

    /**
     * 组合查询，可嵌套
     */
    private List<GroupQuery> groupQueries;

    /**
     * 排序字段
     */
    private List<Sort> sorts;

    /**
     * 页码
     */
    private Integer pn = DEFAULT_PN;

    /**
     * 页大小
     */
    private Integer ps = DEFAULT_PS;

    /**
     * 添加source属性
     *
     * @param field
     */
    public void addIncludeField(String... field) {
        addIncludeField(Lists.newArrayList(field));
    }

    /**
     * 添加source属性
     *
     * @param fields
     */
    public void addIncludeField(Collection fields) {
        if (Objects.isNull(includeFields)) {
            includeFields = new HashSet<>(fields);
        } else {
            includeFields.addAll(fields);
        }
    }

    /**
     * 添加查询条件
     *
     * @param query
     */
    public void addQuery(Query... query) {
        if (Objects.isNull(queries)) {
            queries = Lists.newArrayList(query);
        } else {
            queries.addAll(Lists.newArrayList(query));
        }
    }

    /**
     * 添加查询条件组
     *
     * @param groupQuery
     */
    public void addGroupQuery(GroupQuery... groupQuery) {
        if (Objects.isNull(groupQueries)) {
            groupQueries = Lists.newArrayList(groupQuery);
        } else {
            groupQueries.addAll(Lists.newArrayList(groupQuery));
        }
    }

    /**
     * 添加排序
     *
     * @param sort
     */
    public void addSort(Sort... sort) {
        if (Objects.isNull(sorts)) {
            sorts = Lists.newArrayList(sort);
        } else {
            sorts.addAll(Lists.newArrayList(sort));
        }
    }

    /**
     * from值
     *
     * @return
     */
    public Integer getStart() {
        return pn > 0 ? (pn - 1) * ps : 0;
    }

    /**
     * size值
     *
     * @return
     */
    public Integer getEnd() {
        return ps > MAX_PS ? MAX_PS : ps;
    }

}
