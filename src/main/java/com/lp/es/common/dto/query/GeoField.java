package com.lp.es.common.dto.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * geo属性类
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeoField implements Serializable {

    private static final long serialVersionUID = -6362569676895792855L;

    /**
     * 纬度经度
     */
    private Double lat;
    private Double lng;

    /**
     * 单位：米
     */
    private Double distance;
}
