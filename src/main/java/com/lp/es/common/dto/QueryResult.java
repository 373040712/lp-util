package com.lp.es.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * ES 通用结果DTO
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/02
 **/
@Data
public class QueryResult<T> implements Serializable {

    private static final long serialVersionUID = 2435955006306851869L;

    /**
     * 结果总数
     */
    private Long tc;

    /**
     * 结果记录
     */
    private List<T> data;
}
