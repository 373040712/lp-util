package com.lp.es.common.dto.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * geo box 类
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeoBoundingBox implements Serializable {

    private static final long serialVersionUID = -6016607522223573973L;
    
    /**
     * 左上经纬度
     */
    private Double topLeftLng;
    private Double topLeftLat;

    /**
     * 右下经纬度
     */
    private Double bottomRightLng;
    private Double bottomRightLat;
}
