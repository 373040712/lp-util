package com.lp.es.common.dto.query;

import com.google.common.collect.Lists;
import com.lp.es.common.enums.Condition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * 条件组类
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupQuery implements Serializable {

    private static final long serialVersionUID = 7674968528470897044L;
    /**
     * nested name
     */
    private String nestedName;
    /**
     * 查询
     */
    private List<Query> queries;
    /**
     * 子查询
     */
    private List<GroupQuery> groupQueries;
    /**
     * 条件
     */
    @Builder.Default
    private Condition condition = Condition.MUST;

    /**
     * 添加查询条件
     *
     * @param query
     */
    public void addQuery(Query... query) {
        if (Objects.isNull(queries)) {
            queries = Lists.newArrayList(query);
        } else {
            queries.addAll(Lists.newArrayList(query));
        }
    }

    /**
     * 添加查询条件组
     *
     * @param groupQuery
     */
    public void addGroupQuery(GroupQuery... groupQuery) {
        if (Objects.isNull(groupQueries)) {
            groupQueries = Lists.newArrayList(groupQuery);
        } else {
            groupQueries.addAll(Lists.newArrayList(groupQuery));
        }
    }
}
