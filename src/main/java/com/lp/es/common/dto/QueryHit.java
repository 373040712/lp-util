package com.lp.es.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * ES 结果hit
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/03
 **/
@Data
public class QueryHit implements Serializable {

    private static final long serialVersionUID = -3236362378299106781L;

    /**
     * 属性值
     */
    private Map<String, Object> object;

    /**
     * 排序属性值
     */
    private Map<String, Object> sortValues;
}
