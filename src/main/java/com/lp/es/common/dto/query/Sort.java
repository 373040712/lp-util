package com.lp.es.common.dto.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 排序属性类
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/04
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Sort implements Serializable {

    private static final long serialVersionUID = 8254898406870627940L;

    private String name;
    @Builder.Default
    private Boolean asc = Boolean.TRUE;
    private GeoField geo;
}
