package com.lp.es.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

import static com.lp.es.common.util.Const.*;


/**
 * ES 属性
 *
 * @author liupeng2@tinman.cn
 * @date 2020/07/02
 **/
@Data
public class Property implements Serializable {

    private static final long serialVersionUID = 7183046414022732731L;
    /**
     * 索引字段
     */
    private String name;

    /**
     * 索引类型
     */
    private String type;

    /**
     * 是否建立索引
     */
    private Boolean index = true;

    /**
     * nested属性字段
     */
    private String nestedName;

    /**
     * geo类型判断
     *
     * @return
     */
    public boolean isGeoType() {
        return Objects.equals(type, TYPE_GEO);
    }

    /**
     * 文本类型判断
     *
     * @return
     */
    public boolean isTextType() {
        return Objects.equals(type, TYPE_TEXT);
    }

    /**
     * geo point类型判断
     *
     * @return
     */
    public boolean isGeoTypeAttr() {
        return Objects.equals(type, TYPE_GEO_POINT_ATTRIBUTE);
    }
}
