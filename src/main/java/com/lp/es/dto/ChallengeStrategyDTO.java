package com.lp.es.dto;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author peng.liu
 * @date 2019/4/28 16:07
 */
@Data
@Accessors(chain = true)
public class ChallengeStrategyDTO {

    private String id;

    private String name;

    private String relationId;
    /**
     * 差异率
     */
    private Double diff;

    /**
     * 命中率
     */
    private Double hit;

    public ChallengeStrategyDTO copy() {
        return new ChallengeStrategyDTO().setId(id).setRelationId(relationId).setName(name).setDiff(diff).setHit(hit);
    }
}
