package com.lp.es.dto;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author peng.liu
 * @date 2019/4/28 16:18
 */
@Data
public class ChallengeCombinationDTO {

    private List<ChallengeStrategyDTO> strategyList = Lists.newArrayList();

    private Map<String, Long> statistics = Maps.newHashMap();

}
