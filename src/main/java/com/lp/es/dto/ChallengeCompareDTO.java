package com.lp.es.dto;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author peng.liu
 * @date 2019/4/28 16:15
 */
@Data
public class ChallengeCompareDTO {

    private ChallengeStrategyDTO champion;

    private List<ChallengeStrategyDTO> challenger = Lists.newArrayList();
}
