package com.lp.es.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class QueryResult implements Serializable {

    private static final long serialVersionUID = 2435955006306851869L;

    private Long tc;

    private List<SearchHit> datas;

    @Data
    public static class SearchHit {
        private Map object;
        private Map<String, Object> sortValues;
    }

}
