package com.lp.es.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * ElasticSearch Property
 *
 * @author Steven
 * @date 2018/2/1 上午10:43
 * @since V1.0
 */
@Data
public class EsProperty implements Serializable {

    private static final long serialVersionUID = -8499892254953333826L;

    public static final String TYPE_GEO = "geo_point";
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_GEO_POINT_ATTRIBUTE = "geo_point_attr";

    private String name;

    private String type;

    private Boolean index = true;

    public boolean isGeoType() {
        return Objects.equals(type, TYPE_GEO);
    }

    public boolean isTextType() {
        return Objects.equals(type, TYPE_TEXT);
    }

    public boolean isGeoTypeAttr() {
        return Objects.equals(type, TYPE_GEO_POINT_ATTRIBUTE);
    }

}
