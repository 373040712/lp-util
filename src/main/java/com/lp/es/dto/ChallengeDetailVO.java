package com.lp.es.dto;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * @author peng.liu
 * @date 2019/4/28 15:12
 */
@Data
public class ChallengeDetailVO {

    /**
     * 所有的冠军策略
     */
    private List<ChallengeStrategyDTO> championList = Lists.newArrayList();
    /**
     * 所有冠军策略和挑战者策略
     */
    private List<ChallengeStrategyDTO> strategyList = Lists.newArrayList();
    /**
     * 冠军和挑战者结果对比
     */
    private List<ChallengeCompareDTO> compareList = Lists.newArrayList();
    /**
     * 策略组合
     */
    private List<ChallengeCombinationDTO> combinationList = Lists.newArrayList();

}
