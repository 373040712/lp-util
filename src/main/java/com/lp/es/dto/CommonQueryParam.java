package com.lp.es.dto;

import com.google.common.collect.Lists;
import lombok.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Data
public class CommonQueryParam {

    private static final Integer MAX_PS = 1000;

    private UserType userType;
    private String[] includeFields;
    private List<Query> queries;
    private List<Sort> sorts;

    // 针对组合查询，可嵌套
    private List<GroupQuery> groupQueries;

    private Integer pn = 1;
    private Integer ps = 10;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Query<T> {
        private String name;
        private List<T> in;
        private List<T> nin;
        private T gte;
        private T gt;
        private T lte;
        private T lt;
        private String wildcard;
        private GEOField geo;
        private GEOBoundingBox geoBoundingBox;
        @Builder.Default
        private Condition condition = Condition.MUST;
        private Boolean exists;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GroupQuery {
        private List<Query> queries;
        @Builder.Default
        private Condition condition = Condition.MUST;
        private List<GroupQuery> children;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GEOField {
        private Double lat;
        private Double lng;
        /** 单位：米 */
        private Double distance;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class GEOBoundingBox {
        /** 左上经纬度 */
        private Double topLeftLng;
        private Double topLeftLat;

        /** 右下经纬度 */
        private Double bottomRightLng;
        private Double bottomRightLat;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Sort {
        private String name;
        @Builder.Default
        private Boolean asc = true;
        private GEOField geo;
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public enum UserType {
        CONSIGNOR("consignor_alias", "consignor"), DRIVER("driver_alias", "driver"), ENTERPRISE("enterprise_alias",
                "enterprise"), USER("user_alias", "user");
        private String indexAlias;
        private String indexType;

        public static String[] getAllIndicesAlias() {
            return Arrays.stream(UserType.values()).map(UserType::getIndexAlias).toArray(String[]::new);
        }
    }

    @Getter
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    public enum Condition {
        MUST, MUST_NOT, SHOULD
    }


    public Integer getStart() {
        if (pn > 0) {
            return (pn - 1) * ps;
        }
        return 0;
    }

    public void addQuery(Query query) {
        if (Objects.isNull(queries)) {
            queries = Lists.newArrayList(query);
        } else {
            queries.add(query);
        }
    }

    public void addSort(Sort sort) {
        if (Objects.isNull(sorts)) {
            sorts = Lists.newArrayList(sort);
        } else {
            sorts.add(sort);
        }
    }

    public void addGroupQuery(GroupQuery groupQuery) {
        if (Objects.isNull(groupQueries)) {
            groupQueries = Lists.newArrayList(groupQuery);
        } else {
            groupQueries.add(groupQuery);
        }
    }

}
