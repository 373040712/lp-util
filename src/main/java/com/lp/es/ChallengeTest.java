package com.lp.es;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.*;
import com.lp.es.dto.ChallengeCombinationDTO;
import com.lp.es.dto.ChallengeCompareDTO;
import com.lp.es.dto.ChallengeDetailVO;
import com.lp.es.dto.ChallengeStrategyDTO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.nested.InternalNested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.InternalTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.InternalSum;
import org.elasticsearch.search.aggregations.metrics.sum.SumAggregationBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * @author peng.liu
 * @date 2019/4/28 19:55
 */

public class ChallengeTest {

    private static final String SERVER_DELIMITER = ",";
    private static final String IP_PORT_DELIMITER = ":";
    private String esServers = "10.6.1.195:9300,10.6.1.196:9300,10.6.1.197:9300";
    private String clusterName = "avatar";
    private TransportClient client;

    private static final String ALL = "all";
    private static final String CHAMPION = "champion";
    private static final String CHALLENGER = "challenger";
    private static final String HIT = "hit";
    private static final String DIFF = "diff";

    private static final String PATH = "path";
    private static final String RESULT = "result";

    @Before
    public void before() throws Exception {
        if (Strings.isBlank(esServers) || Strings.isBlank(clusterName)) {
            throw new RuntimeException("ES配置错误");
        }
        client = new PreBuiltTransportClient(
                Settings.builder().put("cluster.name", clusterName).put("client.transport.sniff", true).build());
        String[] servers = esServers.split(SERVER_DELIMITER);
        for (String server : servers) {
            String[] ipAndPort = server.split(IP_PORT_DELIMITER);
            client.addTransportAddress(
                    new InetSocketTransportAddress(InetAddress.getByName(ipAndPort[0]), Integer.valueOf(ipAndPort[1])));
        }
    }

    @Test
    public void queryTest() {
        SearchRequestBuilder searchBuilder =
                client.prepareSearch("snapshot_read").setSearchType(SearchType.DEFAULT).setSize(0);
        NestedAggregationBuilder all = AggregationBuilders.nested(ALL, "ccRelation");
        TermsAggregationBuilder champion = AggregationBuilders.terms(CHAMPION).field("ccRelation.championId");
        TermsAggregationBuilder challenger = AggregationBuilders.terms(CHALLENGER).field("ccRelation.challengerId");
        SumAggregationBuilder hitSum = AggregationBuilders.sum(HIT).field("ccRelation.hit");
        SumAggregationBuilder diffSum = AggregationBuilders.sum(DIFF).field("ccRelation.diff");
        // 公共聚合
        searchBuilder.addAggregation(all);
        all.subAggregation(champion);
        champion.subAggregation(challenger);
        challenger.subAggregation(hitSum);
        SearchResponse hitResponse = searchBuilder.get();
        Table<String, String, Double> hitTable = assembleStrategy(hitResponse, HIT);
        System.out.println(hitTable.toString());

        challenger.subAggregation(diffSum);
        SearchResponse diffResponse = searchBuilder.get();
        Table<String, String, Double> diffTable = assembleStrategy(diffResponse, DIFF);
        System.out.println(diffTable.toString());

        ChallengeDetailVO detailVO = new ChallengeDetailVO();
        // 所有相关策略名称
        Map<String, String> nameMap = Maps.newHashMap();
        // 组装结果
        assembleStrategyCompare(detailVO, hitTable, diffTable, null, nameMap);
        System.out.println(JSON.toJSONString(detailVO));

        SearchRequestBuilder searchBuilderOther =
                client.prepareSearch("snapshot_read").setSearchType(SearchType.DEFAULT).setSize(0);
        Set<String> combinationIds = Sets.newHashSet("a2");
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        combinationIds.forEach(id -> {
            TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("ccPath", id);
            queryBuilder.must(termQueryBuilder);
        });
        searchBuilderOther.setQuery(queryBuilder);
        TermsAggregationBuilder pathTermsBuilder = AggregationBuilders.terms(PATH).field("ccPathStr");
        TermsAggregationBuilder termsBuilder = AggregationBuilders.terms(RESULT).field("result");
        searchBuilderOther.addAggregation(pathTermsBuilder);
        pathTermsBuilder.subAggregation(termsBuilder);
        SearchResponse combinationResp = searchBuilderOther.get();

        long total = combinationResp.getTotalShards();
        List<Terms.Bucket> pathBuckets = getBucket(combinationResp.getAggregations(), PATH);
        List<ChallengeCombinationDTO> combinationList = Lists.newArrayListWithCapacity(pathBuckets.size());
        for (Terms.Bucket pathBucket : pathBuckets) {
            ChallengeCombinationDTO combination = new ChallengeCombinationDTO();
            combination.setStrategyList(getChallengeStrategy(
                    Arrays.stream(pathBucket.getKeyAsString().split("-")).collect(Collectors.toSet()), null, nameMap,
                    null));
            Map<String, Long> statistics = Maps.newHashMap();
            statistics.put("total", total);
            statistics.put("execute", pathBucket.getDocCount());
            List<Terms.Bucket> buckets = getBucket(pathBucket.getAggregations(), RESULT);
            for (Terms.Bucket bucket : buckets) {
                statistics.put(bucket.getKeyAsString(), bucket.getDocCount());
            }
            combination.setStatistics(statistics);
            combinationList.add(combination);
        }
        detailVO.setCombinationList(combinationList);
        System.out.println(JSON.toJSONString(detailVO));
    }

    private void assembleStrategyCompare(ChallengeDetailVO detailVO, Table<String, String, Double> hitTable,
            Table<String, String, Double> diffTable, String championId, Map<String, String> nameMap) {
        // 组装下拉列表选项
        assembleStrategyList(detailVO, hitTable, championId, nameMap);
        // 组装对比结果
        assembleCompareList(detailVO, hitTable, diffTable, championId);
    }

    private void assembleStrategyList(ChallengeDetailVO detailVO, Table<String, String, Double> hitTable,
            String championId, Map<String, String> nameMap) {
        detailVO.setChampionList(getChallengeStrategy(hitTable.rowKeySet(), championId, nameMap, null));
        List<ChallengeStrategyDTO> strategyList = Lists.newArrayList();
        for (ChallengeStrategyDTO dto : detailVO.getChampionList()) {
            strategyList.addAll(
                    getChallengeStrategy(hitTable.row(dto.getId()).keySet(), dto.getId(), nameMap, dto.getId()));
        }
        detailVO.setStrategyList(strategyList);
    }

    private void assembleCompareList(ChallengeDetailVO detailVO, Table<String, String, Double> hitTable,
            Table<String, String, Double> diffTable, String championId) {
        List<ChallengeStrategyDTO> championList = detailVO.getChampionList();
        List<ChallengeStrategyDTO> strategyList = detailVO.getStrategyList();
        if (StringUtils.isNotBlank(championId)) {
            championList = championList.stream().filter(e -> Objects.equals(e.getId(), championId))
                    .collect(Collectors.toList());
            strategyList = strategyList.stream().filter(e -> Objects.equals(e.getRelationId(), championId))
                    .collect(Collectors.toList());
        }
        Map<String, List<ChallengeStrategyDTO>> strategyMap =
                strategyList.stream().collect(groupingBy(e -> e.getRelationId()));

        List<ChallengeCompareDTO> compareList = Lists.newArrayListWithCapacity(championList.size());
        for (ChallengeStrategyDTO champion : championList) {
            ChallengeCompareDTO compare = new ChallengeCompareDTO();
            for (ChallengeStrategyDTO strategy : strategyMap.get(champion.getId())) {
                Double hit = hitTable.get(champion.getId(), strategy.getId());
                Double diff = diffTable.get(champion.getId(), strategy.getId());
                if (Objects.equals(strategy.getId(), strategy.getRelationId())) {
                    compare.setChampion(strategy.copy().setHit(hit).setDiff(diff));
                } else {
                    compare.getChallenger().add(strategy.copy().setHit(hit).setDiff(diff));
                }
            }
            compareList.add(compare);
        }
        detailVO.setCompareList(compareList);
    }

    private List<ChallengeStrategyDTO> getChallengeStrategy(Set<String> treeIds, String firstId,
            Map<String, String> nameMap, String relationId) {
        List<String> list = treeIds.stream().sorted().collect(Collectors.toList());
        if (StringUtils.isNotBlank(firstId) && treeIds.contains(firstId)) {
            list.remove(firstId);
            list.add(0, firstId);
        }
        return list.stream()
                .map(e -> new ChallengeStrategyDTO().setId(e).setRelationId(relationId).setName(nameMap.get(e)))
                .collect(Collectors.toList());
    }

    private Table<String, String, Double> assembleStrategy(SearchResponse response, String biz) {
        Table<String, String, Double> result = HashBasedTable.create();
        InternalNested nested = (InternalNested) getAggregation(response.getAggregations(), ALL);
        if (Objects.isNull(nested)) {
            return result;
        }
        List<Terms.Bucket> championBuckets = getBucket(nested.getAggregations(), CHAMPION);
        if (CollectionUtils.isEmpty(championBuckets)) {
            return result;
        }
        for (Terms.Bucket championBucket : championBuckets) {
            InternalTerms challenger = (InternalTerms) getAggregation(championBucket.getAggregations(), CHALLENGER);
            if (Objects.isNull(challenger)) {
                continue;
            }
            List<Terms.Bucket> challengerBuckets = getBucket(challenger);
            if (CollectionUtils.isEmpty(challengerBuckets)) {
                continue;
            }
            for (Terms.Bucket challengerBucket : challengerBuckets) {
                double rate = 0.0;
                Long docCount = challengerBucket.getDocCount();
                InternalSum sum = (InternalSum) getAggregation(challengerBucket.getAggregations(), biz);

                if (Objects.nonNull(docCount) && Objects.nonNull(sum) && docCount > 0 && sum.getValue() > 0) {
                    BigDecimal decimal = new BigDecimal(sum.getValue());
                    rate = decimal.divide(new BigDecimal(docCount), 4, BigDecimal.ROUND_HALF_DOWN).doubleValue();
                }
                result.put(championBucket.getKeyAsString(), challengerBucket.getKeyAsString(), rate);
            }
        }
        return result;
    }

    private Aggregation getAggregation(Aggregations aggregations, String name) {
        if (Objects.isNull(aggregations) || StringUtils.isBlank(name)) {
            return null;
        }
        return aggregations.get(name);
    }

    private List<Terms.Bucket> getBucket(Aggregations aggregations, String name) {
        return getBucket(getAggregation(aggregations, name));
    }

    private List<Terms.Bucket> getBucket(Aggregation aggregation) {
        if (Objects.isNull(aggregation)) {
            return null;
        }
        if (!(aggregation instanceof InternalTerms)) {
            return null;
        }
        List<Terms.Bucket> buckets = ((InternalTerms) aggregation).getBuckets();
        if (CollectionUtils.isEmpty(buckets)) {
            return null;
        }
        return buckets;
    }
}
