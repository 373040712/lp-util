package com.lp.es;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Delete;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * @author peng.liu
 * @date 2019/1/3 15:45
 */
@Slf4j
public class JavaJestESTest {

    private static final String SERVER_DELIMITER = ",";
    private static final String IP_PORT_DELIMITER = ":";

    private String esServers = "http://10.6.1.196:9200";

    private JestClient client;

    @Before
    public void before() throws Exception {
        if (Strings.isBlank(esServers)) {
            throw new RuntimeException("ES配置错误");
        }
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig.Builder(esServers).multiThreaded(true).build());
        client = factory.getObject();
    }

    @Test
    public void queryTest() throws IOException {
        SearchResult searchResult = doQuery("driver_alias", "driver",
                "{\"query\":{\"bool\":{\"must\":[{\"term\":{\"status\":{\"value\":true}}}]}},\"_source\":[\"userId\",\"realName\",\"mobile\",\"registerTime\"],\"sort\":[{\"registerTime\":{\"order\":\"asc\"}}],\"from\":0,\"size\":20}");
        if (!searchResult.isSucceeded()) {
            log.error("查询失败，查询结果为空");
            return;
        }
        log.info("总量：{}，结果：{}", searchResult.getTotal(), searchResult.getSourceAsString());
    }

    private SearchResult doQuery(String index, String type, String json) throws IOException {
        Search search = new Search.Builder(json).addIndex(index).addType(type).build();
        return client.execute(search);
    }

    private DocumentResult doDelete(String index, String type, Integer id) throws IOException {
        Delete delete = new Delete.Builder(String.valueOf(id)).index(index).type(type).build();
        return client.execute(delete);
    }
}
