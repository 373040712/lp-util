package com.lp.es;

import com.google.common.collect.Lists;
import com.lp.es.common.EsCommonQueryUtil;
import com.lp.es.common.dto.QueryParam;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.util.Strings;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;

/**
 * @author liupeng2@tinman.cn
 * @date 2020/06/02
 **/
public class JavaRestHighESTest {

    private static final int SUCCESS_CODE = 200;
    private static final String SERVER_DELIMITER = ",";
    private static final String IP_PORT_DELIMITER = ":";

    private String esServers = "127.0.0.1:9200";

    private RestHighLevelClient restHighLevelClient;

    @Before
    public void before() throws Exception {
        if (Strings.isBlank(esServers)) {
            throw new RuntimeException("ES配置错误");
        }
        List<HttpHost> list = Lists.newArrayList();
        String[] servers = esServers.split(SERVER_DELIMITER);
        for (String server : servers) {
            String[] ipAndPort = server.split(IP_PORT_DELIMITER);
            list.add(new HttpHost(InetAddress.getByName(ipAndPort[0]), Integer.valueOf(ipAndPort[1]), "http"));
        }
        restHighLevelClient = new RestHighLevelClient(RestClient.builder(list.toArray(new HttpHost[0])).setMaxRetryTimeoutMillis(10000));
    }


    @Test
    public void queryTest() throws IOException {
        EsCommonQueryUtil queryUtil = new EsCommonQueryUtil(restHighLevelClient);
        queryUtil.reloadMappings();
        QueryParam queryParam = new QueryParam();
        //TODO 初始化条件
        queryUtil.commonQuery(queryParam);
    }
}
