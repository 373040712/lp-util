package com.lp.es;

import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lp.es.dto.CommonQueryParam;
import com.lp.es.dto.EsProperty;
import com.lp.es.dto.QueryResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequest;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/**
 * @author peng.liu
 * @date 2019/1/2 17:27
 */
@Slf4j
public class JavaApiESTest {

    private static final String PROPERTIES_KEY = "properties";
    private static final String PROPERTY_TYPE_KEY = "type";
    private static final String PROPERTY_INDEX_KEY = "index";
    private static final String PROPERTY_NAME_DELIMITER = ".";
    private static final String PROPERTY_LON_KEY = "lon";
    private static final String PROPERTY_LAT_KEY = "lat";
    private static final String SERVER_DELIMITER = ",";
    private static final String IP_PORT_DELIMITER = ":";

    public static final Map<String, Map<String, EsProperty>> ALL_TYPE_PROPERTIES_MAP = Maps.newConcurrentMap();

    private String esServers = "10.6.1.195:9300,10.6.1.196:9300,10.6.1.197:9300";

    private String clusterName = "avatar";

    private TransportClient client;

    @Before
    public void before() throws Exception {
        if (Strings.isBlank(esServers) || Strings.isBlank(clusterName)) {
            throw new RuntimeException("ES配置错误");
        }
        client = new PreBuiltTransportClient(
                Settings.builder().put("cluster.name", clusterName).put("client.transport.sniff", true).build());
        String[] servers = esServers.split(SERVER_DELIMITER);
        for (String server : servers) {
            String[] ipAndPort = server.split(IP_PORT_DELIMITER);
            client.addTransportAddress(
                    new InetSocketTransportAddress(InetAddress.getByName(ipAndPort[0]), Integer.valueOf(ipAndPort[1])));
        }
        getMappings();
    }

    @Test
    public void queryTest() {
        CommonQueryParam param = new CommonQueryParam();
        param.setUserType(CommonQueryParam.UserType.CONSIGNOR);
        param.addQuery(CommonQueryParam.Query.builder().name("userId").in(Lists.newArrayList(1113363L)).build());
        param.setIncludeFields(new String[] {"userId", "realName"});
        System.out.println(commonQuery(param));
    }

    private QueryResult commonQuery(CommonQueryParam param) {
        SearchRequestBuilder searchRequestBuilder = client.prepareSearch(param.getUserType().getIndexAlias())
                .setSearchType(SearchType.DEFAULT).setFrom(param.getStart()).setSize(param.getPs())
                .setFetchSource(param.getIncludeFields(), null);
        assembleQueries(param, searchRequestBuilder);
        assembleSorts(param, searchRequestBuilder);
        log.info(searchRequestBuilder.toString());
        SearchResponse searchResponse = searchRequestBuilder.get();
        SearchHits searchHits = searchResponse.getHits();
        QueryResult result = new QueryResult();
        result.setTc(searchHits.getTotalHits());
        List<QueryResult.SearchHit> datas = Lists.newArrayList();

        searchHits.forEach(s -> {
            QueryResult.SearchHit data = new QueryResult.SearchHit();
            data.setObject(s.getSource());
            if (Objects.nonNull(s.getSortValues()) && s.getSortValues().length > 0) {
                Map<String, Object> sortValues = Maps.newLinkedHashMap();
                for (int i = 0; i < param.getSorts().size(); i++) {
                    sortValues.put(param.getSorts().get(i).getName(), s.getSortValues()[i]);
                }
                data.setSortValues(sortValues);
            }
            datas.add(data);
        });
        result.setDatas(datas);
        return result;
    }

    private void assembleQueries(CommonQueryParam param, SearchRequestBuilder searchRequestBuilder) {
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        if (CollectionUtils.isNotEmpty(param.getQueries())) {
            param.getQueries().forEach(query -> buildQuery(boolQuery, query, param.getUserType()));
        }
        if (CollectionUtils.isNotEmpty(param.getGroupQueries())) {
            BoolQueryBuilder outerBoolGroupQuery = QueryBuilders.boolQuery();
            param.getGroupQueries().forEach(groupQuery -> {
                buildGroupQuery(outerBoolGroupQuery, groupQuery, param.getUserType());
            });
            boolQuery.must(outerBoolGroupQuery);
        }
        searchRequestBuilder.setQuery(boolQuery);
    }

    private void assembleSorts(CommonQueryParam param, SearchRequestBuilder searchRequestBuilder) {
        if (CollectionUtils.isNotEmpty(param.getSorts())) {
            param.getSorts().forEach(s -> {
                EsProperty esProperty = getPropertyByTypeAndName(param.getUserType().getIndexType(), s.getName());
                if (esProperty.isGeoType()) {
                    if (Objects.nonNull(s.getGeo())) {
                        searchRequestBuilder.addSort(
                                SortBuilders.geoDistanceSort(s.getName(), s.getGeo().getLat(), s.getGeo().getLng())
                                        .order(s.getAsc() ? SortOrder.ASC : SortOrder.DESC));
                    } else {
                        throw new RuntimeException(String.format("GEO字段【%s】排序需要地理信息参数，请确认已设置", s.getName()));
                    }
                } else {
                    searchRequestBuilder.addSort(
                            SortBuilders.fieldSort(s.getName()).order(s.getAsc() ? SortOrder.ASC : SortOrder.DESC));
                }
            });
        }
    }


    private void buildQuery(BoolQueryBuilder boolQuery, CommonQueryParam.Query query,
            CommonQueryParam.UserType userType) {
        QueryBuilder queryBuilder = null;
        // exists query build
        if (Objects.nonNull(query.getExists())) {
            if (query.getExists()) {
                queryBuilder = QueryBuilders.existsQuery(query.getName());
            } else {
                queryBuilder = QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(query.getName()));
            }
        } else {
            EsProperty esProperty = getPropertyByTypeAndName(userType.getIndexType(), query.getName());
            if (esProperty.isTextType()) {
                queryBuilder = QueryBuilders.constantScoreQuery(
                        QueryBuilders.matchPhraseQuery(query.getName(), IterableUtils.get(query.getIn(), 0)));
            } else if (esProperty.isGeoType() && Objects.nonNull(query.getGeo())) {
                queryBuilder = QueryBuilders.geoDistanceQuery(query.getName())
                        .point(query.getGeo().getLat(), query.getGeo().getLng())
                        .distance(query.getGeo().getDistance(), DistanceUnit.METERS);
            } else if (Objects.nonNull(query.getGte()) || Objects.nonNull(query.getLte())
                    || Objects.nonNull(query.getGt()) || Objects.nonNull(query.getLt())) {
                RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery(query.getName());
                if (Objects.nonNull(query.getGt())) {
                    rangeQueryBuilder.gt(query.getGt());
                } else if (Objects.nonNull(query.getGte())) {
                    rangeQueryBuilder.gte(query.getGte());
                }
                if (Objects.nonNull(query.getLt())) {
                    rangeQueryBuilder.lt(query.getLt());
                } else if (Objects.nonNull(query.getLte())) {
                    rangeQueryBuilder.lte(query.getLte());
                }
                queryBuilder = QueryBuilders.constantScoreQuery(rangeQueryBuilder);
            } else if (StringUtils.isNotBlank(query.getWildcard())) {
                queryBuilder = QueryBuilders
                        .constantScoreQuery(QueryBuilders.wildcardQuery(query.getName(), query.getWildcard()));
            } else if (CollectionUtils.isNotEmpty(query.getIn())) {
                queryBuilder = QueryBuilders
                        .constantScoreQuery(QueryBuilders.termsQuery(query.getName(), query.getIn().toArray()));
            } else if (CollectionUtils.isNotEmpty(query.getNin())) {
                queryBuilder = QueryBuilders.boolQuery()
                        .mustNot(QueryBuilders.termsQuery(query.getName(), query.getNin().toArray()));
            } else if (esProperty.isGeoType() && Objects.nonNull(query.getGeoBoundingBox())) {
                CommonQueryParam.GEOBoundingBox boundingBox = query.getGeoBoundingBox();
                queryBuilder = QueryBuilders.geoBoundingBoxQuery(query.getName()).setCorners(
                        new GeoPoint(boundingBox.getTopLeftLat(), boundingBox.getTopLeftLng()),
                        new GeoPoint(boundingBox.getBottomRightLat(), boundingBox.getBottomRightLng()));
            }
        }

        appendQueryByCondition(boolQuery, queryBuilder, query.getCondition());
    }

    private void buildGroupQuery(BoolQueryBuilder boolQuery, CommonQueryParam.GroupQuery query,
            CommonQueryParam.UserType userType) {
        BoolQueryBuilder groupBoolQuery = QueryBuilders.boolQuery();
        if (CollectionUtils.isNotEmpty(query.getQueries())) {
            query.getQueries().forEach(q -> buildQuery(groupBoolQuery, q, userType));
        }
        if (CollectionUtils.isNotEmpty(query.getChildren())) {
            BoolQueryBuilder outerBoolGroupQuery = QueryBuilders.boolQuery();
            query.getChildren().forEach(q -> buildGroupQuery(outerBoolGroupQuery, q, userType));
            groupBoolQuery.must(outerBoolGroupQuery);
        }
        appendQueryByCondition(boolQuery, groupBoolQuery, query.getCondition());
    }

    private void appendQueryByCondition(BoolQueryBuilder boolQuery, QueryBuilder query,
            CommonQueryParam.Condition condition) {
        if (Objects.equals(CommonQueryParam.Condition.MUST, condition)) {
            boolQuery.must(query);
        } else if (Objects.equals(CommonQueryParam.Condition.MUST_NOT, condition)) {
            boolQuery.mustNot(query);
        } else if (Objects.equals(CommonQueryParam.Condition.SHOULD, condition)) {
            boolQuery.should(query);
        }
    }

    /***************************************** ES属性缓存 *********************************************/
    private EsProperty getPropertyByTypeAndName(String type, String name) {
        Map<String, EsProperty> esProperties = ALL_TYPE_PROPERTIES_MAP.get(type);
        if (Objects.nonNull(esProperties)) {
            return esProperties.get(name);
        }
        return null;
    }

    private void getMappings() throws IOException, ExecutionException, InterruptedException {
        GetMappingsRequest getMappingsRequest =
                new GetMappingsRequest().indices(CommonQueryParam.UserType.getAllIndicesAlias());
        GetMappingsResponse getMappingsResponse = client.admin().indices().getMappings(getMappingsRequest).get();

        ImmutableOpenMap<String, ImmutableOpenMap<String, MappingMetaData>> mappingsByIndex =
                getMappingsResponse.getMappings();

        for (ObjectObjectCursor<String, ImmutableOpenMap<String, MappingMetaData>> indexEntry : mappingsByIndex) {
            if (indexEntry.value.isEmpty()) {
                continue;
            }
            for (ObjectObjectCursor<String, MappingMetaData> typeEntry : indexEntry.value) {
                Map<String, EsProperty> esPropertiesMap = Maps.newLinkedHashMap();
                Map<String, Map> properties = (Map<String, Map>) typeEntry.value.sourceAsMap().get(PROPERTIES_KEY);
                extractEsProperties(properties, null, esPropertiesMap);
                ALL_TYPE_PROPERTIES_MAP.put(typeEntry.key, esPropertiesMap);
            }
        }
    }

    private void extractEsProperties(Map<String, Map> propertiesMap, String parentKey,
            Map<String, EsProperty> esPropertiesMap) {
        if (Objects.nonNull(propertiesMap)) {
            for (Map.Entry<String, Map> entry : propertiesMap.entrySet()) {
                Map entryValue = entry.getValue();
                if (Objects.nonNull(entryValue.get(PROPERTIES_KEY))) {
                    extractEsProperties((Map<String, Map>) entryValue.get(PROPERTIES_KEY),
                            (Objects.isNull(parentKey) ? StringUtils.EMPTY : parentKey + PROPERTY_NAME_DELIMITER)
                                    + entry.getKey(),
                            esPropertiesMap);
                } else {
                    EsProperty esProperty = new EsProperty();
                    esProperty.setName(
                            (Objects.isNull(parentKey) ? StringUtils.EMPTY : parentKey + PROPERTY_NAME_DELIMITER)
                                    + entry.getKey());
                    esProperty.setType((String) entryValue.get(PROPERTY_TYPE_KEY));
                    esProperty.setIndex(Objects.nonNull(entryValue.get(PROPERTY_INDEX_KEY))
                            ? (Boolean) entryValue.get(PROPERTY_INDEX_KEY)
                            : true);
                    esPropertiesMap.put(esProperty.getName(), esProperty);

                    // 因为ES针对geo_point的mapping不会返回其lon和lat属性，而用户在设置includeFields时可能会这样设置，所以这里做个特殊处理，将lon和lat也加入到properties中
                    if (esProperty.isGeoType()) {
                        EsProperty lonProperty = new EsProperty();
                        lonProperty.setName(esProperty.getName() + PROPERTY_NAME_DELIMITER + PROPERTY_LON_KEY);
                        lonProperty.setType(EsProperty.TYPE_GEO_POINT_ATTRIBUTE);
                        lonProperty.setIndex(true);
                        esPropertiesMap.put(lonProperty.getName(), lonProperty);

                        EsProperty latProperty = new EsProperty();
                        latProperty.setName(esProperty.getName() + PROPERTY_NAME_DELIMITER + PROPERTY_LAT_KEY);
                        latProperty.setType(EsProperty.TYPE_GEO_POINT_ATTRIBUTE);
                        latProperty.setIndex(true);
                        esPropertiesMap.put(latProperty.getName(), latProperty);
                    }
                }
            }
        }
    }
}
