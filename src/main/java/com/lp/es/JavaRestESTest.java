package com.lp.es;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.util.Strings;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

/**
 * @author peng.liu
 * @date 2019/1/3 11:11
 */
@Slf4j
public class JavaRestESTest {

    private static final int SUCCESS_CODE = 200;
    private static final String SERVER_DELIMITER = ",";
    private static final String IP_PORT_DELIMITER = ":";

    private String esServers = "10.6.1.195:9200,10.6.1.196:9200,10.6.1.197:9200";

    private RestClient restClient;

    @Before
    public void before() throws Exception {
        if (Strings.isBlank(esServers)) {
            throw new RuntimeException("ES配置错误");
        }
        List<HttpHost> list = Lists.newArrayList();
        String[] servers = esServers.split(SERVER_DELIMITER);
        for (String server : servers) {
            String[] ipAndPort = server.split(IP_PORT_DELIMITER);
            list.add(new HttpHost(InetAddress.getByName(ipAndPort[0]), Integer.valueOf(ipAndPort[1])));
        }
        restClient = RestClient.builder(list.toArray(new HttpHost[0])).setMaxRetryTimeoutMillis(10000).build();
    }

    @Test
    public void queryTest() throws IOException {
        Response response = doQuery("GET", "/driver_alias/_search",
                "{\"query\":{\"bool\":{\"must\":[{\"term\":{\"status\":{\"value\":true}}}]}},\"_source\":[\"userId\",\"realName\",\"mobile\",\"registerTime\"],\"sort\":[{\"registerTime\":{\"order\":\"asc\"}}],\"from\":0,\"size\":20}");
        int code = response.getStatusLine().getStatusCode();
        if (SUCCESS_CODE != code) {
            log.error("查询失败，code={}，result={}", code, EntityUtils.toString(response.getEntity()));
            return;
        }
        String rs = EntityUtils.toString(response.getEntity());
        if (Strings.isBlank(rs)) {
            log.error("查询失败，查询结果为空");
            return;
        }
        List<Map<String, Object>> rsList = new ArrayList<>();
        int rsCount = 0;
        JSONObject jsonObj = JSON.parseObject(rs);
        JSONObject hits = jsonObj.getJSONObject("hits");
        JSONArray arr = hits.getJSONArray("hits");
        if (Objects.nonNull(arr)) {
            rsCount = hits.getIntValue("total");
            if (rsCount > 0) {
                for (int i = 0; i < arr.size(); i++) {
                    String _id = arr.getJSONObject(i).getString("_id");
                    JSONObject obj = arr.getJSONObject(i).getJSONObject("_source");
                    obj.put("_id", _id);
                    Map<String, Object> map = (Map<String, Object>) (obj);
                    rsList.add(map);
                }
            }
        }
        log.info("总量：{}，结果：{}", rsCount, rsList.toString());
    }

    private Response doQuery(String method, String endpoint, String json) throws IOException {
        return restClient.performRequest(method, endpoint, Collections.singletonMap("pretty", "true"),
                new NStringEntity(json, ContentType.APPLICATION_JSON));
    }

    @After
    public void after() throws Exception {
        if (Objects.nonNull(restClient)) {
            restClient.close();
        }
    }
}
