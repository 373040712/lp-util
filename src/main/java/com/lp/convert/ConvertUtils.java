package com.lp.convert;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.collections4.*;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.TypeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 类型转换工具类
 * 
 * @author lixin
 */
public class ConvertUtils {
    private final static Logger LOGGER = LoggerFactory.getLogger(ConvertUtils.class);
    private static ConvertUtilsBean convertUtilsBean = new ConvertUtilsBean();

    public static <T> T convert(Object value, Class<T> targetType) {
        Object converted = convertUtilsBean.convert(value, targetType);
        if (TypeUtils.isInstance(converted, targetType)) {
            return (T) converted;
        } else {
            try {
                T t = targetType.newInstance();
                org.springframework.beans.BeanUtils.copyProperties(value, t);
                return t;
            } catch (Throwable e) {
                LOGGER.error("convert error!", e);
                throw new RuntimeException("convert error!", e);
            }
        }
    }

    public static <T, E> List<T> batchConvert(Collection<E> voList, final Class<T> cls) {
        return CollectionUtils.collect(voList, new Transformer<E, T>() {
            @Override
            public T transform(E e) {
                try {
                    T t = cls.newInstance();
                    if (e instanceof Map) {
                        BeanUtils.populate(t, (Map) e);
                    } else {
                        org.springframework.beans.BeanUtils.copyProperties(e, t);
                    }
                    return t;
                } catch (Throwable t) {
                    throw new RuntimeException("convert error!");
                }
            }
        }, new ArrayList<T>(voList.size()));
    }

    public static <T, E> List<T> extract(Collection<E> sourceList, final String fieldName, final Class<T> cls) {
        return CollectionUtils.collect(sourceList, new Transformer<E, T>() {
            @Override
            public T transform(E e) {
                try {
                    if (e instanceof Map) {
                        return convert(((Map) e).get(fieldName), cls);
                    } else {
                        Field field = FieldUtils.getField(e.getClass(), fieldName, true);
                        return convert(field.get(e), cls);
                    }
                } catch (Throwable t) {
                    throw new RuntimeException("convert error!");
                }
            }
        }, new ArrayList<T>(sourceList.size()));
    }

    /**
     * Batch fill some fields from sourceCollection to targetCollection
     * according to some matched fields</br>
     * 
     * <p>
     * i.e:
     * 
     * <pre>
     * List<User> users = //;
     * List<Employee> employees = //;
     * 
     * Before:
     * for(User user:users){
     *      Employee matchedEmployee = null;
     *      for(Employee employee:employees){
     *          if(employee.getUserId() == user.getId()){//more fields will be union with and logic
     *              matchedEmployee = employee;
     *              break;
     *          }
     *      }
     *      if(null!=matchedEmployee){
     *          user.setCardNumber(matchedEmployee.getIdentityNumber());
     *          //more fill fields...
     *      }
     * }
     * 
     * After:
     * ConvertUtils.batchFill(employees,users,MapUtils.putAll(new HashMap<String,String>(),
     *  new String[]{"userId","id"}),MapUtils.putAll(new HashMap<String,String>(),new String[]{"identityNumber","cardNumber"}));
     * 
     * </pre>
     * </p>
     * 
     * @param source
     *            sourceCollection
     * @param target
     *            targetCollection
     * @param s2tMatchFieldNames
     *            matched field map
     * @param s2tFillFieldNames
     *            filled field map
     */
    public static <S, T> void batchFill(Collection<S> source, Collection<T> target,
            final Map<String, String> s2tMatchFieldNames, Map<String, String> s2tFillFieldNames) {
        if (CollectionUtils.isEmpty(source) || CollectionUtils.isEmpty(target) || MapUtils.isEmpty(s2tMatchFieldNames)
                || MapUtils.isEmpty(s2tFillFieldNames)) {
            return;
        } else {
            for (final T t : target) {
                S s = IterableUtils.find(source, new Predicate<S>() {
                    @Override
                    public boolean evaluate(S s) {
                        for (Map.Entry<String, String> entry : s2tMatchFieldNames.entrySet()) {
                            Object source = get(s, entry.getKey());
                            Object target = get(t, entry.getValue());
                            if (!convertEquals(source, target)) {
                                return false;
                            }
                        }
                        return true;
                    }
                });
                if (null == s) {
                    continue;
                }
                for (Map.Entry<String, String> entry : s2tFillFieldNames.entrySet()) {
                    set(t, entry.getValue(), get(s, entry.getKey()));
                }
            }
        }
    }

    public static boolean convertEquals(Object o1, Object o2) {
        if (o1.hashCode() != o2.hashCode()) {
            return false;
        }

        try {
            return Objects.equals(o2, convert(o1, o2.getClass()));
        } catch (Throwable t) {
        }

        try {
            return Objects.equals(o1, convert(o2, o1.getClass()));
        } catch (Throwable t) {
        }
        return false;
    }

    public static Object get(Object object, String name) {
        if (object instanceof Map) {
            return MapUtils.getObject((Map) object, name);
        } else {
            try {
                return FieldUtils.readField(object, name, true);
            } catch (Throwable e) {
                LOGGER.warn("get {} from {} error!", new Object[] { name, object, e });
                return null;
            }
        }
    }

    public static void set(Object object, String name, Object value) {
        if (object instanceof Map) {
            ((Map) object).put(name, value);
        } else {
            try {
                FieldUtils.writeField(object, name, value, true);
            } catch (Throwable e) {
                LOGGER.warn("set {} in {} to {} error!", new Object[] { name, object, value, e });
            }
        }
    }
}
