package com.lp.range;

import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import org.joda.time.DateTime;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description
 * @Author liupeng@sinoiov.com
 * @Date 2019/9/21 0:15
 */
public class TestRange {
    public static void main(String[] args) {
        LocalDateTime start = LocalDateTime.of(2019, 9, 20, 0, 0, 0, 0);
        LocalDateTime a = LocalDateTime.of(2019, 9, 20, 5, 20, 0, 0);
        Range a1 = Range.open(Date.from(start.toInstant(ZoneOffset.of("+8"))), Date.from(a.toInstant(ZoneOffset.of(
                "+8"))));
        LocalDateTime b = LocalDateTime.of(2019, 9, 20, 5, 19, 0, 0);
        LocalDateTime c = LocalDateTime.of(2019, 9, 20, 5, 22, 0, 0);
        Range a2 = Range.open(Date.from(b.toInstant(ZoneOffset.of("+8"))), Date.from(c.toInstant(ZoneOffset.of(
                "+8"))));
        LocalDateTime d = LocalDateTime.of(2019, 9, 20, 8, 0, 0, 0);
        LocalDateTime e = LocalDateTime.of(2019, 9, 20, 10, 0, 0, 0);
        Range a3 = Range.open(Date.from(d.toInstant(ZoneOffset.of("+8"))), Date.from(e.toInstant(ZoneOffset.of(
                "+8"))));
        LocalDateTime f = LocalDateTime.of(2019, 9, 20, 9, 0, 0, 0);
        LocalDateTime g = LocalDateTime.of(2019, 9, 20, 14, 0, 0, 0);
        Range a4 = Range.open(Date.from(f.toInstant(ZoneOffset.of("+8"))), Date.from(g.toInstant(ZoneOffset.of(
                "+8"))));
        LocalDateTime h = LocalDateTime.of(2019, 9, 20, 18, 0, 0, 0);
        LocalDateTime end = LocalDateTime.of(2019, 9, 20, 23, 59, 59, 999);
        Range a5 = Range.open(Date.from(h.toInstant(ZoneOffset.of("+8"))), Date.from(end.toInstant(ZoneOffset.of(
                "+8"))));
        List<Range> list =
                Lists.newArrayList(a3, a5, a2, a4, a1).stream().sorted(Comparator.comparing(i -> (Date) i.lowerEndpoint())).collect(Collectors.toList());
        List<Range> ranges = new ArrayList<>(list.size());

        Range temp;
        for (int i = 0; i < list.size(); ) {
            temp = list.get(i);
            while (++i < list.size()) {
                Range indexTemp = list.get(i);
                if (!temp.isConnected(indexTemp)) {
                    break;
                }
                temp = temp.span(indexTemp);
            }
            ranges.add(temp);
        }
        System.out.println(ranges);
    }

}
