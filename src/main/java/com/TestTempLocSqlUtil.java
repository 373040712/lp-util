package com;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

/**
 * @author liupeng@newhope.cn
 * @date 2023/09/07
 **/
public class TestTempLocSqlUtil {

    public static void main(String[] args) {
        System.out.println( LocalDate.now().until(LocalDate.now().plusDays(1), ChronoUnit.DAYS));
        Optional.of("1").orElse("2");
    }
}
