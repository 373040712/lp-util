package com;

import cn.hutool.core.collection.CollUtil;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.lp.excel.TExcelUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2023/09/07
 **/
public class TestTempSqlUtil {

    private static final String SQL = "UPDATE stock set location_code = ''{0}'' where id = {1} and location_code = ''{2}'';";
    private static final String EXT_SQL = "UPDATE actual_stock set location_code = ''{0}'' where id = {1} and location_code = ''{2}'';";

    public static void main(String[] args) throws Exception {
        List<List<String>> result = new ArrayList<>();
        List<List<String>> b1List = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_base.xlsx");
        List<List<String>> b2List = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_base_1.xlsx");

        Map<String, String> bMap = Maps.newHashMap();
        b1List.forEach(it -> {
            if (StringUtils.isNotBlank(it.get(0))) {
                bMap.put(it.get(0), it.get(1));
            }
        });
        b2List.forEach(it -> {
            if (StringUtils.isNotBlank(it.get(0))) {
                bMap.put(it.get(0), it.get(1));
            }
        });

        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_data.xlsx");
        List<List<String>> extList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "0907_data_1.xlsx");

        for (List<String> it : aList) {
            if (CollUtil.isEmpty(it)) {
                continue;
            }
            String value = bMap.get(it.get(1));
            if (StringUtils.isBlank(value)) {
                continue;
            }
            result.add(ImmutableList.of(MessageFormat.format(SQL, value, it.get(0), it.get(1))));
        }
//        for (List<String> it : extList) {
//            if (CollUtil.isEmpty(it)) {
//                continue;
//            }
//            String value = bMap.get(it.get(1));
//            if (StringUtils.isBlank(value)) {
//                continue;
//            }
//            result.add(ImmutableList.of(MessageFormat.format(EXT_SQL, value, it.get(0), it.get(1))));
//        }
        TExcelUtils.getInstance().exportObjects2Excel(result, ROOT_PATH + "0907_sql.xlsx");
    }
}
