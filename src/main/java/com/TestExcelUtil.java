package com;

import com.lp.excel.TExcelUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.lp.httpclient.server.AvatarTest.ROOT_PATH;

/**
 * @author liupeng@newhope.cn
 * @date 2024/05/10
 **/
public class TestExcelUtil {

    public static void main(String[] args) throws Exception {
        List<List<String>> result = new ArrayList<>();
        List<List<String>> aList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "AA.xlsx");
        List<List<String>> bList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "A1.xlsx");
        Map<String, String> bMap = bList.stream().filter(Objects::nonNull).filter(i -> StringUtils.isNotBlank(i.get(0)))
                .collect(Collectors.toMap(it -> it.get(0), it -> it.get(1), (v1, v2) -> v2));
        List<List<String>> cList = TExcelUtils.getInstance().readExcel2List(ROOT_PATH + "A2.xlsx");
        Map<String, String> cMap = cList.stream().filter(Objects::nonNull).filter(i -> StringUtils.isNotBlank(i.get(0)))
                .collect(Collectors.toMap(it -> it.get(0), it -> it.get(1), (v1, v2) -> v2));
        for (List<String> it : aList) {
            String bValue = bMap.get(it.get(1));
            if (StringUtils.isNotBlank(bValue)) {
                String cValue = cMap.get(bValue);
               if(StringUtils.isNotBlank(cValue)){
                   it.add(cValue);
               }else{
                   it.add("");
               }
            }else{
                it.add(it.get(5));
            }
            result.add(it);
        }
        TExcelUtils.getInstance().exportObjects2Excel(result, ROOT_PATH + "AR.xlsx");
    }
}
