package com.rule.lizi;

/**
 * @author liupeng@newhope.cn
 * @date 2024/06/14
 **/
public class Room {

    private String name;

    public Room() {
    }

    public Room(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
